# New Dexterity Api

* Maintainer: Alexandros Liarokapis <liarokapis.v@gmail.com>

## Description

Low-level serial protocols are not in general designed to be development-friendly. 
They are mostly tailored for generality, efficiency and/or robustness. They often contain
things like checksums and byte paddings and they require the user to perform an involved serialization
routine in order to create the low-level messages.

Moreover, SDKs provided for such protocols often do not abstract away the communication stream, instead relying on
OS-specific serial interfaces. This means that if the developer wants to store requests into a file, or send through a socket
or even mock the communication stream for testing, he is often forced to re-write the parser and reimplement his own library.

Even then, writing a convenient library is not easy. Timeouts must be graciously handled and adding new requests should be easy.
Callbacks should be avoided and usage of modern communication primitives like futures and promises should be used instead.

What this library does is provide a `communication_interface` class template parameterized by a communication policy that takes care of all of the above. 
In the communication policy the user specifies the parser, the serializers and the deserializers and the library then provides high-level functionality.
The user can queue requests retrieving futures of optional responses and then send them and wait for the results:

```cpp

//all of these are not blocking
auto maybe_response_future_1 = interface.queue_request(high_level_request_1(...), std::chrono::milliseconds(...));
auto maybe_response_future_2 = interface.queue_request(high_level_request_2(...), std::chrono::milliseconds(...));
auto maybe_response_future_3 = interface.queue_request(high_level_request_3(...), std::chrono::milliseconds(...));

interface.out_connection().send_requests();

//blocking calls, they are resolved when a value is retrieved or a timeout occurs.
auto maybe_response_1 = maybe_response_future_1.get();
auto maybe_response_2 = maybe_response_future_2.get();
auto maybe_response_3 = maybe_response_future_3.get();

if (maybe_response_1)
{
    auto& response_1 = *maybe_response_1;
    std::cout << "got response" << response_1 << std::endl;
}
else
{
    std::cout << "request 1 timed out" << std::endl;
}

...

```

The library currently contains two built-in communication policies that aim to help in the development of applications using the NDX Hands.
The first is a communication policy for the outdated NDX protocol that old hands used.

The second is a communication policy for the Dynamixel Protocol 2.0. It is meant to provide a more type-safe alternative compared to the official Dynamixel SDK:

Dynamixel SDK:

```cpp
    std::array<std::uint8_t, 14> memory_slice;
    std::uint8_t error;
    int result = readTxRx(&port, dynamixel_id, 122, memory_slice.size(), memory_slice.begin(), &error);
    if (result == COMM_SUCCESS)
    {
        if (!error)
        {
            std::uint8_t moving = memory_slice[122-122]
            std::uint8_t moving_status = memory_slice[123-122];
            std::int16_t present_current = memory_slice[126-122] | (memory_slice[127-122] << 8);
            std::int32_t present_velocity = memory_slice[128-122] | (memory_slice[129-122] << 8) | (memory_slice[130-122] << 16) | (memory_slice[131-122] << 24);
            std::int32_t present_position = memory_slice[132-122] | (memory_slice[133-122] << 8) | (memory_slice[134-122] << 16) | (memory_slice[135-122] << 24);

            std::cout << (int)moving << " " << (int)moving_status << " " << present_current << " " << present_velocity << " " << present_position << std::endl;
        }
        else
        {
            printRxPacketError(error);
        }
    }
    else 
    {
        printTxRxResult(result);
    }
```

NdApi:

```cpp
    auto maybe_response = interface.send_request(dynx::get_memory_contents_request<
                                                    dynx::memory_handles::moving,
                                                    dynx::memory_handles::moving_status,
                                                    dynx::memory_handles::persent_current,
                                                    dynx::memory_handles::present_velocity,
                                                    dynx::memory_handles::present_position>{}, std::chrono::milliseconds(100));
    if (maybe_response)
    {
        auto& response = *maybe_response;

        std::cout << response.hardware_error_alert << std::endl;

        new_dexterity::match(response.result,
            [](dynx::instruction_error error)
            {
                std::cout << error << std::endl;
            },
            [](auto const& contents)
            {
                std::cout << (int)contents.moving << " " << (int)contents.moving_status << " " contents.present_current << " " <<
                                  contents.present_velocity << " " << contents.present_position << std::endl;
            });
    }
    else
    {
        std::cout << "timeout" << std::endl;
    }

```

Both provide the same functionality but our library is aware of the control table's layout and provides memory handles for 
refering to control table values instead of relying on dynamically-provided addresses. Since the library is aware of the memory handles at compilation time, it can
construct an appropriate struct with members of the correct names and types corresponding to the control table values. This way the user does not have to do any 
deserialization by hand. This is basically equivalent to the use of Dynamixel SDK's readNBytesRxTx generalized to any number of of control table values but without the user
having to provide and take care of documenting and naming the address - it is automatically derived from the library's knowledge of the memory handles.
This way the developer can focus on the functionality instead of serialization details. Our library also promotes handling timeouts
and provides a convenient utility for pattern matching of variants promoting the handling of communication errors.

## 1. Installation:

### Requirements

The library requires at least Boost 1.58.

### Conan

Out library provides a packaging-ready conanfile.py and can be built and uploaded to a conan remote.
In the near future we wish to also provide our own artifactory server as a remote.

One may invoke
```bash
conan create . NdApi/1.0.0@new_dexterity/local
```
to build and install our package locally.
Then consuming packages can just use the above path in their conanfiles. 
Read conan's documentation for more details.

### Deb package/NSIS installer
A Deb package and nsis installer is provided as a pipeline artifact and can be found in
our latest successful pipeline. The installer also installs Boost if not available.
We suggest that instead of relying on these for dependency management, users should instead use Conan as
it avoids the problems of system or user-wide installations.

### CMake
The library provides modern CMake support. One may use the --install target in 
order to install the package. Consuming packages may use the `find_package(NdApi REQUIRED)`
command and then link to the `NdApi::NdApi` target using `target_link_libraries`. 

```bash
git clone https://gitlab.com/new_dexterity_athens_public/nd_api.git
mkdir nd_api/build
cd nd_api/build
cmake .. -DCMAKE_BUILD_TYPE=Release
sudo cmake --build . --target install
```

The above requires cmake to be able to find Boost in your system.
One may also use conan to retrieve the Boost dependency:

```bash
git clone https://gitlab.com/new_dexterity_athens_public/nd_api.git
mkdir nd_api/build
cd nd_api/build
conan install .. 
cmake -DCMAKE_TOOLCHAIN_FILE=conan_paths.cmake -DCMAKE_BUILD_TYPE=Release ..
sudo cmake --build . --target install
```

## 2. Tweaks & Settings

### Configure the latency timer

By default most usb-to-serial devices have a very high latency timer setting
which is not ideal for realtime applications. In linux the setting can be changed
by running

```bash
echo [n] | sudo tee /sys/bus/usb-serial/devices/ttyUSB[x]/latency_timer
```


`n` depends on the milliseconds that the driver will wait for incomming messages. 
We suggest a number between 1 and 5 milliseconds. This also affects timeout issues.
`x` depends on the number of USB devices currently connected. 
The specific number can be found by inspecting
your `dmesg` output.

### Ensure that you have permissions to use the USB ports.

Self explanatory.

## 3. Tests

The tests can be built and run using:
```bash
cmake --build build --target run_tests
```

## 4. Documentation

The documentation can be built using:
```bash
cmake --build build --target dox
```

Pre-built documentation and examples can be found at our [gitlab pages](https://new_dexterity_athens_public.gitlab.io/nd_api/).


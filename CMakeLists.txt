cmake_minimum_required(VERSION 3.4.0)

project(nd_api VERSION 1.0.0 LANGUAGES CXX)

add_definitions(-DBOOST_ALL_NO_LIB)
set(Boost_USE_STATIC_LIBS OFF)
set(BOOST_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME OFF)

find_package(Boost 1.54 COMPONENTS system REQUIRED)
find_package(Threads REQUIRED)
    
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

if(UNIX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror -Wall -pedantic -Wno-maybe-uninitialized -Wno-unused-but-set-variable -Wno-unused-local-typedefs")
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g -O0")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3")
endif()

if (MSVC)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /bigobj -D_SCL_SECURE_NO_WARNINGS")
endif()


add_library(nd-api 
  include/new_dexterity/message_dispatcher.hpp
  include/new_dexterity/async_read_stream.hpp
  include/new_dexterity/write_stream.hpp
  include/new_dexterity/in_connection.hpp
  include/new_dexterity/out_connection.hpp
  include/new_dexterity/communication_interface.hpp
  include/new_dexterity/communication_interface/communication_interface.hpp
  include/new_dexterity/communication_interface/response_slot.hpp
  include/new_dexterity/new_dexterity.hpp
  include/new_dexterity/pipe.hpp
  include/new_dexterity/metaprogramming.hpp
  include/new_dexterity/ndx_protocol/message_parser.hpp
  include/new_dexterity/ndx_protocol/message_dispatcher.hpp
  include/new_dexterity/ndx_protocol/in_connection.hpp
  include/new_dexterity/ndx_protocol/out_connection.hpp
  include/new_dexterity/ndx_protocol/communication_policy.hpp
  include/new_dexterity/ndx_protocol/communication_interface.hpp
  include/new_dexterity/ndx_protocol/protocol.hpp
  include/new_dexterity/ndx_protocol/requests.hpp
  include/new_dexterity/ndx_protocol/requests_io.hpp
  include/new_dexterity/ndx_protocol/responses.hpp
  include/new_dexterity/ndx_protocol/responses_io.hpp
  include/new_dexterity/ndx_protocol/response_of.hpp
  include/new_dexterity/ndx_protocol/serialize_request.hpp
  include/new_dexterity/ndx_protocol/serialize_response.hpp
  include/new_dexterity/ndx_protocol/serialize.hpp
  include/new_dexterity/ndx_protocol/deserialize_base.hpp
  include/new_dexterity/ndx_protocol/deserialize_request.hpp
  include/new_dexterity/ndx_protocol/deserialize_response.hpp
  include/new_dexterity/ndx_protocol/deserialize.hpp
  include/new_dexterity/ndx_protocol/fake_hand_hardware.hpp
  include/new_dexterity/ndx_protocol/discoverer.hpp
  include/new_dexterity/ndx_protocol.hpp
  include/new_dexterity/dynamixel_protocol.hpp
  include/new_dexterity/dynamixel_protocol/communication_interface.hpp
  include/new_dexterity/dynamixel_protocol/communication_policy.hpp
  include/new_dexterity/dynamixel_protocol/deserialize.hpp
  include/new_dexterity/dynamixel_protocol/fake_dynamixel_driver.hpp
  include/new_dexterity/dynamixel_protocol/in_connection.hpp
  include/new_dexterity/dynamixel_protocol/memory_access.hpp
  include/new_dexterity/dynamixel_protocol/memory_content.hpp
  include/new_dexterity/dynamixel_protocol/message_dispatcher.hpp
  include/new_dexterity/dynamixel_protocol/out_connection.hpp
  include/new_dexterity/dynamixel_protocol/parser.hpp
  include/new_dexterity/dynamixel_protocol/requests.hpp
  include/new_dexterity/dynamixel_protocol/response_of.hpp
  include/new_dexterity/dynamixel_protocol/responses.hpp
  include/new_dexterity/dynamixel_protocol/serialize.hpp

  include/new_dexterity/communication_interface/message_handler_queue.hpp
  src/ndx_protocol/protocol.cpp
)


target_link_libraries(nd-api 
    PUBLIC 
        Boost::boost
        Boost::system
        Threads::Threads
)

target_include_directories(nd-api
    PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
        $<INSTALL_INTERFACE:include>)

set_target_properties(nd-api 
    PROPERTIES 
        WINDOWS_EXPORT_ALL_SYMBOLS ON
        POSITION_INDEPENDENT_CODE ON)

target_compile_definitions(nd-api
    PUBLIC
        BOOST_SYSTEM_NO_DEPRECATED
        BOOST_DATE_TIME_NO_LIB
        BOOST_REGEX_NO_LIB)


add_library(NdApi::NdApi ALIAS nd-api)

# Installation

list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

include (GNUInstallDirs)

set_target_properties(nd-api PROPERTIES EXPORT_NAME NdApi)

install(DIRECTORY include/new_dexterity DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})

install(TARGETS nd-api
    EXPORT nd-api-targets
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_RUNDIR}
)

set(INSTALL_CONFIGDIR ${CMAKE_INSTALL_LIBDIR}/cmake/NdApi)

install(EXPORT nd-api-targets
    FILE NdApiTargets.cmake
    NAMESPACE NdApi::
    DESTINATION ${INSTALL_CONFIGDIR}
)

include(CMakePackageConfigHelpers)

write_basic_package_version_file(
    ${CMAKE_CURRENT_BINARY_DIR}/NdApiConfigVersion.cmake
    VERSION ${PROJECT_VERSION}
    COMPATIBILITY AnyNewerVersion
)

set(cmakePrefixPath "${CMAKE_PREFIX_PATH}")

configure_package_config_file(${CMAKE_CURRENT_LIST_DIR}/cmake/NdApiConfig.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/NdApiConfig.cmake
    INSTALL_DESTINATION ${INSTALL_CONFIGDIR}
    PATH_VARS cmakePrefixPath 
)

install(FILES
    ${CMAKE_CURRENT_BINARY_DIR}/NdApiConfig.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/NdApiConfigVersion.cmake
    DESTINATION ${INSTALL_CONFIGDIR}
)


include(AddUninstallTarget)

add_subdirectory(doc)
add_subdirectory(tests)

if ("${CPACK_GENERATOR}" STREQUAL DEB)
    set(CPACK_DEBIAN_PACKAGE_DEPENDS "libboost-dev (>=1.58), libboost-system-dev (>=1.58)")
    set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Alexandros Liarokapis (liarokapis.v@gmail.com)")
endif()


if ("${CPACK_GENERATOR}" STREQUAL NSIS)

	if ("${CMAKE_SIZEOF_VOID_P}" STREQUAL "8")

	set(BOOST_FILE_NAME boost_1_69_0-msvc-14.1-64.exe)
	set(BOOST_EXPECTED_HASH 26b940908edb9ba5e68255c06336978008cc33b31f45f4888e52d9bbf7c4b3de)

	elseif ("${CMAKE_SIZEOF_VOID_P}" STREQUAL "4")

	set(BOOST_FILE_NAME boost_1_69_0-msvc-14.1-32.exe)
	set(BOOST_EXPECTED_HASH c575920161c1eb0061674d73aa8902628843910743ba80322a31f444b36d5d46)

	endif()

file(DOWNLOAD https://kent.dl.sourceforge.net/project/boost/boost-binaries/1.69.0/${BOOST_FILE_NAME} 
    ${CMAKE_CURRENT_SOURCE_DIR}/installers/${BOOST_FILE_NAME}
    EXPECTED_HASH SHA256=${BOOST_EXPECTED_HASH}
)

install(PROGRAMS ${CMAKE_CURRENT_SOURCE_DIR}/installers/${BOOST_FILE_NAME}
        DESTINATION tmp
)

list(APPEND CPACK_NSIS_DEFINES "RequestExecutionLevel admin")

list(APPEND CPACK_NSIS_EXTRA_INSTALL_COMMANDS "
    ExecWait '$INSTDIR\\\\tmp\\\\${BOOST_FILE_NAME}'
")

endif()

include(CPack)

add_executable(real_communication EXCLUDE_FROM_ALL
    examples/dynamixel_protocol/real_communication.cpp)
target_link_libraries(real_communication NdApi::NdApi)

add_executable(fake_communication EXCLUDE_FROM_ALL
    examples/dynamixel_protocol/fake_communication.cpp)
target_link_libraries(fake_communication NdApi::NdApi)

add_executable(real_multi_communication EXCLUDE_FROM_ALL
    examples/dynamixel_protocol/real_multi_communication.cpp)
target_link_libraries(real_multi_communication NdApi::NdApi)

add_executable(fake_multi_communication EXCLUDE_FROM_ALL
    examples/dynamixel_protocol/fake_multi_communication.cpp)
target_link_libraries(fake_multi_communication NdApi::NdApi)

add_executable(calibrate_dynamixel EXCLUDE_FROM_ALL
    examples/dynamixel_protocol/calibrate_dynamixel.cpp)
target_link_libraries(calibrate_dynamixel NdApi::NdApi)

add_executable(zero_dynamixel EXCLUDE_FROM_ALL
    examples/dynamixel_protocol/zero_dynamixel.cpp)
target_link_libraries(zero_dynamixel NdApi::NdApi)

add_custom_target(examples)
add_dependencies(examples real_communication fake_communication real_multi_communication fake_multi_communication calibrate_dynamixel)



#include <catch2/catch.hpp>
#include <new_dexterity/new_dexterity.hpp>
#include <new_dexterity/ndx_protocol.hpp>
#include <boost/variant.hpp>
#include <boost/optional.hpp>

TEST_CASE("Hand Id Response Serializer")
{
    std::uint8_t const hand_ids[3] = {0, 254, 100};

    for (std::size_t i = 0; i != 3; ++i)
    {
        CAPTURE(i);
        std::vector<std::uint8_t> buffer;

        new_dexterity::ndx_protocol::serialize(hand_ids[i], buffer, new_dexterity::ndx_protocol::hand_id_response{ hand_ids[i] });

        REQUIRE(buffer.size() == sizeof(NDHeader));

        auto it = buffer.begin();

        NDHeader header;
        std::copy_n(it, sizeof(header), reinterpret_cast<std::uint8_t*>(&header));
        std::advance(it, sizeof(header));

        REQUIRE(header.id == hand_ids[i]);
        REQUIRE(validate_header_checksum(header));
        REQUIRE(header.header == NDHEAD);
        REQUIRE(header.class_id == MSG_CLASS);
        REQUIRE(header.msg_id == ID_ID);
        REQUIRE(header.payload_len == 0);
    }
}

TEST_CASE("Actuator Info Response Serializer")
{
    std::uint8_t const hand_ids[3] = {0, 254, 100};

    new_dexterity::ndx_protocol::actuator_info_response const responses[3] = {
        { new_dexterity::ndx_protocol::actuator_info{ 100, 200, 300, true, 270, 50},
          new_dexterity::ndx_protocol::actuator_info{ 101, 201, 301, true, 271, 51},
          new_dexterity::ndx_protocol::actuator_info{ 102, 202, 302, true, 272, 52},
          new_dexterity::ndx_protocol::actuator_info{ 103, 203, 303, true, 273, 53},
          new_dexterity::ndx_protocol::actuator_info{ 104, 204, 304, true, 274, 54} },
        { new_dexterity::ndx_protocol::actuator_info{ 200, 300, 400, true, 370, 60},
          new_dexterity::ndx_protocol::actuator_error::disconnected,
          new_dexterity::ndx_protocol::actuator_info{ 202, 302, 402, true, 372, 62},
          new_dexterity::ndx_protocol::actuator_error::disconnected,
          new_dexterity::ndx_protocol::actuator_info{ 204, 304, 404, true, 374, 64} },
        { new_dexterity::ndx_protocol::actuator_error::disconnected,
          new_dexterity::ndx_protocol::actuator_error::disconnected,
          new_dexterity::ndx_protocol::actuator_error::disconnected,
          new_dexterity::ndx_protocol::actuator_info{ 303, 403, 503, true, 473, 73},
          new_dexterity::ndx_protocol::actuator_error::disconnected } };

    boost::variant<new_dexterity::ndx_protocol::actuator_info, new_dexterity::ndx_protocol::actuator_error> const * vars[3][5] = {
        { &responses[0].thumb_actuator_info_response,
          &responses[0].index_actuator_info_response,
          &responses[0].middle_actuator_info_response,
          &responses[0].ring_pinky_actuator_info_response,
          &responses[0].thumb_opposition_actuator_info_response },
        { &responses[1].thumb_actuator_info_response,
          &responses[1].index_actuator_info_response,
          &responses[1].middle_actuator_info_response,
          &responses[1].ring_pinky_actuator_info_response,
          &responses[1].thumb_opposition_actuator_info_response },
        { &responses[2].thumb_actuator_info_response,
          &responses[2].index_actuator_info_response,
          &responses[2].middle_actuator_info_response,
          &responses[2].ring_pinky_actuator_info_response,
          &responses[2].thumb_opposition_actuator_info_response }
    };

    for (std::size_t i = 0; i != 3; ++i)
    {
        CAPTURE(i);
        std::vector<std::uint8_t> buffer;
        new_dexterity::ndx_protocol::serialize(hand_ids[i], buffer, responses[i]);

        REQUIRE(buffer.size() == (sizeof(NDHeader) + sizeof(ACT_header) + 5 * sizeof(ACTx_struct) + 2));

        auto it = buffer.begin();

        NDHeader header;
        std::copy_n(it, sizeof(header), reinterpret_cast<std::uint8_t*>(&header));
        std::advance(it, sizeof(header));

        REQUIRE(header.id == hand_ids[i]);
        REQUIRE(validate_header_checksum(header));
        REQUIRE(header.header == NDHEAD);
        REQUIRE(header.class_id == MSG_CLASS);
        REQUIRE(header.msg_id == ACT_ID);
        REQUIRE(header.payload_len == (sizeof(ACT_header) + 5 * sizeof(ACTx_struct)));

        ACT_header aheader;
        std::copy_n(it, sizeof(aheader), reinterpret_cast<std::uint8_t*>(&aheader));
        std::advance(it, sizeof(aheader));

        REQUIRE(aheader.num_actuators == 5);

        for (std::size_t j = 0; j != aheader.num_actuators; ++j)
        {
            ACTx_struct act;
            std::copy_n(it, sizeof(act), reinterpret_cast<std::uint8_t*>(&act));
            std::advance(it, sizeof(act));

            REQUIRE(act.id == (j+1));
            REQUIRE(act.type == (act.id == 5 ? MOTOR_PWM : DYNX_MOTOR));
            new_dexterity::match(*vars[i][j],
                    [&](new_dexterity::ndx_protocol::actuator_info const & info)
                    {
                        REQUIRE(act.moving == info.moving);    
                        REQUIRE(act.temperature == info.temperature);
                        REQUIRE(act.position == info.position);
                        REQUIRE(act.velocity == info.velocity);
                        REQUIRE(act.torque == info.torque);
                        REQUIRE(act.desired_actuation == info.desired_actuation);
                    },
                    [&](new_dexterity::ndx_protocol::actuator_error const & error)
                    {
                        if (error == new_dexterity::ndx_protocol::actuator_error::overload)
                            REQUIRE(act.status == OVERLOAD);
                        if (error == new_dexterity::ndx_protocol::actuator_error::electrical_shock)
                            REQUIRE(act.status == ELECTRICAL_SHOCK);
                        if (error == new_dexterity::ndx_protocol::actuator_error::disconnected)
                            REQUIRE(act.status == DISCONNECTED);
                        if (error == new_dexterity::ndx_protocol::actuator_error::voltage_error)
                            REQUIRE(act.status == VOLTAGE_ERROR);
                        if (error == new_dexterity::ndx_protocol::actuator_error::software_error)
                            REQUIRE(act.status == SOFTWARE_ERROR);
                        if (error == new_dexterity::ndx_protocol::actuator_error::out_of_limits)
                            REQUIRE(act.status == OUT_OF_LIMITS);
                        if (error == new_dexterity::ndx_protocol::actuator_error::overheating)
                            REQUIRE(act.status == OVERHEATING);
                        if (error == new_dexterity::ndx_protocol::actuator_error::encoder_error)
                            REQUIRE(act.status == ENCODER_ERROR);
                        if (error == new_dexterity::ndx_protocol::actuator_error::out_of_range)
                            REQUIRE(act.status == OUT_OF_RANGE);
                        if (error == new_dexterity::ndx_protocol::actuator_error::unknown_error)
                            REQUIRE(act.status == (1 << 8 - 1));
                    });
            
        }
    }
}

TEST_CASE("Command Actuators Response Serializer", "[communication interface][serializers][command actuators response]")
{
    std::uint8_t const hand_ids[2] = {0, 100};
    new_dexterity::ndx_protocol::command_actuators_response const responses[2] = {
        new_dexterity::ndx_protocol::command_actuators_request_acknowledged {},
        new_dexterity::ndx_protocol::command_actuators_request_not_acknowledged::wrong_command };

    for (std::size_t i = 0; i != 2; ++i)
    {
        CAPTURE(i);

        std::vector<std::uint8_t> buffer;
        new_dexterity::ndx_protocol::serialize(hand_ids[i], buffer, responses[i]);

        REQUIRE(buffer.size() == sizeof(NDHeader) + sizeof(ACK_struct) + 2);

        auto it = buffer.begin();

        NDHeader header;
        std::copy_n(it, sizeof(header), reinterpret_cast<std::uint8_t*>(&header));
        std::advance(it, sizeof(header));

        new_dexterity::match(responses[i],
                [&](new_dexterity::ndx_protocol::command_actuators_request_acknowledged const &)
                {
                    REQUIRE(header.id == hand_ids[i]);
                    REQUIRE(validate_header_checksum(header));
                    REQUIRE(header.header == NDHEAD);
                    REQUIRE(header.class_id == ACK_CLASS);
                    REQUIRE(header.msg_id == ACK_ID);
                    REQUIRE(header.payload_len == sizeof(ACK_struct));

                    ACK_struct ack;
                    std::copy_n(it, sizeof(ack), reinterpret_cast<std::uint8_t*>(&ack));
                    std::advance(it, sizeof(ack));

                    REQUIRE(ack.class_id == COM_CLASS);
                    REQUIRE(ack.msg_id == ACOM_ID);
                },
                [&](new_dexterity::ndx_protocol::command_actuators_request_not_acknowledged const & nack)
                {
                    REQUIRE(header.id == hand_ids[i]);
                    REQUIRE(validate_header_checksum(header));
                    REQUIRE(header.header == NDHEAD);
                    REQUIRE(header.class_id == ACK_CLASS);
                    REQUIRE(header.msg_id == NACK_ID);
                    REQUIRE(header.payload_len == sizeof(ACK_struct));

                    ACK_struct ack;
                    std::copy_n(it, sizeof(ack), reinterpret_cast<std::uint8_t*>(&ack));
                    std::advance(it, sizeof(ack));

                    REQUIRE(ack.class_id == COM_CLASS);
                    REQUIRE(ack.msg_id == ACOM_ID);

                    if (nack == new_dexterity::ndx_protocol::command_actuators_request_not_acknowledged::wrong_command)
                        REQUIRE(ack.err_code == WRONG_COMMAND);
                    if (nack == new_dexterity::ndx_protocol::command_actuators_request_not_acknowledged::wrong_actuator_num)
                        REQUIRE(ack.err_code == A_WRONG_MOTOR_NUM);
                });
    }
}

TEST_CASE("Hand Status Response Serializer", "[communication interface][serializers][hand status response]")
{
    std::uint8_t const hand_ids[3] = {0, 254, 100};
    new_dexterity::ndx_protocol::hand_status_response const responses[3] = {
        new_dexterity::ndx_protocol::hand_status_response { 100, 200, 300, 400, 500, 600 },
        new_dexterity::ndx_protocol::hand_status_response { 200, 300, 400, 500, 600, 700 },
        new_dexterity::ndx_protocol::hand_status_response { 300, 400, 500, 600, 700, 800 } };

    for (std::size_t i = 0; i != 3; ++i)
    {
        CAPTURE(i);
        std::vector<std::uint8_t> buffer;

        new_dexterity::ndx_protocol::serialize(hand_ids[i], buffer, responses[i]);

        REQUIRE(buffer.size() == sizeof(HSTATUS_msg));

        auto it = buffer.begin();

        HSTATUS_msg msg;
        std::copy_n(it, sizeof(msg), reinterpret_cast<std::uint8_t*>(&msg));

        REQUIRE(msg.header.id == hand_ids[i]);
        REQUIRE(validate_header_checksum(msg.header));
        REQUIRE(msg.header.header == NDHEAD);
        REQUIRE(msg.header.class_id == MSG_CLASS);
        REQUIRE(msg.header.msg_id == STH_ID);
        REQUIRE(msg.header.payload_len == sizeof(HSTATUS_msg) - sizeof(NDHeader) - 2);

        REQUIRE(msg.time == responses[i].mcu_time);
        REQUIRE(msg.temperature == responses[i].mcu_temperature);
        REQUIRE(msg.last_error_of_hand == responses[i].last_hand_error_code);
        REQUIRE(msg.commands_recv == responses[i].received_commands);
        REQUIRE(msg.commands_recv_valid == responses[i].valid_received_commands);
        REQUIRE(msg.commands_exec == responses[i].executed_commands);
        
    }
}

TEST_CASE("Communication Status Response Serializer", "[communication interface][serializers][communication status response]")
{
    std::uint8_t const hand_ids[3] = {0, 254, 100};
    new_dexterity::ndx_protocol::communication_status_response const responses[3] = {
        new_dexterity::ndx_protocol::communication_status_response { 100, 200, 300, 400, 500, 600 },
        new_dexterity::ndx_protocol::communication_status_response { 200, 300, 400, 500, 600, 700 },
        new_dexterity::ndx_protocol::communication_status_response { 300, 400, 500, 600, 700, 800 } };

    for (std::size_t i = 0; i != 3; ++i)
    {
        CAPTURE(i);
        std::vector<std::uint8_t> buffer;

        new_dexterity::ndx_protocol::serialize(hand_ids[i], buffer, responses[i]);

        REQUIRE(buffer.size() == sizeof(CSTATUS_msg));

        auto it = buffer.begin();

        CSTATUS_msg msg;
        std::copy_n(it, sizeof(msg), reinterpret_cast<std::uint8_t*>(&msg));

        REQUIRE(msg.header.id == hand_ids[i]);
        REQUIRE(validate_header_checksum(msg.header));
        REQUIRE(msg.header.header == NDHEAD);
        REQUIRE(msg.header.class_id == MSG_CLASS);
        REQUIRE(msg.header.msg_id == STC_ID);
        REQUIRE(msg.header.payload_len == sizeof(CSTATUS_msg) - sizeof(NDHeader) - 2);

        REQUIRE(msg.time == responses[i].mcu_time);
        REQUIRE(msg.last_error_of_comm == responses[i].last_communication_error_code);
        REQUIRE(msg.recv_packets_all == responses[i].received_packets);
        REQUIRE(msg.recv_packets_valid == responses[i].valid_received_packets);
        REQUIRE(msg.tran_packets_all == responses[i].prepared_packets);
        REQUIRE(msg.tran_packets_valid == responses[i].transmitted_packets);
    }
}

TEST_CASE("Actuators Status Response Serializer", "[communication interface][serializers][actuators status response]")
{
    std::uint8_t const hand_ids[3] = {0, 254, 100};
    new_dexterity::ndx_protocol::actuators_status_response const responses[3] = {
        new_dexterity::ndx_protocol::actuators_status_response {  100, 
            new_dexterity::ndx_protocol::actuator_status {20, 30, 40, 50, 60, 70, 80},
            new_dexterity::ndx_protocol::actuator_status {20, 30, 40, 50, 60, 70, 80},
            new_dexterity::ndx_protocol::actuator_status {20, 30, 40, 50, 60, 70, 80},
            new_dexterity::ndx_protocol::actuator_status {20, 30, 40, 50, 60, 70, 80},
            new_dexterity::ndx_protocol::actuator_status {20, 30, 40, 50, 60, 70, 80}
        },
        new_dexterity::ndx_protocol::actuators_status_response {  101, 
            new_dexterity::ndx_protocol::actuator_status {30, 40, 50, 60, 70, 80, 90},
            boost::none,
            new_dexterity::ndx_protocol::actuator_status {30, 40, 50, 60, 70, 80, 90},
            boost::none,
            new_dexterity::ndx_protocol::actuator_status {30, 40, 50, 60, 70, 80, 90}
        },
        new_dexterity::ndx_protocol::actuators_status_response {  102, 
            boost::none,
            new_dexterity::ndx_protocol::actuator_status {40, 50, 60, 70, 80, 90, 100},
            boost::none,
            boost::none,
            boost::none
        } };

    std::vector<std::uint8_t> const ids[3] = { {1, 2, 3, 4, 5}, {1, 3, 5}, {2} };

    std::vector<new_dexterity::ndx_protocol::actuator_status const *> const statuses[3] = {
        { &(responses[0].thumb_actuator_status.get()),
          &(responses[0].index_actuator_status.get()),
          &(responses[0].middle_actuator_status.get()),
          &(responses[0].ring_pinky_actuator_status.get()),
          &(responses[0].thumb_opposition_actuator_status.get()) },
        { &(responses[1].thumb_actuator_status.get()),
          &(responses[1].middle_actuator_status.get()),
          &(responses[1].thumb_opposition_actuator_status.get()) },
        { &(responses[2].index_actuator_status.get()) } };


    for (std::size_t i = 0; i != 3; ++i)
    {
        CAPTURE(i);
        std::vector<std::uint8_t> buffer;

        new_dexterity::ndx_protocol::serialize(hand_ids[i], buffer, responses[i]);

        REQUIRE(buffer.size() == sizeof(NDHeader) + 4 + 1 + ids[i].size() * sizeof(ASTATUSx_struct) + 2);

        auto it = buffer.begin();

        NDHeader header;
        std::copy_n(it, sizeof(header), reinterpret_cast<std::uint8_t*>(&header));
        std::advance(it, sizeof(header));

        REQUIRE(header.id == hand_ids[i]);
        REQUIRE(validate_header_checksum(header));
        REQUIRE(header.header == NDHEAD);
        REQUIRE(header.class_id == MSG_CLASS);
        REQUIRE(header.msg_id == STA_ID);
        REQUIRE(header.payload_len == 4 + 1 + ids[i].size() * sizeof(ASTATUSx_struct));

        std::uint32_t time;
        std::copy_n(it, sizeof(time), reinterpret_cast<std::uint8_t*>(&time));
        std::advance(it, sizeof(time));

        std::uint8_t num;
        std::copy_n(it, sizeof(num), reinterpret_cast<std::uint8_t*>(&num));
        std::advance(it, sizeof(num));

        for (std::size_t j = 0; j != num; ++j)
        {
            ASTATUSx_struct stat;
            std::copy_n(it, sizeof(stat), reinterpret_cast<std::uint8_t*>(&stat));
            std::advance(it, sizeof(stat));

            REQUIRE(stat.id == ids[i][j]);
            REQUIRE(stat.type == ((stat.id == 5) ? MOTOR_PWM : DYNX_MOTOR));
            REQUIRE(stat.last_error == statuses[i][j]->last_actuator_error_code);
            REQUIRE(stat.temperature_max == statuses[i][j]->max_temperature);
            REQUIRE(stat.temperature_min == statuses[i][j]->min_temperature);
            REQUIRE(stat.temperature_avg == statuses[i][j]->avg_temperature);
            REQUIRE(stat.torque_max == statuses[i][j]->max_torque);
            REQUIRE(stat.torque_min == statuses[i][j]->min_torque);
            REQUIRE(stat.torque_avg == statuses[i][j]->avg_torque);
        }
    }
}

struct test_read_config
{
    template <std::size_t I, std::size_t Id, std::size_t Length>
    void operator()(std::tuple<std::integral_constant<std::size_t, I>,
                               std::integral_constant<std::size_t, Id>,
                               std::integral_constant<std::size_t, Length>,
                               new_dexterity::ndx_protocol::read_config_response<Length>> const & tup)
    {
        CAPTURE(I);

        auto const & response = std::get<3>(tup);

        std::vector<std::uint8_t> buffer;
        new_dexterity::ndx_protocol::serialize(Id, buffer, response);

        auto it = buffer.begin();

        new_dexterity::match(response,
                [&](new_dexterity::ndx_protocol::config<Length> const & config)
                {
                    REQUIRE(buffer.size() == sizeof(NDHeader) + Length + 2);
                    NDHeader header;
                    std::copy_n(it, sizeof(header), reinterpret_cast<std::uint8_t*>(&header));
                    std::advance(it, sizeof(header));

                    REQUIRE(header.id == Id);
                    REQUIRE(validate_header_checksum(header));
                    REQUIRE(header.header == NDHEAD);
                    REQUIRE(header.class_id == CFG_CLASS);
                    REQUIRE(header.msg_id == UCF_ID);
                    REQUIRE(header.payload_len == Length);

                    std::array<std::uint8_t, Length> data;
                    std::copy_n(it, Length, data.begin());

                    REQUIRE(data == config.data);
                },
                [&](new_dexterity::ndx_protocol::read_config_request_not_acknowledged const & nack)
                {
                    REQUIRE(buffer.size() == sizeof(NDHeader) + sizeof(ACK_struct) + 2);
                    NDHeader header;
                    std::copy_n(it, sizeof(header), reinterpret_cast<std::uint8_t*>(&header));
                    std::advance(it, sizeof(header));

                    REQUIRE(header.id == Id);
                    REQUIRE(validate_header_checksum(header));
                    REQUIRE(header.header == NDHEAD);
                    REQUIRE(header.class_id == ACK_CLASS);
                    REQUIRE(header.msg_id == NACK_ID);
                    REQUIRE(header.payload_len == sizeof(ACK_struct));

                    ACK_struct ack;
                    std::copy_n(it, sizeof(ack), reinterpret_cast<std::uint8_t*>(&ack));
                    std::advance(it, sizeof(ack));

                    REQUIRE(ack.class_id == CFG_CLASS);
                    REQUIRE(ack.msg_id == UCF_ID);

                    if (nack == new_dexterity::ndx_protocol::read_config_request_not_acknowledged::eeprom_hardware_error)
                        REQUIRE(ack.err_code == CFG_FLASH_ERROR);

                    if (nack == new_dexterity::ndx_protocol::read_config_request_not_acknowledged::incorrect_eeprom_address)
                        REQUIRE(ack.err_code == CFG_ACCESS_ERROR);
                });

    }
};

TEST_CASE("Read Config Response Serializer", "[communication interface][serializers][read config response]")
{
    std::tuple<
        std::tuple<std::integral_constant<std::size_t, 0>,
                   std::integral_constant<std::size_t, 0>,
                   std::integral_constant<std::size_t, 5>,
                   new_dexterity::ndx_protocol::read_config_response<5>>,
        std::tuple<std::integral_constant<std::size_t, 1>,
                   std::integral_constant<std::size_t, 254>,
                   std::integral_constant<std::size_t, 1>,
                   new_dexterity::ndx_protocol::read_config_response<1>>,
        std::tuple<std::integral_constant<std::size_t, 2>,
                   std::integral_constant<std::size_t, 100>,
                   std::integral_constant<std::size_t, 1>,
                   new_dexterity::ndx_protocol::read_config_response<1>>> cases;

    std::array<std::uint8_t, 5> test_data = {{'t', 'e', 's', 't', '1'}};

    std::get<3>(std::get<0>(cases)) = new_dexterity::ndx_protocol::config<5> { test_data };
    std::get<3>(std::get<1>(cases)) = new_dexterity::ndx_protocol::read_config_request_not_acknowledged::incorrect_eeprom_address;
    std::get<3>(std::get<2>(cases)) = new_dexterity::ndx_protocol::read_config_request_not_acknowledged::eeprom_hardware_error;

    new_dexterity::for_each(cases, test_read_config{});
}

struct test_write_config
{
    template <std::size_t I, std::size_t Id>
    void operator()(std::tuple<std::integral_constant<std::size_t, I>,
                               std::integral_constant<std::size_t, Id>,
                               new_dexterity::ndx_protocol::write_config_response> const & tup)
    {
        CAPTURE(I);
        auto const & response = std::get<2>(tup);
        std::vector<std::uint8_t> buffer;
        new_dexterity::ndx_protocol::serialize(Id, buffer, response);

        auto it = buffer.begin();

        new_dexterity::match(response,
                [&](const new_dexterity::ndx_protocol::write_config_request_acknowledged&)
                {
                    REQUIRE(buffer.size() == sizeof(NDHeader) + sizeof(ACK_struct) + 2);
                    NDHeader header;
                    std::copy_n(it, sizeof(header), reinterpret_cast<std::uint8_t*>(&header));
                    std::advance(it, sizeof(header));

                    REQUIRE(header.id == Id);
                    REQUIRE(validate_header_checksum(header));
                    REQUIRE(header.header == NDHEAD);
                    REQUIRE(header.class_id == ACK_CLASS);
                    REQUIRE(header.msg_id == ACK_ID);
                    REQUIRE(header.payload_len == sizeof(ACK_struct));

                    ACK_struct ack;
                    std::copy_n(it, sizeof(ack), reinterpret_cast<std::uint8_t*>(&ack));
                    std::advance(it, sizeof(ack));

                    REQUIRE(ack.class_id == CFG_CLASS);
                    REQUIRE(ack.msg_id == UCF_ID);
                },
                [&](new_dexterity::ndx_protocol::write_config_request_not_acknowledged const & nack)
                {
                    REQUIRE(buffer.size() == sizeof(NDHeader) + sizeof(ACK_struct) + 2);
                    NDHeader header;
                    std::copy_n(it, sizeof(header), reinterpret_cast<std::uint8_t*>(&header));
                    std::advance(it, sizeof(header));

                    REQUIRE(header.id == Id);
                    REQUIRE(validate_header_checksum(header));
                    REQUIRE(header.header == NDHEAD);
                    REQUIRE(header.class_id == ACK_CLASS);
                    REQUIRE(header.msg_id == NACK_ID);
                    REQUIRE(header.payload_len == sizeof(ACK_struct));

                    ACK_struct ack;
                    std::copy_n(it, sizeof(ack), reinterpret_cast<std::uint8_t*>(&ack));
                    std::advance(it, sizeof(ack));

                    REQUIRE(ack.class_id == CFG_CLASS);
                    REQUIRE(ack.msg_id == UCF_ID);

                    if (nack == new_dexterity::ndx_protocol::write_config_request_not_acknowledged::eeprom_hardware_error)
                        REQUIRE(ack.err_code == CFG_FLASH_ERROR);

                    if (nack == new_dexterity::ndx_protocol::write_config_request_not_acknowledged::incorrect_eeprom_address)
                        REQUIRE(ack.err_code == CFG_ACCESS_ERROR);

                    if (nack == new_dexterity::ndx_protocol::write_config_request_not_acknowledged::incorrect_data_format)
                        REQUIRE(ack.err_code == CFG_DATA_ERROR);

                    if (nack == new_dexterity::ndx_protocol::write_config_request_not_acknowledged::incorrect_data_length)
                        REQUIRE(ack.err_code == CFG_LENGTH_ERROR);
                });

    }

};

TEST_CASE("Write Config Response Serializer", "[communication interface][serializers][write config response]")
{
    new_dexterity::ndx_protocol::write_config_response const responses[5] = { 
        new_dexterity::ndx_protocol::write_config_request_acknowledged{},
        new_dexterity::ndx_protocol::write_config_request_not_acknowledged::eeprom_hardware_error,
        new_dexterity::ndx_protocol::write_config_request_not_acknowledged::incorrect_eeprom_address,
        new_dexterity::ndx_protocol::write_config_request_not_acknowledged::incorrect_data_format,
        new_dexterity::ndx_protocol::write_config_request_not_acknowledged::incorrect_data_length };

    std::tuple<
        std::tuple<std::integral_constant<std::size_t, 0>,
                   std::integral_constant<std::size_t, 0>,
                   new_dexterity::ndx_protocol::write_config_response>,
        std::tuple<std::integral_constant<std::size_t, 1>,
                   std::integral_constant<std::size_t, 254>,
                   new_dexterity::ndx_protocol::write_config_response>,
        std::tuple<std::integral_constant<std::size_t, 2>,
                   std::integral_constant<std::size_t, 100>,
                   new_dexterity::ndx_protocol::write_config_response>,
        std::tuple<std::integral_constant<std::size_t, 3>,
                   std::integral_constant<std::size_t, 60>,
                   new_dexterity::ndx_protocol::write_config_response>,
        std::tuple<std::integral_constant<std::size_t, 4>,
                   std::integral_constant<std::size_t, 150>,
                   new_dexterity::ndx_protocol::write_config_response>> cases;

    std::get<2>(std::get<0>(cases)) = new_dexterity::ndx_protocol::write_config_request_acknowledged{};
    std::get<2>(std::get<1>(cases)) = new_dexterity::ndx_protocol::write_config_request_not_acknowledged::eeprom_hardware_error;
    std::get<2>(std::get<2>(cases)) = new_dexterity::ndx_protocol::write_config_request_not_acknowledged::incorrect_eeprom_address;
    std::get<2>(std::get<3>(cases)) = new_dexterity::ndx_protocol::write_config_request_not_acknowledged::incorrect_data_format;
    std::get<2>(std::get<4>(cases)) = new_dexterity::ndx_protocol::write_config_request_not_acknowledged::incorrect_data_length;

    new_dexterity::for_each(cases, test_write_config{});
}


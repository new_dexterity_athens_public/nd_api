#include <catch2/catch.hpp>
#include <new_dexterity/new_dexterity.hpp>
#include <new_dexterity/ndx_protocol.hpp>
#include <boost/asio.hpp>
#include <boost/variant.hpp>
#include <boost/optional.hpp>
#include <iostream>
#include <chrono>
#include <thread>

namespace asio = boost::asio;

template <typename F>
void test(F test_fun)
{
    asio::io_service io_service;

    new_dexterity::pipe_endpoint endp1(io_service);
    new_dexterity::pipe_endpoint endp2(io_service);

    new_dexterity::create_pipe(endp1, endp2);

    new_dexterity::pipe_endpoint endp3(io_service);
    new_dexterity::pipe_endpoint endp4(io_service);

    new_dexterity::create_pipe(endp3, endp4);

    auto& connection_input = endp1;
    auto& connection_output = endp4;

    auto& hand_input = endp3;
    auto& hand_output = endp2;

    new_dexterity::ndx_protocol::in_connection in(connection_input);
    new_dexterity::ndx_protocol::out_connection out(connection_output);

    new_dexterity::ndx_protocol::communication_interface interface(in, out, 1);

    new_dexterity::ndx_protocol::in_connection fake_in(hand_input);
    new_dexterity::ndx_protocol::out_connection fake_out(hand_output);

    new_dexterity::ndx_protocol::fake_hand_hardware hand(fake_in, fake_out, 1);

    auto runner = std::thread([&io_service](){io_service.run();});
        
    test_fun(interface);

    io_service.stop();
    runner.join();
}

void test_actuator_info(new_dexterity::ndx_protocol::communication_interface& interface)
{
    auto future = interface.queue_request(new_dexterity::ndx_protocol::actuator_info_request{}, std::chrono::seconds(1)); 
    interface.out_connection().send_bytes();
    auto result = future.get();

    bool has_response = static_cast<bool>(result);
    REQUIRE(has_response);

    auto info = *result;

    bool has_thumb_actuator_info = info.thumb_actuator_info_response.type() == typeid(new_dexterity::ndx_protocol::actuator_info);
    REQUIRE(has_thumb_actuator_info);
    bool has_index_actuator_info = info.index_actuator_info_response.type() == typeid(new_dexterity::ndx_protocol::actuator_info);
    REQUIRE(has_index_actuator_info);
    bool has_middle_actuator_info = info.middle_actuator_info_response.type() == typeid(new_dexterity::ndx_protocol::actuator_info);
    REQUIRE(has_middle_actuator_info);
    bool has_ring_pinky_actuator_info = info.ring_pinky_actuator_info_response.type() == typeid(new_dexterity::ndx_protocol::actuator_info);
    REQUIRE(has_ring_pinky_actuator_info);
    bool has_thumb_opposition_actuator_info = info.thumb_opposition_actuator_info_response.type() == typeid(new_dexterity::ndx_protocol::actuator_info);
    REQUIRE(has_thumb_opposition_actuator_info);
}

void test_command_actuators(new_dexterity::ndx_protocol::communication_interface& interface)
{
    auto actuator_command = new_dexterity::ndx_protocol::actuator_command{ 0, 0, 0 };
    auto actuator_commands = new_dexterity::ndx_protocol::actuator_commands{ actuator_command, actuator_command, actuator_command, actuator_command, actuator_command };

    auto future = interface.queue_request(new_dexterity::ndx_protocol::command_actuators_request{actuator_commands}, std::chrono::seconds(1)); 
    interface.out_connection().send_bytes();
    auto result = future.get();

    bool has_response = static_cast<bool>(result);
    REQUIRE(has_response);

    auto response = *result;

    bool acknowledged = response.type() == typeid(new_dexterity::ndx_protocol::command_actuators_request_acknowledged);
    REQUIRE(acknowledged);

}

void test_hand_status(new_dexterity::ndx_protocol::communication_interface& interface)
{
    auto future = interface.queue_request(new_dexterity::ndx_protocol::hand_status_request{}, std::chrono::seconds(1)); 
    interface.out_connection().send_bytes();
    auto result = future.get();

    bool has_response = static_cast<bool>(result);
    REQUIRE(has_response);

    auto response = *result;

    REQUIRE(response.mcu_time == 0);
    REQUIRE(response.mcu_temperature == 27);
}

void test_communication_status(new_dexterity::ndx_protocol::communication_interface& interface)
{
    auto future = interface.queue_request(new_dexterity::ndx_protocol::communication_status_request{}, std::chrono::seconds(1)); 
    interface.out_connection().send_bytes();
    auto result = future.get();

    bool has_response = static_cast<bool>(result);
    REQUIRE(has_response);

    auto response = *result;

    REQUIRE(response.mcu_time == 0);
    REQUIRE(response.last_communication_error_code == 0);
    REQUIRE(response.received_packets == 0);
    REQUIRE(response.valid_received_packets == 0);
    REQUIRE(response.prepared_packets == 0);
    REQUIRE(response.transmitted_packets == 0);
}

void test_actuators_status(new_dexterity::ndx_protocol::communication_interface& interface)
{
    auto future = interface.queue_request(new_dexterity::ndx_protocol::actuators_status_request{ {true, true, true, true, true} }, std::chrono::seconds(1)); 
    interface.out_connection().send_bytes();
    auto result = future.get();

    bool has_response = static_cast<bool>(result);
    REQUIRE(has_response);
    
    auto response = *result;

    CAPTURE(response);

    REQUIRE(response.mcu_time == 0);

    bool has_thumb_status = static_cast<bool>(response.thumb_actuator_status);
    REQUIRE(has_thumb_status);
    REQUIRE(response.thumb_actuator_status->last_actuator_error_code == 0);
    REQUIRE(response.thumb_actuator_status->min_temperature == 27);
    REQUIRE(response.thumb_actuator_status->avg_temperature == 27);
    REQUIRE(response.thumb_actuator_status->max_temperature == 27);
    REQUIRE(response.thumb_actuator_status->min_torque == 0);
    REQUIRE(response.thumb_actuator_status->min_torque == 0);
    REQUIRE(response.thumb_actuator_status->min_torque == 0);

    bool has_index_status = static_cast<bool>(response.index_actuator_status);
    REQUIRE(has_index_status);
    REQUIRE(response.index_actuator_status->last_actuator_error_code == 0);
    REQUIRE(response.index_actuator_status->min_temperature == 27);
    REQUIRE(response.index_actuator_status->avg_temperature == 27);
    REQUIRE(response.index_actuator_status->max_temperature == 27);
    REQUIRE(response.index_actuator_status->min_torque == 0);
    REQUIRE(response.index_actuator_status->min_torque == 0);
    REQUIRE(response.index_actuator_status->min_torque == 0);

    bool has_middle_status = static_cast<bool>(response.middle_actuator_status);
    REQUIRE(has_middle_status);
    REQUIRE(response.middle_actuator_status->last_actuator_error_code == 0);
    REQUIRE(response.middle_actuator_status->min_temperature == 27);
    REQUIRE(response.middle_actuator_status->avg_temperature == 27);
    REQUIRE(response.middle_actuator_status->max_temperature == 27);
    REQUIRE(response.middle_actuator_status->min_torque == 0);
    REQUIRE(response.middle_actuator_status->min_torque == 0);
    REQUIRE(response.middle_actuator_status->min_torque == 0);

    bool has_ring_pinky_status = static_cast<bool>(response.ring_pinky_actuator_status);
    REQUIRE(has_ring_pinky_status);
    REQUIRE(response.ring_pinky_actuator_status->last_actuator_error_code == 0);
    REQUIRE(response.ring_pinky_actuator_status->min_temperature == 27);
    REQUIRE(response.ring_pinky_actuator_status->avg_temperature == 27);
    REQUIRE(response.ring_pinky_actuator_status->max_temperature == 27);
    REQUIRE(response.ring_pinky_actuator_status->min_torque == 0);
    REQUIRE(response.ring_pinky_actuator_status->min_torque == 0);
    REQUIRE(response.ring_pinky_actuator_status->min_torque == 0);

    bool has_thumb_opposition_status = static_cast<bool>(response.thumb_opposition_actuator_status);
    REQUIRE(has_thumb_opposition_status);
    REQUIRE(response.thumb_opposition_actuator_status->last_actuator_error_code == 0);
    REQUIRE(response.thumb_opposition_actuator_status->min_temperature == 27);
    REQUIRE(response.thumb_opposition_actuator_status->avg_temperature == 27);
    REQUIRE(response.thumb_opposition_actuator_status->max_temperature == 27);
    REQUIRE(response.thumb_opposition_actuator_status->min_torque == 0);
    REQUIRE(response.thumb_opposition_actuator_status->min_torque == 0);
    REQUIRE(response.thumb_opposition_actuator_status->min_torque == 0);
}

TEST_CASE("Actuator Info Request-Response")
{
    test(test_actuator_info);
}

TEST_CASE("Command Actuators Request-Response")
{
    test(test_command_actuators);
}

TEST_CASE("Hand Status Request-Response")
{
    test(test_hand_status);
}

TEST_CASE("Communication Status Request-Response")
{
    test(test_communication_status);
}

TEST_CASE("Actuators Status Request-Response")
{
    test(test_actuators_status);
}


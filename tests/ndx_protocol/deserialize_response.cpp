#include <catch2/catch.hpp>
#include <cstdint>
#include <vector>
#include <typeinfo>
#include <new_dexterity/ndx_protocol/responses_io.hpp>
#include <new_dexterity/new_dexterity.hpp>
#include <new_dexterity/ndx_protocol.hpp>

template <typename Response>
void test(std::uint8_t id, Response response, std::uint8_t class_id, std::uint8_t msg_id)
{
    std::vector<std::uint8_t> buffer;

    new_dexterity::ndx_protocol::serialize(id, buffer, response);

    new_dexterity::ndx_protocol::message_parser parser;
    parser.parse(buffer, buffer.size());

    auto messages = parser.parsed_messages();

    REQUIRE(messages.size() == 1);
    
    auto& message = messages[0];

    SECTION("identity works")
    {
        auto result = new_dexterity::ndx_protocol::deserializer<Response>::deserialize(message);

        bool has_result = static_cast<bool>(result);

        REQUIRE(has_result);

        REQUIRE((*result) == response);
    }

    SECTION("invalid class id")
    {
        for (std::uint8_t c = 0; c != 4; ++c)
        {
            if (c != class_id)
            {
                message.first.class_id = c;

                auto result = new_dexterity::ndx_protocol::deserializer<Response>::deserialize(message);

                bool has_result = static_cast<bool>(result);

                REQUIRE_FALSE(has_result);
            }
        }
    }

    SECTION("invalid msg id")
    {
        for (std::uint8_t m = 0; m != 64; ++m)
        {
            if (m != msg_id)
            {
                message.first.msg_id = m;

                auto result = new_dexterity::ndx_protocol::deserializer<Response>::deserialize(message);

                bool has_result = static_cast<bool>(result);

                REQUIRE_FALSE(has_result);
            }
        }
    }
}


TEST_CASE("hand id response")
{
    test(101, new_dexterity::ndx_protocol::hand_id_response { 101 }, MSG_CLASS, ID_ID);
}

TEST_CASE("actuators info response")
{
    auto response = new_dexterity::ndx_protocol::actuator_info_response {
                    new_dexterity::ndx_protocol::actuator_info{ 500, 100, 200, false, 300, 400 },
                    new_dexterity::ndx_protocol::actuator_info{ 500, 100, 200, false, 300, 400 },
                    new_dexterity::ndx_protocol::actuator_info{ 500, 100, 200, true, 300, 400 },
                    new_dexterity::ndx_protocol::actuator_info{ 500, 100, 200, false, 300, 400 },
                    new_dexterity::ndx_protocol::actuator_info{ 500, 100, 200, true, 300, 400 } };

    test(101, response, MSG_CLASS, ACT_ID);
}

TEST_CASE("command actuators response")
{
    new_dexterity::ndx_protocol::command_actuators_response const responses[3] = 
    {
        new_dexterity::ndx_protocol::command_actuators_request_acknowledged{},
        new_dexterity::ndx_protocol::command_actuators_request_not_acknowledged::wrong_actuator_num,
        new_dexterity::ndx_protocol::command_actuators_request_not_acknowledged::wrong_command,
    };

    std::uint8_t const msg_ids[3] = 
    {
        ACK_ID,
        NACK_ID,
        NACK_ID
    };

    for (std::size_t i = 0; i != 3; ++i)
    {
        CAPTURE(i);
        test(101, responses[i], ACK_CLASS, msg_ids[i]);
    }
}

TEST_CASE("hand status response")
{
    auto response = new_dexterity::ndx_protocol::hand_status_response { 100, 200, 300, 400, 500, 600 };

    test(101, response, MSG_CLASS, STH_ID);
}

TEST_CASE("communication status response")
{
    auto response = new_dexterity::ndx_protocol::communication_status_response { 200, 300, 400, 500, 600 ,700 };

    test(101, response, MSG_CLASS, STC_ID);
}

TEST_CASE("actuators status response")
{
    new_dexterity::ndx_protocol::actuator_status status = {1,2,3,4,5,6,7};
    new_dexterity::ndx_protocol::actuators_status_response responses[4] = {
        { 102, boost::none, boost::none, status, boost::none, boost::none },
        { 102, status, boost::none, new_dexterity::ndx_protocol::actuator_status{ 2, 3, 4, 5, 6, 7, 8 }, boost::none, boost::none },
        { 102, status, status, status, boost::none, boost::none },
        { 102, status, status, status, status, boost::none }
    };

    for (std::size_t i = 0; i != 4; ++i)
    {
        test(101, responses[i], MSG_CLASS, STA_ID);
    }
}

TEST_CASE("read config response")
{
    new_dexterity::ndx_protocol::read_config_response<5> responses[3] = {
        new_dexterity::ndx_protocol::config<5>{},
        new_dexterity::ndx_protocol::read_config_request_not_acknowledged::incorrect_eeprom_address,
        new_dexterity::ndx_protocol::read_config_request_not_acknowledged::eeprom_hardware_error
    };

    std::uint8_t class_ids[3] = { CFG_CLASS, ACK_CLASS, ACK_CLASS };

    std::uint8_t msg_ids[3] = { UCF_ID, NACK_ID, NACK_ID };

    for (std::size_t i = 0; i != 3; ++i)
    {
        CAPTURE(i);

        test(101, responses[i], class_ids[i], msg_ids[i]);
    }
}

TEST_CASE("write config response")
{
    new_dexterity::ndx_protocol::write_config_response responses[5] = {
        new_dexterity::ndx_protocol::write_config_request_acknowledged{},
        new_dexterity::ndx_protocol::write_config_request_not_acknowledged::eeprom_hardware_error,
        new_dexterity::ndx_protocol::write_config_request_not_acknowledged::incorrect_eeprom_address,
        new_dexterity::ndx_protocol::write_config_request_not_acknowledged::incorrect_data_format,
        new_dexterity::ndx_protocol::write_config_request_not_acknowledged::incorrect_data_length
    };

    std::uint8_t const msg_ids[5] = { ACK_ID, NACK_ID, NACK_ID, NACK_ID, NACK_ID };

    for (std::size_t i = 0; i != 5; ++i)
    {
        CAPTURE(i);
        test(101, responses[i], ACK_CLASS, msg_ids[i]);
    }
}


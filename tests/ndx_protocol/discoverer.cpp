#include <catch2/catch.hpp>
#include <new_dexterity/new_dexterity.hpp>
#include <new_dexterity/ndx_protocol.hpp>
#include <thread>
#include <chrono>
#include <algorithm>

TEST_CASE("discoverer single")
{

    asio::io_service io_service;

    new_dexterity::pipe_endpoint endp1(io_service);
    new_dexterity::pipe_endpoint endp2(io_service);

    new_dexterity::create_pipe(endp1, endp2);

    new_dexterity::pipe_endpoint endp3(io_service);
    new_dexterity::pipe_endpoint endp4(io_service);

    new_dexterity::create_pipe(endp3, endp4);

    auto& discoverer_input = endp1;
    auto& discoverer_output = endp4;

    auto& hand_input = endp3;
    auto& hand_output = endp2;

    new_dexterity::ndx_protocol::in_connection in(discoverer_input);
    new_dexterity::ndx_protocol::out_connection out(discoverer_output);

    new_dexterity::ndx_protocol::discoverer discoverer(in, out);

    new_dexterity::ndx_protocol::in_connection fake_in(hand_input);
    new_dexterity::ndx_protocol::out_connection fake_out(hand_output);

    new_dexterity::ndx_protocol::fake_hand_hardware hand(fake_in, fake_out, 1);

    auto runner = std::thread([&io_service](){io_service.run();});

    auto ids = discoverer.discover(std::chrono::milliseconds(1000));

    REQUIRE(ids.size() == 1);
    REQUIRE(std::find(ids.begin(), ids.end(), 1) != ids.end());

    io_service.stop();
    runner.join();
        
}

TEST_CASE("discoverer double")
{

    asio::io_service io_service;

    new_dexterity::pipe_endpoint endp1(io_service);
    new_dexterity::pipe_endpoint endp2(io_service);

    new_dexterity::create_pipe(endp1, endp2);

    new_dexterity::pipe_endpoint endp3(io_service);
    new_dexterity::pipe_endpoint endp4(io_service);

    new_dexterity::create_pipe(endp3, endp4);

    auto& discoverer_in = endp1;
    auto& discoverer_to_hands = endp4;

    auto& hands_in = endp3;
    auto& hands_to_discoverer = endp2;

    new_dexterity::ndx_protocol::in_connection in(discoverer_in);
    new_dexterity::ndx_protocol::out_connection out(discoverer_to_hands);

    new_dexterity::ndx_protocol::discoverer discoverer(in, out);

    new_dexterity::ndx_protocol::in_connection fake_in(hands_in);
    new_dexterity::ndx_protocol::out_connection fake_out(hands_to_discoverer);

    new_dexterity::ndx_protocol::fake_hand_hardware hand1(fake_in, fake_out, 1);
    new_dexterity::ndx_protocol::fake_hand_hardware hand2(fake_in, fake_out, 2);

    auto runner = std::thread([&io_service](){io_service.run();});

    auto ids = discoverer.discover(std::chrono::milliseconds(1000));

    REQUIRE(ids.size() == 2);
    REQUIRE(std::find(ids.begin(), ids.end(), 1) != ids.end());
    REQUIRE(std::find(ids.begin(), ids.end(), 2) != ids.end());

    io_service.stop();
    runner.join();
        
}

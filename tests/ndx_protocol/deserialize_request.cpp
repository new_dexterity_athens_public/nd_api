#include <catch2/catch.hpp>
#include <new_dexterity/new_dexterity.hpp>
#include <new_dexterity/ndx_protocol/requests_io.hpp>
#include <cstdint>
#include <vector>

template <typename Request>
void test(std::uint8_t id, Request request, std::uint8_t class_id, std::uint8_t msg_id)
{
    std::vector<std::uint8_t> buffer;

    new_dexterity::ndx_protocol::serialize(id, buffer, request);

    new_dexterity::ndx_protocol::message_parser parser;
    parser.parse(buffer, buffer.size());

    auto messages = parser.parsed_messages();

    REQUIRE(messages.size() == 1);
    
    auto& message = messages[0];

    SECTION("identity works")
    {
        auto result = new_dexterity::ndx_protocol::deserializer<Request>::deserialize(message);

        bool has_result = static_cast<bool>(result);

        REQUIRE(has_result);
        REQUIRE((*result) == request);
    }

    SECTION("invalid class id")
    {
        for (std::uint8_t c = 0; c != 4; ++c)
        {
            if (c != class_id)
            {
                message.first.class_id = c;

                auto result = new_dexterity::ndx_protocol::deserializer<Request>::deserialize(message);

                bool has_result = static_cast<bool>(result);

                REQUIRE_FALSE(has_result);
            }
        }
    }

    SECTION("invalid msg id")
    {
        for (std::uint8_t m = 0; m != 64; ++m)
        {
            if (m != msg_id)
            {
                message.first.msg_id = m;

                auto result = new_dexterity::ndx_protocol::deserializer<Request>::deserialize(message);

                bool has_result = static_cast<bool>(result);

                REQUIRE_FALSE(has_result);
            }
        }
    }
}


TEST_CASE("hand id request")
{
    auto request = new_dexterity::ndx_protocol::hand_id_request{};
    test(101, request, MSG_CLASS, ID_ID);
}

TEST_CASE("actuators info request")
{
    auto request = new_dexterity::ndx_protocol::actuator_info_request{};

    test(101, request, MSG_CLASS, ACT_ID);
}

TEST_CASE("command actuators request")
{
    auto command = new_dexterity::ndx_protocol::actuator_command {100, 200, 300};
    auto commands = new_dexterity::ndx_protocol::actuator_commands {command, command, command, command, command};

    auto request = new_dexterity::ndx_protocol::command_actuators_request{commands};

    test(101, request, COM_CLASS, ACOM_ID);
}

TEST_CASE("hand status request")
{
    auto request = new_dexterity::ndx_protocol::hand_status_request {};

    test(101, request, MSG_CLASS, STH_ID);
}

TEST_CASE("communication status request")
{
    auto request = new_dexterity::ndx_protocol::communication_status_request {};

    test(101, request, MSG_CLASS, STC_ID);
}


TEST_CASE("actuators status request")
{
    auto request = new_dexterity::ndx_protocol::actuators_status_request { {true, true, true, true, true} };

    test(101, request, MSG_CLASS, STA_ID);
}

TEST_CASE("software calibration request")
{
    auto request = new_dexterity::ndx_protocol::software_calibration_request {};

    test(101, request, CFG_CLASS, CAL_ID);
}

TEST_CASE("hardware calibration request")
{
    auto request = new_dexterity::ndx_protocol::hardware_calibration_request {};

    test(101, request, CFG_CLASS, CAL_ID);
}

TEST_CASE("read config request")
{
    auto request = new_dexterity::ndx_protocol::read_config_request<123, 234>{};

    test(101, request, CFG_CLASS, UCF_ID);
}

TEST_CASE("write config request")
{
    auto request = new_dexterity::ndx_protocol::write_config_request<123, 12>{ true, {}};

    test(101, request, CFG_CLASS, UCF_ID);
}

#include <catch2/catch.hpp>
#include <new_dexterity/new_dexterity.hpp>
#include <new_dexterity/ndx_protocol.hpp>
#include <boost/variant.hpp>
#include <boost/optional.hpp>
#include <new_dexterity/metaprogramming.hpp>
#include <algorithm>
#include <array>
#include <tuple>

TEST_CASE("Actuator Info Request Serializer", "[communication interface][serializers][actuator info request]")
{
    const std::size_t hand_ids[3] = {0, 254, 100};
    for (std::size_t i = 0; i != 3; ++i)
    {
        CAPTURE(i);
        std::vector<std::uint8_t> buffer;
        new_dexterity::ndx_protocol::serialize(hand_ids[i], buffer, new_dexterity::ndx_protocol::actuator_info_request{});
        REQUIRE(buffer.size() == 8);

        NDHeader header;
        std::memcpy(&header, buffer.data(), sizeof(header));

        REQUIRE(header.id == hand_ids[i]);
        REQUIRE(validate_header_checksum(header));
        REQUIRE(header.header == NDHEAD);
        REQUIRE(header.class_id == MSG_CLASS);
        REQUIRE(header.msg_id == ACT_ID);
        REQUIRE(header.payload_len == 0);
    }
}

TEST_CASE("Command Actuators Request Serializer", "[communication interface][serializers][command actuators request]")
{
    const std::size_t hand_ids[2] = {0, 100};
    const new_dexterity::ndx_protocol::command_actuators_request requests[2] = {
        new_dexterity::ndx_protocol::command_actuators_request { 
            new_dexterity::ndx_protocol::actuator_commands { 
                new_dexterity::ndx_protocol::actuator_command {0, 1, 0}, 
                new_dexterity::ndx_protocol::actuator_command {10, 20, 30}, 
                new_dexterity::ndx_protocol::actuator_command {30, 20, 10}, 
                new_dexterity::ndx_protocol::actuator_command {100, 300, 200}, 
                new_dexterity::ndx_protocol::actuator_command {50, 10, 400} } },

        new_dexterity::ndx_protocol::command_actuators_request { 
            new_dexterity::ndx_protocol::actuator_commands { 
                new_dexterity::ndx_protocol::actuator_command {30, 200, 400}, 
                new_dexterity::ndx_protocol::actuator_command {50, 400, 200}, 
                new_dexterity::ndx_protocol::actuator_command {10, 200, 10}, 
                new_dexterity::ndx_protocol::actuator_command {70, 400, 100}, 
                new_dexterity::ndx_protocol::actuator_command {40, 200, 300} } } };

    for (std::size_t i = 0; i != 2; ++i)
    {
        CAPTURE(i);

        std::vector<std::uint8_t> buffer;
        new_dexterity::ndx_protocol::serialize(hand_ids[i], buffer, requests[i]);

        std::uint16_t payload_len = sizeof(ACT_header) + 5 * sizeof(ACOMx_struct);

        REQUIRE(buffer.size() == 8 + payload_len + 2);

        NDHeader header;
        std::memcpy(&header, buffer.data(), sizeof(header));

        REQUIRE(validate_header_checksum(header));
        REQUIRE(header.header == NDHEAD);
        REQUIRE(header.class_id == COM_CLASS);
        REQUIRE(header.msg_id == ACOM_ID);
        REQUIRE(header.id == hand_ids[i]);
        REQUIRE(header.payload_len == payload_len);
        REQUIRE(validate_checksum(buffer.data()));

        ACT_header aheader;
        std::memcpy(&aheader, buffer.data() + sizeof(header), sizeof(aheader));

        REQUIRE(aheader.num_actuators == 5);
        
        ACOMx_struct c1, c2, c3, c4, c5;

        std::memcpy(&c1, buffer.data() + sizeof(header) + sizeof(aheader) + 0 * sizeof(ACOMx_struct), sizeof(ACOMx_struct));
        std::memcpy(&c2, buffer.data() + sizeof(header) + sizeof(aheader) + 1 * sizeof(ACOMx_struct), sizeof(ACOMx_struct));
        std::memcpy(&c3, buffer.data() + sizeof(header) + sizeof(aheader) + 2 * sizeof(ACOMx_struct), sizeof(ACOMx_struct));
        std::memcpy(&c4, buffer.data() + sizeof(header) + sizeof(aheader) + 3 * sizeof(ACOMx_struct), sizeof(ACOMx_struct));
        std::memcpy(&c5, buffer.data() + sizeof(header) + sizeof(aheader) + 4 * sizeof(ACOMx_struct), sizeof(ACOMx_struct));

        auto commands = requests[i].commands;

        REQUIRE(static_cast<bool>(commands.thumb_actuator_command));
        REQUIRE(static_cast<bool>(commands.index_actuator_command));
        REQUIRE(static_cast<bool>(commands.middle_actuator_command));
        REQUIRE(static_cast<bool>(commands.ring_pinky_actuator_command));
        REQUIRE(static_cast<bool>(commands.thumb_opposition_actuator_command));

        REQUIRE(c1.position == commands.thumb_actuator_command->position);
        REQUIRE(c1.velocity == commands.thumb_actuator_command->velocity);
        REQUIRE(c1.torque == commands.thumb_actuator_command->torque);

        REQUIRE(c2.position == commands.index_actuator_command->position);
        REQUIRE(c2.velocity == commands.index_actuator_command->velocity);
        REQUIRE(c2.torque == commands.index_actuator_command->torque);

        REQUIRE(c3.position == commands.middle_actuator_command->position);
        REQUIRE(c3.velocity == commands.middle_actuator_command->velocity);
        REQUIRE(c3.torque == commands.middle_actuator_command->torque);

        REQUIRE(c4.position == commands.ring_pinky_actuator_command->position);
        REQUIRE(c4.velocity == commands.ring_pinky_actuator_command->velocity);
        REQUIRE(c4.torque == commands.ring_pinky_actuator_command->torque);

        REQUIRE(c5.position == commands.thumb_opposition_actuator_command->position);
        REQUIRE(c5.velocity == commands.thumb_opposition_actuator_command->velocity);
        REQUIRE(c5.torque == commands.thumb_opposition_actuator_command->torque);
    }
}

TEST_CASE("Hand Status Request Serializer", "[communication interface][serializers][hand status request]")
{
    std::size_t hand_ids[3] = {0, 254, 100};
    for (std::size_t i = 0; i != 3; ++i)
    {
        CAPTURE(i);
        std::vector<std::uint8_t> buffer;

        new_dexterity::ndx_protocol::serialize(hand_ids[i], buffer, new_dexterity::ndx_protocol::hand_status_request{});

        REQUIRE(buffer.size() == 8);

        NDHeader header;
        std::memcpy(&header, buffer.data(), sizeof(header));

        REQUIRE(header.id == hand_ids[i]);
        REQUIRE(validate_header_checksum(header));
        REQUIRE(header.header == NDHEAD);
        REQUIRE(header.class_id == MSG_CLASS);
        REQUIRE(header.msg_id == STH_ID);
        REQUIRE(header.payload_len == 0);
    }
}

TEST_CASE("Communication Status Request Serializer", "[communication interface][serializers][communication status request]")
{
    const std::size_t hand_ids[3] = {0, 254, 100};
    for (std::size_t i = 0; i != 3; ++i)
    {
        CAPTURE(i);
        std::vector<std::uint8_t> buffer;

        new_dexterity::ndx_protocol::serialize(hand_ids[i], buffer, new_dexterity::ndx_protocol::communication_status_request{});

        REQUIRE(buffer.size() == 8);

        NDHeader header;
        std::memcpy(&header, buffer.data(), sizeof(header));

        REQUIRE(header.id == hand_ids[i]);
        REQUIRE(validate_header_checksum(header));
        REQUIRE(header.header == NDHEAD);
        REQUIRE(header.class_id == MSG_CLASS);
        REQUIRE(header.msg_id == STC_ID);
        REQUIRE(header.payload_len == 0);
    }
}

TEST_CASE("Actuators Status Request Serializer", "[communication interface][serializers][actuators status request]")
{
    const std::size_t hand_ids[3] = {0, 254, 100};
    const new_dexterity::ndx_protocol::actuator_selection selections[3] = {
        new_dexterity::ndx_protocol::actuator_selection{true, false, false, true, false},
        new_dexterity::ndx_protocol::actuator_selection{false, true, false, true, false},
        new_dexterity::ndx_protocol::actuator_selection{true, true, false, false, true} };
    const std::uint16_t act_num[3] = {2, 2, 3};

    for (std::size_t i = 0; i != 3; ++i)
    {
        CAPTURE(i);
        std::vector<std::uint8_t> buffer;

        new_dexterity::ndx_protocol::serialize(hand_ids[i], buffer, new_dexterity::ndx_protocol::actuators_status_request{ selections[i] });

        std::uint16_t payload_len = 1 + act_num[i];

        REQUIRE(buffer.size() == 8 + payload_len + 2);

        NDHeader header;
        std::memcpy(&header, buffer.data(), sizeof(header));

        REQUIRE(header.id == hand_ids[i]);
        REQUIRE(validate_header_checksum(header));
        REQUIRE(header.header == NDHEAD);
        REQUIRE(header.class_id == MSG_CLASS);
        REQUIRE(header.msg_id == STA_ID);
        REQUIRE(header.payload_len == payload_len);
        REQUIRE(validate_checksum(buffer.data()));

        std::uint8_t num;

        std::memcpy(&num, buffer.data() + sizeof(header), sizeof(num));

        REQUIRE(num == act_num[i]);

        REQUIRE(num >= 0);
        REQUIRE(num <= 5);

        for (std::size_t j = 0; j != num; ++j)
        {
            std::uint8_t id;
            std::memcpy(&id, buffer.data() + sizeof(header) + 1 + j, sizeof(id));

            REQUIRE(id >= 1);
            REQUIRE(id <= 5);

            if (id == 1)
                REQUIRE(selections[i].thumb_actuator);
            if (id == 2)
                REQUIRE(selections[i].index_actuator);
            if (id == 3)
                REQUIRE(selections[i].middle_actuator);
            if (id == 4)
                REQUIRE(selections[i].ring_pinky_actuator);
            if (id == 5)
                REQUIRE(selections[i].thumb_opposition_actuator);
        }
    }
}

TEST_CASE("Software Calibration Serializer", "[communication interface][serializers][software calibration request]")
{
    const std::size_t hand_ids[3] = {0, 254, 100};
    for (std::size_t i = 0; i != 3; ++i)
    {
        CAPTURE(i);
        std::vector<std::uint8_t> buffer;
        new_dexterity::ndx_protocol::serialize(hand_ids[i], buffer, new_dexterity::ndx_protocol::software_calibration_request{});

        REQUIRE(buffer.size() == 8 + 1 + 2);

        NDHeader header;
        std::memcpy(&header, buffer.data(), sizeof(header));

        REQUIRE(header.id == hand_ids[i]);
        REQUIRE(validate_header_checksum(header));
        REQUIRE(header.header == NDHEAD);
        REQUIRE(header.class_id == CFG_CLASS);
        REQUIRE(header.msg_id == CAL_ID);
        REQUIRE(header.payload_len == 1);
        REQUIRE(validate_checksum(buffer.data()));

        std::uint8_t id;
        std::memcpy(&id, buffer.data() + sizeof(header), sizeof(id));

        REQUIRE(id == 2);
    }
}

TEST_CASE("Hardware Calibration Serializer", "[communication interface][serializers][hardware calibration request]")
{
    const std::size_t hand_ids[3] = {0, 254, 100};
    for (std::size_t i = 0; i != 3; ++i)
    {
        CAPTURE(i);
        std::vector<std::uint8_t> buffer;
        new_dexterity::ndx_protocol::serialize(hand_ids[i], buffer, new_dexterity::ndx_protocol::hardware_calibration_request{});

        REQUIRE(buffer.size() == 8 + 1 + 2);

        NDHeader header;
        std::memcpy(&header, buffer.data(), sizeof(header));

        REQUIRE(header.id == hand_ids[i]);
        REQUIRE(validate_header_checksum(header));
        REQUIRE(header.header == NDHEAD);
        REQUIRE(header.class_id == CFG_CLASS);
        REQUIRE(header.msg_id == CAL_ID);
        REQUIRE(header.payload_len == 1);
        REQUIRE(validate_checksum(buffer.data()));

        std::uint8_t id;
        std::memcpy(&id, buffer.data() + sizeof(header), sizeof(id));

        REQUIRE(id == 1);
    }
}

struct test_read_config
{
    template <std::size_t I, std::size_t Id, std::size_t Address, std::size_t Length>
    void operator()(const std::tuple<std::integral_constant<std::size_t, I> , std::integral_constant<std::size_t, Id>, std::integral_constant<std::size_t, Address>, std::integral_constant<std::size_t, Length>>& tup)
    {
        CAPTURE(I);
        std::vector<std::uint8_t> buffer;
        new_dexterity::ndx_protocol::serialize(Id, buffer, 
                new_dexterity::ndx_protocol::read_config_request<Address, Length>{});

        const std::uint16_t payload_len = 3;
        REQUIRE(buffer.size() == 8 + payload_len + 2);

        NDHeader header;
        std::memcpy(&header, buffer.data(), sizeof(header));

        REQUIRE(header.id == Id);
        REQUIRE(validate_header_checksum(header));
        REQUIRE(header.header == NDHEAD);
        REQUIRE(header.class_id == CFG_CLASS);
        REQUIRE(header.msg_id == UCF_ID);
        REQUIRE(header.payload_len == payload_len);

        std::uint8_t rws;
        std::memcpy(&rws, buffer.data() + sizeof(header), sizeof(rws));

        REQUIRE(rws == 0);

        std::uint8_t addr;
        std::uint8_t addr_len;
        std::memcpy(&addr, buffer.data() + sizeof(header) + sizeof(rws), sizeof(addr));
        std::memcpy(&addr_len, 
                buffer.data() + sizeof(header) + sizeof(rws) + sizeof(addr), sizeof(addr_len));

        REQUIRE(addr == Address);
        REQUIRE(addr_len == Length);

    }
};

TEST_CASE("Read Config Serializer", "[communication interface][serializers][read config request]")
{
    std::tuple<
        std::tuple<std::integral_constant<std::size_t, 0>, 
                   std::integral_constant<std::size_t, 0>, 
                   std::integral_constant<std::size_t, 0>, 
                   std::integral_constant<std::size_t, 57>>,
        std::tuple<std::integral_constant<std::size_t, 1>, 
                   std::integral_constant<std::size_t, 254>, 
                   std::integral_constant<std::size_t, 9>, 
                   std::integral_constant<std::size_t, 48>>,
        std::tuple<std::integral_constant<std::size_t, 2>, 
                   std::integral_constant<std::size_t, 100>, 
                   std::integral_constant<std::size_t, 3>, 
                   std::integral_constant<std::size_t, 1>>> cases;

    new_dexterity::for_each(cases, test_read_config{});

}

struct test_write_config
{
    template <std::size_t I, std::size_t Id, std::size_t Address, bool Save, std::size_t Length>
    void operator()(std::tuple<std::integral_constant<std::size_t, I>,
                               std::integral_constant<std::size_t, Id>,
                               std::integral_constant<bool, Save>,
                               std::integral_constant<std::size_t, Address>,
                               std::integral_constant<std::size_t, Length>,
                               std::string> const & tup)
    {
        CAPTURE(I);
        auto const & give = std::get<5>(tup);

        std::vector<std::uint8_t> buffer;

        REQUIRE(give.size() == Length);

        std::array<std::uint8_t, Length> data;

        std::copy_n(give.begin(), Length, data.begin());

        new_dexterity::ndx_protocol::serialize(Id, buffer, 
                new_dexterity::ndx_protocol::write_config_request<Address, Length>{ Save, data });

        const std::uint16_t payload_len = 3 + Length;
        REQUIRE(buffer.size() == 8 + payload_len + 2);

        NDHeader header;
        std::memcpy(&header, buffer.data(), sizeof(header));

        REQUIRE(header.id == Id);
        REQUIRE(validate_header_checksum(header));
        REQUIRE(header.header == NDHEAD);
        REQUIRE(header.class_id == CFG_CLASS);
        REQUIRE(header.msg_id == UCF_ID);
        REQUIRE(header.payload_len == payload_len);

        std::uint8_t rws;
        std::memcpy(&rws, buffer.data() + sizeof(header), sizeof(rws));

        REQUIRE(rws == (Save ? 2 : 1));

        std::uint8_t addr;
        std::uint8_t addr_len;
        std::memcpy(&addr, buffer.data() + sizeof(header) + sizeof(rws), sizeof(addr));
        std::memcpy(&addr_len, 
                buffer.data() + sizeof(header) + sizeof(rws) + sizeof(addr), sizeof(addr_len));

        REQUIRE(addr == Address);
        REQUIRE(addr_len == Length);

        auto start = buffer.data() + sizeof(header) + sizeof(rws) + sizeof(addr) + sizeof(addr_len);
        auto end = start + addr_len;

        REQUIRE(std::equal(start, end, data.begin()));

    }

};

TEST_CASE("Write Config Serializer", "[communication interface][serializers][write config request]")
{
// TODO: Update test cases

    /*
std::tuple<
    std::tuple<std::integral_constant<std::size_t, 0>,
               std::integral_constant<std::size_t, 0>,
               std::integral_constant<bool, true>,
               std::integral_constant<std::size_t, 0>,
               std::integral_constant<std::size_t, 57>,
               std::string>,
    std::tuple<std::integral_constant<std::size_t, 1>,
               std::integral_constant<std::size_t, 254>,
               std::integral_constant<bool, false>,
               std::integral_constant<std::size_t, 9>,
               std::integral_constant<std::size_t, 48>,
               std::string>,
    std::tuple<std::integral_constant<std::size_t, 2>,
               std::integral_constant<std::size_t, 100>,
               std::integral_constant<bool, true>,
               std::integral_constant<std::size_t, 3>,
               std::integral_constant<std::size_t, 1>,
               std::string>> cases;

std::get<0>(std::get<0>(cases)) = {};
std::get<1>(std::get<0>(cases)) = {};
std::get<2>(std::get<0>(cases)) = {};
std::get<3>(std::get<0>(cases)) = {};
std::get<4>(std::get<0>(cases)) = {};
std::get<5>(std::get<0>(cases)) = "123456789012345678901234567890123456789012345678901234567";

std::get<0>(std::get<1>(cases)) = {};
std::get<1>(std::get<1>(cases)) = {};
std::get<2>(std::get<1>(cases)) = {};
std::get<3>(std::get<1>(cases)) = {};
std::get<4>(std::get<1>(cases)) = {};
std::get<5>(std::get<1>(cases)) = "123456789012345678901234567890123456789012345678";

std::get<0>(std::get<2>(cases)) = {};
std::get<1>(std::get<2>(cases)) = {};
std::get<2>(std::get<2>(cases)) = {};
std::get<3>(std::get<2>(cases)) = {};
std::get<4>(std::get<2>(cases)) = {};
std::get<5>(std::get<2>(cases)) = "1";


new_dexterity::for_each(cases, test_write_config{});


const new_dexterity::ndx_protocol::eeprom_address addresses[3] = { { 0, 57}, {9, 48}, { 3, 1 } };
const bool saves[3] = {true, false, true};
const std::string datas[3] = { "123456789012345678901234567890123456789012345678901234567", "123456789012345678901234567890123456789012345678", "1" };

for (std::size_t i = 0; i != 3; ++i)
{
    CAPTURE(i);
    std::vector<std::uint8_t> buffer;
    new_dexterity::ndx_protocol::serialize(hand_ids[i], buffer, 
            new_dexterity::ndx_protocol::write_config_request{ saves[i], addresses[i] , datas[i] });

    const std::uint16_t payload_len = 3 + datas[i].size();
    REQUIRE(buffer.size() == 8 + payload_len + 2);

    NDHeader header;
    std::memcpy(&header, buffer.data(), sizeof(header));

    REQUIRE(header.id == hand_ids[i]);
    REQUIRE(validate_header_checksum(header));
    REQUIRE(header.header == NDHEAD);
    REQUIRE(header.class_id == CFG_CLASS);
    REQUIRE(header.msg_id == UCF_ID);
    REQUIRE(header.payload_len == payload_len);

    std::uint8_t rws;
    std::memcpy(&rws, buffer.data() + sizeof(header), sizeof(rws));

    REQUIRE(rws == (saves[i] ? 2 : 1));

    std::uint8_t addr;
    std::uint8_t addr_len;
    std::memcpy(&addr, buffer.data() + sizeof(header) + sizeof(rws), sizeof(addr));
    std::memcpy(&addr_len, 
            buffer.data() + sizeof(header) + sizeof(rws) + sizeof(addr), sizeof(addr_len));

    REQUIRE(addr == addresses[i].index);
    REQUIRE(addr_len == addresses[i].length);

    auto start = buffer.data() + sizeof(header) + sizeof(rws) + sizeof(addr) + sizeof(addr_len);
    auto end = start + addr_len;

    std::string data(start, end);

    REQUIRE(data == datas[i]);
}
*/

}

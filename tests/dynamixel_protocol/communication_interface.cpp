#include <catch2/catch.hpp>
#include <new_dexterity/dynamixel_protocol.hpp>
#include <new_dexterity/pipe.hpp>
#include <boost/asio.hpp>
#include <boost/variant.hpp>
#include <boost/optional.hpp>
#include <new_dexterity/dynamixel_protocol/requests.hpp>
#include <new_dexterity/dynamixel_protocol/fake_dynamixel_driver.hpp>
#include <new_dexterity/pipe.hpp>
#include <thread>
#include <random>
#include <iostream>

namespace dynx = new_dexterity::dynamixel_protocol;
namespace handles = dynx::memory_handles;

template <class D>
struct random_generator
{
public:
    random_generator()
        : device_()
        , engine_(device_())
        , distribution_()
    {
    }

    std::uint8_t operator()()
    {
        return static_cast<std::uint8_t>(distribution_(engine_));
    }

private:
    std::random_device device_;
    std::default_random_engine engine_;
    std::uniform_int_distribution<D> distribution_;
};

template <class Container>
void print_bytes(Container& container)
{
    for (auto& b : container)
    {
        std::cout << std::hex << (int)b << " ";
    }
    std::cout << std::endl;
}

template <typename F>
static void test(F test_fun)
{
    asio::io_service io_service;

    new_dexterity::pipe_endpoint endp1(io_service);
    new_dexterity::pipe_endpoint endp2(io_service);

    new_dexterity::create_pipe(endp1, endp2);

    new_dexterity::pipe_endpoint endp3(io_service);
    new_dexterity::pipe_endpoint endp4(io_service);

    new_dexterity::create_pipe(endp3, endp4);

    auto& connection_input = endp1;
    auto& connection_output = endp4;

    auto& hand_input = endp3;
    auto& hand_output = endp2;

    dynx::in_connection in(connection_input);
    dynx::out_connection out(connection_output);

    dynx::communication_interface interface(in, out, 1);

    dynx::in_connection fake_in(hand_input);
    dynx::out_connection fake_out(hand_output);

    dynx::fake_dynamixel_driver fake(fake_in, fake_out, 1);

    auto runner = std::async(std::launch::async, [&]{ io_service.run(); });
        
    test_fun(interface, fake);

    io_service.stop();
}

void test_valid_ping(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto result = interface.send_request(dynx::ping_request{}, std::chrono::milliseconds(5000));

    bool has_response = static_cast<bool>(result);

    REQUIRE(has_response);

    auto response = *result;

    bool has_info = response.result.type() == typeid(dynx::ping_info);

    REQUIRE(has_info);

    REQUIRE(fake.successfully_handled_ping);
}

template <std::uint16_t Index, std::uint16_t Length>
void test_valid_dynamic_read_request(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto mresponse = interface.send_request(dynx::dynamic_read_memory_request{ Index, Length }, std::chrono::milliseconds(5000));

    bool has_response = static_cast<bool>(mresponse);

    REQUIRE(has_response);

    auto response = *mresponse;

    std::vector<std::uint8_t> * ptr = boost::get<std::vector<std::uint8_t>>(&response.result);

    REQUIRE(ptr != nullptr);

    auto& vec = * ptr;

    REQUIRE(vec.size() == Length);

    REQUIRE(fake.successfully_handled_read);
}

template <std::size_t Index, std::size_t Length>
void test_valid_read_request(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto mresponse = interface.send_request(dynx::read_memory_request<Index, Length>{}, std::chrono::milliseconds(5000));

    bool has_response = static_cast<bool>(mresponse);

    REQUIRE(has_response);

    auto response = *mresponse;

    bool got_content = response.result.type() == typeid(std::array<std::uint8_t, Length>);

    REQUIRE(got_content);

    REQUIRE(fake.successfully_handled_read);
}

template <std::size_t Index, std::size_t Length>
void test_rom_without_torque_enable_is_rejected(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto mresponse = interface.send_request(dynx::read_memory_request<Index, Length>{}, std::chrono::milliseconds(5000));

    bool has_response = static_cast<bool>(mresponse);

    REQUIRE(has_response);

    auto response = *mresponse;

    auto * instruction_error = boost::get<dynx::instruction_error>(&response.result);

    REQUIRE(instruction_error != nullptr);

    CAPTURE(*instruction_error);
    REQUIRE(*instruction_error == dynx::instruction_error::access_error);

    REQUIRE(!fake.successfully_handled_read);
}

void test_set_torque_enable_is_acked(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto result = interface.send_request(dynx::write_memory_request<64, 1>{ {1} }, std::chrono::milliseconds(5000));

    bool has_response = static_cast<bool>(result);

    REQUIRE(has_response);

    auto response = *result;

    bool got_acked = response.result.type() == typeid(dynx::instruction_acknowledged);

    REQUIRE(got_acked);
    REQUIRE(fake.successfully_handled_write);
}

template <std::size_t Index, std::size_t Length>
void test_rom_with_torque_enable_is_accepted(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    test_set_torque_enable_is_acked(interface, fake);

    auto mresponse = interface.send_request(dynx::read_memory_request<Index, Length>{}, std::chrono::milliseconds(5000));

    bool has_response = static_cast<bool>(mresponse);

    REQUIRE(has_response);

    auto response = *mresponse;

    bool got_content = response.result.type() == typeid(std::array<std::uint8_t, Length>);

    REQUIRE(got_content);
    REQUIRE(fake.successfully_handled_write);
}

template <std::size_t Index, std::size_t Length>
void test_valid_dynamic_write_request(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    random_generator<std::uint16_t> gen; 

    std::vector<std::uint8_t> data(Length);

    auto result = interface.send_request(dynx::dynamic_write_memory_request{ static_cast<std::uint16_t>(Index), std::move(data) }, std::chrono::milliseconds(5000));

    bool has_response = static_cast<bool>(result);

    REQUIRE(has_response);

    auto response = *result;

    bool got_acked = response.result.type() == typeid(dynx::instruction_acknowledged);

    REQUIRE(got_acked);
    REQUIRE(fake.successfully_handled_write);
}


template <std::size_t Index, std::size_t Length>
void test_valid_write_request(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto result = interface.send_request(dynx::write_memory_request<Index, Length>{}, std::chrono::milliseconds(5000));

    bool has_response = static_cast<bool>(result);

    REQUIRE(has_response);

    auto response = *result;

    bool got_acked = response.result.type() == typeid(dynx::instruction_acknowledged);

    REQUIRE(got_acked);
    REQUIRE(fake.successfully_handled_write);
}

template <std::size_t Index, std::size_t Length>
void test_valid_reg_write_request(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto result = interface.send_request(dynx::reg_write_request<Index, Length>{}, std::chrono::milliseconds(5000));

    bool has_response = static_cast<bool>(result);

    REQUIRE(has_response);

    auto response = *result;

    bool got_acked = response.result.type() == typeid(dynx::instruction_acknowledged);

    REQUIRE(got_acked);
    REQUIRE(fake.successfully_handled_reg_write);
}

template <std::size_t Index, std::size_t Length>
void test_valid_dynamic_reg_write_request(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto result = interface.send_request(dynx::dynamic_reg_write_request{ static_cast<std::uint16_t>(Index), std::vector<std::uint8_t>(Length) }, std::chrono::milliseconds(5000));

    bool has_response = static_cast<bool>(result);

    REQUIRE(has_response);

    auto response = *result;

    bool got_acked = response.result.type() == typeid(dynx::instruction_acknowledged);

    REQUIRE(got_acked);
    REQUIRE(fake.successfully_handled_reg_write);
}

template <std::size_t Index, std::size_t Length>
void test_valid_action_request(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    test_valid_reg_write_request<Index, Length>(interface, fake);

    auto result = interface.send_request(dynx::action_request{}, std::chrono::milliseconds(5000));

    bool has_response = static_cast<bool>(result);

    REQUIRE(has_response);

    auto response = *result;

    bool got_acked = response.result.type() == typeid(dynx::instruction_acknowledged);

    REQUIRE(got_acked);
    REQUIRE(fake.successfully_handled_write);
}

template <std::size_t Index, std::size_t Length>
void test_valid_write_read_is_identity(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{

    random_generator<std::uint16_t> gen;

    std::array<std::uint8_t, Length> content;

    for (auto& b : content)
    {
        b = gen();
    }

    auto wresult = interface.send_request(dynx::write_memory_request<Index, Length>{ content }, std::chrono::milliseconds(5000));

    bool whas_response = static_cast<bool>(wresult);

    REQUIRE(whas_response);

    auto wresponse = *wresult;

    bool got_acked = wresponse.result.type() == typeid(dynx::instruction_acknowledged);

    REQUIRE(got_acked);

    auto rmresponse = interface.send_request(dynx::read_memory_request<Index, Length>{}, std::chrono::milliseconds(5000));

    bool rhas_response = static_cast<bool>(rmresponse);

    REQUIRE(rhas_response);

    auto rresponse = *rmresponse;

    auto * retrieved = boost::get<std::array<std::uint8_t, Length>>(&rresponse.result);

    REQUIRE(retrieved != nullptr);

    bool are_equal = std::equal(content.begin(), content.end(), retrieved->begin());

    REQUIRE(are_equal);

}

template <class... Ts>
void test_valid_get_memory_content_request(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto mresponse = interface.send_request(dynx::get_memory_content_request<Ts...>{}, std::chrono::milliseconds(5000));

    bool has_response = static_cast<bool>(mresponse);

    REQUIRE(has_response);

    auto response = *mresponse;

    bool got_content = response.result.type() == typeid(dynx::memory_content<Ts...>);

    REQUIRE(got_content);
    REQUIRE(fake.successfully_handled_read);
}

template <class... Ts>
void test_valid_set_memory_content_request(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    random_generator<std::uint16_t> gen;

    auto result = interface.send_request(dynx::make_set_memory_content_request(Ts{gen()}...), std::chrono::milliseconds(5000));

    bool has_response = static_cast<bool>(result);

    REQUIRE(has_response);

    auto response = *result;

    bool got_acked = response.result.type() == typeid(dynx::instruction_acknowledged);

    REQUIRE(got_acked);
    REQUIRE(fake.successfully_handled_write);
}

template <class Content>
bool test_equal(Content& content1, Content& content2)
{
    return true; 
}

template <class Content, class T, class... Ts>
bool test_equal(Content& content1, Content& content2)
{
    auto mem_ptr = dynx::info_of_t<T>::mem_ptr;
    typename dynx::info_of_t<T>::wrapper& r_slice1 = content1; 
    typename dynx::info_of_t<T>::wrapper& r_slice2 = content2; 

    return (r_slice1.*mem_ptr) == (r_slice2.*mem_ptr) && test_equal<Content, Ts...>(content1, content2);
}

template <class... Ts>
void test_valid_set_get_memory_content_is_identitiy(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    random_generator<std::uint16_t> gen;

    auto request = dynx::make_set_memory_content_request(Ts{ gen() }...);
    
    auto wresult = interface.send_request(request, std::chrono::milliseconds(5000));

    bool whas_response = static_cast<bool>(wresult);

    REQUIRE(whas_response);

    auto wresponse = *wresult;

    bool got_acked = wresponse.result.type() == typeid(dynx::instruction_acknowledged);

    REQUIRE(got_acked);

    auto rmresponse = interface.send_request(dynx::get_memory_content_request<Ts...>{}, std::chrono::milliseconds(5000));

    bool rhas_response = static_cast<bool>(rmresponse);

    auto rresponse = *rmresponse;

    REQUIRE(rhas_response);

    auto * content = boost::get<dynx::memory_content<Ts...>>(&rresponse.result);

    REQUIRE(content != nullptr);

    bool are_equal = test_equal<dynx::memory_content<Ts...>, Ts...>(request, *content);

    REQUIRE(are_equal);
}

void test_valid_factory_reset_request(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto result = interface.send_request(dynx::factory_reset_request::reset_all, std::chrono::milliseconds(5000));

    auto has_response = static_cast<bool>(result);

    REQUIRE(has_response);

    auto response = *result;

    bool got_acked = response.result.type() == typeid(dynx::instruction_acknowledged);

    REQUIRE(got_acked);
    REQUIRE(fake.successfully_handled_factory_reset);
}

void test_valid_reboot_request(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto result = interface.send_request(dynx::reboot_request{}, std::chrono::milliseconds(5000));

    auto has_response = static_cast<bool>(result);

    REQUIRE(has_response);

    auto response = *result;

    bool got_acked = response.result.type() == typeid(dynx::instruction_acknowledged);

    REQUIRE(got_acked);
    REQUIRE(fake.successfully_handled_reboot);
}

template <typename>
struct generate_read;

template <std::size_t Address, std::size_t Length>
struct generate_read<dynx::read_memory_request<Address, Length>>
{
    static dynx::read_memory_request<Address, Length> gen()
    {
        return {};
    }
};

template <typename... Ts>
struct generate_read<dynx::get_memory_content_request<Ts...>>
{
    static dynx::get_memory_content_request<Ts...> gen()
    {
        return {};
    }
};

template <>
struct generate_read<dynx::dynamic_read_memory_request>
{
    static dynx::dynamic_read_memory_request gen()
    {
        static uint8_t i = 0;

        i = (i + 1) % 3;

        if (i == 1)
            return dynx::dynamic_read_memory_request{ 64, 100 };
        else if (i == 2)
            return dynx::dynamic_read_memory_request{ 80, 50 };
        else
            return dynx::dynamic_read_memory_request{ 100, 89};
    }

};

template <>
struct generate_read<dynx::info_request>
{
    static dynx::info_request gen()
    {
        return {};
    }
};

template <std::size_t Address, std::size_t Length>
void test_valid_dynamic_sync_read_request_gets_response(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto& out = interface.out_connection();

    out.access([&](std::vector<std::uint8_t>& buffer)
    {
        dynx::serialize(buffer, dynx::dynamic_sync_read_request{ static_cast<std::uint16_t>(Address), static_cast<std::uint16_t>(Length), {1, 2, 3} });
    });

    auto fut = interface.wait_response_of<dynx::read_memory_request<Address, Length>>(std::chrono::milliseconds(5000));

    out.send_bytes();

    auto mresponse = fut.get();

    bool has_response = static_cast<bool>(mresponse);

    REQUIRE(has_response);

    auto response = *mresponse;

    bool got_content = response.result.type() == typeid(std::array<std::uint8_t, Length>);

    REQUIRE(got_content);
    REQUIRE(fake.successfully_handled_read);
}

template <class Read, class Content>
void test_valid_sync_read_request_gets_response(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto& out = interface.out_connection();

    out.access([&](std::vector<std::uint8_t>& buffer)
    {
        dynx::serialize(buffer, dynx::sync_read<Read>{ generate_read<Read>::gen(), {1, 2, 3} });
    });

    auto fut = interface.wait_response_of<Read>(std::chrono::milliseconds(5000));

    out.send_bytes();

    auto mresponse = fut.get();

    bool has_response = static_cast<bool>(mresponse);

    REQUIRE(has_response);

    auto response = *mresponse;

    bool got_content = response.result.type() == typeid(Content);

    REQUIRE(got_content);
    REQUIRE(fake.successfully_handled_read);
}

template <class Read, class Content>
void test_valid_bulk_read_request_gets_response(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto& out = interface.out_connection();

    out.access([&](std::vector<std::uint8_t>& buffer)
    {
        dynx::serialize(buffer, dynx::make_bulk_read_request(
                                    std::make_pair<std::uint8_t>(1, generate_read<Read>::gen()),
                                    std::make_pair<std::uint8_t>(2, generate_read<Read>::gen()),
                                    std::make_pair<std::uint8_t>(3, generate_read<Read>::gen())));
    });

    auto fut = interface.wait_response_of<Read>(std::chrono::milliseconds(5000));

    out.send_bytes();

    auto mresponse = fut.get();

    bool has_response = static_cast<bool>(mresponse);

    REQUIRE(has_response);

    auto response = *mresponse;

    bool got_content = response.result.type() == typeid(Content);

    REQUIRE(got_content);
    REQUIRE(fake.successfully_handled_read);
}

template <std::size_t Address, std::size_t Length>
void test_valid_dynamic_bulk_read_request_gets_response(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto& out = interface.out_connection();

    out.access([&](std::vector<std::uint8_t>& buffer)
    {
        dynx::serialize(buffer, dynx::dynamic_bulk_read_request{{ 
                std::make_tuple(std::uint8_t(1), std::uint16_t(Address), std::uint16_t(Length)),
                std::make_tuple(std::uint8_t(2), std::uint16_t(Address), std::uint16_t(Length)),
                std::make_tuple(std::uint8_t(3), std::uint16_t(Address), std::uint16_t(Length)),
                    }});

    });

    auto fut = interface.wait_response_of<dynx::read_memory_request<Address, Length>>(std::chrono::milliseconds(5000));

    out.send_bytes();

    auto mresponse = fut.get();

    bool has_response = static_cast<bool>(mresponse);

    REQUIRE(has_response);

    auto response = *mresponse;

    bool got_content = response.result.type() == typeid(std::array<std::uint8_t, Length>);

    REQUIRE(got_content);
    REQUIRE(fake.successfully_handled_read);
}

template <class Write>
void test_valid_sync_write_request(dynx::communication_interface& interface, Write const & write, dynx::fake_dynamixel_driver& fake)
{
    auto& out = interface.out_connection();

    out.access([&](std::vector<std::uint8_t>& buffer)
    {
        dynx::serialize(buffer, dynx::sync_write<Write>{ {{1, write}} });
    });

    out.send_bytes();

    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    REQUIRE(fake.successfully_handled_write);
}

template <std::size_t Address, std::size_t Length>
void test_valid_dynamic_sync_write_request(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto& out = interface.out_connection();

    out.access([&](std::vector<std::uint8_t>& buffer)
    {
        dynx::serialize(buffer, dynx::dynamic_sync_write_request{ static_cast<std::uint16_t>(Address), {{1, std::vector<std::uint8_t>(Length)}} });
    });

    out.send_bytes();

    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    REQUIRE(fake.successfully_handled_write);
}

template <class Write>
void test_valid_bulk_write_request(dynx::communication_interface& interface, Write const & write, dynx::fake_dynamixel_driver& fake)
{
    auto& out = interface.out_connection();
    out.access([&](std::vector<std::uint8_t>& buffer)
    {
        dynx::serialize(buffer, dynx::make_bulk_write_request(std::make_pair(1, write)));
    });

    out.send_bytes();

    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    REQUIRE(fake.successfully_handled_write);
}

template <std::size_t Address, std::size_t Length>
void test_valid_dynamic_bulk_write_request(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto& out = interface.out_connection();
    out.access([&](std::vector<std::uint8_t>& buffer)
    {
        dynx::serialize(buffer, dynx::dynamic_bulk_write_request{{ 
                std::make_tuple(std::uint8_t(1), std::uint16_t(Address), std::vector<std::uint8_t>(Length)),
                std::make_tuple(std::uint8_t(1), std::uint16_t(Address), std::vector<std::uint8_t>(Length)),
                std::make_tuple(std::uint8_t(1), std::uint16_t(Address), std::vector<std::uint8_t>(Length))
                }});
    });
    out.send_bytes();

    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    REQUIRE(fake.successfully_handled_write);
}

template <bool Enable>
void test_set_torque_enable_request(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto result = interface.send_request(dynx::make_set_memory_content_request(handles::torque_enable{ Enable }), std::chrono::milliseconds(1000));

    bool has_response = static_cast<bool>(result);

    REQUIRE(has_response);

    auto response = *result;

    bool got_acked = response.result.type() == typeid(dynx::instruction_acknowledged);

    REQUIRE(got_acked);
    REQUIRE(fake.successfully_handled_write);
}

void test_info_request(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto result = interface.send_request(dynx::info_request{}, std::chrono::milliseconds(5000));

    bool has_response = static_cast<bool>(result);

    REQUIRE(has_response);

    auto response = *result;

    auto * ptr = boost::get<dynx::info>(&response.result);

    REQUIRE(ptr != nullptr);

    REQUIRE(fake.successfully_handled_read);
}

template <std::size_t Position, std::size_t VelocityLimit, std::size_t CurrentLimit>
void test_command_request(dynx::communication_interface& interface, dynx::fake_dynamixel_driver& fake)
{
    auto result = interface.send_request(dynx::command_request { static_cast<std::int32_t>(Position), 
                                                                 static_cast<std::uint32_t>(VelocityLimit), 
                                                                 static_cast<std::uint32_t>(CurrentLimit) },  std::chrono::milliseconds(1000));

    bool has_response = static_cast<bool>(result);

    REQUIRE(has_response);

    auto response = *result;

    bool got_acked = response.result.type() == typeid(dynx::instruction_acknowledged);

    REQUIRE(got_acked);
    REQUIRE(fake.successfully_handled_write);
}

TEST_CASE("valid ping request works gets ping info", "[dynamixels]")
{
    test(test_valid_ping);
}

TEST_CASE("valid dynamic read memory request gets raw memory", "[dynamixels]")
{
    test(test_valid_dynamic_read_request<64, 100>);
    test(test_valid_dynamic_read_request<80, 50>);
    test(test_valid_dynamic_read_request<100, 89>);
}

TEST_CASE("valid read memory request gets raw memory", "[dynamixels]")
{
    test(test_valid_read_request<64, 100>);
    test(test_valid_read_request<80, 50>);
    test(test_valid_read_request<100, 89>);
}

TEST_CASE("read ROM without control enable unset is rejected", "[dynamixels]")
{
    test(test_rom_without_torque_enable_is_rejected<10, 50>);
    test(test_rom_without_torque_enable_is_rejected<30, 80>);
    test(test_rom_without_torque_enable_is_rejected<20, 10>);
}

TEST_CASE("read ROM with control enable set is accepted", "[dynamixels]")
{
    test(test_rom_with_torque_enable_is_accepted<10, 50>);
    test(test_rom_with_torque_enable_is_accepted<30, 80>);
    test(test_rom_with_torque_enable_is_accepted<20, 10>);
}

TEST_CASE("valid dynamic write memory request gets acked", "[dynamixels]")
{
    test(test_valid_dynamic_write_request<76, 10>);
    test(test_valid_dynamic_write_request<86, 20>);
    test(test_valid_dynamic_write_request<110, 6>);
}

TEST_CASE("valid write memory request gets acked", "[dynamixels]")
{
    test(test_valid_write_request<76, 10>);
    test(test_valid_write_request<86, 20>);
    test(test_valid_write_request<110, 6>);
}

TEST_CASE("valid register write memory request gets acked", "[dynamixels]")
{
    test(test_valid_reg_write_request<76, 10>);
    test(test_valid_reg_write_request<86, 20>);
    test(test_valid_reg_write_request<110, 6>);
}

TEST_CASE("valid dynamic register write memory request gets acked", "[dynamixels]")
{
    test(test_valid_dynamic_reg_write_request<76, 10>);
    test(test_valid_dynamic_reg_write_request<86, 20>);
    test(test_valid_dynamic_reg_write_request<110, 6>);
}

TEST_CASE("valid action request gets acked", "[dynamixels]")
{
    test(test_valid_action_request<76, 10>);
    test(test_valid_action_request<86, 20>);
    test(test_valid_action_request<110, 6>);
}

TEST_CASE("valid write-read is identity", "[dynamixels]")
{
    test(test_valid_write_read_is_identity<76, 10>);
    test(test_valid_write_read_is_identity<86, 20>);
    test(test_valid_write_read_is_identity<110, 6>);
}

TEST_CASE("valid get memory content request gets memory content", "[dynamixels]")
{
    test(test_valid_get_memory_content_request<handles::torque_enable, handles::led, handles::status_return_level, handles::registered_instruction>);
    test(test_valid_get_memory_content_request<handles::goal_current, handles::goal_velocity, handles::profile_acceleration, handles::profile_velocity>);
    test(test_valid_get_memory_content_request<handles::moving, handles::moving_status, handles::present_pwm, handles::present_current, handles::present_velocity>);
}

TEST_CASE("valid set memory content request gets acked", "[dynamixels]")
{
    test(test_valid_set_memory_content_request<handles::velocity_i_gain, handles::velocity_p_gain, handles::position_d_gain>);
    test(test_valid_set_memory_content_request<handles::feedforward_2nd_gain, handles::feedforward_1st_gain, handles::bus_watchdog>);
    test(test_valid_set_memory_content_request<handles::indirect_address_26, handles::indirect_address_27, handles::indirect_address_28, handles::indirect_data_1>);
}

TEST_CASE("valid set/get memory content is identity", "[dynamixels]")
{
    test(test_valid_set_get_memory_content_is_identitiy<handles::velocity_i_gain, handles::velocity_p_gain, handles::position_d_gain>);
    test(test_valid_set_get_memory_content_is_identitiy<handles::position_p_gain, handles::feedforward_2nd_gain, handles::feedforward_1st_gain, handles::bus_watchdog>);
    test(test_valid_set_get_memory_content_is_identitiy<handles::indirect_address_26, handles::indirect_address_27, handles::indirect_address_28, handles::indirect_data_1>);
}

TEST_CASE("valid factory reset request gets acked", "[dynamixels]")
{
    test(test_valid_factory_reset_request);
}

TEST_CASE("valid reboot request gets acked", "[dynamixels]")
{
    test(test_valid_reboot_request);
}

TEST_CASE("valid dynamic sync - read gets raw memory", "[dynamixels]")
{
    test(test_valid_dynamic_sync_read_request_gets_response<64, 100>);
    test(test_valid_dynamic_sync_read_request_gets_response<80, 50>);
    test(test_valid_dynamic_sync_read_request_gets_response<100, 89>);
}

TEST_CASE("valid sync - read memory request gets raw memory", "[dynamixels]")
{
    test(test_valid_sync_read_request_gets_response<dynx::read_memory_request<64, 100>, std::array<std::uint8_t, 100>>);
    test(test_valid_sync_read_request_gets_response<dynx::read_memory_request<80, 50>, std::array<std::uint8_t, 50>>);
    test(test_valid_sync_read_request_gets_response<dynx::read_memory_request<100, 89>, std::array<std::uint8_t, 89>>);
}

TEST_CASE("valid sync - dynamic read memory request gets raw memory", "[dynamixels]")
{
    //TODO: this is unacceptable! The addresses should not be implicit, I am just doing this because of the hard deadline but it must be rewritten!
    test(test_valid_sync_read_request_gets_response<dynx::dynamic_read_memory_request, std::vector<std::uint8_t>>);
    test(test_valid_sync_read_request_gets_response<dynx::dynamic_read_memory_request, std::vector<std::uint8_t>>);
    test(test_valid_sync_read_request_gets_response<dynx::dynamic_read_memory_request, std::vector<std::uint8_t>>);
}

TEST_CASE("valid sync - get memory content request gets memory content", "[dynamixels]")
{
    using req1 = dynx::get_memory_content_request<handles::torque_enable, handles::led, handles::status_return_level, handles::registered_instruction>;
    using cont1 = dynx::memory_content<handles::torque_enable, handles::led, handles::status_return_level, handles::registered_instruction>;

    test(test_valid_sync_read_request_gets_response<req1, cont1>);

    using req2 = dynx::get_memory_content_request<handles::goal_current, handles::goal_velocity, handles::profile_acceleration, handles::profile_velocity>;
    using cont2 = dynx::memory_content<handles::goal_current, handles::goal_velocity, handles::profile_acceleration, handles::profile_velocity>;

    test(test_valid_sync_read_request_gets_response<req2, cont2>);

    using req3 = dynx::get_memory_content_request<handles::moving, handles::moving_status, handles::present_pwm, handles::present_current, handles::present_velocity>;
    using cont3 = dynx::memory_content<handles::moving, handles::moving_status, handles::present_pwm, handles::present_current, handles::present_velocity>;

    test(test_valid_sync_read_request_gets_response<req3, cont3>);
}

TEST_CASE("valid sync - info request gets info", "[dynamixels]")
{
    test(test_valid_sync_read_request_gets_response<dynx::info_request, dynx::info>);
}

TEST_CASE("valid dynamic sync - write is handled", "[dynamixels]")
{
    test(test_valid_dynamic_sync_write_request<76, 10>);
    test(test_valid_dynamic_sync_write_request<86, 20>);
    test(test_valid_dynamic_sync_write_request<110, 6>);
}

TEST_CASE("valid sync - write memory request is handled", "[dynamixels]")
{
    auto req1 = dynx::write_memory_request<76, 10>{};
    test([&req1](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_sync_write_request(i, req1, fake); });
    auto req2 = dynx::write_memory_request<86, 20>{};
    test([&req2](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_sync_write_request(i, req2, fake); });
    auto req3 = dynx::write_memory_request<110, 6>{};
    test([&req3](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_sync_write_request(i, req3, fake); });
}

TEST_CASE("valid sync - dynamic write memory request is handled", "[dynamixels]")
{
    auto req1 = dynx::dynamic_write_memory_request{76, std::vector<std::uint8_t>(10)};
    test([&req1](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_sync_write_request(i, req1, fake); });
    auto req2 = dynx::dynamic_write_memory_request{86, std::vector<std::uint8_t>(20)};
    test([&req2](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_sync_write_request(i, req2, fake); });
    auto req3 = dynx::dynamic_write_memory_request{110, std::vector<std::uint8_t>(6)};
    test([&req3](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_sync_write_request(i, req3, fake); });
}

TEST_CASE("valid sync - set memory content request is handled", "[dynamixels]")
{
    random_generator<std::uint16_t> gen;

    auto req1 = dynx::make_set_memory_content_request(handles::velocity_i_gain{gen()}, handles::velocity_p_gain{gen()}, handles::position_d_gain{gen()});
    test([&req1](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_sync_write_request(i, req1, fake); });
    auto req2 = dynx::make_set_memory_content_request(handles::feedforward_2nd_gain{gen()}, handles::feedforward_1st_gain{gen()}, handles::bus_watchdog{gen()});
    test([&req2](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_sync_write_request(i, req2, fake); });
    auto req3 = dynx::make_set_memory_content_request(handles::indirect_address_26{gen()}, handles::indirect_address_27{gen()}, handles::indirect_address_28{gen()}, handles::indirect_data_1{gen()});
    test([&req3](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_sync_write_request(i, req3, fake); });
}

TEST_CASE("valid sync - command request is handled", "[dynamixels]")
{
    auto req1 = dynx::command_request{100, 200, 300};
    test([&req1](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_sync_write_request(i, req1, fake); });
    auto req2 = dynx::command_request{50, 100, 100};
    test([&req2](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_sync_write_request(i, req2, fake); });
    auto req3 = dynx::command_request{-60, 50, 300};
    test([&req3](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_sync_write_request(i, req3, fake); });
}

TEST_CASE("valid dynamic bulk - read request gets raw memory", "[dynamixels]")
{
    test(test_valid_dynamic_bulk_read_request_gets_response<64, 100>);
    test(test_valid_dynamic_bulk_read_request_gets_response<80, 50>);
    test(test_valid_dynamic_bulk_read_request_gets_response<100, 89>);
}

TEST_CASE("valid bulk - read memory request gets raw memory", "[dynamixels]")
{
    test(test_valid_bulk_read_request_gets_response<dynx::read_memory_request<64, 100>, std::array<std::uint8_t, 100>>);
    test(test_valid_bulk_read_request_gets_response<dynx::read_memory_request<80, 50>, std::array<std::uint8_t, 50>>);
    test(test_valid_bulk_read_request_gets_response<dynx::read_memory_request<100, 89>, std::array<std::uint8_t, 89>>);
}

TEST_CASE("valid bulk - dynamic read memory request gets raw memory", "[dynamixels]")
{
    test(test_valid_bulk_read_request_gets_response<dynx::dynamic_read_memory_request, std::vector<std::uint8_t>>);
    test(test_valid_bulk_read_request_gets_response<dynx::dynamic_read_memory_request, std::vector<std::uint8_t>>);
    test(test_valid_bulk_read_request_gets_response<dynx::dynamic_read_memory_request, std::vector<std::uint8_t>>);
}

TEST_CASE("valid bulk - get memory content request gets memory content", "[dynamixels]")
{
    using req1 = dynx::get_memory_content_request<handles::torque_enable, handles::led, handles::status_return_level, handles::registered_instruction>;
    using cont1 = dynx::memory_content<handles::torque_enable, handles::led, handles::status_return_level, handles::registered_instruction>;

    test(test_valid_bulk_read_request_gets_response<req1, cont1>);

    using req2 = dynx::get_memory_content_request<handles::goal_current, handles::goal_velocity, handles::profile_acceleration, handles::profile_velocity>;
    using cont2 = dynx::memory_content<handles::goal_current, handles::goal_velocity, handles::profile_acceleration, handles::profile_velocity>;

    test(test_valid_bulk_read_request_gets_response<req2, cont2>);

    using req3 = dynx::get_memory_content_request<handles::moving, handles::moving_status, handles::present_pwm, handles::present_current, handles::present_velocity>;
    using cont3 = dynx::memory_content<handles::moving, handles::moving_status, handles::present_pwm, handles::present_current, handles::present_velocity>;

    test(test_valid_bulk_read_request_gets_response<req3, cont3>);
}

TEST_CASE("valid bulk - info request gets info", "[dynamixels]")
{
    test(test_valid_bulk_read_request_gets_response<dynx::info_request, dynx::info>);
}

TEST_CASE("valid dynamic bulk - write memory request is handled", "[dynamixels]")
{
    test(test_valid_dynamic_bulk_write_request<76, 10>);
    test(test_valid_dynamic_bulk_write_request<86, 20>);
    test(test_valid_dynamic_bulk_write_request<110, 6>);
}

TEST_CASE("valid bulk - write memory request is handled", "[dynamixels]")
{
    auto req1 = dynx::write_memory_request<76, 10>{};
    test([&req1](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_bulk_write_request(i, req1, fake); });
    auto req2 = dynx::write_memory_request<86, 20>{};
    test([&req2](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_bulk_write_request(i, req2, fake); });
    auto req3 = dynx::write_memory_request<110, 6>{};
    test([&req3](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_bulk_write_request(i, req3, fake); });
}

TEST_CASE("valid bulk - dynamic write memory request is handled", "[dynamixels]")
{
    auto req1 = dynx::dynamic_write_memory_request{76, std::vector<std::uint8_t>(10)};
    test([&req1](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_bulk_write_request(i, req1, fake); });
    auto req2 = dynx::dynamic_write_memory_request{86, std::vector<std::uint8_t>(20)};
    test([&req2](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_bulk_write_request(i, req2, fake); });
    auto req3 = dynx::dynamic_write_memory_request{110, std::vector<std::uint8_t>(6)};
    test([&req3](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_bulk_write_request(i, req3, fake); });
}

TEST_CASE("valid bulk - set memory content request is handled", "[dynamixels]")
{
    random_generator<std::uint16_t> gen;

    auto req1 = dynx::make_set_memory_content_request(handles::velocity_i_gain{gen()}, handles::velocity_p_gain{gen()}, handles::position_d_gain{gen()});
    test([&req1](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_bulk_write_request(i, req1, fake); });
    auto req2 = dynx::make_set_memory_content_request(handles::feedforward_2nd_gain{gen()}, handles::feedforward_1st_gain{gen()}, handles::bus_watchdog{gen()});
    test([&req2](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_bulk_write_request(i, req2, fake); });
    auto req3 = dynx::make_set_memory_content_request(handles::indirect_address_26{gen()}, handles::indirect_address_27{gen()}, handles::indirect_address_28{gen()}, handles::indirect_data_1{gen()});
    test([&req3](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_bulk_write_request(i, req3, fake); });
}

TEST_CASE("valid bulk - command request is handled", "[dynamixels]")
{
    random_generator<std::uint16_t> gen;

    auto req1 = dynx::command_request{100, 200, 300};
    auto req2 = dynx::command_request{50, 100, 100};
    auto req3 = dynx::command_request{-60, 50, 300};

    test([&req1](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_bulk_write_request(i, req1, fake); });
    test([&req2](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_bulk_write_request(i, req2, fake); });
    test([&req3](dynx::communication_interface& i, dynx::fake_dynamixel_driver& fake) { test_valid_bulk_write_request(i, req3, fake); });
}


TEST_CASE("set torque enable request is handled", "[dynamixels]")
{
    test(test_set_torque_enable_request<false>);
    test(test_set_torque_enable_request<true>);
}

TEST_CASE("get actuator info is handled", "[dynamixels]")
{
    test(test_info_request);
}

TEST_CASE("command request is handled", "[dynamixels]")
{
    test(test_command_request<100, 100, 100>);
    test(test_command_request<50, 200, 50>);
    test(test_command_request<70, 150, 70>);
    test(test_command_request<20, 50, 150>);
    test(test_command_request<200, 40, 400>);
}


cmake_minimum_required(VERSION 3.4.0)

option(TEST_COVERAGE "Provides support for tests coverage using lcov" OFF)

if(UNIX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror -Wall -pedantic -Wno-maybe-uninitialized -Wno-unused-but-set-variable -Wno-unused-local-typedefs")
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g -O0")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3")
endif()

if (MSVC)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -D_SCL_SECURE_NO_WARNINGS")
endif()

if (TEST_COVERAGE)
    include(CodeCoverage)

    APPEND_COVERAGE_COMPILER_FLAGS()
endif()

add_executable(tests EXCLUDE_FROM_ALL
    testmain.cpp
    ndx_protocol/serialize_request.cpp
    ndx_protocol/serialize_response.cpp
    ndx_protocol/deserialize_request.cpp
    ndx_protocol/deserialize_response.cpp
    ndx_protocol/discoverer.cpp
    ndx_protocol/communication_interface.cpp
    dynamixel_protocol/communication_interface.cpp
    )

target_link_libraries(tests PRIVATE Threads::Threads NdApi::NdApi)

target_include_directories(tests 
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/include)

set_target_properties(tests
    PROPERTIES 
        CXX_STANDARD 11
        CXX_STANDARD_REQUIRED ON)

add_custom_target(run_tests 
    COMMAND $<TARGET_FILE:tests> -a
    DEPENDS tests
    )

if (TEST_COVERAGE)
SETUP_TARGET_FOR_COVERAGE_LCOV(
    NAME run_tests_coverage
    EXECUTABLE tests
    DEPENDENCIES tests
    LCOV_ARGS --no-external --directory ${PROJECT_SOURCE_DIR}/include 
)
endif()

add_custom_target(cppcheck
    COMMAND ${PROJECT_SOURCE_DIR}/scripts/cppcheck.sh ${CMAKE_BUILD_DIRECTORY}
    USES_TERMINAL
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
)

add_custom_target(valgrind
    COMMAND valgrind --track-origins=yes -v --error-exitcode=1 $<TARGET_FILE:tests>
    USES_TERMINAL
    DEPENDS tests
)


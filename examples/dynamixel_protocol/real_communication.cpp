//! @example
//! @brief Example of real single-dynamixel communication

#include <new_dexterity/dynamixel_protocol.hpp>
#include <new_dexterity/utilities.hpp>
#include <boost/asio.hpp>
#include <iostream>
#include <thread>

namespace dynx = new_dexterity::dynamixel_protocol;
namespace asio = boost::asio;

constexpr std::uint8_t device_id = 1;
constexpr auto baud_rate = 3000000;
constexpr auto device_name = "/dev/ttyUSB0";

int main()
{
    //Create executor

    asio::io_service io_service;

    //Create port object

    asio::serial_port port(io_service);

    //Configure port

    port.open(device_name);
    port.set_option(asio::serial_port::baud_rate(baud_rate));
    port.set_option(asio::serial_port::flow_control(asio::serial_port::flow_control::none));
    port.set_option(asio::serial_port::parity(asio::serial_port::parity::none));
    port.set_option(asio::serial_port::stop_bits(asio::serial_port::stop_bits::one));
    port.set_option(asio::serial_port::character_size(8));

    //Create connection objects

    dynx::in_connection in(port);
    dynx::out_connection out(port);

    //Create communication interface for dynamixel with the specified id.

    dynx::communication_interface interface(in, out, device_id);

    //Start runner

    auto runner = std::async(std::launch::async, [&]{ io_service.run(); });

    //Disable torque enable

    auto disable_request = dynx::make_set_memory_content_request(
                            dynx::memory_handles::torque_enable { false }
                          );

    auto mdisable_response = interface.send_request(disable_request, std::chrono::milliseconds(1000));

    //We check if we got a response
    if (mdisable_response)
    {
        // We retrieve it
        auto disable_response = *mdisable_response;

        // We check for any hardware error alerts

        if (disable_response.hardware_error_alert)
        {
            std::cout << "disable torque request got hardware error alert" << std::endl;
        }

        // response.result is a boost variant
        // the match utility is used for type-safe and convenient pattern matching on types.

        new_dexterity::match(disable_response.result,
                [](dynx::instruction_error error)
                {
                    std::cout << "disable torque request got error response: " << error << std::endl;
                },
                [](dynx::instruction_acknowledged)
                {
                    std::cout << "disable torque request got acknowledged" << std::endl;
                });
    }
    else
    {
        std::cout << "no disable torque response" << std::endl;
    }

    //Set operating mode to 5 - Current-based position control

    auto operating_mode_request = dynx::make_set_memory_content_request(
                                    dynx::memory_handles::operating_mode{ 5 }
                                  );

    auto mmode_response = interface.send_request(operating_mode_request, std::chrono::milliseconds(1000));

    if (mmode_response)
    {
        auto mode_response = *mmode_response;
        
        if (mode_response.hardware_error_alert)
        {
            std::cout << "operating mode request got hardware error alert" << std::endl;
        }

        new_dexterity::match(mode_response.result,
                [](dynx::instruction_error error)
                {
                    std::cout << "operating mode request got error response: " << error << std::endl;
                },
                [](dynx::instruction_acknowledged)
                {
                    std::cout << "operating mode request got acknowledged" << std::endl;
                });
    }
    else
    {
        std::cout << "no operating mode response" << std::endl;
    }


    //Set torque enable request

    auto enable_request = dynx::make_set_memory_content_request(
                            dynx::memory_handles::torque_enable { true }
                          );

    auto menable_response = interface.send_request(enable_request, std::chrono::milliseconds(1000));

    if (menable_response)
    {
        auto enable_response = *menable_response;

        if (enable_response.hardware_error_alert)
        {
            std::cout << "enable torque request got hardware error alert" << std::endl;
        }

        new_dexterity::match(enable_response.result,
                [](dynx::instruction_error error)
                {
                    std::cout << "enable torque request got error response: " << error << std::endl;
                },
                [](dynx::instruction_acknowledged)
                {
                    std::cout << "enable torque request got acknowledged" << std::endl;
                });
    }
    else
    {
        std::cout << "no enable torque response" << std::endl;
    }

    //Ping request

    dynx::ping_request ping_request;

    auto mping_response = interface.send_request(ping_request, std::chrono::milliseconds(1000));

    if (mping_response)
    {
        auto ping_response = *mping_response;

        if (ping_response.hardware_error_alert)
        {
            std::cout << "ping request got hardware error alert" << std::endl;
        }

        new_dexterity::match(ping_response.result,
                [](dynx::instruction_error error)
                {
                    std::cout << "ping request got error response: " << error << std::endl;
                },
                [](dynx::ping_info const & info)
                {
                    std::cout << "modelNumber: " << (int)info.model_number << 
                               ", firmwareVersion: " << (int)info.firmware_version << std::endl;
                });
    }
    else
    {
        std::cout << "no ping response" << std::endl;
    }

    for (int i = 0; i != 10; ++i)
    {

        //Command request

        dynx::command_request command_request;
        command_request.position = i * 50;
        command_request.velocity_limit = 200; 
        command_request.current_limit = 100;

        auto mcommand_response = interface.send_request(command_request, std::chrono::milliseconds(1000));

        if (mcommand_response)
        {
            auto command_response = *mcommand_response;

            if (command_response.hardware_error_alert)
            {
                std::cout << "command request got hardware error alert" << std::endl;
            }

            new_dexterity::match(command_response.result,
                    [](dynx::instruction_error error)
                    {
                        std::cout << "command request got error response: " << error << std::endl;
                    },
                    [](dynx::instruction_acknowledged)
                    {
                        std::cout << "command request got acknowledged" << std::endl;
                    });
        }
        else
        {
            std::cout << "no command response" << std::endl;
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        // Info request

        dynx::info_request info_request;

        auto minfo_response = interface.send_request(info_request, std::chrono::milliseconds(1000));

        if (minfo_response)
        {
            auto info_response = *minfo_response;

            if (info_response.hardware_error_alert)
            {
                std::cout << "info request got hardware error alert" << std::endl;
            }

            new_dexterity::match(info_response.result,
                    [](dynx::instruction_error error)
                    {
                        std::cout << "info request got error response: " << error << std::endl;
                    },
                    [](dynx::info const & info)
                    {
                        static constexpr auto DYNX_POSITION_TO_DEG = 0.088;
                        static constexpr auto DYNX_VELOCITY_TO_RPM = 0.229;
                        static constexpr auto DYNX_CURRENT_TO_MA = 1.34;

                        std::cout << "position: " << (int)info.position * DYNX_POSITION_TO_DEG << " (deg), " <<
                                     "velocity: " << (int)info.velocity * DYNX_VELOCITY_TO_RPM << " (rpm), " <<
                                     "current: " << (int)info.current * DYNX_CURRENT_TO_MA << " (mA), " << 
                                     "temperature: " << (int)info.temperature << " (C)" << std::endl;
                    });
        }
        else
        {
            std::cout << "no info response" << std::endl;
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }

    // Command request

    dynx::command_request command_request;
    command_request.position = 0;
    command_request.velocity_limit = 200; 
    command_request.current_limit = 100;

    interface.send_request(command_request, std::chrono::milliseconds(1000));

    port.close();
    io_service.stop();

    return 0;
}

#include <chrono>
#include <future>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <boost/numeric/conversion/cast.hpp>
#include <new_dexterity/dynamixel_protocol.hpp>
#include <new_dexterity/utilities.hpp>
#include <boost/variant.hpp>

namespace ndx = new_dexterity::dynamixel_protocol;

    template <typename T, typename Cleanup, typename... Handlers>
bool handle_optional(std::string info, Cleanup cleanup, boost::optional<T> const & t, Handlers&&... handlers)
{
    if (t) 
    {
        auto response = *t;
        new_dexterity::match(response.result, std::forward<Handlers>(handlers)...);
        return true;
    }
    else
    {
        std::cerr << info << " timeout" << std::endl;
        return true;
    }
}

template <typename Cleanup>
bool homing_offset(ndx::communication_interface& interface, Cleanup cleanup, int32_t offset)
{
    auto res = interface.send_request(ndx::make_set_memory_content_request(ndx::memory_handles::homing_offset{ offset }), std::chrono::seconds(1));

    if (!handle_optional("set homing offset request", cleanup, res, 
                [](ndx::instruction_acknowledged const &) { std::cerr << "set homing offset request acked" << std::endl; },
                [](ndx::instruction_error const & err) { std::cerr << "set homing offset request nacked: " << static_cast<int>(err) << std::endl; }))
    {
        return 0;
    }

    return 1;
}

std::tuple<int32_t, int32_t, std::int16_t> read_data(ndx::communication_interface& interface)
{

    boost::optional<std::tuple<int32_t, int32_t, std::int16_t>> ret;

    while (!ret)
    {
        auto res = interface.send_request(ndx::info_request {}, std::chrono::milliseconds(30)); 

        handle_optional("get actuator info request", []{}, res, 
                [&ret](ndx::info const & info) 
                {
                ret = std::make_tuple(info.position, info.velocity, info.current);
                },
                [](ndx::instruction_error const & err) {});
    }

    return *ret;
}

template <typename Cleanup>
bool enable_torque(ndx::communication_interface& interface, Cleanup cleanup, bool enable)
{
    auto res = interface.send_request(ndx::make_set_memory_content_request(ndx::memory_handles::torque_enable{ enable }), std::chrono::seconds(1));

    if (!handle_optional("set enable torque request", cleanup, res, 
                [](ndx::instruction_acknowledged const &) { std::cerr << "set enable torque request acked" << std::endl; },
                [](ndx::instruction_error const & err) { std::cerr << "set enable torque request nacked" << std::endl; }))
    {
        return 0;
    }

    return 1;
}

template <typename Cleanup>
bool go_to(ndx::communication_interface& interface, Cleanup cleanup, int32_t position, uint32_t velocity, std::int16_t torque)
{
    std::cout << "sending command: position(" << position << "), velocity(" << velocity << "), torque(" << torque << ")" << std::endl;
    auto res1 = interface.send_request(ndx::command_request { position, velocity, torque }, std::chrono::seconds(1));

    if (!handle_optional("command actuator request", cleanup, res1, 
                [](ndx::instruction_acknowledged const &){ std::cerr << "command actuator requests acked" << std::endl;},
                [](ndx::instruction_error const & err){ std::cerr << "command actuator requests nacked" << static_cast<int>(err) << std::endl;}))

    {
        return 0;
    }

    boost::optional<int32_t> start_position;

    bool stopped = false;

    // waiting for dynamixel to stop moving 
    while (!stopped)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(20));

        auto res12 = interface.send_request(ndx::info_request {}, std::chrono::seconds(1)); 

        handle_optional("get actuator info request", cleanup, res12, 
                [&](ndx::info const & info) 
                {

                if (!start_position)
                    start_position = info.position;


                if (!info.moving)
                    stopped = true;

                },
                [](ndx::instruction_error const & err) {});
    }

    return 1;
}

    template <typename Cleanup>
boost::optional<std::pair<int32_t, int32_t>> search_first(ndx::communication_interface& interface, Cleanup cleanup, std::int32_t end, std::uint32_t vel_limit, std::int16_t tor_limit)
{
    // enabling dynamixel
    if (!enable_torque(interface, cleanup, true))
        return boost::none;

    std::this_thread::sleep_for(std::chrono::milliseconds(50));

    // setting command to flexed position
    while (!go_to(interface, cleanup, end, vel_limit, tor_limit));

    std::this_thread::sleep_for(std::chrono::milliseconds(50));

    auto tup_last = read_data(interface);

    std::this_thread::sleep_for(std::chrono::milliseconds(50));

    // disabling dynamixel
    if (!enable_torque(interface, cleanup, false))
        return boost::none;

    std::this_thread::sleep_for(std::chrono::milliseconds(5));

    auto tup_first = read_data(interface);

    auto count = 0;
    while (count < 5)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(5));

        tup_first = read_data(interface);

        if (std::get<1>(tup_first) == 0)
            ++count;

    }

    return std::make_pair(std::get<0>(tup_first), std::get<0>(tup_last));
}

int main(int argc, char *argv[])
{
    if (!(argc == 6))
    {
        std::cerr << "options: usb_port baud_rate actuator_id velocity torque" << std::endl;
        return EXIT_FAILURE;
    }

    asio::io_service io_service;

    asio::serial_port port(io_service);

    port.open(argv[1]);
    port.set_option(asio::serial_port::baud_rate(std::stoul(argv[2])));
    port.set_option(asio::serial_port::flow_control(asio::serial_port::flow_control::none));
    port.set_option(asio::serial_port::parity(asio::serial_port::parity::none));
    port.set_option(asio::serial_port::stop_bits(asio::serial_port::stop_bits::one));
    port.set_option(asio::serial_port::character_size(8));

    ndx::in_connection in(port);
    ndx::out_connection out(port);

    auto id = boost::numeric_cast<std::uint8_t>(std::stoul(argv[3]));
    ndx::communication_interface interface(in, out, id);

    auto runner = std::async(std::launch::async, [&io_service]{io_service.run();});

    auto cleanup = [&]{ io_service.stop(); };

    // disabling dynamixel
    if(!enable_torque(interface, cleanup, false))
        return 1;

    std::this_thread::sleep_for(std::chrono::milliseconds(50));

    // resetting homing offset

    if (!homing_offset(interface, cleanup, 0.0))
        return 1;

    std::this_thread::sleep_for(std::chrono::milliseconds(50));

    // enabling dynamixel
    if (!enable_torque(interface, cleanup, true))
        return 1;

    auto m_first_pair = search_first(interface, cleanup, 10000, 200, 100);

    if (!m_first_pair)
        return 1;

    std::cout << "first: " << m_first_pair->first << "second: " << m_first_pair->second << std::endl;

    if (!enable_torque(interface, cleanup, true))
        return 1;

    auto m_pair = search_first(interface, cleanup, m_first_pair->second, std::stof(argv[4]), boost::numeric_cast<std::int16_t>(std::stol(argv[5])));

    if (!m_pair)
        return 1;

    auto first_position = m_pair->first;
    auto last_position = m_pair->second;

    std::cerr << "first position: " << first_position << std::endl;
    std::cerr << "last position: " << last_position << std::endl;

    int32_t last_stucked = 1000000;

    while (true)
    {
        if (!enable_torque(interface, cleanup, true))
            return 1;

        auto m_pair2 = search_first(interface, cleanup, 0.5 * (last_position + first_position), std::stof(argv[4]), boost::numeric_cast<std::int16_t>(std::stol(argv[5])));

        if (!m_pair2)
            return 1;

        first_position = m_pair2->first;
        last_position = m_pair2->second;

        std::cerr << "first: " << first_position << "last: " << last_position << std::endl;

        if (std::abs(last_position - first_position) <= 1)
        {
            std::cout << "entered" << std::endl; 
            decltype(m_pair2) m_c_pair; 

            std::cerr << "last stucked: " << last_stucked << std::endl;

            while (!(m_c_pair = search_first(interface, cleanup, std::min(first_position + 10, last_stucked), 50, 200)));

            auto c_pair = *m_c_pair;

            std::cerr << "c_first: " << c_pair.first << "first_position: " << first_position << std::endl;

            if (std::abs(c_pair.first - first_position) <= 1)
                break;

            last_stucked = first_position;

            last_position = last_stucked;
            first_position = c_pair.first;
        }

    }


    std::cerr << "final: " << first_position << std::endl;

    //setting final homing offset

    if (id == 1 || id == 2)
    {
        if (!homing_offset(interface, cleanup, first_position))
            return 1;
    }
    else
    {
        if (!homing_offset(interface, cleanup, -first_position))
            return 1;
    }

    port.close();

    io_service.stop();

    return EXIT_SUCCESS;

}

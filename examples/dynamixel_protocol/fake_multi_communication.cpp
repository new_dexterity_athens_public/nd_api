//! @example
//! @brief Example of fake multiple dynamixel communication

#include <new_dexterity/dynamixel_protocol.hpp>
#include <new_dexterity/utilities.hpp>
#include <new_dexterity/pipe.hpp>
#include <boost/asio.hpp>
#include <iostream>
#include <thread>

namespace dynx = new_dexterity::dynamixel_protocol;
namespace asio = boost::asio;

int main()
{
    //Create executor

    asio::io_service io_service;

    //Create endpoints

    new_dexterity::pipe_endpoint endp1(io_service);
    new_dexterity::pipe_endpoint endp2(io_service);

    new_dexterity::pipe_endpoint endp3(io_service);
    new_dexterity::pipe_endpoint endp4(io_service);

    //Create pipes

    new_dexterity::create_pipe(endp1, endp2);
    new_dexterity::create_pipe(endp3, endp4);

    // Create connections

    dynx::in_connection in(endp1);
    dynx::out_connection out(endp4);

    dynx::in_connection fake_in(endp3);
    dynx::out_connection fake_out(endp2);

    //Create communication interfaces

    dynx::communication_interface interface1(in, out, 1);
    dynx::communication_interface interface2(in, out, 2);
    dynx::communication_interface interface3(in, out, 3);
    dynx::communication_interface interface4(in, out, 4);
    dynx::communication_interface interface5(in, out, 5);
    dynx::communication_interface interface6(in, out, 6);


    // Create fake dynamixel drivers

    dynx::fake_dynamixel_driver fake1(fake_in, fake_out, 1);
    dynx::fake_dynamixel_driver fake2(fake_in, fake_out, 2);
    dynx::fake_dynamixel_driver fake3(fake_in, fake_out, 3);
    dynx::fake_dynamixel_driver fake4(fake_in, fake_out, 4);
    dynx::fake_dynamixel_driver fake5(fake_in, fake_out, 5);
    dynx::fake_dynamixel_driver fake6(fake_in, fake_out, 6);

    //Start runner

    auto runner = std::async(std::launch::async, [&]{ io_service.run(); });

    //Disable torque enable

    auto disable_procedure = [&](dynx::communication_interface & interface, int n)
    {
        auto disable_request = dynx::make_set_memory_content_request(
                                dynx::memory_handles::torque_enable { false }
                              );

        auto mdisable_response = interface.send_request(disable_request, std::chrono::milliseconds(1000));

        //We check if we got a response
        if (mdisable_response)
        {
            // We retrieve it
            auto disable_response = *mdisable_response;

            // We check for any hardware error alerts

            if (disable_response.hardware_error_alert)
            {
                std::cout << "(" << n << ") disable torque request got hardware error alert" << std::endl;
            }

            // response.result is a boost variant
            // the match utility is used for type-safe and convenient pattern matching on types.

            new_dexterity::match(disable_response.result,
                    [&](dynx::instruction_error error)
                    {
                        std::cout << "(" << n << ") disable torque request got error response: " << error << std::endl;
                    },
                    [&](dynx::instruction_acknowledged)
                    {
                        std::cout << "(" << n << ") disable torque request got acknowledged" << std::endl;
                    });
        }
        else
        {
            std::cout << "(" << n << ") no disable torque response" << std::endl;
        }
    };

    disable_procedure(interface1, 1);
    disable_procedure(interface2, 2);
    disable_procedure(interface3, 3);
    disable_procedure(interface4, 4);
    disable_procedure(interface5, 5);
    disable_procedure(interface6, 6);

    //Set operating mode to 5 - Current-based position control

    auto operating_mode_procedure = [&](dynx::communication_interface& interface, int n)
    {
        auto operating_mode_request = dynx::make_set_memory_content_request(
                                        dynx::memory_handles::operating_mode{ 5 }
                                      );

        auto mmode_response = interface.send_request(operating_mode_request, std::chrono::milliseconds(1000));

        if (mmode_response)
        {
            auto mode_response = *mmode_response;
            
            if (mode_response.hardware_error_alert)
            {
                std::cout << "(" << n << ") operating mode request got hardware error alert" << std::endl;
            }

            new_dexterity::match(mode_response.result,
                    [&](dynx::instruction_error error)
                    {
                        std::cout << "(" << n << ") operating mode request got error response: " << error << std::endl;
                    },
                    [&](dynx::instruction_acknowledged)
                    {
                        std::cout << "(" << n << ") operating mode request got acknowledged" << std::endl;
                    });
        }
        else
        {
            std::cout << "(" << n << ") no operating mode response" << std::endl;
        }
    };

    operating_mode_procedure(interface1, 1);
    operating_mode_procedure(interface2, 2);
    operating_mode_procedure(interface3, 3);
    operating_mode_procedure(interface4, 4);
    operating_mode_procedure(interface5, 5);
    operating_mode_procedure(interface6, 6);


    //Set torque enable request

    auto enable_procedure = [&](dynx::communication_interface& interface, int n)
    {
        auto enable_request = dynx::make_set_memory_content_request(
                                dynx::memory_handles::torque_enable { true }
                              );

        auto menable_response = interface.send_request(enable_request, std::chrono::milliseconds(1000));

        if (menable_response)
        {
            auto enable_response = *menable_response;

            if (enable_response.hardware_error_alert)
            {
                std::cout << "(" << n << ") enable torque request got hardware error alert" << std::endl;
            }

            new_dexterity::match(enable_response.result,
                    [&](dynx::instruction_error error)
                    {
                        std::cout << "(" << n << ") enable torque request got error response: " << error << std::endl;
                    },
                    [&](dynx::instruction_acknowledged)
                    {
                        std::cout << "(" << n << ") enable torque request got acknowledged" << std::endl;
                    });
        }
        else
        {
            std::cout << "(" << n << ") no enable torque response" << std::endl;
        }
    };

    enable_procedure(interface1, 1);
    enable_procedure(interface2, 2);
    enable_procedure(interface3, 3);
    enable_procedure(interface4, 4);
    enable_procedure(interface5, 5);
    enable_procedure(interface6, 6);

    //Ping request

    auto ping_procedure = [&](dynx::communication_interface& interface, int n)
    {

        dynx::ping_request ping_request;

        auto mping_response = interface.send_request(ping_request, std::chrono::milliseconds(1000));

        if (mping_response)
        {
            auto ping_response = *mping_response;

            if (ping_response.hardware_error_alert)
            {
                std::cout << "(" << n << ") ping request got hardware error alert" << std::endl;
            }

            new_dexterity::match(ping_response.result,
                    [&](dynx::instruction_error error)
                    {
                        std::cout << "(" << n << ") ping request got error response: " << error << std::endl;
                    },
                    [&](dynx::ping_info const & info)
                    {
                        std::cout << "(" << n << ") modelNumber: " << (int)info.model_number << 
                                   ", firmwareVersion: " << (int)info.firmware_version << std::endl;
                    });
        }
        else
        {
            std::cout << "(" << n << ") no ping response" << std::endl;
        }
    };

    ping_procedure(interface1, 1);
    ping_procedure(interface2, 2);
    ping_procedure(interface3, 3);
    ping_procedure(interface4, 4);
    ping_procedure(interface5, 5);
    ping_procedure(interface6, 6);

    auto total_start = std::chrono::high_resolution_clock::now();

    double count = 0;
    double ms = 0;

    while (true)
    {
        auto cycle_start = std::chrono::high_resolution_clock::now();

        auto total_duration = cycle_start - total_start;

        if (std::chrono::duration_cast<std::chrono::seconds>(total_duration).count() >= 5)
            break;

        dynx::command_request command_request;
        command_request.position = std::chrono::duration_cast<std::chrono::milliseconds>(total_duration).count();
        command_request.velocity_limit = 200; 
        command_request.current_limit = 100;

        dynx::sync_write<dynx::command_request> sync_write;
        sync_write.requests.emplace_back(1, command_request);
        sync_write.requests.emplace_back(2, command_request);
        sync_write.requests.emplace_back(3, command_request);
        sync_write.requests.emplace_back(4, command_request);
        sync_write.requests.emplace_back(5, command_request);
        sync_write.requests.emplace_back(6, command_request);

        dynx::sync_read<dynx::info_request> sync_read;
        sync_read.ids.emplace_back(1);
        sync_read.ids.emplace_back(2);
        sync_read.ids.emplace_back(3);
        sync_read.ids.emplace_back(4);
        sync_read.ids.emplace_back(5);
        sync_read.ids.emplace_back(6);

        out.access([&](std::vector<std::uint8_t>& buffer)
        {
            dynx::serialize(buffer, sync_write); 
            dynx::serialize(buffer, sync_read); 
        });

        auto minfo_fut1 = interface1.wait_response_of<dynx::info_request>(std::chrono::milliseconds(1000));
        auto minfo_fut2 = interface2.wait_response_of<dynx::info_request>(std::chrono::milliseconds(1000));
        auto minfo_fut3 = interface3.wait_response_of<dynx::info_request>(std::chrono::milliseconds(1000));
        auto minfo_fut4 = interface4.wait_response_of<dynx::info_request>(std::chrono::milliseconds(1000));
        auto minfo_fut5 = interface5.wait_response_of<dynx::info_request>(std::chrono::milliseconds(1000));
        auto minfo_fut6 = interface6.wait_response_of<dynx::info_request>(std::chrono::milliseconds(1000));

        out.send_bytes();

        auto handle_read = [](decltype(minfo_fut1)& minfo_fut, int n)
        {
            auto minfo_response = minfo_fut.get();

            if (minfo_response)
            {
                auto info_response = *minfo_response;

                if (info_response.hardware_error_alert)
                {
                    std::cout << "(" << n << ") info request got hardware error alert" << std::endl;
                }

                new_dexterity::match(info_response.result,
                        [&](dynx::instruction_error error)
                        {
                            std::cout << "(" << n << ") info request got error response: " << error << std::endl;
                        },
                        [&](dynx::info const & info)
                        {
                            static constexpr auto DYNX_POSITION_TO_DEG = 0.088;
                            static constexpr auto DYNX_VELOCITY_TO_RPM = 0.229;
                            static constexpr auto DYNX_CURRENT_TO_MA = 1.34;

                            std::cout << "(" << n << ") position: " << (int)info.position * DYNX_POSITION_TO_DEG << " (deg), " <<
                                         "velocity: " << (int)info.velocity * DYNX_VELOCITY_TO_RPM << " (rpm), " <<
                                         "current: " << (int)info.current * DYNX_CURRENT_TO_MA << " (mA), " << 
                                         "temperature: " << (int)info.temperature << " (C)" << std::endl;
                        });
            }
            else
            {
                std::cout << "(" << n << ") no info response" << std::endl;
            }
        };

        handle_read(minfo_fut1, 1);
        handle_read(minfo_fut2, 2);
        handle_read(minfo_fut3, 3);
        handle_read(minfo_fut4, 4);
        handle_read(minfo_fut5, 5);
        handle_read(minfo_fut6, 6);

        auto cycle_end = std::chrono::high_resolution_clock::now();

        ms = (ms * count + std::chrono::duration_cast<std::chrono::microseconds>(cycle_end - cycle_start).count())/(count + 1);
        ++count;
    }

    std::cout << "avg hz: " << 1000000 / ms << std::endl;

    // Command request

    dynx::command_request command_request;
    command_request.position = 0;
    command_request.velocity_limit = 200; 
    command_request.current_limit = 100;

    interface1.send_request(command_request, std::chrono::milliseconds(1000));
    interface2.send_request(command_request, std::chrono::milliseconds(1000));
    interface3.send_request(command_request, std::chrono::milliseconds(1000));
    interface4.send_request(command_request, std::chrono::milliseconds(1000));
    interface5.send_request(command_request, std::chrono::milliseconds(1000));
    interface6.send_request(command_request, std::chrono::milliseconds(1000));

    
    endp1.close();
    endp2.close();
    endp3.close();
    endp4.close();

    io_service.stop();

    return 0;
}

#include <chrono>
#include <future>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <boost/numeric/conversion/cast.hpp>
#include <new_dexterity/dynamixel_protocol.hpp>
#include <new_dexterity/utilities.hpp>
#include <boost/variant.hpp>

namespace ndx = new_dexterity::dynamixel_protocol;
int main(int argc, char *argv[])
{
    if (!(argc == 4))
    {
        std::cerr << "options: usb_port baud_rate actuator_id" << std::endl;
        return EXIT_FAILURE;
    }

    asio::io_service io_service;

    asio::serial_port port(io_service);

    port.open(argv[1]);
    port.set_option(asio::serial_port::baud_rate(std::stoul(argv[2])));
    port.set_option(asio::serial_port::flow_control(asio::serial_port::flow_control::none));
    port.set_option(asio::serial_port::parity(asio::serial_port::parity::none));
    port.set_option(asio::serial_port::stop_bits(asio::serial_port::stop_bits::one));
    port.set_option(asio::serial_port::character_size(8));

    ndx::in_connection in(port);
    ndx::out_connection out(port);

    auto id = boost::numeric_cast<std::uint8_t>(std::stoul(argv[3]));

    ndx::communication_interface interface(in, out, id);

    auto runner = std::async(std::launch::async, [&io_service]{io_service.run();});

    std::cout << "rebooting" << std::endl;

    auto mreboot_response = interface.send_request(ndx::reboot_request{}, std::chrono::milliseconds(500));

    if (!mreboot_response)
    {
        std::cout << "timed out" << std::endl;
        return EXIT_FAILURE;
    }

    auto reboot_response = *mreboot_response;

    if (!new_dexterity::match<bool>(reboot_response.result,
                [](ndx::instruction_error)
                {
                    return false;
                },
                [](ndx::instruction_acknowledged)
                {
                    return true;
                }))
    {
        std::cout << "not acknowledged" << std::endl;
        return EXIT_FAILURE;
    }

    std::this_thread::sleep_for(std::chrono::seconds(2));

    std::cout << "rebooted" << std::endl;

    std::cout << "resetting offset" << std::endl;
    auto mresetting_homing_offset_response = interface.send_request(ndx::make_set_memory_content_request(ndx::memory_handles::homing_offset{0}), std::chrono::milliseconds(500));

    if (!mresetting_homing_offset_response)
    {
        std::cout << "timed out" << std::endl;
        return EXIT_FAILURE;
    }

    auto resetting_homing_offset_response = *mresetting_homing_offset_response;

    if (!new_dexterity::match<bool>(resetting_homing_offset_response.result,
                [](ndx::instruction_error)
                {
                    return false;
                },
                [](ndx::instruction_acknowledged)
                {
                    return true;
                }))
    {
        std::cout << "not acknowledged" << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "reset offset" << std::endl;

    std::cout << "enabling torque" << std::endl;
    auto menable_torque_response = interface.send_request(ndx::make_set_memory_content_request(ndx::memory_handles::torque_enable{true}), std::chrono::milliseconds(500));

    if (!menable_torque_response)
    {
        std::cout << "timed out" << std::endl;
        return EXIT_FAILURE;
    }

    auto enable_torque_response = *menable_torque_response;

    if (!new_dexterity::match<bool>(enable_torque_response.result,
                [](ndx::instruction_error)
                {
                    return false;
                },
                [](ndx::instruction_acknowledged)
                {
                    return true;
                }))
    {
        std::cout << "not acknowledged" << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "enabled torque" << std::endl;

    std::cout << "getting position" << std::endl;
    auto mposition_response = interface.send_request(ndx::get_memory_content_request<ndx::memory_handles::present_position>{}, std::chrono::milliseconds(500));

    if (!mposition_response)
    {
        std::cout << "timed out" << std::endl;
        return EXIT_FAILURE;
    }

    auto position_response = *mposition_response;

    auto mposition = new_dexterity::match<boost::optional<std::int32_t>>(position_response.result,
            [](ndx::instruction_error)
            {
                return boost::none;
            },
            [](ndx::memory_content<ndx::memory_handles::present_position> const & info)
            {
                return info.present_position;
            });

    if (!mposition)
    {
        std::cout << "instruction error" << std::endl;
        return EXIT_FAILURE;
    }

    auto position = *mposition;

    std::cout << "got position: " << position << std::endl;

    std::cout << "disabling torque" << std::endl;
    auto mdisable_torque_response = interface.send_request(ndx::make_set_memory_content_request(ndx::memory_handles::torque_enable{false}), std::chrono::milliseconds(500));

    if (!mdisable_torque_response)
    {
        std::cout << "timed out" << std::endl;
        return EXIT_FAILURE;
    }

    auto disable_torque_response = *mdisable_torque_response;

    if (!new_dexterity::match<bool>(disable_torque_response.result,
                [](ndx::instruction_error)
                {
                    return false;
                },
                [](ndx::instruction_acknowledged)
                {
                    return true;
                }))
    {
        std::cout << "not acknowledged" << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "disabled torque" << std::endl;

    std::cout << "setting offset" << std::endl;

    decltype(position) target_offset = 0;

    if (id == 1 || id == 2)
        target_offset = position;
    else
        target_offset = -position;
        
    auto mhoming_offset_response = interface.send_request(ndx::make_set_memory_content_request(ndx::memory_handles::homing_offset{target_offset}), std::chrono::milliseconds(500));

    if (!mhoming_offset_response)
    {
        std::cout << "timed out" << std::endl;
        return EXIT_FAILURE;
    }

    auto homing_offset_response = *mhoming_offset_response;

    if (!new_dexterity::match<bool>(homing_offset_response.result,
                [](ndx::instruction_error)
                {
                    return false;
                },
                [](ndx::instruction_acknowledged)
                {
                    return true;
                }))
    {
        std::cout << "not acknowledged" << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "set offset" << std::endl;

    port.close();

    io_service.stop();

    return EXIT_SUCCESS;

}

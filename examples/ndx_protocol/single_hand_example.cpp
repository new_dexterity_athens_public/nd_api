#include <new_dexterity/new_dexterity.hpp>
#include <new_dexterity/ndx_protocol.hpp>
#include <boost/asio.hpp>
#include <boost/numeric/conversion/cast.hpp>

/*! @example
 *  @brief Single hand example
 */

namespace asio = boost::asio;
namespace ndx = new_dexterity::ndx_protocol;

void example_procedure(new_dexterity::ndx_protocol::communication_interface&, std::size_t);

int main(int argc, char *argv[]) 
{

    if (!(argc == 4 || argc == 5))
    {
        std::cout << "options: usb_port baud_rate hand_id [millis]" << std::endl;
        return EXIT_FAILURE;
    }

    asio::io_service io_service;

    asio::serial_port port(io_service);

    port.open(argv[1]);
    port.set_option(asio::serial_port::baud_rate(std::stoul(argv[2])));
    port.set_option(asio::serial_port::flow_control(asio::serial_port::flow_control::none));
    port.set_option(asio::serial_port::parity(asio::serial_port::parity::none));
    port.set_option(asio::serial_port::stop_bits(asio::serial_port::stop_bits::one));
    port.set_option(asio::serial_port::character_size(8));

    new_dexterity::ndx_protocol::in_connection in(port);
    new_dexterity::ndx_protocol::out_connection out(port);

    new_dexterity::ndx_protocol::communication_interface interface(in, out, boost::numeric_cast<std::uint8_t>(std::stoul(argv[3])));

    auto runner = std::thread([&io_service](){io_service.run();});
        
    example_procedure(interface, (argc == 4) ? 200 : std::stoul(argv[4]));

    port.close();

    io_service.stop();

    runner.join();


    return EXIT_SUCCESS;
}



#include <new_dexterity/new_dexterity.hpp>
#include <new_dexterity/ndx_protocol.hpp>
#include <boost/asio.hpp>
#include <vector>
#include <cstdint>

namespace ndx = new_dexterity::ndx_protocol;

void handle_actuator_info(std::string, const boost::variant<ndx::actuator_info, ndx::actuator_error>&);


int main(int argc, char * argv[])
{
    if (!(argc == 3))
    {
        std::cout << "options: usb_port and baud_rate" << std::endl;
        return EXIT_FAILURE;
    }

    asio::io_service io_service;

    asio::serial_port port(io_service);

    port.open(argv[1]);
    port.set_option(asio::serial_port::baud_rate(std::stoul(argv[2])));
    port.set_option(asio::serial_port::flow_control(asio::serial_port::flow_control::none));
    port.set_option(asio::serial_port::parity(asio::serial_port::parity::none));
    port.set_option(asio::serial_port::stop_bits(asio::serial_port::stop_bits::one));
    port.set_option(asio::serial_port::character_size(8));

    ndx::in_connection in(port);
    ndx::out_connection out(port);

    auto runner = std::async(std::launch::async, [&](){ io_service.run(); });

    ndx::discoverer discoverer(in, out);
    auto hand_ids = discoverer.discover(std::chrono::milliseconds(100));

    std::vector<std::unique_ptr<ndx::communication_interface>> interfaces;

    for (auto hand_id : hand_ids)
        interfaces.emplace_back(new ndx::communication_interface(in, out, hand_id));

    std::vector<std::future<boost::optional<ndx::actuator_info_response>>> futures;

    for (auto& interface : interfaces)
        futures.emplace_back(interface->wait_response_of<ndx::actuator_info_request>(std::chrono::milliseconds(1000)));

    std::vector<std::uint8_t> buffer;
    ndx::serialize(254, buffer, ndx::actuator_info_request{});
    out.queue_bytes(buffer.begin(), buffer.end());
    out.send_bytes();

    for (std::size_t i = 0; i != interfaces.size(); ++i)
    {
        std::cout << "HAND ID: " << (int)hand_ids[i] << std::endl;

        std::cout << "BEGIN ACTUATOR INFO \n";

        auto result = futures[i].get();

        if (result)
        {
            auto response = *result;
            handle_actuator_info("THUMB ACTUATOR", response.thumb_actuator_info_response);
            handle_actuator_info("INDEX ACTUATOR", response.index_actuator_info_response);
            handle_actuator_info("MIDDLE ACTUATOR", response.middle_actuator_info_response);
            handle_actuator_info("RING-PINKY ACTUATOR", response.ring_pinky_actuator_info_response);
            handle_actuator_info("THUMB OPPOSITION ACTUATOR", response.thumb_opposition_actuator_info_response);
        }
        else
        {
            std::cout << "  ACTUATOR INFO RESPONSE TIMEOUT" << "\n";
        }
        std::cout << "END ACTUATOR INFO" << "\n";
        std::cout << std::endl;
    }

    io_service.stop();

    return EXIT_SUCCESS;
}

#include <new_dexterity/new_dexterity.hpp>
#include <new_dexterity/ndx_protocol.hpp>
#include <boost/asio.hpp>
#include <thread>
#include <chrono>
#include <iostream>

/*! @example
 *  @brief Example discoverer usage
 */
namespace asio = boost::asio;
namespace ndx = new_dexterity::ndx_protocol;

int main(int argc, char * argv[])
{

    if (argc != 3)
    {
        std::cout << "Please provide usb port and baudrate" << std::endl;
        return EXIT_FAILURE;
    }

    asio::io_service io_service;

    asio::serial_port port(io_service);
    port.open(argv[1]);
    port.set_option(asio::serial_port::baud_rate(std::stoi(argv[2], nullptr)));
    port.set_option(asio::serial_port::flow_control(asio::serial_port::flow_control::none));
    port.set_option(asio::serial_port::parity(asio::serial_port::parity::none));
    port.set_option(asio::serial_port::stop_bits(asio::serial_port::stop_bits::one));
    port.set_option(asio::serial_port::character_size(8));

    ndx::in_connection in(port);
    ndx::out_connection out(port);

    ndx::discoverer discoverer(in, out);

    auto runner = std::thread([&io_service](){io_service.run();});

    auto ids = discoverer.discover(std::chrono::seconds(1));

    for (auto id : ids)
    {
        std::cout << "detected hand with id: " << (int)id << std::endl;
    }

    port.close();
    io_service.stop();
    runner.join();

    return EXIT_SUCCESS;
}


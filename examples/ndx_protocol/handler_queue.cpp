#include <new_dexterity/communication_interface/response_handler_queue.hpp>
#include <memory>
#include <boost/asio.hpp>
#include <boost/optional.hpp>
#include <future>
#include <thread>
#include <iostream>
#include <new_dexterity/ndx_protocol/communication_policy.hpp>


int main()
{
    using request_type = new_dexterity::ndx_protocol::actuator_info_request;

    using response_type = new_dexterity::ndx_protocol::actuator_info_response;

    using policy = new_dexterity::ndx_protocol::communication_policy;

    std::vector<std::uint8_t> buffer;
    policy::serialize(254, buffer, response_type{});
    new_dexterity::ndx_protocol::message_parser parser;
    parser.parse(buffer.data(), buffer.size());

    auto message = parser.parsed_messages()[0];

    std::uint8_t id = 254;

    new_dexterity::message_handler_queue<policy> queue(id);

    auto handler1 = [](boost::optional<response_type> const & response) { if (!response) std::cout << "timeout" << std::endl; else std::cout << "response" << std::endl; };
    auto handler2 = [](boost::optional<response_type> const & response) { if (!response) std::cout << "timeout" << std::endl; else std::cout << "response" << std::endl; };
    auto handler3 = [](boost::optional<response_type> const & response) { if (!response) std::cout << "timeout" << std::endl; else std::cout << "response" << std::endl; };
    auto handler4 = [](boost::optional<response_type> const & response) { if (!response) std::cout << "timeout" << std::endl; else std::cout << "response" << std::endl; };

    queue.queue<request_type>(handler1, std::chrono::seconds(2));
    queue.queue<request_type>(handler2, std::chrono::seconds(2));
    queue.queue<request_type>(handler3, std::chrono::seconds(2));
    queue.queue<request_type>(handler4, std::chrono::seconds(2));

    std::this_thread::sleep_for(std::chrono::seconds(3));

    std::cout << queue.size() << std::endl;

    return 0;
}

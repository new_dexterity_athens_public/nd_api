#include <new_dexterity/new_dexterity.hpp>
#include <new_dexterity/ndx_protocol.hpp>
#include <boost/asio.hpp>
#include <boost/variant.hpp>
#include <boost/optional.hpp>
#include <iostream>
#include <chrono>
#include <thread>
#include <sstream>
#include <iomanip>
#include <ctime>
#include <mutex>

/*! @example
 * @brief Example test procedure used on the other examples.
 */

namespace ndx = new_dexterity::ndx_protocol;

void handle_actuator_info(std::string actuator_name, const boost::variant<ndx::actuator_info, ndx::actuator_error>& response);
void handle_actuator_status(std::string actuator_name, const boost::optional<ndx::actuator_status>& status);

void example_procedure(ndx::communication_interface& interface, std::size_t millis)
{

    auto command = ndx::actuator_command { 0, 100, 200};
    auto commands = ndx::actuator_commands { command, command, command, command, command};

    std::array<std::array<float, 9>, 5> states = { { { { 0.0, 2.0,   4.0,   8.0,  10.0,   8.0,  4.0,  2.0 , 0.0} },
                                                     { { 0.0, 40.0, 80.0, 120.0, 160.0, 120.0, 80.0, 40.0 , 0.0} },
                                                     { { 0.0, 40.0, 80.0, 120.0, 160.0, 120.0, 80.0, 40.0 , 0.0} },
                                                     { { 0.0, 40.0, 80.0, 120.0, 160.0, 120.0, 80.0, 40.0 , 0.0} },
                                                     { { 0.0, 10.0, 20.0,  30.0,  40.0,  50.0, 40.0, 20.0 , 0.0} } } };

    for (std::size_t i = 0; i != 9; ++i)
    {
        commands.thumb_actuator_command->position = states[0][i];
        commands.index_actuator_command->position = states[1][i];
        commands.middle_actuator_command->position = states[2][i];
        commands.ring_pinky_actuator_command->position = states[3][i];
        commands.thumb_opposition_actuator_command->position = states[4][i];

        auto test1_f = interface.queue_request(ndx::actuator_info_request{}, std::chrono::milliseconds(60));
        auto test2_f = interface.queue_request(ndx::command_actuators_request{commands}, std::chrono::milliseconds(60));
        auto test3_f = interface.queue_request(ndx::hand_status_request{}, std::chrono::milliseconds(60));
        auto test4_f = interface.queue_request(ndx::communication_status_request{}, std::chrono::milliseconds(60));
        auto test5_f = interface.queue_request(ndx::actuators_status_request {{ true, true, true, true, true}}, std::chrono::milliseconds(60));

        interface.out_connection().send_bytes();

        auto test1 = test1_f.get();
        auto test2 = test2_f.get();
        auto test3 = test3_f.get();
        auto test4 = test4_f.get();
        auto test5 = test5_f.get();

        std::cout << "BEGIN ACTUATOR INFO \n";
        if (test1)
        {
            auto response = *test1;
            handle_actuator_info("THUMB ACTUATOR", response.thumb_actuator_info_response);
            handle_actuator_info("INDEX ACTUATOR", response.index_actuator_info_response);
            handle_actuator_info("MIDDLE ACTUATOR", response.middle_actuator_info_response);
            handle_actuator_info("RING-PINKY ACTUATOR", response.ring_pinky_actuator_info_response);
            handle_actuator_info("THUMB OPPOSITION ACTUATOR", response.thumb_opposition_actuator_info_response);
        }
        else
        {
            std::cout << "  ACTUATOR INFO RESPONSE TIMEOUT" << "\n";
        }
        std::cout << "END ACTUATOR INFO" << "\n";
        std::cout << std::endl;

        std::cout << "BEGIN COMMANDS \n";
        if (test2)
        {
            auto response = *test2;
            new_dexterity::match(response,
                    [](const ndx::command_actuators_request_acknowledged&)
                    {
                        std::cout << "  COMMANDS ACKNOWLEDGED" << "\n";
                    },
                    [&](const ndx::command_actuators_request_not_acknowledged& error)
                    {
                        std::cout << "  COMMANDS NOT ACKNOWLEDGED" << "\n";

                        if (error == ndx::command_actuators_request_not_acknowledged::wrong_actuator_num)
                        {
                            std::cout << "  WRONG NUMBER OF ACTUATORS" << "\n";
                        }

                        if (error == ndx::command_actuators_request_not_acknowledged::wrong_command)
                        {
                            std::cout << "  WRONG COMMAND" << "\n";
                        }
                    });
        }
        else
        {
            std::cout << "  COMMANDS TIMEOUT" << "\n";
        }

        std::cout << "END COMMANDS" << "\n";
        std::cout << std::endl;

        
        std::cout << "BEGIN HAND STATUS \n";
        if (test3)
        {
            auto response = *test3;
            std::cout << "  mcu time: " << response.mcu_time << "\n";
            std::cout << "  mcu temperature: " << response.mcu_temperature << "\n";
            std::cout << "  last hand error code: " << response.last_hand_error_code << "\n";
            std::cout << "  received commands: " << response.received_commands << "\n";
            std::cout << "  valid received commands: " << response.valid_received_commands << "\n";
            std::cout << "  executed commands: " << response.executed_commands << "\n";
        }
        else
        {
            std::cout << "  HAND STATUS TIMEOUT" << "\n";
        }
        std::cout << "END HAND STATUS" << "\n";
        std::cout << std::endl;



        std::cout << "BEGIN COMMUNICATION STATUS \n";
        if (test4)
        {
            auto response = *test4;
            std::cout << "  mcu time: " << response.mcu_time << "\n";
            std::cout << "  last communication error code: " << response.last_communication_error_code << "\n";
            std::cout << "  received packets: " << response.received_packets << "\n";
            std::cout << "  valid received packets: " << response.valid_received_packets << "\n";
            std::cout << "  prepared packets: " << response.prepared_packets << "\n";
            std::cout << "  transmitted packets: " << response.transmitted_packets << "\n";
        }
        else
        {
            std::cout << "  COMMUNICATION STATUS TIMEOUT" << "\n";
        }
        std::cout << "END COMMUNICATION STATUS" << "\n";
        std::cout << std::endl;

    
        std::cout << "BEGIN ACTUATORS STATUS \n";
        if (test5)
        {
            auto response = *test5;
            std::cout << "  mcu time: " << response.mcu_time << "\n";
            handle_actuator_status("THUMB ACTUATOR", response.thumb_actuator_status);
            handle_actuator_status("INDEX ACTUATOR", response.index_actuator_status);
            handle_actuator_status("MIDDLE ACTUATOR", response.middle_actuator_status);
            handle_actuator_status("RING-PINKY ACTUATOR", response.ring_pinky_actuator_status);
            handle_actuator_status("THUMB OPPOSITION ACTUATOR", response.thumb_opposition_actuator_status);
        }
        else
        {
            std::cout << "  ACTUATORS STATUS TIMEOUT" << "\n";
        }
        std::cout << "END ACTUATORS STATUS" << "\n";
        std::cout << std::endl;


        std::cout << "sent requests" << "\n" << std::endl;


        std::this_thread::sleep_for(std::chrono::milliseconds(millis));

    }

}

void handle_actuator_info(std::string actuator_name, const boost::variant<ndx::actuator_info, ndx::actuator_error>& response)
{
    new_dexterity::match(response,
            [&](const ndx::actuator_info& info)
            {
                std::cout << "  " << actuator_name << " INFO:" << "\n";
                std::cout << "    position: " << info.position << "\n";
                std::cout << "    velocity: " << info.velocity << "\n";
                std::cout << "    torque: " << info.torque << "\n";
                std::cout << "    moving: " << info.moving << "\n";
                std::cout << "    temperature: " << info.temperature << "\n";
                std::cout << "    desired_actuation: " << info.desired_actuation << "\n";
            },
            [&](const ndx::actuator_error& error)
            {
                std::cout << "  " << actuator_name << " ERROR:" << "\n";

                if (error == ndx::actuator_error::overload)
                {
                    std::cout << "   overload" << "\n";
                }

                if (error == ndx::actuator_error::electrical_shock)
                {
                    std::cout << "   electrical shock" << "\n";
                }

                if (error == ndx::actuator_error::disconnected)
                {
                    std::cout << "   disconnected" << "\n";
                }

                if (error == ndx::actuator_error::voltage_error)
                {
                    std::cout << "   voltage_error" << "\n";
                }

                if (error == ndx::actuator_error::software_error)
                {
                    std::cout << "   software_error" << "\n";
                }

                if (error == ndx::actuator_error::out_of_limits)
                {
                    std::cout << "   out_of_limits" << "\n";
                }

                if (error == ndx::actuator_error::overheating)
                {
                    std::cout << "   overheating" << "\n";
                }

                if (error == ndx::actuator_error::encoder_error)
                {
                    std::cout << "   encoder_error" << "\n";
                }

                if (error == ndx::actuator_error::out_of_range)
                {
                    std::cout << "   out_of_range" << "\n";
                }

                if (error == ndx::actuator_error::unknown_error)
                {
                    std::cout << "   unknown_error" << "\n";
                }

            });
}

void handle_actuator_status(std::string actuator_name, const boost::optional<ndx::actuator_status>& status)
{
        if (status)
        {
            std::cout << "  " << actuator_name << " status: " << "\n";
            std::cout << "    last_actuator_error_code: " << (unsigned int)status->last_actuator_error_code << "\n";
            std::cout << "    max_temperature: " << status->max_temperature << "\n";
            std::cout << "    min temperature: " << status->min_temperature << "\n";
            std::cout << "    avg_temperature: " << status->avg_temperature << "\n";
            std::cout << "    max_torque: " << status->max_torque << "\n";
            std::cout << "    min_torque: " << status->min_torque << "\n";
            std::cout << "    avg_torque: " << status->avg_torque << "\n";
        }
}


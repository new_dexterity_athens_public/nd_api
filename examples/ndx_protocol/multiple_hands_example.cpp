#include <new_dexterity/new_dexterity.hpp>
#include <new_dexterity/ndx_protocol.hpp>
#include <boost/asio.hpp>
#include <thread>
#include <future>
#include <boost/numeric/conversion/cast.hpp>

/*! @example
 *  @brief Multiple hands example
 */

namespace asio = boost::asio;
namespace ndx = new_dexterity::ndx_protocol;

void example_procedure(ndx::communication_interface&, std::size_t);

int main(int argc, char *argv[]) 
{

    if (!(argc == 5 || argc == 6))
    {
        std::cout << "options: usb_port baud_rate hand_1_id hand_2_id [millis]" << std::endl;
        return EXIT_FAILURE;
    }

    asio::io_service io_service;

    asio::serial_port port(io_service);

    port.open(argv[1]);
    port.set_option(asio::serial_port::baud_rate(std::stoul(argv[2])));
    port.set_option(asio::serial_port::flow_control(asio::serial_port::flow_control::none));
    port.set_option(asio::serial_port::parity(asio::serial_port::parity::none));
    port.set_option(asio::serial_port::stop_bits(asio::serial_port::stop_bits::one));
    port.set_option(asio::serial_port::character_size(8));

    ndx::in_connection in(port);
    ndx::out_connection out(port);

    ndx::communication_interface interface1(in, out, boost::numeric_cast<std::uint8_t>(std::stoul(argv[3])));
    ndx::communication_interface interface2(in, out, boost::numeric_cast<std::uint8_t>(std::stoul(argv[4])));

    auto runner = std::thread([&io_service](){io_service.run();});

    auto _1 = std::async(std::launch::async, example_procedure, std::ref(interface1), (argc == 5) ? 200 : std::stoul(argv[5])); 
    auto _2 = std::async(std::launch::async, example_procedure, std::ref(interface2), (argc == 5) ? 200 : std::stoul(argv[5])); 

    _1.get();
    _2.get();

    port.close();

    io_service.stop();

    runner.join();


    return EXIT_SUCCESS;
}



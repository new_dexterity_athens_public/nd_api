#include <new_dexterity/new_dexterity.hpp>
#include <new_dexterity/ndx_protocol.hpp>
#include <boost/asio.hpp>

namespace ndx = new_dexterity::ndx_protocol;

int main()
{
    boost::asio::io_service io_service;

    new_dexterity::pipe_endpoint pipe1_in(io_service);
    new_dexterity::pipe_endpoint pipe1_out(io_service);
    new_dexterity::create_pipe(pipe1_in, pipe1_out);

    new_dexterity::pipe_endpoint pipe2_in(io_service);
    new_dexterity::pipe_endpoint pipe2_out(io_service);
    new_dexterity::create_pipe(pipe2_in, pipe2_out);

    ndx::in_connection interface_in(pipe1_in);
    ndx::out_connection interface_out(pipe2_out);

    ndx::in_connection fake_in(pipe2_in);
    ndx::out_connection fake_out(pipe1_out);

    ndx::communication_interface interface(interface_in, interface_out, 1);

    ndx::fake_hand_hardware fake(fake_in, fake_out, 1);

    auto runner = std::async(std::launch::async, [&]{ io_service.run(); });

    for (std::size_t i = 0; i != 100000; ++i)
    {
        auto p1 = std::chrono::high_resolution_clock::now();
        auto future = interface.queue_request(ndx::actuator_info_request{}, std::chrono::milliseconds(500));
        interface_out.send_bytes();
        auto result = future.get();
        if (result)
        {
            std::cout << "got result" << std::endl;
        }
        else
        {
            std::cout << "timeout" << std::endl;
        }
        auto p2 = std::chrono::high_resolution_clock::now();
        std::cout << "duration: " << std::chrono::duration_cast<std::chrono::microseconds>(p2-p1).count() << std::endl;
    }

    io_service.stop();

    return EXIT_SUCCESS;
}

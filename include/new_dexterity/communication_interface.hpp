#ifndef ND_NETWORK_COMMUNICATION_INTERFACE_HPP
#define ND_NETWORK_COMMUNICATION_INTERFACE_HPP

//! @file
//! @brief Communication Interface definition

#include <boost/asio.hpp>
#include <boost/variant.hpp>
#include <boost/optional.hpp>
#include <boost/asio/high_resolution_timer.hpp>
#include <new_dexterity/message_dispatcher.hpp>
#include <chrono>
#include <thread>
#include <condition_variable>
#include <new_dexterity/utilities.hpp>
#include <new_dexterity/in_connection.hpp>
#include <new_dexterity/out_connection.hpp>
#include <new_dexterity/communication_interface/communication_interface.hpp>


namespace asio = boost::asio;

namespace new_dexterity
{


/*! @brief Queues and sends requests.
 * 
 * The class provides a way to queue requests and retrieve responses from a connection.
 * For each queued request, it is possible to either subscribe a callback or get a future from which the response may be retrieved.
 * @tparam CommunicationPolicy The policy describing the low-level communication protocol and the message transformations.
 * @code {.cpp}
 * //the type of the parser used to parse messages.
 * using parser_type = typename CommunicationPolicy::parser_type;
 *
 * //the type of the parser's parsed messages.
 * using message_type = typename parser_type::message_type;
 *
 * //the parser should be default construcible.
 * parser_type p;
 *
 * //the parser should support a parser method taking a uint8_t pointer and the length of bytes.
 * char const * bytes = ...;
 * std::size_t num_of_bytes = ...;
 * p.parse(bytes, num_of_bytes);
 *
 * //the parser should support a parsed_messages method returning a range-for-loop Iterable representing the message_type parsed messages.
 * auto messages& = p.parsed_messages();
 * for (message_type const & message : messages)
 * {
 *     ...
 * }
 *
 * //the parser should provide a clear method to clear the parsed messages
 * p.clear();
 *
 * // the type of the underlying state used to differentiate between different ports in a single communication channel
 * using implicit_identifier = typename CommunicationPolicy::implicit_identifier;
 *
 * // a type-level mapper of requests to responses
 * template <typename T>
 * using response_of = typename CommunicationPolicy::template response_of<T>;
 *
 * // a static member serializer template 
 * // i is of type CommunicationPolicy::implicit_identifier
 * // v is of type std::vector<std::uint8_t>
 * // r is any high-level request supported by the communication_interface. In fact what is legal
 * // here defines what requests the communication_interface supports.
 * CommunicationPolicy::serialize(i, v, r);
 *
 * // a static member deserializer template
 * // i is of type CommunicationPolicy::implicit_identifier
 * // Response is the type of any high-level response supported by the communication_interface.
 * // m is of type CommunicationPolicy::parser_type::message_type
 * boost::optional<Response> mresponse = CommunicationPolicy::template deserialize<Response>(i, m)
 * @endcode
 * 
 */

template <typename CommunicationPolicy>
class communication_interface
{
public:
    using policy = CommunicationPolicy;
    using implementation = impl::communication_interface<CommunicationPolicy>;
    using in_connection_type = typename implementation::in_connection_type;
    using implicit_identifier = typename implementation::implicit_identifier;

    template <typename T>
    using response_of = typename CommunicationPolicy::template response_of<T>;

    /*! @param in The object representing the incomming connection.
     *  @param out The object representing the outgoing connection.
     *  @param id The identifier hand identifier.
     */

    communication_interface(in_connection_type & in, new_dexterity::out_connection& out, implicit_identifier id)
        : impl(in, out, id)
    {
    }

    /*! \brief Queues a request and subscribes a callback.
     *  @tparam Request One of the high-level request types.
     *  @tparam Callback 
     *  @code{.cpp}
     *  //f is Callback, p is response_of<Request>
     *  f(p);
     *  f(boost::none);
     *  @endcode
     *  @tparam Duration 
     *  @code{.cpp}
     *  //d is Duration
     *  auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(d);
     *  @endcode
     *  @param request The request to be queued.
     *  @param callback The callback to be called with an optional response. `boost::none` is given in case of a timeout.
     *  @param duration The duration after which a timeout will occur.
     */
    template <typename Request, typename Callback, typename Duration>
    void queue_request(Request const & request, Callback callback, Duration duration)
    {
        static_assert(CommunicationPolicy::can_distinguish_responses, "Can't queue requests if responses can not be distinguised. Use the send_request member instead.");
        queue_request_unchecked(request, callback, duration);
    }

    /*! \brief Queues a request and does not subscribe anything.
     *  @tparam Request One of the high-level request types.
     *  @param request The request to be queued.
     */
    template <typename Request>
    void queue_request(Request const & request)
    {
        static_assert(CommunicationPolicy::can_distinguish_responses, "Can't queue requests if responses can not be distinguised. Use the send_request member instead.");
        queue_request_unchecked(request);
    }

    template <typename Request>
    void send_request(Request const & request)
    {
        queue_request_unchecked(request);
        out_connection().send_bytes();
    }

    /*! \brief Queues a request and returns a future.
     *  @tparam Request One of the high-level request types.
     *  @tparam Duration 
     *  @code{.cpp}
     *  //d is Duration
     *  auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(d);
     *  @endcode
     *  @param request The request to be queued.
     *  @param duration The duration after which a timeout will occur.
     *  @return A future from which an optional response may be retrieved. `boost::none` is given in case of a timeout.
     */
    template <typename Request, typename Duration>
    std::future<boost::optional<response_of<Request>>> queue_request(Request const & request, Duration duration)
    {
        static_assert(CommunicationPolicy::can_distinguish_responses, "Can't queue requests if responses can not be distinguised. Use the send_request member instead.");
        return queue_request_unchecked(request, duration);

    }

    template <typename Request, typename Duration>
    boost::optional<response_of<Request>> send_request(Request const & request, Duration duration)
    {
        auto fut = queue_request_unchecked(request, duration);
        out_connection().send_bytes();
        return fut.get();
    }

    /*! @brief Used to wait for the appropriate response without serializing the request. 
     *  Useful when sending bytes to the connection directly. Can be used for instance when serializing
     *  a request using the broadcast address directly to the connection.
     *  @tparam Request One of the high-level request types.
     *  @tparam Duration 
     *  @code{.cpp}
     *  //d is Duration
     *  auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(d);
     *  @endcode
     *  @param duration The duration after which a timeout will occur.
     *  @param callback The callback to call with the result.
     *  @return A future from which an optional response may be retrieved. `boost::none` is given in case of a timeout.
     */

    template <typename Request, typename Callback, typename Duration>
    void wait_response_of(Callback callback, Duration duration)
    {
        impl.template wait_response_of<Request>(callback, duration);
    }

    /*! \brief Returns a future.
     *  @tparam Request One of the high-level request types.
     *  @tparam Duration 
     *  @code{.cpp}
     *  //d is Duration
     *  auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(d);
     *  @endcode
     *  @param duration The duration after which a timeout will occur.
     *  @return A future from which an optional response may be retrieved. `boost::none` is given in case of a timeout.
     */
    template <typename Request, typename Duration>
    std::future<boost::optional<response_of<Request>>> wait_response_of(Duration duration)
    {
        auto promise = std::make_shared<std::promise<boost::optional<response_of<Request>>>>();
        wait_response_of<Request>([promise](boost::optional<response_of<Request>> const & opt){ promise->set_value(opt); }, duration);
        return promise->get_future();
    }

    /*! @brief Retrieves the association incoming connection.
     * @return The associated incoming connection.
     */

    in_connection_type& in_connection() 
    {
        return impl.in_connection();
    }

    /*! @brief Retrieves the association outgoing connection.
     * @return The associated outgoing connection.
     */

    new_dexterity::out_connection& out_connection() 
    {
        return impl.out_connection();
    }


private:
    template <typename Request>
    void queue_request_unchecked(Request const & request)
    {
        return impl.queue_request(request);
    }

    template <typename Request, typename Duration>
    std::future<boost::optional<response_of<Request>>> queue_request_unchecked(Request const & request, Duration duration)
    {
        auto promise = std::make_shared<std::promise<boost::optional<response_of<Request>>>>();
        queue_request_unchecked(request, [promise](boost::optional<response_of<Request>> const & opt){ promise->set_value(opt); }, duration);
        return promise->get_future();

    }

    template <typename Request, typename Callback, typename Duration>
    void queue_request_unchecked(Request const & request, Callback callback, Duration duration)
    {
        return impl.queue_request(request, callback, duration);
    }

    impl::communication_interface<CommunicationPolicy> impl;
};

} // new_dexterity

#endif

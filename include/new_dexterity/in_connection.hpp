#ifndef ND_IN_CONNECTION_HPP
#define ND_IN_CONNECTION_HPP

//! @file
//! @brief in_connection definition

#include <memory>
#include <vector>
#include <utility>
#include <cstdint>
#include <algorithm>
#include <iterator>
#include <mutex>
#include <new_dexterity/message_dispatcher.hpp>
#include <new_dexterity/write_stream.hpp>

namespace new_dexterity
{

/*! @brief A class representing the incoming connection
 * 
 * Multiple listeners may be interested in the incoming messages.
 * This class allows independent objects to subscribe to the incoming messages
 * of the stream.
 */

template <typename Parser>
class in_connection
{
public:
    using message_type = typename new_dexterity::message_dispatcher<Parser>::message_type;
    using callback_type = typename new_dexterity::message_dispatcher<Parser>::callback_type;

    /*! @brief Generic constructor suporting multiple Stream types.
     * @tparam AsyncReadStream A type supporting the following expressions:
     * @code{.cpp}
     * //s is AsyncReadStream, v is std::vector<std::uint8_t>
     * //c is std::function<void(boost::system::error_code, std::size_t)>
     * s.async_read_some(boost::asio::buffer(v), c);
     * @endcode
     * @param read_stream The stream to read from.
     */

    template <typename AsyncReadStream>
    in_connection(AsyncReadStream& read_stream)
        : dispatcher_(read_stream, [this](message_type const & msg) { handle_message(msg); })
        , unique_id_()
    {
    }

    /*! @brief Subscribes a callback to call whenever an incoming message is detected.
     * @param callback The callback to call.
     * @return A id used to unsubscribe when needed.
     */

    std::size_t subscribe(callback_type callback)
    {
        std::lock_guard<std::mutex> guard(subs_mutex_);
        subscribers_.emplace_back(unique_id_, callback);
        return unique_id_++;
    }

    /*! @brief Unsubscribes a previously subscribed callback.
     * @param id The id given when subscribed. 
     */

    void unsubscribe(std::size_t id)
    {
        std::lock_guard<std::mutex> guard(subs_mutex_);
        subscribers_.erase(
                std::remove_if(
                    subscribers_.begin(),
                    subscribers_.end(),
                    [id](std::pair<std::size_t, callback_type> const & p)
                    {
                        return p.first == id;
                    }),
                subscribers_.end());
    }

private:

    void handle_message(message_type const & message)
    {
        std::lock_guard<std::mutex> guard(subs_mutex_);

        for (auto& subscriber : subscribers_)
        {
            subscriber.second(message);
        }
    }

    new_dexterity::message_dispatcher<Parser> dispatcher_;
    std::mutex subs_mutex_;
    std::size_t unique_id_;
    std::vector<std::pair<std::size_t, callback_type>> subscribers_;
};

/* @brief RAII class for subscribing. Currently the only class providing subscription
 * is the @ref new_dexterity::in_connection class.
 */

template <typename Subscription>
class auto_subscribe
{
public:
    using  callback_type = typename Subscription::callback_type;

    auto_subscribe(Subscription& sub, callback_type callback)
        : sub_(sub)
        , id_(sub_.subscribe(callback))
    {
    }

    ~auto_subscribe()
    {
        sub_.unsubscribe(id_);
    }
    
private:
    Subscription& sub_;
    std::size_t id_;
};

} // new_dexterity

#endif

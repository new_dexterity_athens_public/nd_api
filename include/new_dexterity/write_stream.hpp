#ifndef ND_WRITE_STREAM_HPP
#define ND_WRITE_STREAM_HPP

//! @file
//! @brief SyncWriteStream type erasure interface.
#include <boost/asio.hpp>

namespace new_dexterity
{
    class write_stream
    {
    public:
        virtual ~write_stream() {}
        virtual void write(boost::asio::mutable_buffers_1 const & cb) = 0;
    };

    template <typename SyncWriteStream>
    class write_stream_wrapper : public write_stream
    {
    public:
        explicit write_stream_wrapper(SyncWriteStream& write_stream)
            : write_stream_(write_stream)
        {
        }

        void write(boost::asio::mutable_buffers_1 const & mb) final
        {
            asio::write(write_stream_, mb);
        }

    private:
        SyncWriteStream& write_stream_;
    };
}

#endif

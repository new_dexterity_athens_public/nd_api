#ifndef ND_MESSAGE_DISPATCHER_HPP
#define ND_MESSAGE_DISPATCHER_HPP

//! @file
//! @brief Message Dispatcher definition

#include <boost/asio.hpp>
#include <functional>
#include <memory>
#include <new_dexterity/async_read_stream.hpp>

namespace asio = boost::asio;

namespace new_dexterity
{
    /*! @brief Consumes a stream and calls a registered callback whenever a message is parsed.
     * @tparam Parser 
     * @code{.cpp}
     * // p is Parser, v is std::array<std::uint8_t, 1024>, n is std::size_t
     * p.parse(v, n);
     * for (auto const & m : p.parsed_messages());
     * p.clear();
     * @endcode
     */
    template <typename Parser>
    class message_dispatcher_worker : public std::enable_shared_from_this<message_dispatcher_worker<Parser>>
    {
    public:
        using message_type = typename Parser::message_type;
        using callback_type = std::function<void(const message_type&)>;

        /*! @brief Constructs an `std::shared_ptr<message_dispatcher_worker>`.
         *
         * We need this because the class derives from `std::enable_shared_from_this` which requires an `std::shared_ptr` instance.
         *
         * Why we need to derive from `std::enable_shared_from_this`:
         * 
         * The worker requires an existing read_stream to work. In the general case when constructing a worker it is not always
         * possible to close the stream or stop the associated `boost::asio::io_service`. Since the
         * worker registers a self method to be called asynchronously, it cannot be destructed before the asynchronous operation completes.
         * This ties the worker's lifetime with the operation to be completed meaning that we can't just call a destructor manually or when
         * going out of scope. What we need is to tie the lifetime of the worker as long as the method is registered as an asynchronous operation handler.
         * Using `std::enable_shared_from_this` and copying lambda capture we can increase the reference count of our worker until the lambda is destructed
         * after the operation completes.
         */
        template <typename... Args>
        static std::shared_ptr<message_dispatcher_worker<Parser>> create(Args&&... args)
        {
            return std::shared_ptr<message_dispatcher_worker>(new message_dispatcher_worker(std::forward<Args>(args)...));
        }


        /*! @brief Starts the stream reading process.
         */
        void start()
        {
            running_ = true;
            auto self(this->shared_from_this());
            read_stream_->async_read_some(asio::buffer(buffer_), [self](boost::system::error_code const & ec, std::size_t br){ self->process(ec, br); });
        }


        /*! @brief Stops the stream reading process.
         */
        void stop()
        {
            running_ = false;
        }


    private:
        /*! @brief Generic constructor supporting multiple Stream types.
         *  @tparam AsyncReadStream
         *  @code{.cpp}
         *  //s is AsyncReadStream, v is std::vector<std::uint8_t>
         *  //c is std::function<void(boost::system::error_code, std::size_t)>
         *  s.async_read_some(boost::asio::buffer(v), c);
         *  @endcode
         *  @param read_stream The stream to read from.
         *  @param callback The callback to call whenever a message is parsed.
         */
        template <typename AsyncReadStream>
        message_dispatcher_worker(AsyncReadStream& read_stream, callback_type callback)
            : read_stream_(new new_dexterity::async_read_stream_wrapper<AsyncReadStream>(read_stream))
            , running_(false)
            , callback_(callback)
        {
        }

        void process(boost::system::error_code const & ec, std::size_t bytes_received)
        {
            if (ec || !running_)
            {
                return;
            }

            parser_.parse(buffer_, bytes_received);

            for (auto const & parsed_message : parser_.parsed_messages())
            {
                    callback_(parsed_message);
            }

            parser_.clear();

            auto self(this->shared_from_this());
            read_stream_->async_read_some(asio::buffer(buffer_), [self](boost::system::error_code const & ec, std::size_t br){ self->process(ec, br); });
        }

        std::unique_ptr<new_dexterity::async_read_stream> read_stream_;
        Parser parser_;
        std::array<std::uint8_t, 1024> buffer_;
        bool running_;
        callback_type callback_;
    };


    /*! @brief Convenience start/stop message_dispatcher_worker RAII wrapper.
     */
    template <typename Parser>
    class message_dispatcher
    {
    public:
        using message_type = typename Parser::message_type;
        using callback_type = typename new_dexterity::message_dispatcher_worker<Parser>::callback_type;

        template <typename... Args>
        explicit message_dispatcher(Args&&... args)
            : worker_(new_dexterity::message_dispatcher_worker<Parser>::create(std::forward<Args>(args)...))
        {
            worker_->start();
        }

        message_dispatcher(const message_dispatcher&) = delete;
        message_dispatcher& operator=(const message_dispatcher&) = delete;

        message_dispatcher(message_dispatcher&&) = default;
        message_dispatcher& operator=(message_dispatcher&&) = default;

        ~message_dispatcher()
        {
            worker_->stop();
        }

    private:
        std::shared_ptr<new_dexterity::message_dispatcher_worker<Parser>> worker_;
    };

}

#endif

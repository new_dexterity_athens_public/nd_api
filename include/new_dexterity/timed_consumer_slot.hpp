#ifndef ND_TIMED_CONSUMER_SLOT_HPP
#define ND_TIMED_CONSUMER_SLOT_HPP

//! @file
//! @brief Implementation of timed consumer slots used in the definition of the Communication Interface.

#include <mutex>
#include <condition_variable>
#include <functional>
#include <boost/optional.hpp>
#include <atomic>
#include <boost/variant.hpp>
#include <new_dexterity/utilities.hpp>
#include <thread>

namespace new_dexterity
{

template <typename Consumable>
class timed_consumer_slot
{
    struct initializing {};
    struct waiting_for_consumer {};
    struct waiting_for_consumable { std::function<void (const boost::optional<Consumable>&)> consumer; 
                                    std::chrono::milliseconds limit; };

public:
    timed_consumer_slot()
        : running_(true)
        , state_(initializing{})

    {
        std::unique_lock<std::mutex> lock(mutex_);

        thread_ = std::thread([this](){process();});
        condition_.wait(lock, [&]{ return state_.type() == typeid(waiting_for_consumer); } );
    }

    ~timed_consumer_slot()
    {
        {
            std::lock_guard<std::mutex> lock(mutex_);
            running_ = false;
            condition_.notify_all();
        }

        thread_.join();
    }

    template <typename Consumer, typename Duration>
    void set_consumer(Consumer consumer, Duration duration)
    {
        std::unique_lock<std::mutex> lock(mutex_);

        condition_.wait(lock, [&]{ return state_.type() == typeid(waiting_for_consumer); } );

        state_ = waiting_for_consumable { consumer, std::chrono::duration_cast<std::chrono::milliseconds>(duration) };

        condition_.notify_all();

    }
    
    void feed(Consumable const & consumable)
    {
        std::unique_lock<std::mutex> lock(mutex_);

        new_dexterity::match(state_,
                [&,this](waiting_for_consumable const & waiting)
                {
                    waiting.consumer(consumable);
                    state_ = waiting_for_consumer {};
                    condition_.notify_all();
                },
                new_dexterity::ignore{});
    }

private:

    void process()
    {
        std::unique_lock<std::mutex> lock(mutex_);
        while (running_)
        {
            new_dexterity::match(state_,
                    [&, this](const initializing&)
                    {
                        state_ = waiting_for_consumer{};
                        condition_.notify_all();
                    },
                    [&, this](const waiting_for_consumer&)
                    {
                        condition_.wait(lock);
                    },
                    [&, this](waiting_for_consumable const & waiting)
                    {
                        auto result = condition_.wait_for(lock, waiting.limit, [&]{ return state_.type() == typeid(waiting_for_consumer); });

                        if (!running_) return;

                        if (!result)
                        {
                            waiting.consumer(boost::none);
                            state_ = waiting_for_consumer {};
                            condition_.notify_all();
                        }
                    });
        }
        
    }

    bool running_;
    std::mutex mutex_;
    std::thread thread_;
    std::condition_variable condition_;
    boost::variant<initializing, waiting_for_consumer, waiting_for_consumable> state_;
};


} // new_dexteity

#endif

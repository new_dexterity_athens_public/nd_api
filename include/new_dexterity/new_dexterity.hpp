#ifndef ND_NEW_DEXTERITY_HPP
#define ND_NEW_DEXTERITY_HPP

//! @file
//! @brief Convenience header

#include <new_dexterity/in_connection.hpp>
#include <new_dexterity/out_connection.hpp>
#include <new_dexterity/communication_interface.hpp>
#include <new_dexterity/utilities.hpp>
#include <new_dexterity/ndx_protocol.hpp>
#include <new_dexterity/pipe.hpp>
#include <new_dexterity/metaprogramming.hpp>

//! @brief New Dexterity Namespace
namespace new_dexterity {}

#endif

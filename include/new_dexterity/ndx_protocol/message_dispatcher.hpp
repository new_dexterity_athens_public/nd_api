#ifndef ND_NDX_PROTOCOL_MESSAGE_DISPATCHER_HPP
#define ND_NDX_PROTOCOL_MESSAGE_DISPATCHER_HPP

#include <new_dexterity/message_dispatcher.hpp>
#include <new_dexterity/ndx_protocol/message_parser.hpp>

namespace new_dexterity
{
namespace ndx_protocol
{

    using message_dispatcher = new_dexterity::message_dispatcher<new_dexterity::ndx_protocol::message_parser>;

}
}

#endif

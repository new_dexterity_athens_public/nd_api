#ifndef ND_NDX_PROTOCOL_REQUESTS_HPP
#define ND_NDX_PROTOCOL_REQUESTS_HPP

#include <new_dexterity/utilities.hpp>
#include <vector>
#include <utility>
#include <cstdint>
#include <tuple>
#include <boost/optional.hpp>
#include <boost/operators.hpp>

namespace new_dexterity
{
namespace ndx_protocol
{

struct hand_id_request { };

inline bool operator==(hand_id_request const & lhs, hand_id_request const & rhs)
{
    return true;
}


struct actuator_selection 
{
    bool thumb_actuator; 
    bool index_actuator;
    bool middle_actuator;
    bool ring_pinky_actuator;
    bool thumb_opposition_actuator;
};

inline bool operator==(actuator_selection const & lhs, actuator_selection const & rhs)
{
        return std::tie(lhs.thumb_actuator, lhs.index_actuator, lhs.middle_actuator, lhs.ring_pinky_actuator, lhs.thumb_opposition_actuator) ==
               std::tie(rhs.thumb_actuator, rhs.index_actuator, rhs.middle_actuator, rhs.ring_pinky_actuator, rhs.thumb_opposition_actuator);
}

struct actuator_info_request { };

inline bool operator==(actuator_info_request const & lhs, actuator_info_request const & rhs)
{
    return true;
}



struct actuator_command 
{
    float position;
    float velocity;
    float torque;
};

inline bool operator==(actuator_command const & lhs, actuator_command const & rhs)
{
    return std::tie(lhs.position, lhs.velocity, lhs.torque) == std::tie(rhs.position, rhs.velocity, rhs.torque);
    
}


struct actuator_commands
{
    boost::optional<actuator_command> thumb_actuator_command;
    boost::optional<actuator_command> index_actuator_command;
    boost::optional<actuator_command> middle_actuator_command;
    boost::optional<actuator_command> ring_pinky_actuator_command;
    boost::optional<actuator_command> thumb_opposition_actuator_command;
};

inline bool operator==(actuator_commands const & lhs, actuator_commands const & rhs)
{
    return std::tie(lhs.thumb_actuator_command, lhs.index_actuator_command, lhs.middle_actuator_command, lhs.ring_pinky_actuator_command, lhs.thumb_opposition_actuator_command) ==
           std::tie(rhs.thumb_actuator_command, rhs.index_actuator_command, rhs.middle_actuator_command, rhs.ring_pinky_actuator_command, rhs.thumb_opposition_actuator_command);

}

struct command_actuators_request
{
    actuator_commands commands;
};

inline bool operator==(command_actuators_request const & lhs, command_actuators_request const & rhs)
{
    return lhs.commands == rhs.commands;
}


struct hand_status_request {};

inline bool operator==(hand_status_request const & lhs, hand_status_request const & rhs)
{
    return true;
}

struct communication_status_request {};

inline bool operator==(communication_status_request const & lhs, communication_status_request const & rhs)
{
    return true;
}

struct actuators_status_request
{
    actuator_selection actuators;
};

inline bool operator==(actuators_status_request const & lhs, actuators_status_request const & rhs)
{
    return lhs.actuators == rhs.actuators;
}

template <std::size_t Address, std::size_t Length>
struct read_config_request
{
};

struct dynamic_read_config_request
{
    std::uint16_t address; 
    std::uint16_t length;
};

template <std::size_t Address1, std::size_t Address2, std::size_t Length1, std::size_t Length2>
inline bool operator==(read_config_request<Address1, Length1> const & lhs, read_config_request<Address2, Length2> const & rhs)
{
    return Address1 == Address2 && Length1 == Length2;
}

template <std::size_t Address, std::size_t Length>
struct write_config_request
{
    bool save;
    std::array<std::uint8_t, Length> data;
};

struct dynamic_write_config_request
{
    bool save;
    std::uint16_t address; 
    std::vector<std::uint8_t> data;
};

template <std::size_t Address1, std::size_t Address2, std::size_t Length1, std::size_t Length2>
inline bool operator==(write_config_request<Address1, Length1> const & lhs, write_config_request<Address2, Length2> const & rhs)
{
    return Address1 == Address2 && Length1 == Length2 && lhs.save == rhs.save && lhs.data == rhs.data;
}

struct software_calibration_request {};

inline bool operator==(software_calibration_request const & lhs, software_calibration_request const & rhs)
{
    return true;
}

struct hardware_calibration_request {};

inline bool operator==(hardware_calibration_request const & lhs, hardware_calibration_request const & rhs)
{
    return true;
}

}
}//new_dexterity

#endif

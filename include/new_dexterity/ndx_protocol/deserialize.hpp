#ifndef ND_NDX_PROTOCOL_DESERIALIZE_HPP

#include <new_dexterity/ndx_protocol/deserialize_base.hpp>
#include <new_dexterity/ndx_protocol/deserialize_request.hpp>
#include <new_dexterity/ndx_protocol/deserialize_response.hpp>

#endif

#ifndef ND_NDX_PROTOCOL_COMMUNICATION_POLICY_HPP
#define ND_NDX_PROTOCOL_COMMUNICATION_POLICY_HPP

#include <tuple>
#include <new_dexterity/ndx_protocol/response_of.hpp>
#include <new_dexterity/ndx_protocol/message_parser.hpp>
#include <new_dexterity/ndx_protocol/serialize.hpp>
#include <new_dexterity/ndx_protocol/deserialize.hpp>
#include <boost/optional.hpp>

namespace new_dexterity
{
    namespace ndx_protocol
    {

        struct communication_policy
        {
            using parser_type = message_parser;

            using message_type = parser_type::message_type;

            template <typename Request>
            using response_of = typename new_dexterity::ndx_protocol::response_of<Request>::type;

            using implicit_identifier = std::uint8_t;

            template <typename Buffer, typename Request>
            static void serialize(implicit_identifier x, Buffer & y, Request const & z) 
            {
                new_dexterity::ndx_protocol::serialize(x, y, z);
            }

            template <typename Response>
            static boost::optional<Response> 
            deserialize(implicit_identifier x, const message_type& msg)
            {
                if (msg.first.id != x)
                    return boost::none;

                return new_dexterity::ndx_protocol::deserializer<Response>::deserialize(msg);
            }

            static constexpr bool can_distinguish_responses = true;
        };
    }
}

#endif

#ifndef ND_NDX_PROTOCOL_RESPONSE_OF_HPP
#define ND_NDX_PROTOCOL_RESPONSE_OF_HPP

#include <new_dexterity/ndx_protocol/requests.hpp>
#include <new_dexterity/ndx_protocol/responses.hpp>
#include <boost/variant.hpp>

namespace new_dexterity
{
namespace ndx_protocol
{

template <typename T>
struct response_of;

template <class T>
using response_of_t = typename response_of<T>::type;

template <>
struct response_of<actuator_info_request>
{
    using type = actuator_info_response;
};


template <>
struct response_of<command_actuators_request>
{
    using type = boost::variant<command_actuators_request_acknowledged, command_actuators_request_not_acknowledged>;
};


template <>
struct response_of<hand_status_request>
{
    using type = hand_status_response;
};


template <>
struct response_of<communication_status_request>
{
    using type = communication_status_response;
};

template <>
struct response_of<actuators_status_request>
{
    using type = actuators_status_response;
};


template <std::size_t Address, std::size_t Length>
struct response_of<read_config_request<Address, Length>>
{
    using type = read_config_response<Length>;
};

template <std::size_t Address, std::size_t Length>
struct response_of<write_config_request<Address, Length>>
{
    using type = write_config_response;
};

template <>
struct response_of<dynamic_read_config_request>
{
    using type = dynamic_read_config_response;
};

template <>
struct response_of<dynamic_write_config_request>
{
    using type = write_config_response;
};

}
} //new_dexterity

#endif

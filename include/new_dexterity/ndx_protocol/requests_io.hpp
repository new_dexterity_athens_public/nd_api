#ifndef ND_NDX_PROTOCOL_REQUESTS_IO_HPP
#define ND_NDX_PROTOCOL_REQUESTS_IO_HPP

#include <iostream>
#include <boost/optional/optional_io.hpp>
#include <new_dexterity/ndx_protocol/requests.hpp>

namespace new_dexterity
{
namespace ndx_protocol
{

inline std::ostream& operator<<(std::ostream& lhs, actuator_selection const & rhs)
{
    return lhs << "actuator_selection { thumb_actuator = " << rhs.thumb_actuator << 
                                     ", index_actuator = " << rhs.index_actuator <<
                                     ", middle_actuator = " << rhs.middle_actuator <<
                                     ", ring_pinky_actuator = " << rhs.ring_pinky_actuator <<
                                     ", thumb_opposition_actuator = " << rhs.thumb_opposition_actuator << " }";
}

inline std::ostream& operator<<(std::ostream& lhs, actuator_info_request const & rhs)
{
    return lhs << "actuator_info_request {}";
}

inline std::ostream& operator<<(std::ostream& lhs, actuator_command const & rhs)
{
    return lhs << "actuator_command { position = " << rhs.position << ", velocity = " << rhs.velocity << ", torque = " << rhs.torque << " }";
}

inline std::ostream& operator<<(std::ostream& lhs, actuator_commands const & rhs)
{
    return lhs << "actuator_commands { thumb_actuator_command = " << rhs.thumb_actuator_command <<
                                    ", index_actuator_command = " << rhs.index_actuator_command <<
                                    ", middle_actuator_command = " << rhs.middle_actuator_command <<
                                    ", ring_pinky_actuator_command = " << rhs.ring_pinky_actuator_command <<
                                    ", thumb_opposition_actuator_command = " << rhs.thumb_opposition_actuator_command << " }";
}

inline std::ostream& operator<<(std::ostream& lhs, command_actuators_request const & rhs)
{
    return lhs << "command_actuators_request { commands = " << rhs.commands << " }";
}

inline std::ostream& operator<<(std::ostream& lhs, hand_status_request const & rhs)
{
    return lhs << "hand_status_request { }";
}

inline std::ostream& operator<<(std::ostream& lhs, communication_status_request const & rhs)
{
    return lhs << "communication_status_request { }";
}

inline std::ostream& operator<<(std::ostream& lhs, actuators_status_request const & rhs)
{
    return lhs << "actuators_status_request { selection = " << rhs.actuators << " }";
}


}
} // new_dexterity

#endif

#ifndef ND_NDX_PROTOCOL_RESPONSES_HPP
#define ND_NDX_PROTOCOL_RESPONSES_HPP

#include <boost/variant.hpp>
#include <boost/optional.hpp>
#include <array>

namespace new_dexterity
{
namespace ndx_protocol
{

struct hand_id_response { std::uint8_t id; };

inline bool operator==(hand_id_response const & lhs, hand_id_response const & rhs)
{
    return true;
}

struct command_actuators_request_acknowledged {};

inline bool operator==(command_actuators_request_acknowledged const & lhs, command_actuators_request_acknowledged const & rhs)
{
    return true;
}

enum class command_actuators_request_not_acknowledged
{
    wrong_actuator_num,
    wrong_command
};

using command_actuators_response = boost::variant<command_actuators_request_acknowledged, command_actuators_request_not_acknowledged>;

struct actuator_info
{
    float position;
    float velocity;
    float torque;
    bool moving;
    std::int16_t temperature;
    float desired_actuation;
};

inline bool operator==(actuator_info const & lhs, actuator_info const & rhs)
{
    return std::tie(lhs.position, lhs.velocity, lhs.torque, lhs.moving, lhs.temperature, lhs.desired_actuation) == 
           std::tie(rhs.position, rhs.velocity, rhs.torque, rhs.moving, rhs.temperature, rhs.desired_actuation);
}

enum class actuator_error
{
    overload,
    electrical_shock,
    disconnected,
    voltage_error,
    software_error,
    out_of_limits,
    overheating,
    encoder_error,
    out_of_range,
    unknown_error
};

struct actuator_info_response
{
    boost::variant<actuator_info, actuator_error> thumb_actuator_info_response;
    boost::variant<actuator_info, actuator_error> index_actuator_info_response;
    boost::variant<actuator_info, actuator_error> middle_actuator_info_response;
    boost::variant<actuator_info, actuator_error> ring_pinky_actuator_info_response;
    boost::variant<actuator_info, actuator_error> thumb_opposition_actuator_info_response;
};

inline bool operator==(actuator_info_response const & lhs, actuator_info_response const & rhs)
{
    return std::tie(lhs.thumb_actuator_info_response, lhs.index_actuator_info_response, lhs.middle_actuator_info_response, 
                    lhs.ring_pinky_actuator_info_response, lhs.thumb_opposition_actuator_info_response) ==
           std::tie(rhs.thumb_actuator_info_response, rhs.index_actuator_info_response, rhs.middle_actuator_info_response, 
                    rhs.ring_pinky_actuator_info_response, rhs.thumb_opposition_actuator_info_response);
}

struct hand_status_response
{
    std::uint32_t mcu_time;
    std::uint16_t mcu_temperature;
    std::uint16_t last_hand_error_code;  
    std::uint32_t received_commands;
    std::uint32_t valid_received_commands;
    std::uint32_t executed_commands;
};

inline bool operator==(hand_status_response const & lhs, hand_status_response const & rhs)
{
    return std::tie(lhs.mcu_time, lhs.mcu_temperature, lhs.last_hand_error_code,  lhs.received_commands, lhs.valid_received_commands, lhs.executed_commands) ==
           std::tie(rhs.mcu_time, rhs.mcu_temperature, rhs.last_hand_error_code,  rhs.received_commands, rhs.valid_received_commands, rhs.executed_commands);
}

struct communication_status_response
{
    std::uint32_t mcu_time;
    std::uint16_t last_communication_error_code;
    std::uint32_t received_packets;
    std::uint32_t valid_received_packets;
    std::uint32_t prepared_packets;
    std::uint32_t transmitted_packets;
};

inline bool operator==(communication_status_response const & lhs, communication_status_response const & rhs)
{
    return std::tie(lhs.mcu_time, lhs.last_communication_error_code, lhs.received_packets, lhs.valid_received_packets, lhs.prepared_packets, lhs.transmitted_packets) == 
           std::tie(rhs.mcu_time, rhs.last_communication_error_code, rhs.received_packets, rhs.valid_received_packets, rhs.prepared_packets, rhs.transmitted_packets);

}

struct actuator_status
{
    std::uint16_t last_actuator_error_code;
    std::uint16_t max_temperature;
    std::uint16_t min_temperature;
    std::uint16_t avg_temperature;
    std::uint16_t max_torque;
    std::uint16_t min_torque;
    std::uint16_t avg_torque;
};

inline bool operator==(actuator_status const & lhs, actuator_status const & rhs)
{
    return std::tie(lhs.last_actuator_error_code, lhs.max_temperature, lhs.min_temperature, lhs.avg_temperature, lhs.max_torque, lhs.min_torque, lhs.avg_torque) ==
           std::tie(rhs.last_actuator_error_code, rhs.max_temperature, rhs.min_temperature, rhs.avg_temperature, rhs.max_torque, rhs.min_torque, rhs.avg_torque);
}

struct actuators_status_response
{
    std::uint32_t mcu_time;
    boost::optional<actuator_status> thumb_actuator_status;
    boost::optional<actuator_status> index_actuator_status;
    boost::optional<actuator_status> middle_actuator_status;
    boost::optional<actuator_status> ring_pinky_actuator_status;
    boost::optional<actuator_status> thumb_opposition_actuator_status;
};

inline bool operator==(actuators_status_response const & lhs, actuators_status_response const & rhs)
{
    return std::tie(lhs.mcu_time, lhs.thumb_actuator_status, lhs.index_actuator_status, lhs.middle_actuator_status, lhs.ring_pinky_actuator_status, lhs.thumb_opposition_actuator_status) == 
           std::tie(rhs.mcu_time, rhs.thumb_actuator_status, rhs.index_actuator_status, rhs.middle_actuator_status, rhs.ring_pinky_actuator_status, rhs.thumb_opposition_actuator_status);
}

enum class read_config_request_not_acknowledged
{
    incorrect_eeprom_address,
    eeprom_hardware_error
};

template <std::size_t Length>
struct config
{
    std::array<std::uint8_t, Length> data;
};

struct dynamic_config
{
    std::vector<std::uint8_t> data;
};

template <std::size_t Length1, std::size_t Length2>
inline bool operator==(config<Length1> const & lhs, config<Length2> const & rhs)
{
    return Length1 && Length2 && lhs.data == rhs.data;
}

template <std::size_t Length>
using read_config_response = boost::variant<
      config<Length>, 
      read_config_request_not_acknowledged>;

using dynamic_read_config_response = boost::variant<
      dynamic_config,
      read_config_request_not_acknowledged>;

struct write_config_request_acknowledged {};

inline bool operator==(write_config_request_acknowledged const & lhs, write_config_request_acknowledged const & rhs)
{
    return true;
}

enum class write_config_request_not_acknowledged
{
    incorrect_eeprom_address,
    eeprom_hardware_error,
    incorrect_data_format,
    incorrect_data_length
};

using write_config_response = boost::variant<write_config_request_acknowledged, write_config_request_not_acknowledged>;

}
} //new_dexterity

#endif

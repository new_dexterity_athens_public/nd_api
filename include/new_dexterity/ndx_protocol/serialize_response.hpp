#ifndef ND_NDX_PROTOCOL_SERIALIZE_RESPONSE_HPP
#define ND_NDX_PROTOCOL_SERIALIZE_RESPONSE_HPP

#include <new_dexterity/ndx_protocol/responses.hpp>
#include <new_dexterity/utilities.hpp>

namespace new_dexterity
{
namespace ndx_protocol
{

    template <typename Buffer>
    void serialize(std::uint8_t hand_id, Buffer& buffer, new_dexterity::ndx_protocol::hand_id_response const & response)
    {
        NDHeader header;
        header.header = NDHEAD;
        header.id = hand_id;
        header.class_id = MSG_CLASS;
        header.msg_id = ID_ID;
        header.payload_len = 0;

        calculate_header_checksum(&header);

        std::copy_n(reinterpret_cast<std::uint8_t*>(&header), sizeof(header), std::back_inserter(buffer));
    }

    template <typename Buffer>
    void serialize(std::uint8_t hand_id, Buffer& buffer, new_dexterity::ndx_protocol::command_actuators_response const & response)
    {
        new_dexterity::match(response,
                [&](const new_dexterity::ndx_protocol::command_actuators_request_acknowledged)
                {
                    NDHeader header;
                    header.header = NDHEAD;
                    header.id = hand_id;
                    header.class_id = ACK_CLASS;
                    header.msg_id = ACK_ID;
                    header.payload_len = sizeof(ACK_struct);

                    calculate_header_checksum(&header);

                    const std::size_t start = buffer.size();

                    std::copy_n(reinterpret_cast<std::uint8_t*>(&header), sizeof(header), std::back_inserter(buffer));

                    ACK_struct ack;
                    ack.class_id = COM_CLASS;
                    ack.msg_id = ACOM_ID;
                    ack.err_code = 0;

                    std::copy_n(reinterpret_cast<std::uint8_t*>(&ack), sizeof(ack), std::back_inserter(buffer));
                    buffer.push_back({});
                    buffer.push_back({});

                    calculate_checksum(buffer.data() + start);
                },
                [&](const new_dexterity::ndx_protocol::command_actuators_request_not_acknowledged nack)
                {
                    NDHeader header;
                    header.header = NDHEAD;
                    header.id = hand_id;
                    header.class_id = ACK_CLASS;
                    header.msg_id = NACK_ID;
                    header.payload_len = sizeof(ACK_struct);

                    calculate_header_checksum(&header);

                    const std::size_t start = buffer.size();

                    std::copy_n(reinterpret_cast<std::uint8_t*>(&header), sizeof(header), std::back_inserter(buffer));
                    ACK_struct ack;
                    ack.class_id = COM_CLASS;
                    ack.msg_id = ACOM_ID;
                    ack.err_code = 0;

                    if (nack == new_dexterity::ndx_protocol::command_actuators_request_not_acknowledged::wrong_actuator_num)
                        ack.err_code = A_WRONG_MOTOR_NUM;

                    if (nack == new_dexterity::ndx_protocol::command_actuators_request_not_acknowledged::wrong_command)
                        ack.err_code = WRONG_COMMAND;

                    std::copy_n(reinterpret_cast<std::uint8_t*>(&ack), sizeof(ack), std::back_inserter(buffer));
                    buffer.push_back({});
                    buffer.push_back({});

                    calculate_checksum(buffer.data() + start);
                });
    }

    template <typename Buffer>
    void serialize(std::uint8_t hand_id, Buffer& buffer, new_dexterity::ndx_protocol::actuator_info_response const & response)
    {
        NDHeader header;
        header.header = NDHEAD;
        header.id = hand_id;
        header.class_id = MSG_CLASS;
        header.msg_id = ACT_ID;
        header.payload_len = sizeof(ACT_header) + 5 * sizeof(ACTx_struct);

        calculate_header_checksum(&header);

        const std::size_t start = buffer.size();

        std::copy_n(reinterpret_cast<std::uint8_t*>(&header), sizeof(header), std::back_inserter(buffer));

        ACT_header aheader = {0, 5, 0};

        std::copy_n(reinterpret_cast<std::uint8_t*>(&aheader), sizeof(aheader), std::back_inserter(buffer));

        ACTx_struct c1;
        ACTx_struct c2;
        ACTx_struct c3;
        ACTx_struct c4;
        ACTx_struct c5;

        auto fill = [&](ACTx_struct& c, std::uint8_t id, boost::variant<new_dexterity::ndx_protocol::actuator_info, new_dexterity::ndx_protocol::actuator_error> const & v)
        {
            new_dexterity::match(v,
                    [&](new_dexterity::ndx_protocol::actuator_info const & info)
                    {
                        c = {id, static_cast<std::uint8_t>(id == 5 ? MOTOR_PWM : DYNX_MOTOR), CONNECTED, info.moving, info.temperature, info.position, info.velocity, info.torque, info.desired_actuation};
                    },
                    [&](new_dexterity::ndx_protocol::actuator_error const & error)
                    {
                        std::uint8_t err;
                        if (error == new_dexterity::ndx_protocol::actuator_error::disconnected) err = DISCONNECTED;
                        if (error == new_dexterity::ndx_protocol::actuator_error::out_of_limits) err = OUT_OF_LIMITS;
                        if (error == new_dexterity::ndx_protocol::actuator_error::out_of_range) err = OUT_OF_RANGE;
                        if (error == new_dexterity::ndx_protocol::actuator_error::software_error) err = SOFTWARE_ERROR;
                        if (error == new_dexterity::ndx_protocol::actuator_error::overload) err = OVERLOAD;
                        if (error == new_dexterity::ndx_protocol::actuator_error::electrical_shock) err = ELECTRICAL_SHOCK;
                        if (error == new_dexterity::ndx_protocol::actuator_error::encoder_error) err = ENCODER_ERROR;
                        if (error == new_dexterity::ndx_protocol::actuator_error::overheating) err = OVERHEATING;
                        if (error == new_dexterity::ndx_protocol::actuator_error::voltage_error) err = VOLTAGE_ERROR;
                        if (error == new_dexterity::ndx_protocol::actuator_error::unknown_error) err = (1 << 8) - 1;
                        c = {id, static_cast<std::uint8_t>(id == 5 ? MOTOR_PWM : DYNX_MOTOR), err, 0, 0, 0, 0, 0 };
                    });
        };

        fill(c1, 1, response.thumb_actuator_info_response);
        fill(c2, 2, response.index_actuator_info_response);
        fill(c3, 3, response.middle_actuator_info_response);
        fill(c4, 4, response.ring_pinky_actuator_info_response);
        fill(c5, 5, response.thumb_opposition_actuator_info_response);

        std::copy_n(reinterpret_cast<std::uint8_t*>(&c1), sizeof(c1), std::back_inserter(buffer));
        std::copy_n(reinterpret_cast<std::uint8_t*>(&c2), sizeof(c2), std::back_inserter(buffer));
        std::copy_n(reinterpret_cast<std::uint8_t*>(&c3), sizeof(c3), std::back_inserter(buffer));
        std::copy_n(reinterpret_cast<std::uint8_t*>(&c4), sizeof(c4), std::back_inserter(buffer));
        std::copy_n(reinterpret_cast<std::uint8_t*>(&c5), sizeof(c5), std::back_inserter(buffer));

        buffer.push_back({});
        buffer.push_back({});

        calculate_checksum(buffer.data() + start);
    }

    template <typename Buffer>
    void serialize(std::uint8_t hand_id, Buffer& buffer, new_dexterity::ndx_protocol::hand_status_response const & response)
    {
        NDHeader header;
        header.header = NDHEAD;
        header.id = hand_id;
        header.class_id = MSG_CLASS;
        header.msg_id = STH_ID;
        header.payload_len = sizeof(HSTATUS_msg) - sizeof(NDHeader) - 2;

        calculate_header_checksum(&header);

        HSTATUS_msg msg;
        msg.header = header;
        msg.time = response.mcu_time;
        msg.temperature = response.mcu_temperature;
        msg.last_error_of_hand = response.last_hand_error_code;
        msg.commands_recv = response.received_commands;
        msg.commands_recv_valid = response.valid_received_commands;
        msg.commands_exec = response.executed_commands;

        calculate_checksum(reinterpret_cast<std::uint8_t*>(&msg));

        std::copy_n(reinterpret_cast<std::uint8_t*>(&msg), sizeof(msg), std::back_inserter(buffer));
    }

    template <typename Buffer>
    void serialize(std::uint8_t hand_id, Buffer& buffer, new_dexterity::ndx_protocol::communication_status_response const & response)
    {
        NDHeader header;
        header.header = NDHEAD;
        header.id = hand_id;
        header.class_id = MSG_CLASS;
        header.msg_id = STC_ID;
        header.payload_len = sizeof(CSTATUS_msg) - sizeof(NDHeader) - 2;

        calculate_header_checksum(&header);


        CSTATUS_msg msg;
        msg.header = header;
        msg.time = response.mcu_time;
        msg.reserved1 = 0;
        msg.last_error_of_comm = response.last_communication_error_code;
        msg.recv_packets_all = response.received_packets;
        msg.recv_packets_valid = response.valid_received_packets;
        msg.tran_packets_all = response.prepared_packets;
        msg.tran_packets_valid = response.transmitted_packets;

        calculate_checksum(reinterpret_cast<std::uint8_t*>(&msg));

        std::copy_n(reinterpret_cast<std::uint8_t*>(&msg), sizeof(msg), std::back_inserter(buffer));
    }

    template <typename Buffer>
    void serialize(std::uint8_t hand_id, Buffer& buffer, new_dexterity::ndx_protocol::actuators_status_response const & response)
    {

        std::uint8_t num = 0;
        if (response.thumb_actuator_status) ++num;
        if (response.index_actuator_status) ++num;
        if (response.middle_actuator_status) ++num;
        if (response.ring_pinky_actuator_status) ++num;
        if (response.thumb_opposition_actuator_status) ++num;

        const std::size_t start = buffer.size(); 

        NDHeader header;
        header.header = NDHEAD;
        header.id = hand_id;
        header.class_id = MSG_CLASS;
        header.msg_id = STA_ID;
        header.payload_len = sizeof(response.mcu_time) + 1 + num * sizeof(ASTATUSx_struct);

        calculate_header_checksum(&header);

        std::copy_n(reinterpret_cast<std::uint8_t const*>(&header), sizeof(header), std::back_inserter(buffer));

        std::copy_n(reinterpret_cast<std::uint8_t const*>(&response.mcu_time), sizeof(response.mcu_time), std::back_inserter(buffer));

        buffer.push_back(num);

        auto fill = [&](std::uint8_t id, boost::optional<new_dexterity::ndx_protocol::actuator_status> const & status)
        {
            if (status)
            {
                ASTATUSx_struct ast;
                ast.id = id;
                ast.type = (id == 5) ? MOTOR_PWM : DYNX_MOTOR;
                ast.temperature_max = status->max_temperature;
                ast.temperature_avg = status->avg_temperature;
                ast.temperature_min = status->min_temperature;
                ast.torque_max = status->max_torque;
                ast.torque_min = status->min_torque;
                ast.torque_avg = status->avg_torque;
                ast.last_error = status->last_actuator_error_code;

                std::copy_n(reinterpret_cast<std::uint8_t*>(&ast), sizeof(ast), std::back_inserter(buffer));
            }
        };
        
        fill(1, response.thumb_actuator_status);
        fill(2, response.index_actuator_status);
        fill(3, response.middle_actuator_status);
        fill(4, response.ring_pinky_actuator_status);
        fill(5, response.thumb_opposition_actuator_status);

        buffer.push_back({});
        buffer.push_back({});

        calculate_checksum(buffer.data() + start);
    }
    template <typename Buffer, std::size_t Length>
    void serialize(std::uint8_t hand_id, Buffer& buffer, new_dexterity::ndx_protocol::read_config_response<Length> const & response)
    {
        new_dexterity::match(response,
                [&](config<Length> const & config)
                {
                    NDHeader header;
                    header.header = NDHEAD;
                    header.id = hand_id;
                    header.class_id = CFG_CLASS;
                    header.msg_id = UCF_ID;
                    header.payload_len = static_cast<std::uint8_t>(Length);
                    
                    calculate_header_checksum(&header);

                    const std::size_t start = buffer.size();

                    std::copy_n(reinterpret_cast<std::uint8_t*>(&header), sizeof(header), std::back_inserter(buffer));

                    std::copy_n(config.data.begin(), Length, std::back_inserter(buffer));

                    buffer.push_back({});
                    buffer.push_back({});

                    calculate_checksum(buffer.data() + start);
                },
                [&](read_config_request_not_acknowledged const & nack)
                {
                    NDHeader header;
                    header.header = NDHEAD;
                    header.id = hand_id;
                    header.class_id = ACK_CLASS;
                    header.msg_id = NACK_ID;
                    header.payload_len = sizeof(ACK_struct);

                    calculate_header_checksum(&header);

                    const std::size_t start = buffer.size();

                    std::copy_n(reinterpret_cast<std::uint8_t*>(&header), sizeof(header), std::back_inserter(buffer));
                    ACK_struct ack;
                    ack.class_id = CFG_CLASS;
                    ack.msg_id = UCF_ID;

                    if (nack == new_dexterity::ndx_protocol::read_config_request_not_acknowledged::eeprom_hardware_error)
                        ack.err_code = CFG_FLASH_ERROR;
                    if (nack == new_dexterity::ndx_protocol::read_config_request_not_acknowledged::incorrect_eeprom_address)
                        ack.err_code = CFG_ACCESS_ERROR;

                    std::copy_n(reinterpret_cast<std::uint8_t*>(&ack), sizeof(ack), std::back_inserter(buffer));

                    buffer.push_back({});
                    buffer.push_back({});

                    calculate_checksum(buffer.data() + start);
                });
    }

    template <typename Buffer>
    void serialize(std::uint8_t hand_id, Buffer& buffer, new_dexterity::ndx_protocol::write_config_response const & response)
    {
        new_dexterity::match(response,
                [&](write_config_request_acknowledged)
                {
                    NDHeader header;
                    header.header = NDHEAD;
                    header.id = hand_id;
                    header.class_id = ACK_CLASS;
                    header.msg_id = ACK_ID;
                    header.payload_len = sizeof(ACK_struct);

                    calculate_header_checksum(&header);

                    const std::size_t start = buffer.size();

                    std::copy_n(reinterpret_cast<std::uint8_t*>(&header), sizeof(header), std::back_inserter(buffer));

                    ACK_struct ack;
                    ack.class_id = CFG_CLASS;
                    ack.msg_id = UCF_ID;
                    ack.err_code = 0;

                    std::copy_n(reinterpret_cast<std::uint8_t*>(&ack), sizeof(ack), std::back_inserter(buffer));
                    buffer.push_back({});
                    buffer.push_back({});

                    calculate_checksum(buffer.data() + start);
                },
                [&](write_config_request_not_acknowledged const & nack)
                {
                    NDHeader header;
                    header.header = NDHEAD;
                    header.id = hand_id;
                    header.class_id = ACK_CLASS;
                    header.msg_id = NACK_ID;
                    header.payload_len = sizeof(ACK_struct);

                    calculate_header_checksum(&header);

                    const std::size_t start = buffer.size();

                    std::copy_n(reinterpret_cast<std::uint8_t*>(&header), sizeof(header), std::back_inserter(buffer));
                    ACK_struct ack;
                    ack.class_id = CFG_CLASS;
                    ack.msg_id = UCF_ID;

                    if (nack == new_dexterity::ndx_protocol::write_config_request_not_acknowledged::eeprom_hardware_error)
                        ack.err_code = CFG_FLASH_ERROR;
                    if (nack == new_dexterity::ndx_protocol::write_config_request_not_acknowledged::incorrect_eeprom_address)
                        ack.err_code = CFG_ACCESS_ERROR;
                    if (nack == new_dexterity::ndx_protocol::write_config_request_not_acknowledged::incorrect_data_length)
                        ack.err_code = CFG_LENGTH_ERROR;
                    if (nack == new_dexterity::ndx_protocol::write_config_request_not_acknowledged::incorrect_data_format)
                        ack.err_code = CFG_DATA_ERROR;

                    std::copy_n(reinterpret_cast<std::uint8_t*>(&ack), sizeof(ack), std::back_inserter(buffer));

                    buffer.push_back({});
                    buffer.push_back({});

                    calculate_checksum(buffer.data() + start);
                });
    }

}
}

#endif

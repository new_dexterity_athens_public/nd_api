#ifndef ND_NDX_PROTOCOL_DESERIALIZE_RESPONSE_HPP
#define ND_NDX_PROTOCOL_DESERIALIZE_RESPONSE_HPP

#include <boost/optional.hpp>
#include <new_dexterity/ndx_protocol/protocol.hpp>
#include <new_dexterity/ndx_protocol/responses.hpp>
#include <new_dexterity/ndx_protocol/deserialize_base.hpp>
#include <utility>

namespace new_dexterity
{
namespace ndx_protocol
{

    template <>
    struct deserializer<new_dexterity::ndx_protocol::hand_id_response>
    {
        static boost::optional<new_dexterity::ndx_protocol::hand_id_response>
        deserialize(message_type const & msg)
        {
            auto& header = msg.first;
            auto& message = msg.second;
            
            if (header.class_id != MSG_CLASS)  
                return boost::none;
     
            if (header.msg_id != ID_ID)
                return boost::none;
     
            if (message.size() != 0)
                return boost::none;

            return new_dexterity::ndx_protocol::hand_id_response{ header.id };
        }
    };

    template <>
    struct deserializer<new_dexterity::ndx_protocol::actuator_info_response>
    {
        static boost::optional<new_dexterity::ndx_protocol::actuator_info_response> 
        deserialize(message_type const & msg)
        {
            auto& header = msg.first;
            auto& message = msg.second;
            
            if (header.class_id != MSG_CLASS)  
                return boost::none;
     
            if (header.msg_id != ACT_ID)
                return boost::none;
     
            if (message.size() < sizeof(ACT_header))
                return boost::none;
     
            auto it = message.begin();
     
            ACT_header act;
            std::copy_n(it, sizeof(act), reinterpret_cast<std::uint8_t*>(&act));
            std::advance(it, sizeof(act));
     
            if (act.num_actuators > 5)
                return boost::none;
     
            if (static_cast<std::size_t>(std::distance(it, message.end())) != act.num_actuators * sizeof(ACTx_struct))
                return boost::none;
     
            actuator_info_response response;
     
            bool thumb_actuator_set = false;
            bool index_actuator_set = false;
            bool middle_actuator_set = false;
            bool ring_pinky_actuator_set = false;
            bool thumb_opposition_actuator_set = false;
     
            for (std::size_t i = 0; i != act.num_actuators; ++i)
            {
                ACTx_struct actx;
                std::copy_n(it, sizeof(actx), reinterpret_cast<std::uint8_t*>(&actx));
                std::advance(it, sizeof(actx));
     
                if (actx.id >= 1 && actx.id <= 5)
                {
                    boost::variant<actuator_info, actuator_error>* p = nullptr;
                    if (actx.id == 1) 
                    {
                        p = &response.thumb_actuator_info_response;
                        thumb_actuator_set = true;
                    }
                    else if (actx.id == 2) 
                    {
                        p = &response.index_actuator_info_response;
                        index_actuator_set = true;
                    }
                    else if (actx.id == 3) 
                    {
                        p = &response.middle_actuator_info_response;
                        middle_actuator_set = true;
                    }
                    else if (actx.id == 4) 
                    {
                        p = &response.ring_pinky_actuator_info_response;
                        ring_pinky_actuator_set = true;
                    }
                    else if (actx.id == 5)
                    {
                        p = &response.thumb_opposition_actuator_info_response;
                        thumb_opposition_actuator_set = true;
                    }
     
                    if (p != nullptr)
                    {
                        if (actx.status == DISCONNECTED) *p = actuator_error::disconnected;
                        else if (actx.status == OUT_OF_LIMITS) *p = actuator_error::out_of_limits;
                        else if (actx.status == OUT_OF_RANGE) *p = actuator_error::out_of_range;
                        else if (actx.status == SOFTWARE_ERROR) *p = actuator_error::software_error;
                        else if (actx.status == OVERLOAD) *p = actuator_error::overload;
                        else if (actx.status == ELECTRICAL_SHOCK) *p = actuator_error::electrical_shock;
                        else if (actx.status == ENCODER_ERROR) *p = actuator_error::encoder_error;
                        else if (actx.status == OVERHEATING) *p = actuator_error::overheating;
                        else if (actx.status == VOLTAGE_ERROR) *p = actuator_error::voltage_error;
                        else if (actx.status == CONNECTED)
                        {
                            actuator_info info;
                            info.position = actx.position;
                            info.velocity = actx.velocity;
                            info.torque = actx.torque;
                            info.moving = actx.moving;
                            info.temperature = actx.temperature;
                            info.desired_actuation = actx.desired_actuation;
     
                            *p = info;
                        }
                        else *p = actuator_error::unknown_error;
                    }
                }
                else return boost::none;
             }
     
     
             if (thumb_actuator_set && index_actuator_set && middle_actuator_set && ring_pinky_actuator_set && thumb_opposition_actuator_set)
             {
                 return response;
             }
           
             return boost::none;
        }
    };

    template <>
    struct deserializer<new_dexterity::ndx_protocol::command_actuators_response>
    {
        static boost::optional<new_dexterity::ndx_protocol::command_actuators_response>
        deserialize(message_type const & msg)
        {
            auto& header = msg.first;
            auto& message = msg.second;
            
            if (header.class_id != ACK_CLASS)
                return boost::none;
     
            if (!(header.msg_id == ACK_ID || header.msg_id == NACK_ID))
                return boost::none;
     
            if (message.size() != sizeof(ACK_struct))
                return boost::none;

            ACK_struct ack;
            std::copy_n(message.begin(), sizeof(ack), reinterpret_cast<std::uint8_t*>(&ack));

            if (ack.class_id != COM_CLASS)
                return boost::none;

            if (ack.msg_id != ACOM_ID)
                return boost::none;

            if (header.msg_id == ACK_ID)
            {
                return (new_dexterity::ndx_protocol::command_actuators_response)new_dexterity::ndx_protocol::command_actuators_request_acknowledged{};
            }

            if (header.msg_id == NACK_ID)
            {
                if (ack.err_code == A_WRONG_MOTOR_NUM)
                {
                    return (new_dexterity::ndx_protocol::command_actuators_response)new_dexterity::ndx_protocol::command_actuators_request_not_acknowledged::wrong_actuator_num;
                }
                else if (ack.err_code == WRONG_COMMAND)
                {
                    return (new_dexterity::ndx_protocol::command_actuators_response)new_dexterity::ndx_protocol::command_actuators_request_not_acknowledged::wrong_command;
                }
            }

            return boost::none;
        }
    };

    template <>
    struct deserializer<new_dexterity::ndx_protocol::hand_status_response>
    {
        static boost::optional<new_dexterity::ndx_protocol::hand_status_response>
        deserialize(message_type const & msg)
        {
            auto& header = msg.first;
            auto& message = msg.second;
            
            if (header.class_id != MSG_CLASS)
                return boost::none;
     
            if (header.msg_id != STH_ID)
                return boost::none;
     
            if (message.size() != sizeof(HSTATUS_msg) - sizeof(NDHeader) - 2)
                return boost::none;

            auto it = message.begin(); 
            HSTATUS_msg hmsg;
            std::copy_n(it, sizeof(hmsg) - sizeof(NDHeader) - 2, reinterpret_cast<std::uint8_t*>(&hmsg) + sizeof(NDHeader));

            new_dexterity::ndx_protocol::hand_status_response response;

            response.mcu_time = hmsg.time;
            response.mcu_temperature = hmsg.temperature;
            response.last_hand_error_code = hmsg.last_error_of_hand;
            response.received_commands = hmsg.commands_recv;
            response.valid_received_commands = hmsg.commands_recv_valid;
            response.executed_commands = hmsg.commands_exec;

            return response;
        }
    };

    template <>
    struct deserializer<new_dexterity::ndx_protocol::communication_status_response>
    {
        static boost::optional<new_dexterity::ndx_protocol::communication_status_response>
        deserialize(message_type const & msg)
        {
            auto& header = msg.first;
            auto& message = msg.second;
            
            if (header.class_id != MSG_CLASS)
                return boost::none;
     
            if (header.msg_id != STC_ID)
                return boost::none;
     
            if (message.size() != sizeof(CSTATUS_msg) - sizeof(NDHeader) - 2)
                return boost::none;

            auto it = message.begin(); 

            CSTATUS_msg cmsg;
            std::copy_n(it, sizeof(cmsg) - sizeof(NDHeader) - 2, reinterpret_cast<std::uint8_t*>(&cmsg) + sizeof(NDHeader));

            new_dexterity::ndx_protocol::communication_status_response response;

            response.mcu_time = cmsg.time;
            response.last_communication_error_code = cmsg.last_error_of_comm;
            response.received_packets = cmsg.recv_packets_all;
            response.valid_received_packets = cmsg.recv_packets_valid;
            response.prepared_packets = cmsg.tran_packets_all;
            response.transmitted_packets = cmsg.tran_packets_valid;

            return response;
        }
    };

    template <>
    struct deserializer<new_dexterity::ndx_protocol::actuators_status_response>
    {
        static boost::optional<new_dexterity::ndx_protocol::actuators_status_response>
        deserialize(message_type const & msg)
        {
            auto& header = msg.first;
            auto& message = msg.second;
            
            if (header.class_id != MSG_CLASS)
                return boost::none;
     
            if (header.msg_id != STA_ID)
                return boost::none;
     
            if (message.size() < 5)
                return boost::none;

            auto it = message.begin(); 

            std::uint32_t time;
            std::copy_n(it, sizeof(time), reinterpret_cast<std::uint8_t*>(&time));
            std::advance(it, sizeof(time));

            std::uint8_t num;
            std::copy_n(it, sizeof(num), reinterpret_cast<std::uint8_t*>(&num));
            std::advance(it, sizeof(num));

            if (num > 5)
                return boost::none;

            if (static_cast<std::size_t>(std::distance(it, message.end())) != num * sizeof(ASTATUSx_struct)) 
                return boost::none;

            new_dexterity::ndx_protocol::actuators_status_response response;
            response.mcu_time = time;

            for (std::size_t i = 0; i != num; ++i)
            {
                ASTATUSx_struct ast;
                std::copy_n(it, sizeof(ast), reinterpret_cast<std::uint8_t*>(&ast));
                std::advance(it, sizeof(ast));

                if (!(ast.id >= 1 && ast.id <= 5))
                    return boost::none;

                new_dexterity::ndx_protocol::actuator_status status;
                status.last_actuator_error_code = ast.last_error;
                status.max_temperature = ast.temperature_max;
                status.avg_temperature = ast.temperature_avg;
                status.min_temperature = ast.temperature_min;
                status.max_torque = ast.torque_max; 
                status.avg_torque = ast.torque_avg;
                status.min_torque = ast.torque_min;

                if (ast.id == 1)
                {
                    if (ast.type != DYNX_MOTOR) 
                        return boost::none;

                    response.thumb_actuator_status = status;
                }

                if (ast.id == 2)
                {
                    if (ast.type != DYNX_MOTOR) 
                        return boost::none;

                    response.index_actuator_status = status;
                }

                if (ast.id == 3)
                {
                    if (ast.type != DYNX_MOTOR) 
                        return boost::none;

                    response.middle_actuator_status = status;
                }

                if (ast.id == 4)
                {
                    if (ast.type != DYNX_MOTOR) 
                        return boost::none;

                    response.ring_pinky_actuator_status = status;
                }

                if (ast.id == 5)
                {
                    if (ast.type != MOTOR_PWM) 
                        return boost::none;

                    response.thumb_opposition_actuator_status = status;
                }

            }

            return response;
        }
    };

    template <std::size_t Length>
    struct deserializer<new_dexterity::ndx_protocol::read_config_response<Length>>
    {
        static boost::optional<new_dexterity::ndx_protocol::read_config_response<Length>>
        deserialize(message_type const & msg)
        {
            auto& header = msg.first;
            auto& message = msg.second;
            
            if (!(header.class_id == CFG_CLASS || header.class_id == ACK_CLASS))
                return boost::none;

            if (header.class_id == CFG_CLASS)
            {
                if (header.msg_id != UCF_ID)
                    return boost::none;

                if (message.size() != Length)
                    return boost::none;

                std::array<std::uint8_t, Length> response;

                std::copy_n(message.begin(), Length, response.begin());

                return (new_dexterity::ndx_protocol::read_config_response<Length>)new_dexterity::ndx_protocol::config<Length>{ response };
            }

            if (header.class_id == ACK_CLASS)
            {
                if (header.msg_id != NACK_ID)
                    return boost::none;

                if (message.size() != sizeof(ACK_struct))
                    return boost::none;

                ACK_struct ack;
                std::copy_n(message.begin(), sizeof(ack), reinterpret_cast<std::uint8_t*>(&ack));

                if (ack.class_id != CFG_CLASS)
                    return boost::none;

                if (ack.msg_id != UCF_ID)
                    return boost::none;

                if (ack.err_code == CFG_FLASH_ERROR)
                {
                    return (new_dexterity::ndx_protocol::read_config_response<Length>)new_dexterity::ndx_protocol::read_config_request_not_acknowledged::eeprom_hardware_error;
                }
                
                if (ack.err_code == CFG_ACCESS_ERROR)
                {
                    return (new_dexterity::ndx_protocol::read_config_response<Length>)new_dexterity::ndx_protocol::read_config_request_not_acknowledged::incorrect_eeprom_address;
                }
            }

            return boost::none;
        }
    };

    template <>
    struct deserializer<new_dexterity::ndx_protocol::dynamic_read_config_response>
    {
        static boost::optional<new_dexterity::ndx_protocol::dynamic_read_config_response>
        deserialize(message_type const & msg)
        {
            auto& header = msg.first;
            auto& message = msg.second;
            
            if (!(header.class_id == CFG_CLASS || header.class_id == ACK_CLASS))
                return boost::none;

            if (header.class_id == CFG_CLASS)
            {
                if (header.msg_id != UCF_ID)
                    return boost::none;

                std::vector<std::uint8_t> response = message;

                return (new_dexterity::ndx_protocol::dynamic_read_config_response)new_dexterity::ndx_protocol::dynamic_config{ response };
            }

            if (header.class_id == ACK_CLASS)
            {
                if (header.msg_id != NACK_ID)
                    return boost::none;

                if (message.size() != sizeof(ACK_struct))
                    return boost::none;

                ACK_struct ack;
                std::copy_n(message.begin(), sizeof(ack), reinterpret_cast<std::uint8_t*>(&ack));

                if (ack.class_id != CFG_CLASS)
                    return boost::none;

                if (ack.msg_id != UCF_ID)
                    return boost::none;

                if (ack.err_code == CFG_FLASH_ERROR)
                {
                    return (new_dexterity::ndx_protocol::dynamic_read_config_response)new_dexterity::ndx_protocol::read_config_request_not_acknowledged::eeprom_hardware_error;
                }
                
                if (ack.err_code == CFG_ACCESS_ERROR)
                {
                    return (new_dexterity::ndx_protocol::dynamic_read_config_response)new_dexterity::ndx_protocol::read_config_request_not_acknowledged::incorrect_eeprom_address;
                }
            }

            return boost::none;
        }
    };

    template <>
    struct deserializer<new_dexterity::ndx_protocol::write_config_response>
    {
        static boost::optional<new_dexterity::ndx_protocol::write_config_response>
        deserialize(message_type const & msg)
        {
            auto& header = msg.first;
            auto& message = msg.second;
            
            if (header.class_id != ACK_CLASS)
                return boost::none;

            if (!(header.msg_id == ACK_ID || header.msg_id == NACK_ID))
                return boost::none;

            if (message.size() != sizeof(ACK_struct))
                return boost::none;

            ACK_struct ack;
            std::copy_n(message.begin(), sizeof(ack), reinterpret_cast<std::uint8_t*>(&ack));

            if (ack.class_id != CFG_CLASS)
                return boost::none;

            if (ack.msg_id != UCF_ID)
                return boost::none;

            if (header.msg_id == ACK_ID)
            {
                if (ack.err_code != 0)
                    return boost::none;

                return (new_dexterity::ndx_protocol::write_config_response)new_dexterity::ndx_protocol::write_config_request_acknowledged{};
            }

            if (header.msg_id == NACK_ID)
            {
                if (ack.err_code == CFG_FLASH_ERROR)
                {
                    return (new_dexterity::ndx_protocol::write_config_response)new_dexterity::ndx_protocol::write_config_request_not_acknowledged::eeprom_hardware_error;
                }
                
                if (ack.err_code == CFG_ACCESS_ERROR)
                {
                    return (new_dexterity::ndx_protocol::write_config_response)new_dexterity::ndx_protocol::write_config_request_not_acknowledged::incorrect_eeprom_address;
                }

                if (ack.err_code == CFG_DATA_ERROR)
                {
                    return (new_dexterity::ndx_protocol::write_config_response)new_dexterity::ndx_protocol::write_config_request_not_acknowledged::incorrect_data_format;
                }
                
                if (ack.err_code == CFG_LENGTH_ERROR)
                {
                    return (new_dexterity::ndx_protocol::write_config_response)new_dexterity::ndx_protocol::write_config_request_not_acknowledged::incorrect_data_length;
                }
            }

            return boost::none;
        }
    };

}
} // new_dexterity

#endif

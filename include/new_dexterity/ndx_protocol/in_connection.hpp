#ifndef ND_NDX_PROTOCOL_IN_CONNECTION_HPP
#define ND_NDX_PROTOCOL_IN_CONNECTION_HPP

#include <new_dexterity/in_connection.hpp>
#include <new_dexterity/ndx_protocol/message_parser.hpp>

namespace new_dexterity
{
namespace ndx_protocol
{
    using in_connection = new_dexterity::in_connection<new_dexterity::ndx_protocol::message_parser>;

}
}

#endif

#ifndef ND_NDX_PROTOCOL_SERIALIZE_REQUEST_HPP
#define ND_NDX_PROTOCOL_SERIALIZE_REQUEST_HPP

#include <new_dexterity/ndx_protocol/protocol.hpp>
#include <new_dexterity/ndx_protocol/requests.hpp>
#include <type_traits>

namespace new_dexterity
{
namespace ndx_protocol
{

/* 
 *
 * @tparam Buffer
 * @code{.cpp}
 * // b is Buffer, c is std::uint8_t
 * b.push_back(c);
 * @endcode
 *
 * @param hand_id The NDX hand id.
 * @param buffer The @p Buffer to serialize into.
 * @param request The @ref new_dexterity::actuator_info_request to serialize.
 *
 * @ingroup Functions
 */
template <typename Buffer>
void serialize(std::uint8_t hand_id, Buffer& buffer, hand_id_request const & request)
{
        NDHeader header;
        header.header = NDHEAD;
        header.id = hand_id;
        header.class_id = MSG_CLASS;
        header.msg_id = ID_ID;
        header.payload_len = 0;

        calculate_header_checksum(&header);

        std::copy_n(reinterpret_cast<std::uint8_t*>(&header), sizeof(header), std::back_inserter(buffer));
}

/* 
 * 
 * @tparam Buffer
 * @code{.cpp}
 * // b is Buffer, c is std::uint8_t
 * b.push_back(c);
 * @endcode
 *
 * @param hand_id The NDX hand id.
 * @param buffer The @p Buffer to serialize into.
 * @param request The @ref new_dexterity::actuator_info_request to serialize.
 *
 * @ingroup Functions
 */
template <typename Buffer>
void serialize(std::uint8_t hand_id, Buffer& buffer, actuator_info_request const & request)
{
        NDHeader header;
        header.header = NDHEAD;
        header.id = hand_id;
        header.class_id = MSG_CLASS;
        header.msg_id = ACT_ID;
        header.payload_len = 0;

        calculate_header_checksum(&header);

        std::copy_n(reinterpret_cast<std::uint8_t*>(&header), sizeof(header), std::back_inserter(buffer));
}

/* 
 * 
 * @tparam Buffer
 * @code{.cpp}
 * // b is Buffer, c is std::uint8_t
 * std::size_t n = buffer.size();
 * b.push_back(c);
 * std::uint8_t * p = b.data() + n;
 * @endcode
 *
 * @param hand_id The NDX hand id.
 * @param buffer The @p Buffer to serialize into.
 * @param request The @ref new_dexterity::command_actuators_request to serialize.
 *
 * @ingroup Functions
 */
template <typename Buffer>
void serialize(std::uint8_t hand_id, Buffer& buffer, command_actuators_request const & request)
{
    std::uint8_t commands_num = 0;

    std::size_t start = buffer.size();

    auto const & commands = request.commands;

    if (commands.thumb_actuator_command)
        ++commands_num;
    if (commands.index_actuator_command)
        ++commands_num;
    if (commands.middle_actuator_command)
        ++commands_num;
    if (commands.ring_pinky_actuator_command)
        ++commands_num;
    if (commands.thumb_opposition_actuator_command)
        ++commands_num;

    NDHeader header;
    header.header = NDHEAD;
    header.id = hand_id;
    header.class_id = COM_CLASS;
    header.msg_id = ACOM_ID;
    header.payload_len = sizeof(ACOM_header) + commands_num * sizeof(ACOMx_struct);


    calculate_header_checksum(&header);

    std::copy_n(reinterpret_cast<std::uint8_t*>(&header), sizeof(header), std::back_inserter(buffer));

    ACOM_header aheader = {0, commands_num, 0};

    std::copy_n(reinterpret_cast<std::uint8_t*>(&aheader), sizeof(aheader), std::back_inserter(buffer));

    if (commands.thumb_actuator_command)
    {
        ACOMx_struct c = {1, DYNX_MOTOR, 0, commands.thumb_actuator_command->position, commands.thumb_actuator_command->velocity, commands.thumb_actuator_command->torque};
        std::copy_n(reinterpret_cast<std::uint8_t*>(&c), sizeof(c), std::back_inserter(buffer));
    }

    if (commands.index_actuator_command)
    {
        ACOMx_struct c = {2, DYNX_MOTOR, 0, commands.index_actuator_command->position, commands.index_actuator_command->velocity, commands.index_actuator_command->torque};
        std::copy_n(reinterpret_cast<std::uint8_t*>(&c), sizeof(c), std::back_inserter(buffer));
    }

    if (commands.middle_actuator_command)
    {
        ACOMx_struct c = {3, DYNX_MOTOR, 0, commands.middle_actuator_command->position, commands.middle_actuator_command->velocity, commands.middle_actuator_command->torque};
        std::copy_n(reinterpret_cast<std::uint8_t*>(&c), sizeof(c), std::back_inserter(buffer));
    }

    if (commands.ring_pinky_actuator_command)
    {
        ACOMx_struct c = {4, DYNX_MOTOR, 0, commands.ring_pinky_actuator_command->position, commands.ring_pinky_actuator_command->velocity, commands.ring_pinky_actuator_command->torque};
        std::copy_n(reinterpret_cast<std::uint8_t*>(&c), sizeof(c), std::back_inserter(buffer));
    }

    if (commands.thumb_opposition_actuator_command)
    {
        ACOMx_struct c = {5, MOTOR_PWM, 0, commands.thumb_opposition_actuator_command->position, commands.thumb_opposition_actuator_command->velocity, commands.thumb_opposition_actuator_command->torque};
        std::copy_n(reinterpret_cast<std::uint8_t*>(&c), sizeof(c), std::back_inserter(buffer));
    }


    buffer.push_back({});
    buffer.push_back({});

    calculate_checksum(buffer.data() + start);
}

/* 
 * 
 * @tparam Buffer
 * @code{.cpp}
 * // b is Buffer, c is std::uint8_t
 * std::size_t n = buffer.size();
 * b.push_back(c);
 * std::uint8_t * p = b.data() + n;
 * @endcode
 *
 * @param hand_id The NDX hand id.
 * @param buffer The @p Buffer to serialize into.
 * @param request The @ref new_dexterity::software_calibration_request to serialize.
 *
 * @ingroup Functions
 */
template <typename Buffer>
void serialize(std::uint8_t hand_id, Buffer& buffer, software_calibration_request const & request)
{
    std::size_t start = buffer.size();

    NDHeader header;
    header.header = NDHEAD;
    header.id = hand_id;
    header.class_id = CFG_CLASS;
    header.msg_id = CAL_ID;
    header.payload_len = sizeof(std::uint8_t);

    calculate_header_checksum(&header);

    std::copy_n(reinterpret_cast<std::uint8_t*>(&header), sizeof(header), std::back_inserter(buffer));

    std::uint8_t calib_type = 2;

    std::copy_n(reinterpret_cast<std::uint8_t*>(&calib_type), sizeof(calib_type), std::back_inserter(buffer));

    buffer.push_back({});
    buffer.push_back({});

    calculate_checksum(buffer.data() + start);
}

/* 
 * 
 * @tparam Buffer
 * @code{.cpp}
 * // b is Buffer, c is std::uint8_t
 * std::size_t n = buffer.size();
 * b.push_back(c);
 * std::uint8_t * p = b.data() + n;
 * @endcode
 *
 * @param hand_id The NDX hand id.
 * @param buffer The @p Buffer to serialize into.
 * @param request The @ref new_dexterity::hardware_calibration_request to serialize.
 *
 * @ingroup Functions
 */
template <typename Buffer>
void serialize(std::uint8_t hand_id, Buffer& buffer, hardware_calibration_request const & request)
{
    std::size_t start = buffer.size();

    NDHeader header;
    header.header = NDHEAD;
    header.id = hand_id;
    header.class_id = CFG_CLASS;
    header.msg_id = CAL_ID;
    header.payload_len = sizeof(std::uint8_t);

    calculate_header_checksum(&header);

    std::copy_n(reinterpret_cast<std::uint8_t*>(&header), sizeof(header), std::back_inserter(buffer));

    std::uint8_t calib_type = 1;

    std::copy_n(reinterpret_cast<std::uint8_t*>(&calib_type), sizeof(calib_type), std::back_inserter(buffer));

    buffer.push_back({});
    buffer.push_back({});

    calculate_checksum(buffer.data() + start);
}


/* 
 * 
 * @tparam Buffer
 * @code{.cpp}
 * // b is Buffer, c is std::uint8_t
 * std::size_t n = buffer.size();
 * b.push_back(c);
 * @endcode
 *
 * @param hand_id The NDX hand id.
 * @param buffer The @p Buffer to serialize into.
 * @param request The @ref new_dexterity::hand_status_request to serialize.
 *
 * @ingroup Functions
 */
template <typename Buffer>
void serialize(std::uint8_t hand_id, Buffer& buffer, hand_status_request const & request)
{
    NDHeader header;
    header.header = NDHEAD;
    header.id = hand_id;
    header.class_id = MSG_CLASS;
    header.msg_id = STH_ID;
    header.payload_len = 0;

    calculate_header_checksum(&header);

    std::copy_n(reinterpret_cast<std::uint8_t*>(&header), sizeof(header), std::back_inserter(buffer));
}

/* 
 * 
 * @tparam Buffer
 * @code{.cpp}
 * // b is Buffer, c is std::uint8_t
 * std::size_t n = buffer.size();
 * b.push_back(c);
 * @endcode
 *
 * @param hand_id The NDX hand id.
 * @param buffer The @p Buffer to serialize into.
 * @param request The @ref new_dexterity::communication_status_request to serialize.
 *
 * @ingroup Functions
 */
template <typename Buffer>
void serialize(std::uint8_t hand_id, Buffer& buffer, communication_status_request const & request)
{
    NDHeader header;
    header.header = NDHEAD;
    header.id = hand_id;
    header.class_id = MSG_CLASS;
    header.msg_id = STC_ID;
    header.payload_len = 0;

    calculate_header_checksum(&header);

    std::copy_n(reinterpret_cast<std::uint8_t*>(&header), sizeof(header), std::back_inserter(buffer));
}

/* 
 * 
 * @tparam Buffer
 * @code{.cpp}
 * // b is Buffer, c is std::uint8_t
 * std::size_t n = buffer.size();
 * b.push_back(c);
 * std::uint8_t * p = b.data() + n;
 * @endcode
 *
 * @param hand_id The NDX hand id.
 * @param buffer The @p Buffer to serialize into.
 * @param request The @ref new_dexterity::actuators_status_request to serialize.
 *
 * @ingroup Functions
 */
template <typename Buffer>
void serialize(std::uint8_t hand_id, Buffer& buffer, actuators_status_request const & request)
{
    std::size_t start = buffer.size();

    auto const & actuators = request.actuators;

    NDHeader header;
    header.header = NDHEAD;
    header.id = hand_id;
    header.class_id = MSG_CLASS;
    header.msg_id = STA_ID;


    std::uint8_t actuator_num = 0;

    if (actuators.thumb_actuator)
        ++actuator_num;
    if (actuators.index_actuator)
        ++actuator_num;
    if (actuators.middle_actuator)
        ++actuator_num;
    if (actuators.ring_pinky_actuator)
        ++actuator_num;
    if (actuators.thumb_opposition_actuator)
        ++actuator_num;

    header.payload_len = 1 + actuator_num;

    calculate_header_checksum(&header);

    std::copy_n(reinterpret_cast<std::uint8_t*>(&header), sizeof(header), std::back_inserter(buffer));

    buffer.push_back(actuator_num);

    if (actuators.thumb_actuator)
        buffer.push_back(1);
    if (actuators.index_actuator)
        buffer.push_back(2);
    if (actuators.middle_actuator)
        buffer.push_back(3);
    if (actuators.ring_pinky_actuator)
        buffer.push_back(4);
    if (actuators.thumb_opposition_actuator)
        buffer.push_back(5);

    buffer.push_back({});
    buffer.push_back({});

    calculate_checksum(buffer.data() + start);
}

template <typename Buffer>
void serialize(std::uint8_t hand_id, Buffer& buffer, dynamic_read_config_request const & request)
{
    std::size_t start = buffer.size();

    NDHeader header;
    header.header = NDHEAD;
    header.id = hand_id;
    header.class_id = CFG_CLASS;
    header.msg_id = UCF_ID;
    header.payload_len = 3;

    calculate_header_checksum(&header);

    std::copy_n(reinterpret_cast<std::uint8_t*>(&header), sizeof(header), std::back_inserter(buffer));

    buffer.push_back(0);
    buffer.push_back(static_cast<std::uint8_t>(request.address));
    buffer.push_back(static_cast<std::uint8_t>(request.length));
    buffer.push_back({});
    buffer.push_back({});

    calculate_checksum(buffer.data() + start);
}

/* 
 * 
 * @tparam Buffer
 * @code{.cpp}
 * // b is Buffer, c is std::uint8_t
 * std::size_t n = buffer.size();
 * b.push_back(c);
 * std::uint8_t * p = b.data() + n;
 * @endcode
 *
 * @param hand_id The NDX hand id.
 * @param buffer The @p Buffer to serialize into.
 * @param request The @ref new_dexterity::read_config_request to serialize.
 *
 * @ingroup Functions
 */
template <typename Buffer, std::size_t Address, std::size_t Length>
void serialize(std::uint8_t hand_id, Buffer& buffer, read_config_request<Address, Length> const & request)
{
    serialize(hand_id, buffer, dynamic_read_config_request{ static_cast<std::uint8_t>(Address), static_cast<std::uint8_t>(Length) });
}

template <typename Buffer>
void serialize(std::uint8_t hand_id, Buffer& buffer, dynamic_write_config_request const & request)
{
    std::size_t start = buffer.size();

    NDHeader header;
    header.header = NDHEAD;
    header.id = hand_id;
    header.class_id = CFG_CLASS;
    header.msg_id = UCF_ID;

    header.payload_len = 3 + request.data.size();

    calculate_header_checksum(&header);

    std::copy_n(reinterpret_cast<std::uint8_t*>(&header), sizeof(header), std::back_inserter(buffer));



    buffer.push_back(request.save ? static_cast<std::uint8_t>(2) : static_cast<std::uint8_t>(1));
    buffer.push_back(static_cast<std::uint8_t>(request.address));
    buffer.push_back(static_cast<std::uint8_t>(request.data.size()));

    std::copy(request.data.begin(), request.data.end(), std::back_inserter(buffer));

    buffer.push_back({});
    buffer.push_back({});

    calculate_checksum(buffer.data() + start);
}

/* 
 * 
 * @tparam Buffer
 * @code{.cpp}
 * // b is Buffer, c is std::uint8_t
 * std::size_t n = buffer.size();
 * b.push_back(c);
 * std::uint8_t * p = b.data() + n;
 * @endcode
 *
 * @param hand_id The NDX hand id.
 * @param buffer The @p Buffer to serialize into.
 * @param request The @ref new_dexterity::write_config_request to serialize.
 *
 * @ingroup Functions
 */
template <typename Buffer, std::size_t Address, std::size_t Length>
void serialize(std::uint8_t hand_id, Buffer& buffer, write_config_request<Address, Length> const & request)
{
    std::size_t start = buffer.size();

    NDHeader header;
    header.header = NDHEAD;
    header.id = hand_id;
    header.class_id = CFG_CLASS;
    header.msg_id = UCF_ID;

    header.payload_len = 3 + Length;

    calculate_header_checksum(&header);

    std::copy_n(reinterpret_cast<std::uint8_t*>(&header), sizeof(header), std::back_inserter(buffer));

    buffer.push_back(request.save ? static_cast<std::uint8_t>(2) : static_cast<std::uint8_t>(1));
    buffer.push_back(static_cast<std::uint8_t>(Address));
    buffer.push_back(static_cast<std::uint8_t>(Length));

    std::copy_n(request.data.begin(), request.data.size(), std::back_inserter(buffer));

    buffer.push_back({});
    buffer.push_back({});

    calculate_checksum(buffer.data() + start);
}

}
} // new_dexterity

#endif

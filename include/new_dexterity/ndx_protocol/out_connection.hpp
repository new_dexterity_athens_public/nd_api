#ifndef ND_NDX_PROTOCOL_OUT_CONNECTION_HPP
#define ND_NDX_PROTOCOL_OUT_CONNECTION_HPP

#include <new_dexterity/out_connection.hpp>

namespace new_dexterity
{
namespace ndx_protocol
{
    using out_connection = new_dexterity::out_connection;
}
}

#endif

#ifndef ND_NDX_PROTOCOL_MESSAGE_PARSER_H
#define ND_NDX_PROTOCOL_MESSAGE_PARSER_H

// Message Parser definition

#include <vector>
#include <cstdint>
#include <new_dexterity/ndx_protocol/protocol.hpp>
#include <utility>
#include <boost/variant.hpp>
#include <new_dexterity/utilities.hpp>

namespace new_dexterity
{

namespace ndx_protocol
{
    /*  Parses bytes in small-endian format and collects NDX messages.
     *
     * It does all the low-level work of searching for the NDX header and verifying
     * the messages found. If it detects a NDX header but fails to verify the header checksum,
     * it will reuse the assumed header's bytes. If it fails to verify the payload's checksum,
     * it will reuse the assumed payload's bytes.
     */ 

    class message_parser
    {
        public:
            using header_type = NDHeader;
            using payload_type = std::vector<std::uint8_t>;
            using message_type = std::pair<header_type, payload_type>;

        private:
            struct parsing_header_code_low_byte {};

            struct parsing_header_code_high_byte {};

            struct parsing_device_id {};


            struct parsing_class_and_msg_id { std::uint8_t device_id; };

            struct parsing_payload_len_low_byte { std::uint8_t device_id;
                std::uint8_t class_id;
                std::uint8_t msg_id; };

            struct parsing_payload_len_high_byte { std::uint8_t device_id;
                std::uint8_t class_id;
                std::uint8_t msg_id; 
                std::uint8_t payload_len_low_byte; };

            struct parsing_header_checksum_0 { std::uint8_t device_id;
                std::uint8_t class_id;
                std::uint8_t msg_id; 
                std::uint8_t payload_len_low_byte;
                std::uint8_t payload_len_high_byte; };

            struct parsing_header_checksum_1 { std::uint8_t device_id;
                std::uint8_t class_id;
                std::uint8_t msg_id; 
                std::uint8_t payload_len_low_byte;
                std::uint8_t payload_len_high_byte;
                std::uint8_t header_checksum_0; };

            struct validating_header { header_type header; };

            struct parsing_payload { header_type header; payload_type payload; };

            struct parsing_message_checksum_1 { header_type header; payload_type payload; std::uint8_t message_checksum_0; };

            struct validating_message { header_type header; payload_type payload; std::uint8_t message_checksum_0; std::uint8_t message_checksum_1; };

            using state_type = boost::variant<parsing_header_code_low_byte , 
                                         parsing_header_code_high_byte , 
                                         parsing_device_id , 
                                         parsing_class_and_msg_id , 
                                         parsing_payload_len_low_byte , 
                                         parsing_payload_len_high_byte , 
                                         parsing_header_checksum_0 , 
                                         parsing_header_checksum_1 , 
                                         parsing_payload , 
                                         parsing_message_checksum_1>;

            auto next(parsing_header_code_low_byte& state, uint8_t c) -> boost::variant<parsing_header_code_low_byte, parsing_header_code_high_byte>
            {
                if (c == 'd')
                    return parsing_header_code_high_byte{};
                else
                    return parsing_header_code_low_byte{};
            }

            auto next(parsing_header_code_high_byte& state, uint8_t c) -> boost::variant<parsing_header_code_low_byte, parsing_device_id>
            {
                if (c == 'n')
                    return parsing_device_id{};
                else
                    return parsing_header_code_low_byte{};
            }

            auto next(parsing_device_id& state, uint8_t c) -> parsing_class_and_msg_id
            {
                return parsing_class_and_msg_id{c};
            }

            auto next(parsing_class_and_msg_id& state, uint8_t c) -> parsing_payload_len_low_byte
            {
                uint8_t class_id = c & 0x03;
                uint8_t msg_id = (c & 0xfc) >> 2;

                return parsing_payload_len_low_byte { state.device_id, class_id, msg_id };
            }

            auto next(parsing_payload_len_low_byte& state, uint8_t c) -> parsing_payload_len_high_byte
            {
                return parsing_payload_len_high_byte { state.device_id, state.class_id, state.msg_id, c };
            }

            auto next(parsing_payload_len_high_byte& state, uint8_t c) -> parsing_header_checksum_0
            {
                return parsing_header_checksum_0 { state.device_id, state.class_id, state.msg_id, state.payload_len_low_byte, c };
            }

            auto next(parsing_header_checksum_0& state, uint8_t c) -> parsing_header_checksum_1
            {
                return parsing_header_checksum_1 { state.device_id, state.class_id, state.msg_id, state.payload_len_low_byte, state.payload_len_high_byte, c };
            }

            auto next(parsing_header_checksum_1& state, uint8_t c) -> state_type
            {
                header_type header;
                header.header = NDHEAD;
                header.id = state.device_id;
                header.class_id = state.class_id;
                header.msg_id = state.msg_id;
                header.payload_len = state.payload_len_low_byte | state.payload_len_high_byte << 8;
                header.cs[0] = state.header_checksum_0;
                header.cs[1] = c;

                if (validate_header_checksum(header) == 1)
                {
                    if (header.payload_len == 0)
                    {
                        parsed_messages_.push_back({header, {}});
                        return parsing_header_code_low_byte {}; 
                    }
                            
                    return parsing_payload { header, {}};
                }
                else
                {
                    state_ = parsing_header_code_low_byte {};
                    parse(reinterpret_cast<std::uint8_t*>(&header)+2, sizeof(header)-2);
                    return state_;
                }
            }

            auto next(parsing_payload& state, uint8_t c) -> boost::variant<parsing_payload, parsing_message_checksum_1>
            {
                if (state.payload.size() < state.header.payload_len)
                {
                    state.payload.push_back(c);
                    return parsing_payload { state.header, std::move(state.payload) };
                }
                else 
                {
                    return parsing_message_checksum_1 { state.header, std::move(state.payload), c};
                }
            }

            auto next(parsing_message_checksum_1& state, uint8_t c) -> state_type
            {
                auto message_checksum_1 = c;

                std::vector<std::uint8_t> buffer;
                std::copy_n(reinterpret_cast<std::uint8_t*>(&state.header), sizeof(state.header), std::back_inserter(buffer));
                std::copy(state.payload.begin(), state.payload.end(), std::back_inserter(buffer));
                buffer.push_back(state.message_checksum_0);
                buffer.push_back(message_checksum_1);

                if (validate_checksum(buffer.data()) == 1)
                {
                        parsed_messages_.emplace_back(state.header, std::move(state.payload));
                        return parsing_header_code_low_byte {}; 
                }
                else
                {
                    auto tmp_buffer = std::move(state.payload);
                    state_ = parsing_header_code_low_byte {};
                    tmp_buffer.push_back(state.message_checksum_0);
                    tmp_buffer.push_back(message_checksum_1);
                    parse(tmp_buffer, tmp_buffer.size());
                    return state_;
                }
            }


            struct update_state
            {
                update_state(std::uint8_t byte, message_parser& parser)
                    : byte_(byte), parser_(parser)
                {
                }

                template <typename State>
                void operator()(State & state)
                {
                    parser_.state_ = parser_.next(state, byte_);
                }

                std::uint8_t byte_;
                message_parser& parser_;
            };


        public:
            message_parser() : state_(parsing_header_code_low_byte{})
            {
            }


            /* 
             *  Parse \p bytes_read bytes from the start of the given \p buffer.
             *
             * \tparam Buffer A type supporting the following expressions:
             * @code{.cpp}
             * //b is Buffer, i is std::size_t, c is std::uint8_t
             * uint8_t z = b[i];
             * b.push_back(c);
             * uint8_t *p = b.data();
             * @endcode
             *
             * \param buffer 
             *   \p Buffer from which to read bytes.
             * \param bytes_read 
             *   Number of bytes to read.
             */
            template <typename Buffer>
            void parse(Buffer const & buffer, std::size_t bytes_read)
            {
                for (std::size_t i = 0; i != bytes_read; ++i)
                {
                    auto byte = buffer[i];

                    auto visitor = update_state(byte, *this);
                    new_dexterity::match(state_, visitor);
                }
            }

            /*  Clears the parsed messages list.
             */
            void clear() 
            { 
                parsed_messages_.clear();
            }

            /*  Returns a vector of parsed messages.
             */
            std::vector<message_type> const & parsed_messages() const
            {
                return parsed_messages_;
            }

        private:
            state_type state_;
            std::vector<message_type> parsed_messages_;

    };

}
}

#endif

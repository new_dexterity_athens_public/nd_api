#ifndef ND_NDX_PROTOCOL_DESERIALIZE_BASE_HPP
#define ND_NDX_PROTOCOL_DESERIALIZE_BASE_HPP

#include <boost/optional.hpp>
#include <new_dexterity/ndx_protocol/message_parser.hpp>
#include <utility>

namespace new_dexterity
{
namespace ndx_protocol
{
    using message_type = new_dexterity::ndx_protocol::message_parser::message_type;

    template <typename T>
    struct deserializer;

}
}

#endif

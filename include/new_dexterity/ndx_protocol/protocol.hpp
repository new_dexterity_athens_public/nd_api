/**
 * Stripped down NDX protocol file
 *
 * Check out the new dexterity manual for usage.
 *
 * @author Panos Marantos
 * @date 3 Apr 2018
 */

#ifndef INC_PROTOCOL_H_
#define INC_PROTOCOL_H_

#include <stdint.h>
#include <string.h>

using real = float;
#define NDHEAD 0x6e64

#define ACK_CLASS 0
#define NACK_ID 0
#define ACK_ID 1


#define MSG_CLASS 1
#define ID_ID 1
#define ACT_ID 2
#define JNT_ID 3
#define HND_ID 4
#define STH_ID 5
#define STC_ID 6
#define STA_ID 7

#define COM_CLASS 2
#define ACOM_ID 1
#define FCOM_ID 2
#define JCOM_ID 3
#define HRST_ID 4
#define FRST_ID 5

#define CFG_CLASS 3
#define GMT_ID 1
#define GJT_ID 2
#define GFR_ID 3
#define GHD_ID 4
#define CAL_ID 5
#define UCF_ID 6

typedef enum {DISCONNECTED=0, CONNECTED, OUT_OF_LIMITS, OUT_OF_RANGE, SOFTWARE_ERROR,OVERLOAD,ELECTRICAL_SHOCK,ENCODER_ERROR,OVERHEATING,VOLTAGE_ERROR} actuator_status;
typedef enum {VALID=0,UNKNOWN_HEADER, CLASS_ID_ERROR, MSG_ID_ERROR, CHECKSUM_FAILED, HEADER_CHECKSUM_FAILED, ERROR_LENGTH, UNRESOLVED_ERROR} ack_status;
typedef enum {A_WRONG_MOTOR_NUM=10,A_UNKNOWN_ID,A_UNKNOWN_MODE,WRONG_COMMAND} ack_actuator_status;
typedef enum {CFG_FLASH_ERROR=10, CFG_ACCESS_ERROR, CFG_DATA_ERROR, CFG_LENGTH_ERROR} cfg_status;
typedef enum {MOTOR_PWM=0, DYNX_MOTOR} actuator_type;
typedef enum {POSITION_MODE=0,VELOCITY_MODE,POSITION_CURRENT_MODE, UNKNOWN_MODE} actuator_mode;

typedef enum {LOWER_POSITION=0, UPPER_POSITION, VELOCITY_MAX, TORQUE_MAX} joint_limit_status;
typedef enum {STEADY=0, COLLISION, MOVING} joint_status;
typedef enum {RIGID=0, FLEX} joint_type;


#pragma pack(push, 1)
typedef struct {
    uint16_t header; 
    uint8_t id;  
    uint8_t class_id :2; 
    uint8_t msg_id :6; 
    uint16_t payload_len; 
    uint8_t cs[2]; 
} NDHeader;
#pragma pack(pop)


#pragma pack(push, 1)
typedef struct {
    NDHeader header; 
}  DEV_ID_msg;
#pragma pack(pop)



#pragma pack(push, 1)
typedef struct {
    uint8_t class_id :2; 
    uint8_t msg_id :6; 
    uint8_t err_code; 
} ACK_struct;
#pragma pack(pop)



#pragma pack(push, 1)
typedef struct {
    NDHeader header; 
    ACK_struct ack; 
    uint8_t cs[2]; 
} ACK_msg;
#pragma pack(pop)



#pragma pack(push, 1)
typedef struct {
    uint8_t id :6; 
    uint8_t type :2; 
    uint8_t status :7; 
    uint8_t moving :1; 
    int16_t temperature; 
    real position ; 
    real velocity; 
    real torque; 
    real desired_actuation; 
} ACTx_struct;
#pragma pack(pop)



#pragma pack(push, 1)
typedef struct {
    uint16_t age;  
    uint8_t num_actuators; 
    uint8_t reserved1; 
} ACT_header;
#pragma pack(pop)



#pragma pack(push, 1)
typedef struct {
    uint8_t id :6; 
    uint8_t type :2; 
    uint16_t reserved1; 
    real position; 
    real velocity; 
    real torque; 
} ACOMx_struct;
#pragma pack(pop)



#pragma pack(push, 1)
typedef struct {
    uint16_t age;  
    uint8_t num_actuators; 
    uint8_t reserved1; 
} ACOM_header;
#pragma pack(pop)



#pragma pack(push, 1)
typedef struct {
    uint8_t id :6; 
    uint8_t type :2; 
    uint8_t flag; 
    uint16_t joints_assigned; 
    real position_min; 
    real position_max; 
    real position_zero; 
    real velocity_limit; 
    real current_limit; 
} CMOTx_struct;
#pragma pack(pop)



#pragma pack(push, 1)
typedef struct
{
    NDHeader header; 
    uint32_t time; 
    int16_t temperature; 
    uint16_t last_error_of_hand; 
    uint32_t commands_recv; 
    uint32_t commands_recv_valid; 
    uint32_t commands_exec; 
    uint8_t cs[2]; 
} HSTATUS_msg ;
#pragma pack(pop)



#pragma pack(push, 1)
typedef struct
{
    NDHeader header; 
    uint32_t time; 
    uint16_t last_error_of_comm; 
    uint16_t reserved1; 
    uint32_t recv_packets_all; 
    uint32_t recv_packets_valid; 
    uint32_t tran_packets_all; 
    uint32_t tran_packets_valid; 
    uint8_t cs[2]; 
} CSTATUS_msg;
#pragma pack(pop)



#pragma pack(push, 1)
typedef struct
{
    uint8_t id :6; 
    uint8_t type :2; 
    uint16_t last_error; 
    int16_t temperature_max; 
    int16_t temperature_min; 
    int16_t temperature_avg; 
    uint16_t torque_max; 
    uint16_t torque_min; 
    uint16_t torque_avg; 
} ASTATUSx_struct;
#pragma pack(pop)


//Messages global variables
extern ACK_msg ack_msg;
extern DEV_ID_msg devid_msg;
extern HSTATUS_msg hstatus_msg;
extern CSTATUS_msg cstatus_msg;


void calculate_header_checksum(NDHeader *header);


void calculate_checksum(uint8_t *msg);



int validate_header_checksum(NDHeader header);


int validate_checksum(const uint8_t *msg);

#endif 


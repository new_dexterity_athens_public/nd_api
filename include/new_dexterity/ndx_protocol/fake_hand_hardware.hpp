#ifndef ND_NDX_PROTOCOL_FAKE_HAND_HARDWARE_HPP
#define ND_NDX_PROTOCOL_FAKE_HAND_HARDWARE_HPP

//  Fake hand hardware mocker definition.

#include <new_dexterity/ndx_protocol/protocol.hpp>
#include <new_dexterity/ndx_protocol/message_dispatcher.hpp>
#include <new_dexterity/ndx_protocol/requests.hpp>
#include <new_dexterity/ndx_protocol/responses.hpp>
#include <new_dexterity/ndx_protocol/serialize.hpp>
#include <new_dexterity/ndx_protocol/deserialize.hpp>
#include <new_dexterity/out_connection.hpp>
#include <new_dexterity/ndx_protocol/message_dispatcher.hpp>
#include <new_dexterity/ndx_protocol/in_connection.hpp>
#include <boost/asio.hpp>
#include <cstring>
#include <memory>
#include <cstddef>
#include <algorithm>

namespace asio = boost::asio;

namespace new_dexterity
{
namespace ndx_protocol
{

    /*  A mocking class mimicking a real NDX hand.
     * 
     * For development purpose it is convenient to have a class that mimicks the responses
     * of a real NDX hand. This class provides a limited subset of the NDX hand's functionality.
     * Currently it does not support the complete set of requests/responses.
     */
    class fake_hand_hardware
    {
        private:
            using message_type = new_dexterity::ndx_protocol::message_dispatcher::message_type;
            using in_connection = new_dexterity::ndx_protocol::in_connection;

        public:
            /* @param in The object representing the incoming connection.
             *  @param out The object representing the outgoing connection.
             *  @param hand_id The id of the fake hand.
             */
            fake_hand_hardware(in_connection& in, new_dexterity::out_connection& out, std::uint8_t hand_id)
                : mcu_time_()
                , received_commands_()
                , valid_received_commands_()
                , executed_commands_()
                , mcu_temperature_(27)
                , last_hand_error_code_()
                , last_communication_error_code_()
                , received_packets_()
                , valid_received_packets_()
                , prepared_packets_()
                , transmitted_packets_()
                , hand_id_(hand_id)
                , out_connection_(out)
                , auto_subscribe_(in,  [this](message_type const & msg){ handle_message(msg); })
            {
            }

        private:
            template <typename Buffer>
            void prepare_command_actuators_response(Buffer& buffer, const command_actuators_request& request)
            {

                    if (request.commands.thumb_actuator_command)
                    {
                        pos[0] = request.commands.thumb_actuator_command->position;
                        vel[0] = request.commands.thumb_actuator_command->velocity;
                        torque[0] = request.commands.thumb_actuator_command->torque;
                    }

                    if (request.commands.index_actuator_command)
                    {
                        pos[1] = request.commands.index_actuator_command->position;
                        vel[1] = request.commands.index_actuator_command->velocity;
                        torque[1] = request.commands.index_actuator_command->torque;
                    }

                    if (request.commands.middle_actuator_command)
                    {
                        pos[2] = request.commands.middle_actuator_command->position;
                        vel[2] = request.commands.middle_actuator_command->velocity;
                        torque[2] = request.commands.middle_actuator_command->torque;
                    }

                    if (request.commands.ring_pinky_actuator_command)
                    {
                        pos[3] = request.commands.ring_pinky_actuator_command->position;
                        vel[3] = request.commands.ring_pinky_actuator_command->velocity;
                        torque[3] = request.commands.ring_pinky_actuator_command->torque;
                    }

                    if (request.commands.thumb_opposition_actuator_command)
                    {
                        pos[4] = request.commands.thumb_opposition_actuator_command->position;
                        vel[4] = request.commands.thumb_opposition_actuator_command->velocity;
                        torque[4] = request.commands.thumb_opposition_actuator_command->torque;
                    }

                    new_dexterity::ndx_protocol::serialize(hand_id_, buffer, 
                            (new_dexterity::ndx_protocol::command_actuators_response)
                                new_dexterity::ndx_protocol::command_actuators_request_acknowledged{});


                    ++valid_received_commands_;
                    ++executed_commands_;
            }

            template <typename Buffer>
            void prepare_actuator_info_response(Buffer& buffer)
            {
                new_dexterity::ndx_protocol::serialize(hand_id_, buffer,
                        new_dexterity::ndx_protocol::actuator_info_response 
                        {
                            new_dexterity::ndx_protocol::actuator_info{pos[0], vel[0], torque[0], false, 52, 0},
                            new_dexterity::ndx_protocol::actuator_info{pos[1], vel[1], torque[1], false, 52, 0},
                            new_dexterity::ndx_protocol::actuator_info{pos[2], vel[2], torque[2], false, 52, 0},
                            new_dexterity::ndx_protocol::actuator_info{pos[3], vel[3], torque[3], false, 52, 0},
                            new_dexterity::ndx_protocol::actuator_info{pos[4], vel[4], torque[4], false, 52, 0}
                        });
            }

            template <typename Buffer>
            void prepare_hand_status_response(Buffer& buffer)
            {
                new_dexterity::ndx_protocol::hand_status_response response;
                response.mcu_time = mcu_time_;
                response.mcu_temperature = mcu_temperature_;
                response.last_hand_error_code = last_hand_error_code_;
                response.received_commands = received_commands_;
                response.valid_received_commands = valid_received_commands_;
                response.executed_commands = executed_commands_;
                new_dexterity::ndx_protocol::serialize(hand_id_, buffer, response);
            }

            template <typename Buffer>
            void prepare_communication_status_response(Buffer& buffer)
            {
                new_dexterity::ndx_protocol::communication_status_response response;
                response.mcu_time = mcu_time_;
                response.last_communication_error_code = last_communication_error_code_;
                response.received_packets = received_packets_;
                response.valid_received_packets = valid_received_packets_;
                response.prepared_packets = prepared_packets_;
                response.transmitted_packets = transmitted_packets_;

                new_dexterity::ndx_protocol::serialize(hand_id_, buffer, response);
            }

            template <typename Buffer>
            void prepare_actuators_status_response(Buffer& buffer, actuators_status_request const & request)
            {
                new_dexterity::ndx_protocol::actuators_status_response response;

                response.mcu_time = mcu_time_;

                new_dexterity::ndx_protocol::actuator_status status;
                status.last_actuator_error_code = 0;
                status.max_temperature = 27;
                status.avg_temperature = 27;
                status.min_temperature = 27;
                status.max_torque = 0;
                status.avg_torque = 0;
                status.min_torque = 0;

                if (request.actuators.thumb_actuator)
                    response.thumb_actuator_status = status;
                if (request.actuators.index_actuator)
                    response.index_actuator_status = status;
                if (request.actuators.middle_actuator)
                    response.middle_actuator_status = status;
                if (request.actuators.ring_pinky_actuator)
                    response.ring_pinky_actuator_status = status;
                if (request.actuators.thumb_opposition_actuator)
                    response.thumb_opposition_actuator_status = status;

                new_dexterity::ndx_protocol::serialize(hand_id_, buffer, response);
            }

            template <typename Buffer>
            void transmit(Buffer& buffer)
            {
                ++prepared_packets_;
                ++transmitted_packets_;
                out_connection_.access([&](std::vector<std::uint8_t> & buff)
                {
                    std::copy(buffer.begin(), buffer.end(), std::back_inserter(buff));   
                });

                out_connection_.send_bytes();
            }

            void handle_message(message_type const & message)
            {
                auto& header = message.first;

                if (header.id == 254)
                {
                    auto result = new_dexterity::ndx_protocol::deserializer<hand_id_request>::deserialize(message);

                    if (result)
                    {
                        std::vector<std::uint8_t> buffer;
                        new_dexterity::ndx_protocol::serialize(hand_id_, buffer, new_dexterity::ndx_protocol::hand_id_response { hand_id_ });
                        transmit(buffer);
                        ++valid_received_packets_;
                    }
                }

                if (header.id != hand_id_)
                    return;

                {
                    auto result = new_dexterity::ndx_protocol::deserializer<command_actuators_request>::deserialize(message);

                    if (result)
                    {
                        std::vector<std::uint8_t> buffer;
                        prepare_command_actuators_response(buffer, *result);
                        transmit(buffer);
                        ++valid_received_packets_;
                    }
                }

                {
                    auto result = new_dexterity::ndx_protocol::deserializer<actuator_info_request>::deserialize(message);

                    if (result)
                    {
                        std::vector<std::uint8_t> buffer;
                        prepare_actuator_info_response(buffer);
                        transmit(buffer);
                        ++valid_received_packets_;
                    }
                }

                {
                    auto result = new_dexterity::ndx_protocol::deserializer<hand_status_request>::deserialize(message);

                    if (result)
                    {
                        std::vector<std::uint8_t> buffer;
                        prepare_hand_status_response(buffer);
                        transmit(buffer);
                        ++valid_received_packets_;
                    }
                }

                {
                    auto result = new_dexterity::ndx_protocol::deserializer<communication_status_request>::deserialize(message);

                    if (result)
                    {
                        std::vector<std::uint8_t> buffer;
                        prepare_communication_status_response(buffer);
                        transmit(buffer);
                        ++valid_received_packets_;
                    }
                }

                {
                    auto result = new_dexterity::ndx_protocol::deserializer<actuators_status_request>::deserialize(message);
                    
                    if (result)
                    {
                        std::vector<std::uint8_t> buffer;
                        prepare_actuators_status_response(buffer, *result);
                        transmit(buffer);
                        ++valid_received_packets_;
                    }
                }

                ++received_packets_;
            }

        private:
            float pos[5] = {0}, vel[5] = {0}, torque[5] = {0};
            std::uint32_t mcu_time_, received_commands_, valid_received_commands_, executed_commands_; 
            std::uint16_t mcu_temperature_, last_hand_error_code_;
            std::uint16_t last_communication_error_code_;
            std::uint32_t received_packets_, valid_received_packets_, prepared_packets_, transmitted_packets_;
            std::uint8_t hand_id_;
            new_dexterity::out_connection& out_connection_;
            new_dexterity::auto_subscribe<in_connection> auto_subscribe_;
    };

}
} // new_dexterity

#endif


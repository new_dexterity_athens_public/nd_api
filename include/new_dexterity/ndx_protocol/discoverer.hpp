#ifndef ND_NDX_PROTOCOL_DISCOVERER_HPP
#define ND_NDX_PROTOCOL_DISCOVERER_HPP

#include <new_dexterity/ndx_protocol/message_dispatcher.hpp>
#include <new_dexterity/ndx_protocol/serialize.hpp>
#include <new_dexterity/ndx_protocol/message_dispatcher.hpp>
#include <new_dexterity/ndx_protocol/in_connection.hpp>
#include <memory>
#include <algorithm>
#include <mutex>
#include <vector>
#include <thread>

namespace new_dexterity
{
namespace ndx_protocol
{

/*  A class used to query the id of connected hands.
 *
 * The @ref new_dexterity::communication_interface class models the communication with a hand with a known id.
 * It's often the case that we don't know the id of a hand. This class uses the broadcast address to retrieve 
 * a vector of connected hand ids.
 */

class discoverer
{
public:

    using message_type = new_dexterity::ndx_protocol::message_dispatcher::message_type;
    using in_connection_type = new_dexterity::ndx_protocol::in_connection;

    /* @param in The object representing the incoming connection.
     */

    discoverer(in_connection_type& in, new_dexterity::out_connection& out)
        : out_connection_(out)
        , auto_subscribe_(in, [this](message_type const & message){ handle_message(message); })
    {
    }


    /*  Method used to query the hand ids.
     *
     * @tparam Duration 
     * @code
     * // d is Duration
     * std::this_thread::sleep_for(d);
     * @endcode
     *
     * @param duration The Duration used to wait for responses.
     * @result A vector containing the hand ids.
     */

    template <typename Duration>
    std::vector<std::uint8_t> discover(Duration duration)
    {
        {
            std::lock_guard<std::mutex> guard(mutex_);

            ids_.clear();

            out_connection_.access([](std::vector<std::uint8_t>& buffer)
            {
                new_dexterity::ndx_protocol::serialize(254, buffer, new_dexterity::ndx_protocol::hand_id_request{});
            }); 

            out_connection_.send_bytes();
        }

        std::this_thread::sleep_for(duration);

        return ids_;
    }

private:

    void handle_message(message_type const & message)
    {
        auto& header = message.first;

        std::lock_guard<std::mutex> guard(mutex_);
            
        if (header.class_id == MSG_CLASS && header.msg_id == ID_ID)
        {
            ids_.push_back(header.id);
        }

    }

    std::vector<std::uint8_t> ids_;
    std::mutex mutex_;
    new_dexterity::out_connection& out_connection_;
    new_dexterity::auto_subscribe<in_connection_type> auto_subscribe_;

};

}

} // new_dexterity


#endif

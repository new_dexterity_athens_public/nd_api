#ifndef ND_NDX_PROTOCOL_COMMUNICATION_INTERFACE_HPP
#define ND_NDX_PROTOCOL_COMMUNICATION_INTERFACE_HPP

#include <new_dexterity/communication_interface.hpp>
#include <new_dexterity/ndx_protocol/communication_policy.hpp>

namespace new_dexterity
{
namespace ndx_protocol
{
    using communication_interface = new_dexterity::communication_interface<new_dexterity::ndx_protocol::communication_policy>;
}
}

#endif

#ifndef ND_NDX_PROTOCOL_DESERIALIZE_REQUEST_HPP
#define ND_NDX_PROTOCOL_DESERIALIZE_REQUEST_HPP

#include <boost/optional.hpp>
#include <new_dexterity/ndx_protocol/protocol.hpp>
#include <new_dexterity/ndx_protocol/requests.hpp>
#include <new_dexterity/ndx_protocol/deserialize_base.hpp>
#include <utility>

namespace new_dexterity
{
namespace ndx_protocol
{
    template <>
    struct deserializer<new_dexterity::ndx_protocol::hand_id_request>
    {
        static boost::optional<new_dexterity::ndx_protocol::hand_id_request>
        deserialize(message_type const & msg)
        {
            auto& header = msg.first;
            auto& message = msg.second;
            
            if (header.class_id != MSG_CLASS)  
                return boost::none;
     
            if (header.msg_id != ID_ID)
                return boost::none;
     
            if (message.size() != 0)
                return boost::none;

            return new_dexterity::ndx_protocol::hand_id_request{};
        }
    };

    template <>
    struct deserializer<new_dexterity::ndx_protocol::actuator_info_request>
    {
        static boost::optional<new_dexterity::ndx_protocol::actuator_info_request>
        deserialize(message_type const & msg)
        {
            auto& header = msg.first;
            auto& message = msg.second;
            
            if (header.class_id != MSG_CLASS)  
                return boost::none;
     
            if (header.msg_id != ACT_ID)
                return boost::none;
     
            if (message.size() != 0)
                return boost::none;

            return new_dexterity::ndx_protocol::actuator_info_request{};
        }
    };


    template <>
    struct deserializer<new_dexterity::ndx_protocol::command_actuators_request>
    {
        static boost::optional<new_dexterity::ndx_protocol::command_actuators_request>
        deserialize(message_type const & msg)
        {
            auto& header = msg.first;
            auto& message = msg.second;
            
            if (header.class_id != COM_CLASS)  
                return boost::none;
     
            if (header.msg_id != ACOM_ID)
                return boost::none;
     
            if (message.size() < sizeof(ACOM_header))
                return boost::none;

            auto it = message.begin();

            ACOM_header acom;
            std::copy_n(it, sizeof(acom), reinterpret_cast<std::uint8_t*>(&acom));
            std::advance(it, sizeof(acom));

            if (acom.num_actuators > 5)
                return boost::none;

            if (static_cast<std::size_t>(std::distance(it, message.end())) != acom.num_actuators * sizeof(ACOMx_struct))
                return boost::none;

            auto commands = new_dexterity::ndx_protocol::actuator_commands();

            for (std::size_t i = 0; i != acom.num_actuators; ++i)
            {
                ACOMx_struct acomx;
                std::copy_n(it, sizeof(acomx), reinterpret_cast<std::uint8_t*>(&acomx));
                std::advance(it, sizeof(acomx));

                new_dexterity::ndx_protocol::actuator_command command;
                command.position = acomx.position;
                command.velocity = acomx.velocity;
                command.torque = acomx.torque;

                if (!(acomx.id >= 1 && acomx.id <= 5))
                    return boost::none;

                if (acomx.id == 1)
                {
                    if (acomx.type != DYNX_MOTOR)
                        return boost::none;

                    commands.thumb_actuator_command = command;
                }

                if (acomx.id == 2)
                {
                    if (acomx.type != DYNX_MOTOR)
                        return boost::none;
                    commands.index_actuator_command = command;
                }

                if (acomx.id == 3)
                {
                    if (acomx.type != DYNX_MOTOR)
                        return boost::none;
                    commands.middle_actuator_command = command;
                }

                if (acomx.id == 4)
                {
                    if (acomx.type != DYNX_MOTOR)
                        return boost::none;
                    commands.ring_pinky_actuator_command = command;
                }

                if (acomx.id == 5)
                {
                    if (acomx.type != MOTOR_PWM)
                        return boost::none;
                    commands.thumb_opposition_actuator_command = command;
                }

            }

            return new_dexterity::ndx_protocol::command_actuators_request{commands};
        }
    };

    template <>
    struct deserializer<new_dexterity::ndx_protocol::hand_status_request>
    {
        static boost::optional<new_dexterity::ndx_protocol::hand_status_request>
        deserialize(message_type const & msg)
        {
            auto& header = msg.first;
            auto& message = msg.second;
            
            if (header.class_id != MSG_CLASS)  
                return boost::none;
     
            if (header.msg_id != STH_ID)
                return boost::none;
     
            if (message.size() != 0)
                return boost::none;

            return new_dexterity::ndx_protocol::hand_status_request{};
        }
    };

    template <>
    struct deserializer<new_dexterity::ndx_protocol::communication_status_request>
    {
        static boost::optional<new_dexterity::ndx_protocol::communication_status_request>
        deserialize(message_type const & msg)
        {
            auto& header = msg.first;
            auto& message = msg.second;
            
            if (header.class_id != MSG_CLASS)  
                return boost::none;
     
            if (header.msg_id != STC_ID)
                return boost::none;
     
            if (message.size() != 0)
                return boost::none;

            return new_dexterity::ndx_protocol::communication_status_request{};
        }
    };

    template <>
    struct deserializer<new_dexterity::ndx_protocol::actuators_status_request>
    {
        static boost::optional<new_dexterity::ndx_protocol::actuators_status_request>
        deserialize(message_type const & msg)
        {
            auto& header = msg.first;
            auto& message = msg.second;
            
            if (header.class_id != MSG_CLASS)  
                return boost::none;
     
            if (header.msg_id != STA_ID)
                return boost::none;
     
            if (message.size() < 1)
                return boost::none;

            auto it = message.begin();

            std::uint8_t num;
            std::copy_n(it, sizeof(num), reinterpret_cast<std::uint8_t*>(&num));
            std::advance(it, sizeof(num));

            if (num > 5)
                return boost::none;

            if (static_cast<std::size_t>(std::distance(it, message.end())) != num)
                return boost::none;

            auto actuators = new_dexterity::ndx_protocol::actuator_selection { false, false, false, false, false };

            for (std::size_t  i = 0; i != num; ++i)
            {
                std::uint8_t id;
                std::copy_n(it, sizeof(id), reinterpret_cast<std::uint8_t*>(&id));
                std::advance(it, sizeof(id));

                if (!(id >= 1 && id <= 5))
                    return boost::none;

                if (id == 1)
                    actuators.thumb_actuator = true;
                if (id == 2)
                    actuators.index_actuator = true;
                if (id == 3)
                    actuators.middle_actuator = true;
                if (id == 4)
                    actuators.ring_pinky_actuator = true;
                if (id == 5)
                    actuators.thumb_opposition_actuator = true;
            }

            return new_dexterity::ndx_protocol::actuators_status_request{ actuators };
        }
    };

    template <>
    struct deserializer<new_dexterity::ndx_protocol::software_calibration_request>
    {
        static boost::optional<new_dexterity::ndx_protocol::software_calibration_request>
        deserialize(message_type const & msg)
        {
            auto& header = msg.first;
            auto& message = msg.second;
            
            if (header.class_id != CFG_CLASS)  
                return boost::none;
     
            if (header.msg_id != CAL_ID)
                return boost::none;

            if (message.size() != 1)
                return boost::none;

            auto it = message.begin();

            std::uint8_t id;
            std::copy_n(it, sizeof(id), reinterpret_cast<std::uint8_t*>(&id));
            std::advance(it, sizeof(id));

            if (!(id == 2))
                return boost::none;

            return new_dexterity::ndx_protocol::software_calibration_request{};
        }
    };

    template <>
    struct deserializer<new_dexterity::ndx_protocol::hardware_calibration_request>
    {
        static boost::optional<new_dexterity::ndx_protocol::hardware_calibration_request>
        deserialize(message_type const & msg)
        {
            auto& header = msg.first;
            auto& message = msg.second;
            
            if (header.class_id != CFG_CLASS)  
                return boost::none;
     
            if (header.msg_id != CAL_ID)
                return boost::none;
     
            if (message.size() != 1)
                return boost::none;

            auto it = message.begin();

            std::uint8_t id;
            std::copy_n(it, sizeof(id), reinterpret_cast<std::uint8_t*>(&id));
            std::advance(it, sizeof(id));

            if (!(id == 1))
                return boost::none;

            return new_dexterity::ndx_protocol::hardware_calibration_request{};
        }
    };

    template <std::size_t Address, std::size_t Length>
    struct deserializer<new_dexterity::ndx_protocol::read_config_request<Address, Length>>
    {
        static boost::optional<new_dexterity::ndx_protocol::read_config_request<Address, Length>>
        deserialize(message_type const & msg)
        {
            auto& header = msg.first;
            auto& message = msg.second;
            
            if (header.class_id != CFG_CLASS)  
                return boost::none;
     
            if (header.msg_id != UCF_ID)
                return boost::none;
     
            if (message.size() != 3)
                return boost::none;

            auto it = message.begin();

            std::uint8_t flag;
            std::copy_n(it, sizeof(flag), reinterpret_cast<std::uint8_t*>(&flag));
            std::advance(it, sizeof(flag));

            if (flag != 0)
                return boost::none;

            std::uint8_t address;
            std::copy_n(it, sizeof(address), reinterpret_cast<std::uint8_t*>(&address));
            std::advance(it, sizeof(address));

            if (address != Address)
                return boost::none;

            std::uint8_t length;
            std::copy_n(it, sizeof(length), reinterpret_cast<std::uint8_t*>(&length));
            std::advance(it, sizeof(length));

            if (length != Length)
                return boost::none;

            return new_dexterity::ndx_protocol::read_config_request<Address,Length> { };
        }
    };

    template <std::size_t Address, std::size_t Length>
    struct deserializer<new_dexterity::ndx_protocol::write_config_request<Address,Length>>
    {
        static boost::optional<new_dexterity::ndx_protocol::write_config_request<Address,Length>>
        deserialize(message_type const & msg)
        {
            auto& header = msg.first;
            auto& message = msg.second;
            
            if (header.class_id != CFG_CLASS)  
                return boost::none;
     
            if (header.msg_id != UCF_ID)
                return boost::none;
     
            if (message.size() < 3)
                return boost::none;

            auto it = message.begin();

            std::uint8_t flag;
            std::copy_n(it, sizeof(flag), reinterpret_cast<std::uint8_t*>(&flag));
            std::advance(it, sizeof(flag));

            if (!(flag == 1 || flag == 2))
                return boost::none;

            std::uint8_t address;
            std::copy_n(it, sizeof(address), reinterpret_cast<std::uint8_t*>(&address));
            std::advance(it, sizeof(address));

            if (address != Address)
                return boost::none;

            std::uint8_t length;
            std::copy_n(it, sizeof(length), reinterpret_cast<std::uint8_t*>(&length));
            std::advance(it, sizeof(length));

            if (length != Length)
                return boost::none;

            if (static_cast<std::size_t>(std::distance(it, message.end())) != length)
                return boost::none;

            std::array<std::uint8_t, Length> data;
            std::copy_n(it, length, data.begin());

            return new_dexterity::ndx_protocol::write_config_request<Address, Length> { (flag == 1 ? false : true), data};
        }
    };

}
} // new_dexterity

#endif

#ifndef ND_NDX_PROTOCOL_RESPONSES_IO_HPP
#define ND_NDX_PROTOCOL_RESPONSES_IO_HPP

#include <iostream>
#include <new_dexterity/ndx_protocol/responses.hpp>
#include <boost/optional/optional_io.hpp>

namespace new_dexterity
{
namespace ndx_protocol
{ 
inline std::ostream& operator<<(std::ostream& out, hand_id_response const & rhs)
{
    return out << "hand_id_response { " << "id = " << rhs.id << " }";
}

inline std::ostream& operator<<(std::ostream& out, command_actuators_request_acknowledged const & rhs)
{
    return out << "command_actuators_request_acknowledged {}";
}

inline std::ostream& operator<<(std::ostream& out, command_actuators_request_not_acknowledged const & rhs)
{

    if (rhs == command_actuators_request_not_acknowledged::wrong_actuator_num)
        return out << "command_actuators_request_not_acknowledged::wrong_actuator_num";
    if (rhs == command_actuators_request_not_acknowledged::wrong_command)
        return out << "command_actuators_request_not_acknowledged::wrong_command";
    return out;
}

inline std::ostream& operator<<(std::ostream& out, actuator_info const & rhs)
{
    return out << "actuator_info { position = " << rhs.position << 
                                 " velocity = " << rhs.velocity << 
                                 " torque = " << rhs.torque << 
                                 " moving = " << rhs.moving << 
                                 " temperature = " << rhs.temperature << 
                                 " desired_actuation = " << rhs.desired_actuation << " }";
}

inline std::ostream& operator<<(std::ostream& out, actuator_error const & rhs)
{
    if (rhs == actuator_error::overload)
        return out << "actuator_error::overload";
    if (rhs == actuator_error::electrical_shock)
        return out << "actuator_error::electrical_shock";
    if (rhs == actuator_error::disconnected)
        return out << "actuator_error::disconnected";
    if (rhs == actuator_error::voltage_error)
        return out << "actuator_error::voltage_error";
    if (rhs == actuator_error::software_error)
        return out << "actuator_error::software_error";
    if (rhs == actuator_error::out_of_limits)
        return out << "actuator_error::out_of_limits";
    if (rhs == actuator_error::overheating)
        return out << "actuator_error::overheating";
    if (rhs == actuator_error::encoder_error)
        return out << "actuator_error::encoder_error";
    if (rhs == actuator_error::out_of_range)
        return out << "actuator_error::out_of_range";
    if (rhs == actuator_error::unknown_error)
        return out << "actuator_error::unknown_error";
    return out;
}

inline std::ostream& operator<<(std::ostream& out, actuator_info_response const & rhs)
{
    return out << "actuator_info_response { thumb_actuator_info_response = " << rhs.thumb_actuator_info_response <<
                                          " index_actuator_info_response = " << rhs.index_actuator_info_response <<
                                          " middle_actuator_info_response = " << rhs.middle_actuator_info_response <<
                                          " ring_pinky_actuator_info_response = " << rhs.ring_pinky_actuator_info_response <<
                                          " thumb_opposition_actuator_info_response = " << rhs.thumb_opposition_actuator_info_response << " }";

}

inline std::ostream& operator<<(std::ostream& out, hand_status_response const & rhs)
{
    return out << "hand_status_response { mcu_time = " << rhs.mcu_time << 
                                        " mcu_temperature = " << rhs.mcu_temperature << 
                                        " last_hand_error_code = " << rhs.last_hand_error_code << 
                                        " received_commands = " << rhs.received_commands << 
                                        " valid_received_commands = " << rhs.valid_received_commands << 
                                        " executed_commands = " << rhs.executed_commands << " }";
}

inline std::ostream& operator<<(std::ostream& out, communication_status_response const & rhs)
{
    return out << "communication_status_response { mcu_time = " << rhs.mcu_time << 
                                                 " last_communication_error_code = " << rhs.last_communication_error_code << 
                                                 " received_packets = " << rhs.received_packets << 
                                                 " valid_received_packets = " << rhs.valid_received_packets << 
                                                 " prepared_packets = " << rhs.prepared_packets << 
                                                 " transmitted_packets = " << rhs.transmitted_packets << " }";
}

inline std::ostream& operator<<(std::ostream& out, actuator_status const & rhs)
{
    return out << "actuator_status {" <<
                                  " last_actuator_error_code = " << rhs.last_actuator_error_code << 
                                  " max_temperature = " << rhs.max_temperature << 
                                  " min_temperature = " << rhs.min_temperature <<
                                  " avg_temperature = " << rhs.avg_temperature <<
                                  " max_torque = " << rhs.max_torque <<
                                  " min_torque = " << rhs.min_torque <<
                                  " avg_torque = " << rhs.avg_torque << " }";

}

inline std::ostream& operator<<(std::ostream& out, actuators_status_response const & rhs)
{
    return out << "actuators_status_response { " << 
                   " mcu_time = " << rhs.mcu_time <<
                   " thumb_actuator_status = " << rhs.thumb_actuator_status <<
                   " index_actuator_status = " << rhs.index_actuator_status <<
                   " middle_actuator_status = " << rhs.middle_actuator_status <<
                   " ring_pinky_actuator_status = " << rhs.ring_pinky_actuator_status <<
                   " thumb_opposition_actuator_status = " << rhs.thumb_opposition_actuator_status << " }";
}

template <std::size_t Length>
inline std::ostream& operator<<(std::ostream& out, config<Length> const & rhs)
{
    out << "config { data = ";

    for (auto c : rhs.data)
        out << "0x" << std::hex << (int)c << std::dec << " ";

    out << "}";

    return out;
}

inline std::ostream& operator<<(std::ostream& out, read_config_request_not_acknowledged const & rhs)
{
    if (rhs == read_config_request_not_acknowledged::incorrect_eeprom_address)
        return out << "read_config_request_not_acknowledged::incorrect_eeprom_address";
    if (rhs == read_config_request_not_acknowledged::eeprom_hardware_error)
        return out << "read_config_request_not_acknowledged::eeprom_hardware_error";
    return out;
}

inline std::ostream& operator<<(std::ostream& out, write_config_request_acknowledged const & rhs)
{
    return out << "write_config_request_acknowledged {}";
}

inline std::ostream& operator<<(std::ostream& out, write_config_request_not_acknowledged const & rhs)
{
    if (rhs == write_config_request_not_acknowledged::incorrect_eeprom_address)
        return out << "write_config_request_not_acknowledged::incorrect_eeprom_address";
    if (rhs == write_config_request_not_acknowledged::eeprom_hardware_error)
        return out << "write_config_request_not_acknowledged::eeprom_hardware_error";
    if (rhs == write_config_request_not_acknowledged::incorrect_data_format)
        return out << "write_config_request_not_acknowledged::incorrect_data_format";
    if (rhs == write_config_request_not_acknowledged::incorrect_data_length)
        return out << "write_config_request_not_acknowledged::incorrect_data_length";
    return out;
}

}

} // new_dexterity

#endif

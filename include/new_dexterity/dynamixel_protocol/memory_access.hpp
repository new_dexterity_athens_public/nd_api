#ifndef ND_DYNAMIXEL_PROTOCOL_MEMORY_ACCESS_HPP
#define ND_DYNAMIXEL_PROTOCOL_MEMORY_ACCESS_HPP

namespace new_dexterity
{
namespace dynamixel_protocol
{
    
template <std::size_t Address, std::size_t Length>
struct memory_access 
{
    static constexpr std::size_t address = Address;
    static constexpr std::size_t length = Length;
    static constexpr std::size_t end_address = address + length - 1;
    static constexpr bool value = true;

    static_assert(length != 0, "Memory access of 0 length does not make sense");
    static_assert(address >= 0 && end_address <= 661, "Out of bounds memory access");
};

}
}

#endif

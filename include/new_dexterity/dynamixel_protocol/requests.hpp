#ifndef ND_DYNAMIXEL_PROTOCOL_REQUESTS_HPP
#define ND_DYNAMIXEL_PROTOCOL_REQUESTS_HPP

//! @file
//! @brief Definition of Dynamixel Protocol 2.0 requests

#include <array>
#include <cstddef>
#include <cstdint>
#include <type_traits>
#include <utility>
#include <tuple>
#include <vector>
#include <new_dexterity/dynamixel_protocol/memory_content.hpp>


namespace new_dexterity
{
namespace dynamixel_protocol
{

/*! 
 * @brief Ping request.
 * @ingroup Requests
 */

struct ping_request { };

/*! 
 * @brief Dynamic read memory request
 * @ingroup Requests
 */

struct dynamic_read_memory_request
{
    //! @brief The control-table starting address of the request.
    std::uint16_t address;
    //! @brief The number of bytes to retrieve starting from the address.
    std::uint16_t length;
};

/*!
 * @brief Dynamic write memory request
 * @ingroup Requests
 */
struct dynamic_write_memory_request
{
    //! @brief The control-table starting address of the request.
    std::uint16_t address;
    //! @brief The bytes to write.
    std::vector<std::uint8_t> data;
};

/*!
 * @brief Dynamic register write request
 * @ingroup Requests
 */
struct dynamic_reg_write_request
{
    //! @brief The control-table starting address of the request.
    std::uint16_t address;
    //! @brief The bytes to write
    std::vector<std::uint8_t> data;
};

/*!
 * @brief Action request
 * @ingroup Requests
 */
struct action_request { };

/*!
 * @brief Factory reset request
 * @ingroup Requests
 */
enum class factory_reset_request
{
    reset_all,
    reset_all_except_id,
    reset_all_except_id_and_baudrate
};

/*!
 * @brief Reboot request
 * @ingroup Requests
 */
struct reboot_request { };


/*! 
 * @brief Dynamic sync read request
 * @ingroup Requests
 */
struct dynamic_sync_read_request
{
    //! @brief The control-table starting address of the request.
    std::uint16_t address;
    //! @brief The number of bytes to retrieve starting from the address.
    std::uint16_t length;
    //! @brief The ids of the dynamixels.
    std::vector<std::uint8_t> ids;
};

/*! 
 * @brief Dynamic sync write request
 * @ingroup Requests
 */
struct dynamic_sync_write_request
{
    //! @brief The control-table starting address of the request.
    std::uint16_t address;
    //! @brief A Vector of dynamixel id - bytes pairs. The bytes vectors should have the same size.
    std::vector<std::pair<std::uint8_t, std::vector<std::uint8_t>>> data;
};

/*!
 * @brief Dynamic bulk read request
 * @ingroup Requests
 */
struct dynamic_bulk_read_request
{
    //! @brief  A vector of tuples of dynamixel id - beginning address - requested bytes.
    std::vector<std::tuple<std::uint8_t, std::uint16_t, std::uint16_t>> data;
};

/*!
 * @brief Dynamic bulk write request
 * @ingroup Requests
 */
struct dynamic_bulk_write_request
{
    //! @brief A vector of tuples of dynamixel id - beginning address - bytes to be written. The bytes vectors can have different sizes.
    std::vector<std::tuple<std::uint8_t, std::uint16_t, std::vector<std::uint8_t>>> data;
};

/*! 
 * @brief Compile-time checked address-based read memory request.
 *
 * @ingroup Requests
 *
 * This version performs simple compile-time checks to ensure that
 * the request is not out of bounds.
 * @tparam Address The control-table starting address of the request.
 * @tparam Length The number of bytes to retrieve starting from the Address.
 */
template <std::size_t Address, std::size_t Length>
struct read_memory_request 
{
    static_assert(memory_access<Address, Length>::value, "");
};

/*! 
 * @brief Compile-time checked address-based write memory request.
 *
 * @ingroup Requests
 *
 * This version performs simple compile-time checks to ensure that
 * the request is not out of bounds.
 * @tparam Address The control-table starting address of the request.
 * @tparam Length The number of bytes to write starting from the Address.
 */
template <std::size_t Address, std::size_t Length>
struct write_memory_request 
{
    //! @brief The data to write to the control table starting from the specified Address.
    std::array<std::uint8_t, Length> data;

    static_assert(memory_access<Address, Length>::value, "");
};

/*! 
 * @brief Compile-time checked address-based register write memory request.
 *
 * @ingroup Requests
 * 
 * This version performs simple compile-time checks to ensure that
 * the request is not out of bounds.
 * Registers a write memory request to be performed using the @ref action_request .
 * @tparam Address The control-table starting address of the request.
 * @tparam Length The number of bytes to write starting from the Address.
 */
template <std::size_t Address, std::size_t Length>
struct reg_write_request 
{
    //! @brief The data to write to the control table starting from the specified Address.
    std::array<std::uint8_t, Length> data;

    static_assert(memory_access<Address, Length>::value, "");
};

/*!
 * @brief Sync read request.
 *
 * @ingroup Requests
 *
 * Simultaneously performs a read memory request to the specified dynamixels.
 * @tparam Request The type of read request. One can provide any of the documented read memory requests.
 */
  
template <class Request>
struct sync_read
{ 
    //! @brief The request to b simultaneuously sent to the specified dynamixels.
    Request request;
    //! @brief The ids of the dynamixels to send the request to.
    std::vector<std::uint8_t> ids;
};

/*!
 * @brief Sync write request.
 *
 * @ingroup Requests
 *
 * Simultaneuously performs a write memory request to the specified dynamixels.
 * @tparam Request The type of write request. One can provide any of the documented write memory requests.
 */
template <class Request>
struct sync_write
{
    /*! @brief A vector of id-request pairs. 
     *
     * The requests have the same type as the one specified on the template parameter.
     * In case of dynamic_write_memory_request, the address and length should be the same 
     * on all of them.
     */

    std::vector<std::pair<std::uint8_t, Request>> requests;
};

/*!
 * @brief Convenient info request.
 *
 * @ingroup Requests
 *
 * This is a convenience request meant to retrieve a range of useful control table values.
 * Retrieves the moving flag, present position, velocity, current and temperature.
 */
struct info_request{};

/*!
 * @brief Convenient command request.
 *
 * @ingroup Requests
 * 
 * This is a convenience request meant to simultanously set the goal position, 
 * the velocity limit and the current limit.
 */
struct command_request
{
    //! @brief Goal position. No conversion is done.
    std::int32_t position;
    //! @brief Velocity limit. No conversion is done.
    std::uint32_t velocity_limit;
    //! @brief Current limit. No conversion is done.
    std::int16_t current_limit;
};


/*!
 * @brief Bulk read request.
 *
 * @ingroup Requests
 *
 * Simultaneously performs a read memory request to the specified dynamixels.
 * @tparam Requests A type pack of the requests to be performed. One can provide any of the documented read memory requests.
 */

/* TODO
 * The requirement of the Requests pack means that the number of requests is limited by the compile-time information.
 * This is stricly uneccesary.
 * A better approach would be to make the bulk read request take a vector of type-erased request serializers.
 * A single helper could then be used to construct the object by using the appropriate helpers from the serializers file.
 * This would also merge the dynamic_bulk_read and bulk_read requests.
 */

template <class... Requests>
struct bulk_read
{
    /*! @brief a tuple of id-request pairs. 
     * The requests should correspond to the parameter pack.
     * This is inconvenient so the @ref make_bulk_read_request helper is provided to help create bulk_read objects.
     */

    std::tuple<
        std::pair<
            std::uint8_t,
            Requests>...> requests;
};

/*!
 * @brief Bulk write request.
 *
 * @ingroup Requests
 *
 * Simultaneusly performs a write memory request to the specified dynamixels.
 * @tparam Requests a Type pack of the requests to be performed. One can provide any of the documented write memory requests.
 */

/* TODO
 * The requirement of the Requests pack means that the number of requests is limited by the compile-time information.
 * This is stricly uneccesary.
 * A better approach would be to make the bulk write request take a vector of type-erased request serializers.
 * A single helper could then be used to construct the object by using the appropriate helpers from the serializers file.
 * This would also merge the dynamic_bulk_write and bulk_write requests.
 */

template <class... Requests>
struct bulk_write
{
    /*! @brief a tuple of id-requests pairs.
     * The request should correspond to the parameter pack.
     * This is inconvenient so the @ref make_bulk_write_request helper is provided to help create bulk_write objects.
     */
    std::tuple<
        std::pair<
            std::uint8_t,
            Requests>...> requests;
};


/*!
 * @brief Helper to create bulk read requests.
 *
 * Accepts a variadic list of pairs of ids and read memory requests and builds the corresponding bulk_read object with
 * the correct parameter pack.
 */
template <class... Pairs>
auto make_bulk_read_request(Pairs&&... ts) -> bulk_read<typename std::decay<decltype(std::declval<Pairs>().second)>::type...>
{
    return bulk_read<typename std::decay<decltype(std::declval<Pairs>().second)>::type...>{std::make_tuple(std::forward<Pairs>(ts)...)};
}

/*!
 * @brief Helper to create bulk write requests.
 *
 * Accepts a variadic list of pairs of ids and write memory requests and builds the corresponding bulk_write object with
 * the correct parameter pack.
 */
template <class... Pairs>
auto make_bulk_write_request(Pairs&&... ts) -> bulk_write<typename std::decay<decltype(std::declval<Pairs>().second)>::type...>
{
    return bulk_write<typename std::decay<decltype(std::declval<Pairs>().second)>::type...>{std::make_tuple(std::forward<Pairs>(ts)...)};
}

/*! 
 * @brief A control-table aware high-level read memory request.
 *
 * @ingroup Requests
 *
 * This is the bread and butter of this library. If it wasn't for this, @ref set_memory_content_request and @ref get_memory_content_response,
 * this library would not have much purpose. The user supplies a list of @ref memory_handles representing the control-table values and the library automatically
 * extracts the address and length information forming the lower level read request.
 */

template <class... Ts>
struct get_memory_content_request : 
    details::non_asserting_memory_content_range_info<Ts...>
{
};

/*! 
 * @brief A control-table aware high-level write memory request.
 *
 * @ingroup Requests
 *
 * This is the bread and butter of this library. If it wasn't for this, @ref get_memory_content_request and @ref get_memory_content_response,
 * this library would not have much purpose. The user supplies a list of @ref memory_handles representing the control-table values and the library automatically
 * extracts the address and length information and serializes the handles forming the lower level write request.
 * It is not meant to be constructed manually, instead Use the @ref make_set_memory_content_request helper.
 */
template <class... Ts>
struct set_memory_content_request : 
    details::asserting_memory_content_range_info<Ts...>,
    details::assert_is_writable<Ts...>, memory_content<Ts...>
{
    using memory_content<Ts...>::memory_content;
};

/*!
 * @brief Helper to create @ref set_memory_content_request "set_memory_content_requests"
 *
 * Accepts a variadic list of @ref memory_handles and  builds the corresponding @ref set_memory_content_request object.
 */
template <class... Ts>
auto make_set_memory_content_request(Ts&&... ts) -> set_memory_content_request<typename std::decay<Ts>::type...>
{
    return set_memory_content_request<typename std::decay<Ts>::type...>(std::forward<Ts>(ts)...);
}


}
}

#endif

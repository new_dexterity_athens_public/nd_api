#ifndef ND_DYNAMIXEL_PROTOCOL_RESPONSE_OF_HPP
#define ND_DYNAMIXEL_PROTOCOL_RESPONSE_OF_HPP

#include <new_dexterity/dynamixel_protocol/responses.hpp>
#include <new_dexterity/dynamixel_protocol/requests.hpp>

namespace new_dexterity
{
namespace dynamixel_protocol
{

template <typename T>
struct response_of;

template <std::size_t Address, std::size_t Length>
struct response_of<read_memory_request<Address, Length>>
{
    using type = read_memory_response<Length>;
};

template <std::size_t Address, std::size_t Length>
struct response_of<write_memory_request<Address, Length>>
{
    using type = write_memory_response;
};

template <class... Ts>
struct response_of<set_memory_content_request<Ts...>>
{
    using type = write_memory_response;
};

template <class... Ts>
struct response_of<get_memory_content_request<Ts...>>
{
    using type = get_memory_content_response<Ts...>;
};

template <>
struct response_of<ping_request>
{
    using type = ping_response;
};

template <std::size_t Address, std::size_t Length>
struct response_of<reg_write_request<Address, Length>>
{
    using type = reg_write_response;
};

template <>
struct response_of<action_request>
{
    using type = action_response;
};

template <>
struct response_of<factory_reset_request>
{
    using type = factory_reset_response;
};

template <>
struct response_of<reboot_request>
{
    using type = reboot_response;
};

template <>
struct response_of<dynamic_read_memory_request>
{
    using type = dynamic_read_memory_response;
};

template <>
struct response_of<dynamic_write_memory_request>
{
    using type = write_memory_response;
};

template <>
struct response_of<dynamic_reg_write_request>
{
    using type = reg_write_response;
};

template <>
struct response_of<command_request>
{
    using type = write_memory_response;
};

template <>
struct response_of<info_request>
{
    using type = info_response;
};

}
}

#endif

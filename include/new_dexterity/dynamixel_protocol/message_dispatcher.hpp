#ifndef ND_DYNAMIXEL_PROTOCOL_MESSAGE_DISPATCHER_HPP
#define ND_DYNAMIXEL_PROTOCOL_MESSAGE_DISPATCHER_HPP

#include <new_dexterity/message_dispatcher.hpp>
#include <new_dexterity/dynamixel_protocol/parser.hpp>

namespace new_dexterity
{
namespace dynamixel_protocol
{

    using message_dispatcher = new_dexterity::message_dispatcher<new_dexterity::dynamixel_protocol::parser>;

}
}

#endif

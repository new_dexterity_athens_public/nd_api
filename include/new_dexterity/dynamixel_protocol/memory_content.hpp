#ifndef ND_DYNAMIXEL_PROTOCOL_MEMORY_CONTENT_HPP
#define ND_DYNAMIXEL_PROTOCOL_MEMORY_CONTENT_HPP

#include <boost/mpl/vector.hpp>
#include <boost/mpl/sort.hpp>
#include <boost/mpl/inserter.hpp>
#include <type_traits>
#include <utility>
#include <new_dexterity/dynamixel_protocol/memory_access.hpp>

namespace new_dexterity
{
namespace dynamixel_protocol
{

template <class Wrapper, std::size_t Address, class ContentType, bool IsWritable, ContentType Wrapper::*MemPtr, std::size_t NextAddress>
struct memory_metainfo : memory_access<Address, sizeof(ContentType)>
{
    using wrapper = Wrapper;
    using content_type = ContentType;
    static bool constexpr is_writable = IsWritable;
    static constexpr ContentType Wrapper::*mem_ptr = MemPtr;
    static constexpr std::size_t next_valid_address = NextAddress;

    static_assert(memory_metainfo::length == sizeof(memory_metainfo::content_type), "Content type and memory length should be the same");
};

template <class T>
struct info_of;

template <class T>
using info_of_t = typename info_of<T>::type;

//! @brief Memory handles namespace
namespace memory_handles
{
}

#define DEFINE_MEMORY_HANDLE(name, address, tp, w, next)                                           \
    namespace memory_handles {                                                                     \
    struct name                                                                                    \
    {                                                                                              \
        tp value;                                                                                  \
    };                                                                                             \
                                                                                                   \
    struct name##_wrapper                                                                          \
    {                                                                                              \
                                                                                                   \
        name##_wrapper() = default;                                                                \
                                                                                                   \
        name##_wrapper(tp const & value)                                                           \
            : name(value)                                                                          \
        {                                                                                          \
        }                                                                                          \
                                                                                                   \
        tp name;                                                                                   \
    };                                                                                             \
    }                                                                                              \
                                                                                                   \
    template <>                                                                                    \
    struct info_of<memory_handles::name>                                                         \
    {                                                                                              \
        using type = memory_metainfo<memory_handles::name##_wrapper, address, tp, w, &memory_handles::name##_wrapper::name, next>; \
    };

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::model_number
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#model-number> model number </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 * @var new_dexterity::dynamixel_protocol::memory_handles::model_number::value value
 */
DEFINE_MEMORY_HANDLE(model_number,           0,   std::uint16_t, false, 2)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::model_information
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#model-information> model information </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(model_information,      2,   std::uint32_t, false, 6)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::firmware_version
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#firmware-version> firmware version </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(firmware_version,       6,   std::uint8_t,  false, 7)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::id
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#id> id </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(id,                     7,   std::uint8_t,  true, 8)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::baud_rate
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#baud-rate> baud rate </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(baud_rate,              8,   std::uint8_t,  true, 9)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::return_delay_time
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#return-delay-time> return delay time </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(return_delay_time,      9,   std::uint8_t,  true, 10)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::drive_mode
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#drive-mode> drive mode </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(drive_mode,             10,  std::uint8_t,  true, 11)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::operating_mode
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#operating-mode> operating mode </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(operating_mode,         11,  std::uint8_t,  true, 12)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::secondary_id
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#secondary-id> secondary id </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(secondary_id,           12,  std::uint8_t,  true, 13)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::protocol_version
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#protocol-version> protocol version </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(protocol_version,       13,  std::uint8_t,  true, 20)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::homing_offset
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#homing-offset> homing offset </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(homing_offset,          20,  std::int32_t,  true, 24)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::moving_threshold
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#moving-threshold> moving threshold </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(moving_threshold,       24,  std::uint32_t, true, 31)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::temperature_limit
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#temperature-limit> temperature limit </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(temperature_limit,      31,  std::uint8_t,  true, 32)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::max_voltage_limit
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#max-voltage-limit> max voltage limit </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(max_voltage_limit,      32,  std::uint16_t, true, 34)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::min_voltage_limit
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#min-voltage-limit> min voltage limit </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(min_voltage_limit,      34,  std::uint16_t, true, 36)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::pwm_limit
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#pwm-limit> pwm limit </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(pwm_limit,              36,  std::int16_t, true, 38)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::current_limit
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#current-limit> current limit </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(current_limit,          38,  std::int16_t, true, 44)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::velocity_limit
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#velocity-limit> velocity limit </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(velocity_limit,         44,  std::int32_t, true, 48)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::max_position_limit
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#max-position-limit> max position limit </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(max_position_limit,     48,  std::int32_t, true, 52)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::min_position_limit
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#min-position-limit> min position limit </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(min_position_limit,     52,  std::int32_t, true, 63)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::shutdown
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#shutdown> shutdown </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(shutdown,               63,  std::uint8_t,  true, 64)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::torque_enable
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#torque-enable> torque enable </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(torque_enable,          64,  std::uint8_t,  true, 65)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::led
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#led> led </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(led,                    65,  std::uint8_t,  true, 68)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::status_return_level
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#status-return-level> status return level </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(status_return_level,    68,  std::uint8_t,  true, 69)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::registered_instruction
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#registered-instruction> registered instruction </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(registered_instruction, 69,  std::uint8_t,  false, 70)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::hardware_error_status
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#hardware-error-status> hardware error status </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(hardware_error_status,  70,  std::uint8_t,  false, 76)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::velocity_i_gain
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#velocity-i-gain> velocity i gain </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(velocity_i_gain,        76,  std::uint16_t, true, 78)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::velocity_p_gain
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#velocity-p-gain> velocity p gain </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(velocity_p_gain,        78,  std::uint16_t, true, 80)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::position_d_gain
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#position-d-gain> position d gain </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(position_d_gain,        80,  std::uint16_t, true, 82)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::position_i_gain
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#position-i-gain> position i gain </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(position_i_gain,        82,  std::uint16_t, true, 84)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::position_p_gain
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#position-p-gain> position p gain </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(position_p_gain,        84,  std::uint16_t, true, 88)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::feedforward_2nd_gain
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#feedforward-2nd-gain> feedforward 2nd gain </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(feedforward_2nd_gain,   88,  std::uint16_t, true, 90)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::feedforward_1st_gain
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#feedforward-1st-gain> feedforward 1st gain </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(feedforward_1st_gain,   90,  std::uint16_t, true, 98)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::bus_watchdog
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#bus-watchdog> bus watchdog </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(bus_watchdog,           98,  std::uint8_t,  true, 100)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::goal_pwm
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#goal-pwm> goal pwm </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(goal_pwm,               100, std::int16_t,  true, 102)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::goal_current
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#goal-current> goal current </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(goal_current,           102, std::int16_t,  true, 104)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::goal_velocity
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#goal-velocity> goal velocity </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(goal_velocity,          104, std::int32_t,  true, 108)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::profile_acceleration
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#profile-acceleration> profile acceleration </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(profile_acceleration,   108, std::uint32_t, true, 112)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::profile_velocity
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#profile-velocity> profile velocity </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(profile_velocity,       112, std::uint32_t, true, 116)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::goal_position
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#goal-position> goal position </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(goal_position,          116, std::int32_t,  true, 120)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::realtime_tick
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#realtime-tick> realtime tick </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(realtime_tick,          120, std::uint16_t, false, 122)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::moving
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#moving> moving </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(moving,                 122, std::uint8_t,  false, 123)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::moving_status
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#moving-status> moving status </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(moving_status,          123, std::uint8_t,  false, 124)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::present_pwm
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#present-pwm> present pwm </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(present_pwm,            124, std::int16_t, false, 126)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::present_current
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#present-current> present current </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(present_current,        126, std::int16_t, false, 128)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::present_velocity
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#present-velocity> present velocity </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(present_velocity,       128, std::int32_t, false, 132)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::present_position
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#present-position> present position </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(present_position,       132, std::int32_t, false, 136)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::velocity_trajectory
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#velocity-trajectory> velocity trajectory </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(velocity_trajectory,    136, std::int32_t, false, 140)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::position_trajectory
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#position-trajectory> position trajectory </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(position_trajectory,    140, std::int32_t, false, 144)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::present_input_voltage
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#present-input-voltage> present input voltage </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(present_input_voltage,  144, std::uint16_t, false, 146)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::present_temperature
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#present-temperature> present temperature </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(present_temperature,    146, std::uint8_t,  false, 168)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_1
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-1> indirect address 1 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_1,     168, std::uint16_t, true, 170)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_2
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-2> indirect address 2 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_2,     170, std::uint16_t, true, 172)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_3
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-3> indirect address 3 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_3,     172, std::uint16_t, true, 174)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_4
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-4> indirect address 4 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_4,     174, std::uint16_t, true, 176)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_5
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-5> indirect address 5 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_5,     176, std::uint16_t, true, 178)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_6
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-6> indirect address 6 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_6,     178, std::uint16_t, true, 180)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_7
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-7> indirect address 7 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_7,     180, std::uint16_t, true, 182)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_8
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-8> indirect address 8 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_8,     182, std::uint16_t, true, 184)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_9
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-9> indirect address 9 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_9,     184, std::uint16_t, true, 186)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_10
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-10> indirect address 10 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_10,    186, std::uint16_t, true, 188)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_11
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-11> indirect address 11 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_11,    188, std::uint16_t, true, 190)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_12
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-12> indirect address 12 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_12,    190, std::uint16_t, true, 192)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_13
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-13> indirect address 13 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_13,    192, std::uint16_t, true, 194)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_14
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-14> indirect address 14 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_14,    194, std::uint16_t, true, 196)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_15
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-15> indirect address 15 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_15,    196, std::uint16_t, true, 198)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_16
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-16> indirect address 16 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_16,    198, std::uint16_t, true, 200)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_17
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-17> indirect address 17 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_17,    200, std::uint16_t, true, 202)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_18
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-18> indirect address 18 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_18,    202, std::uint16_t, true, 204)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_19
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-19> indirect address 19 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_19,    204, std::uint16_t, true, 206)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_20
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-20> indirect address 20 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_20,    206, std::uint16_t, true, 208)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_21
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-21> indirect address 21 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_21,    208, std::uint16_t, true, 210)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_22
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-22> indirect address 22 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_22,    210, std::uint16_t, true, 212)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_23
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-23> indirect address 23 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_23,    212, std::uint16_t, true, 214)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_24
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-24> indirect address 24 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_24,    214, std::uint16_t, true, 216)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_25
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-25> indirect address 25 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_25,    216, std::uint16_t, true, 218)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_26
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-26> indirect address 26 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_26,    218, std::uint16_t, true, 220)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_27
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-27> indirect address 27 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_27,    220, std::uint16_t, true, 222)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_28
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-28> indirect address 28 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_28,    222, std::uint16_t, true, 224)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_1
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-1> indirect data 1 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_1,        224, std::uint8_t,  true, 225)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_2
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-2> indirect data 2 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_2,        225, std::uint8_t,  true, 226)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_3
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-3> indirect data 3 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_3,        226, std::uint8_t,  true, 227)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_4
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-4> indirect data 4 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_4,        227, std::uint8_t,  true, 228)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_5
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-5> indirect data 5 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_5,        228, std::uint8_t,  true, 229)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_6
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-6> indirect data 6 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_6,        229, std::uint8_t,  true, 230)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_7
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-7> indirect data 7 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_7,        230, std::uint8_t,  true, 231)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_8
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-8> indirect data 8 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_8,        231, std::uint8_t,  true, 232)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_9
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-9> indirect data 9 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_9,        232, std::uint8_t,  true, 233)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_10
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-10> indirect data 10 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_10,       233, std::uint8_t,  true, 234)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_11
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-11> indirect data 11 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_11,       234, std::uint8_t,  true, 235)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_12
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-12> indirect data 12 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_12,       235, std::uint8_t,  true, 236)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_13
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-13> indirect data 13 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_13,       236, std::uint8_t,  true, 237)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_14
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-14> indirect data 14 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_14,       237, std::uint8_t,  true, 238)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_15
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-15> indirect data 15 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_15,       238, std::uint8_t,  true, 239)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_16
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-16> indirect data 16 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_16,       239, std::uint8_t,  true, 240)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_17
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-17> indirect data 17 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_17,       240, std::uint8_t,  true, 241)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_18
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-18> indirect data 18 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_18,       241, std::uint8_t,  true, 242)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_19
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-19> indirect data 19 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_19,       242, std::uint8_t,  true, 243)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_20
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-20> indirect data 20 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_20,       243, std::uint8_t,  true, 244)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_21
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-21> indirect data 21 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_21,       244, std::uint8_t,  true, 245)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_22
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-22> indirect data 22 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_22,       245, std::uint8_t,  true, 246)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_23
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-23> indirect data 23 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_23,       246, std::uint8_t,  true, 247)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_24
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-24> indirect data 24 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_24,       247, std::uint8_t,  true, 248)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_25
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-25> indirect data 25 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_25,       248, std::uint8_t,  true, 249)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_26
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-26> indirect data 26 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_26,       249, std::uint8_t,  true, 250)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_27
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-27> indirect data 27 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_27,       250, std::uint8_t,  true, 251)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_28
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-28> indirect data 28 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_28,       251, std::uint8_t,  true, 578)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_29
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-29> indirect address 29 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_29,    578, std::uint16_t, true, 580)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_30
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-30> indirect address 30 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_30,    580, std::uint16_t, true, 582)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_31
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-31> indirect address 31 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_31,    582, std::uint16_t, true, 584)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_32
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-32> indirect address 32 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_32,    584, std::uint16_t, true, 586)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_33
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-33> indirect address 33 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_33,    586, std::uint16_t, true, 588)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_34
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-34> indirect address 34 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_34,    588, std::uint16_t, true, 590)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_35
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-35> indirect address 35 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_35,    590, std::uint16_t, true, 592)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_36
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-36> indirect address 36 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_36,    592, std::uint16_t, true, 594)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_37
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-37> indirect address 37 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_37,    594, std::uint16_t, true, 596)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_38
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-38> indirect address 38 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_38,    596, std::uint16_t, true, 598)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_39
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-39> indirect address 39 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_39,    598, std::uint16_t, true, 600)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_40
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-40> indirect address 40 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_40,    600, std::uint16_t, true, 602)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_41
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-41> indirect address 41 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_41,    602, std::uint16_t, true, 604)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_42
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-42> indirect address 42 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_42,    604, std::uint16_t, true, 606)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_43
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-43> indirect address 43 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_43,    606, std::uint16_t, true, 608)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_44
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-44> indirect address 44 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_44,    608, std::uint16_t, true, 610)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_45
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-45> indirect address 45 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_45,    610, std::uint16_t, true, 612)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_46
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-46> indirect address 46 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_46,    612, std::uint16_t, true, 614)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_47
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-47> indirect address 47 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_47,    614, std::uint16_t, true, 616)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_48
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-48> indirect address 48 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_48,    616, std::uint16_t, true, 618)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_49
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-49> indirect address 49 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_49,    618, std::uint16_t, true, 620)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_50
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-50> indirect address 50 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_50,    620, std::uint16_t, true, 622)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_51
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-51> indirect address 51 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_51,    622, std::uint16_t, true, 624)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_52
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-52> indirect address 52 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_52,    624, std::uint16_t, true, 626)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_53
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-53> indirect address 53 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_53,    626, std::uint16_t, true, 628)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_54
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-54> indirect address 54 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_54,    628, std::uint16_t, true, 630)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_55
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-55> indirect address 55 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_55,    630, std::uint16_t, true, 632)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_address_56
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-address-56> indirect address 56 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_address_56,    632, std::uint16_t, true, 634)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_29
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-29> indirect data 29 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_29,       634, std::uint8_t,  true, 635)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_30
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-30> indirect data 30 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_30,       635, std::uint8_t,  true, 636)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_31
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-31> indirect data 31 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_31,       636, std::uint8_t,  true, 637)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_32
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-32> indirect data 32 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_32,       637, std::uint8_t,  true, 638)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_33
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-33> indirect data 33 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_33,       638, std::uint8_t,  true, 639)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_34
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-34> indirect data 34 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_34,       639, std::uint8_t,  true, 640)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_35
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-35> indirect data 35 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_35,       640, std::uint8_t,  true, 641)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_36
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-36> indirect data 36 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_36,       641, std::uint8_t,  true, 642)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_37
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-37> indirect data 37 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_37,       642, std::uint8_t,  true, 643)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_38
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-38> indirect data 38 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_38,       643, std::uint8_t,  true, 644)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_39
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-39> indirect data 39 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_39,       644, std::uint8_t,  true, 645)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_40
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-40> indirect data 40 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_40,       645, std::uint8_t,  true, 646)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_41
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-41> indirect data 41 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_41,       646, std::uint8_t,  true, 647)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_42
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-42> indirect data 42 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_42,       647, std::uint8_t,  true, 648)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_43
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-43> indirect data 43 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_43,       648, std::uint8_t,  true, 649)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_44
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-44> indirect data 44 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_44,       649, std::uint8_t,  true, 650)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_45
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-45> indirect data 45 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_45,       650, std::uint8_t,  true, 651)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_46
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-46> indirect data 46 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_46,       651, std::uint8_t,  true, 652)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_47
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-47> indirect data 47 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_47,       652, std::uint8_t,  true, 653)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_48
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-48> indirect data 48 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_48,       653, std::uint8_t,  true, 654)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_49
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-49> indirect data 49 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_49,       654, std::uint8_t,  true, 655)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_50
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-50> indirect data 50 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_50,       655, std::uint8_t,  true, 656)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_51
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-51> indirect data 51 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_51,       656, std::uint8_t,  true, 657)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_52
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-52> indirect data 52 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_52,       657, std::uint8_t,  true, 658)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_53
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-53> indirect data 53 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_53,       658, std::uint8_t,  true, 659)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_54
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-54> indirect data 54 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_54,       659, std::uint8_t,  true, 660)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_55
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-55> indirect data 55 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_55,       660, std::uint8_t,  true, 661)

/*!
 * @class new_dexterity::dynamixel_protocol::memory_handles::indirect_data_56
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#indirect-data-56> indirect data 56 </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */
DEFINE_MEMORY_HANDLE(indirect_data_56,       661, std::uint8_t,  true, 661)


#undef DEFINE_MEMORY_HANDLE

namespace details
{

    template <bool AssertContinuity, class... Ts>
    struct memory_content_range_info;
    
    template <class T, bool AssertContinuity>
    struct memory_content_range_info<AssertContinuity, T> { 
        static constexpr std::size_t start_address = info_of_t<T>::address;
        static constexpr std::size_t total_length = info_of_t<T>::length;
    };

    template <class T, class... Ts, bool AssertContinuity>
    struct memory_content_range_info<AssertContinuity, T, Ts...>
    {
        static constexpr std::size_t start_address = info_of_t<T>::address;
        static constexpr std::size_t request_length = info_of_t<T>::length;

        static constexpr std::size_t next_start_address = memory_content_range_info<AssertContinuity, Ts...>::start_address;
        static constexpr std::size_t next_valid_address = info_of_t<T>::next_valid_address;
        static constexpr std::size_t total_length = memory_content_range_info<AssertContinuity, Ts...>::total_length + (next_start_address - start_address);

        static_assert(!AssertContinuity || 
                (next_start_address == next_valid_address),
                "Only continuous memory content can be specified otherwise control table values may be ovewritten by mistake");
    };


    template <class... Ts>
    struct is_writable_assertion;

    template <class T>
    struct is_writable_assertion<T>
    {
        static_assert(info_of_t<T>::is_writable, "The memory content specified should be writable");
    };

    template <class T, class... Ts>
    struct is_writable_assertion<T, Ts...> : is_writable_assertion<T>, is_writable_assertion<Ts...>
    {
    };

    template <class T>
    struct get_address_of
    {
        using type = boost::mpl::int_<info_of_t<T>::address>;
    };

    template <class V, class T>
    struct variadic_append;

    template <template <typename...> class V, class... Ts, class T>
    struct variadic_append<V<Ts...>, T>
    {
        using type = V<Ts..., T>;
    };

    template <template <class...> class Wrapper, class... Ts>
    using sort_on_address = 
        typename boost::mpl::fold<
            typename boost::mpl::sort<
                boost::mpl::vector<Ts...>,
                boost::mpl::less<
                    details::get_address_of<boost::mpl::_1>, 
                    details::get_address_of<boost::mpl::_2>>>::type,
            Wrapper<>,
            details::variadic_append<boost::mpl::_1, boost::mpl::_2>
        >::type;

    template <class... Ts>
    struct asserting_memory_content_range_info_helper : details::memory_content_range_info<true, Ts...> {};

    template <class... Ts>
    using asserting_memory_content_range_info = sort_on_address<asserting_memory_content_range_info_helper, Ts...>;

    template <class... Ts>
    struct non_asserting_memory_content_range_info_helper : details::memory_content_range_info<false, Ts...> {};

    template <class... Ts>
    using non_asserting_memory_content_range_info = sort_on_address<non_asserting_memory_content_range_info_helper, Ts...>;
    
    template <class... Ts>
    using assert_is_writable = sort_on_address<details::is_writable_assertion, Ts...>;
}

/*!
 * @brief Class used to implement @ref set_memory_content_request and @ref get_memory_content_response.
 *
 * @ingroup Requests
 * @ingroup Responses
 *
 * It uses TMP to inherit from wrappers of the @ref memory_handles that provide the values that the handles represent
 * as member values with the correct name, size and signess.
 */
template <class... Ts>
// cppcheck-suppress syntaxError
struct memory_content : info_of_t<Ts>::wrapper...  
{
    memory_content() = default;

    memory_content(Ts const & ... args)
        : info_of_t<Ts>::wrapper(args.value)...
    {
    }
};


} // dynamixel_protocol
} // new_dexterity

/*
 * @class new_dexterity::dynamixel_protocol::STRUCT_NAME
 * @brief A memory handle representing the <a href=http://emanual.robotis.com/docs/en/dxl/x/xh430-v350/#WITH_DASHES> WITH_SPACES </a> value of the XH430-V350's control table.
 * @ingroup MemoryHandles
 */


#endif

#ifndef ND_DYNAMIXEL_PROTOCOL_COMMUNICATION_POLICY_HPP
#define ND_DYNAMIXEL_PROTOCOL_COMMUNICATION_POLICY_HPP

#include <tuple>
#include <new_dexterity/dynamixel_protocol/requests.hpp>
#include <new_dexterity/dynamixel_protocol/responses.hpp>
#include <new_dexterity/dynamixel_protocol/serialize.hpp>
#include <new_dexterity/dynamixel_protocol/deserialize.hpp>
#include <new_dexterity/dynamixel_protocol/response_of.hpp>
#include <new_dexterity/dynamixel_protocol/parser.hpp>
#include <boost/optional.hpp>

namespace new_dexterity
{
namespace dynamixel_protocol
{

    struct communication_policy
    {
        using parser_type = new_dexterity::dynamixel_protocol::parser;

        using message_type = parser_type::message_type;

        template <typename Request>
        using response_of = typename new_dexterity::dynamixel_protocol::response_of<Request>::type;

        using implicit_identifier = std::uint8_t;

        template <typename Buffer, typename Request>
        static void serialize(implicit_identifier id, Buffer& b, Request const & req)
        {
            new_dexterity::dynamixel_protocol::serialize(id, b, req);
        }

        template <typename Response>
        static boost::optional<Response>
        deserialize(implicit_identifier id, message_type const & msg)
        {
            using test = new_dexterity::dynamixel_protocol::deserializer<Response>;

            return test::deserialize(id, msg);
        }

        static constexpr bool can_distinguish_responses = true;

    };
}
}

#endif

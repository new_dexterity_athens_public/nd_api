#ifndef ND_DYNAMIXEL_PROTOCOL_SERIALIZE_HPP
#define ND_DYNAMIXEL_PROTOCOL_SERIALIZE_HPP

#include <new_dexterity/dynamixel_protocol/requests.hpp>
#include <new_dexterity/dynamixel_protocol/parser.hpp>
#include <new_dexterity/dynamixel_protocol/memory_content.hpp>
#include <type_traits>
#include <tuple>

namespace new_dexterity
{
namespace dynamixel_protocol
{
    template <typename ItSource, typename ItDest>
    std::size_t add_byte_stuffings(ItSource it, ItSource end, ItDest dest)
    {
        std::uint8_t state = 0;

        std::size_t byte_stuffings = 0;

        while (it != end) 
        {
            switch (state)
            {
            case 0:
                if (*it == 0xff)
                    state = 1;
                break;
            case 1:
                if (*it == 0xff)
                    state = 2;
                else
                    state = 0;
                break;
            case 2:
                if (*it == 0xfd)
                    state = 3;
                else if (*it == 0xff)
                    state = 2;
                else
                    state = 0;
                break;
            case 3:
                *dest = 0xfd;
                ++byte_stuffings;
                state = 0;
                break;
            }

            *dest = *it++;
        }

        return byte_stuffings;
    }

    template <class Buffer>
    void serialize(uint8_t id, Buffer& buffer, ping_request const & request)
    {
        auto start = buffer.size();

        buffer.push_back(0xff);
        buffer.push_back(0xff);
        buffer.push_back(0xfd);

        buffer.push_back(0x00);

        buffer.push_back(id);

        buffer.push_back(0x03);
        buffer.push_back(0x00);

        buffer.push_back(0x01);

        uint16_t no_crc_total_len = buffer.size() - start;

        auto crc = update_crc(0, &buffer[start], no_crc_total_len);

        buffer.push_back(crc & 0xff);
        buffer.push_back((crc >> 8) & 0xff);
    }



    template <class Buffer>
    void serialize(uint8_t id, Buffer& buffer, dynamic_read_memory_request const & request)
    {
        auto start = buffer.size();

        buffer.push_back(0xff);
        buffer.push_back(0xff);
        buffer.push_back(0xfd);

        buffer.push_back(0x00);

        buffer.push_back(id);

        buffer.push_back(0x07);
        buffer.push_back(0x00);

        buffer.push_back(0x02);

        buffer.push_back(request.address & 0xff);
        buffer.push_back(request.address >> 8 & 0xff);

        buffer.push_back(request.length & 0xff);
        buffer.push_back(request.length >> 8 & 0xff);

        auto crc = update_crc(0, buffer.data() + start, 12);

        buffer.push_back(crc & 0xff);
        buffer.push_back((crc >> 8) & 0xff);
    }

    namespace details
    {
        template <class Buffer>
        void write_helper(uint8_t instruction, uint8_t id, uint16_t address, uint16_t length, Buffer& buffer, uint8_t const * data)
        {
            auto start = buffer.size();

            buffer.push_back(0xff);
            buffer.push_back(0xff);
            buffer.push_back(0xfd);

            buffer.push_back(0x00);

            buffer.push_back(id);

            buffer.push_back(0x00);
            buffer.push_back(0x00);

            buffer.push_back(instruction);

            buffer.push_back(address & 0xff);
            buffer.push_back(address >> 8 & 0xff);

            add_byte_stuffings(data, data + length, std::back_inserter(buffer));

            uint16_t no_crc_total_len = buffer.size() - start;
            uint16_t payload_len = no_crc_total_len - 7;
            uint16_t packet_len = payload_len + 2;

            buffer[start + 5] = packet_len & 0xff;
            buffer[start + 6] = (packet_len >> 8) & 0xff;

            auto crc = update_crc(0, &buffer[start], no_crc_total_len);

            buffer.push_back(crc & 0xff);
            buffer.push_back((crc >> 8) & 0xff);
        }

    }

    template <typename Buffer>
    void serialize(uint8_t id, Buffer& buffer, dynamic_write_memory_request const & request)
    {
        details::write_helper(0x03, id, request.address, request.data.size(), buffer, request.data.data());
    }

    template <class Buffer>
    void serialize(uint8_t id, Buffer& buffer, dynamic_reg_write_request const & request)
    {
        details::write_helper(0x04, id, request.address, request.data.size(), buffer, request.data.data());
    }

    template <class Buffer>
    void serialize(uint8_t id, Buffer& buffer, action_request)
    {
        auto start = buffer.size();

        buffer.push_back(0xff);
        buffer.push_back(0xff);
        buffer.push_back(0xfd);

        buffer.push_back(0x00);

        buffer.push_back(id);

        buffer.push_back(0x03);
        buffer.push_back(0x00);

        buffer.push_back(0x05);

        uint16_t no_crc_total_len = buffer.size() - start;
        auto crc = update_crc(0, buffer.data() + start, no_crc_total_len);

        buffer.push_back(crc & 0xff);
        buffer.push_back((crc >> 8) & 0xff);
    }

    template <typename Buffer>
    void serialize(uint8_t id, Buffer& buffer, factory_reset_request const & request)
    {
        auto start = buffer.size();

        buffer.push_back(0xff);
        buffer.push_back(0xff);
        buffer.push_back(0xfd);

        buffer.push_back(0x00);

        buffer.push_back(id);

        buffer.push_back(0x04);
        buffer.push_back(0x00);

        buffer.push_back(0x06);

        if (request == factory_reset_request::reset_all)
            buffer.push_back(0xff);
        if (request == factory_reset_request::reset_all_except_id)
            buffer.push_back(0x01);
        if (request == factory_reset_request::reset_all_except_id_and_baudrate)
            buffer.push_back(0x02);

        uint16_t no_crc_total_len = buffer.size() - start;

        auto crc = update_crc(0, &buffer[start], no_crc_total_len);

        buffer.push_back(crc & 0xff);
        buffer.push_back((crc >> 8) & 0xff);
    }

    template <typename Buffer>
    void serialize(uint8_t id, Buffer& buffer, reboot_request const & request)
    {
        auto start = buffer.size();

        buffer.push_back(0xff);
        buffer.push_back(0xff);
        buffer.push_back(0xfd);

        buffer.push_back(0x00);

        buffer.push_back(id);

        buffer.push_back(0x03);
        buffer.push_back(0x00);

        buffer.push_back(0x08);

        uint16_t no_crc_total_len = buffer.size() - start;

        auto crc = update_crc(0, &buffer[start], no_crc_total_len);

        buffer.push_back(crc & 0xff);
        buffer.push_back((crc >> 8) & 0xff);
    }

    template <typename Buffer>
    void serialize(Buffer& buffer, dynamic_sync_read_request const & request)
    {
            if (request.ids.size() == 0)
                throw std::invalid_argument("dynamic_sync_read_request should not have empty ids.");

            auto start = buffer.size();

            buffer.push_back(0xff);
            buffer.push_back(0xff);
            buffer.push_back(0xfd);

            buffer.push_back(0x00);

            buffer.push_back(0xfe);

            buffer.push_back({});
            buffer.push_back({});

            buffer.push_back(0x82);

            buffer.push_back(request.address & 0xff);
            buffer.push_back(request.address >> 8 & 0xff);
            buffer.push_back(request.length & 0xff);
            buffer.push_back(request.length >> 8 & 0xff);

            add_byte_stuffings(request.ids.begin(), request.ids.end(), std::back_inserter(buffer));

            uint16_t no_crc_total_len = buffer.size() - start;
            uint16_t payload_len = no_crc_total_len - 7;
            uint16_t packet_len = payload_len + 2;

            buffer[start + 5] = packet_len & 0xff;
            buffer[start + 6] = (packet_len >> 8) & 0xff;

            auto crc = update_crc(0, &buffer[start], no_crc_total_len);

            buffer.push_back(crc & 0xff);
            buffer.push_back((crc >> 8) & 0xff);
    }

    template <typename Buffer>
    void serialize(Buffer& buffer, dynamic_sync_write_request const & request)
    {
            if (request.data.size() == 0)
                throw std::invalid_argument("dynamic_sync_write_request should not have empty data.");
            auto& first = request.data[0];

            for (auto const & p : request.data)
            {
                if (p.second.size() != first.second.size())
                    throw std::invalid_argument("Length specified on dynamic_sync_write_request and length of the given payloads should be the same");
            }

            auto start = buffer.size(); 

            buffer.push_back(0xff);
            buffer.push_back(0xff);
            buffer.push_back(0xfd);

            buffer.push_back(0x00);

            buffer.push_back(0xfe);

            buffer.push_back(0x00);
            buffer.push_back(0x00);

            buffer.push_back(0x83);

            buffer.push_back(request.address & 0xff);
            buffer.push_back((request.address >> 8) & 0xff);
            buffer.push_back(first.second.size() & 0xff);
            buffer.push_back((first.second.size() >> 8) & 0xff);

            std::vector<std::uint8_t> tmp_buffer;

            for (auto const & p : request.data)
            {
                tmp_buffer.push_back(p.first);
                tmp_buffer.insert(tmp_buffer.end(), p.second.begin(), p.second.end());
            }

            add_byte_stuffings(tmp_buffer.begin(), tmp_buffer.end(), std::back_inserter(buffer));

            uint16_t no_crc_total_len = buffer.size() - start;
            uint16_t payload_len = no_crc_total_len - 7;
            uint16_t packet_len = payload_len + 2;

            buffer[start + 5] = packet_len & 0xff;
            buffer[start + 6] = (packet_len >> 8) & 0xff;

            auto crc = update_crc(0, &buffer[start], no_crc_total_len);

            buffer.push_back(crc & 0xff);
            buffer.push_back((crc >> 8) & 0xff);
    }

    template <typename Buffer>
    void serialize(Buffer& buffer, dynamic_bulk_read_request const & request)
    {
        if (request.data.size() == 0)
            throw std::invalid_argument("dynamic_bulk_read_request should not have empty data.");
        auto start = buffer.size();

        buffer.push_back(0xff);
        buffer.push_back(0xff);
        buffer.push_back(0xfd);

        buffer.push_back(0x00);
        buffer.push_back(0xfe);

        buffer.push_back({});
        buffer.push_back({});

        buffer.push_back(0x92);

        std::vector<std::uint8_t> tmp_buffer;

        for (auto const & tup : request.data)
        {
            auto const & id = std::get<0>(tup);
            auto const & address = std::get<1>(tup);
            auto const & length = std::get<2>(tup);

            buffer.push_back(id);
            buffer.push_back(address & 0xff);
            buffer.push_back((address  >> 8) & 0xff);
            buffer.push_back(length & 0xff);
            buffer.push_back((length >> 8) & 0xff);
        }

        add_byte_stuffings(tmp_buffer.begin(), tmp_buffer.end(), std::back_inserter(buffer));

        uint16_t no_crc_total_len = buffer.size() - start;
        uint16_t payload_len = no_crc_total_len - 7;
        uint16_t packet_len = payload_len + 2;

        buffer[start + 5] = packet_len & 0xff;
        buffer[start + 6] = (packet_len >> 8) & 0xff;

        auto crc = update_crc(0, &buffer[start], no_crc_total_len);

        buffer.push_back(crc & 0xff);
        buffer.push_back((crc >> 8) & 0xff);

    }

    template <typename Buffer>
    void serialize(Buffer& buffer, dynamic_bulk_write_request const & request)
    {
        if (request.data.size() == 0)
            throw std::invalid_argument("dynamic_bulk_write_request should not have empty data.");

        auto start = buffer.size();

        buffer.push_back(0xff);
        buffer.push_back(0xff);
        buffer.push_back(0xfd);

        buffer.push_back(0x00);
        buffer.push_back(0xfe);

        buffer.push_back({});
        buffer.push_back({});

        buffer.push_back(0x93);

        std::vector<std::uint8_t> tmp_buffer;

        for (auto const & tup : request.data)
        {
            auto const & id = std::get<0>(tup);
            auto const & address = std::get<1>(tup);
            auto const & data = std::get<2>(tup);

            tmp_buffer.push_back(id);
            tmp_buffer.push_back(address & 0xff);
            tmp_buffer.push_back((address  >> 8) & 0xff);
            tmp_buffer.push_back(data.size() & 0xff);
            tmp_buffer.push_back((data.size() >> 8) & 0xff);

            tmp_buffer.insert(tmp_buffer.end(), data.begin(), data.end());
        }

        add_byte_stuffings(tmp_buffer.begin(), tmp_buffer.end(), std::back_inserter(buffer));

        uint16_t no_crc_total_len = buffer.size() - start;
        uint16_t payload_len = no_crc_total_len - 7;
        uint16_t packet_len = payload_len + 2;

        buffer[start + 5] = packet_len & 0xff;
        buffer[start + 6] = (packet_len >> 8) & 0xff;

        auto crc = update_crc(0, &buffer[start], no_crc_total_len);

        buffer.push_back(crc & 0xff);
        buffer.push_back((crc >> 8) & 0xff);
    }

    template <typename Buffer, std::size_t Address, std::size_t Length>
    void serialize(std::uint8_t id, Buffer& buffer, const read_memory_request<Address, Length>& request)
    {
        serialize(id, buffer, dynamic_read_memory_request{ static_cast<std::uint16_t>(Address), static_cast<std::uint16_t>(Length) });
    }

    template <typename Buffer, std::size_t Address, std::size_t Length>
    void serialize(std::uint8_t id, Buffer& buffer, write_memory_request<Address, Length> const & request)
    {
        std::vector<std::uint8_t> buff;
        std::copy(request.data.begin(), request.data.end(), std::back_inserter(buff));
        serialize(id, buffer, dynamic_write_memory_request{ static_cast<std::uint16_t>(Address), std::move(buff)});
    }

    template <typename Buffer, std::size_t Address, std::size_t Length>
    void serialize(std::uint8_t id, Buffer& buffer, reg_write_request<Address, Length> const & request)
    {
        std::vector<std::uint8_t> buff;
        std::copy(request.data.begin(), request.data.end(), std::back_inserter(buff));
        serialize(id, buffer, dynamic_reg_write_request{ static_cast<std::uint16_t>(Address), std::move(buff)});
    }

    template <class Buffer, class... Ts>
    void serialize(std::uint8_t id, Buffer& buffer, get_memory_content_request<Ts...> const & request)
    {
            serialize(id, buffer, read_memory_request<
                get_memory_content_request<Ts...>::start_address,
                get_memory_content_request<Ts...>::total_length>{});

    }

    namespace details
    {
            template <std::size_t Index, class T, class Buffer, typename std::enable_if<sizeof(T) == 1>::type* = nullptr>
            void ser_primitive(Buffer& buffer, T value) { 
                buffer[Index+0] = (value & 0xff);
            }

            template <std::size_t Index, class T, class Buffer, typename std::enable_if<sizeof(T) == 2>::type* = nullptr>
            void ser_primitive(Buffer& buffer, T value) { 
                buffer[Index+0] = (value & 0xff);
                buffer[Index+1] = ((value >> 8) & 0xff);
            }

            template <std::size_t Index, class T, class Buffer, typename std::enable_if<sizeof(T) == 4>::type* = nullptr>
            void ser_primitive(Buffer& buffer, T value) { 
                buffer[Index+0] = (value & 0xff);
                buffer[Index+1] = ((value >> 8) & 0xff);
                buffer[Index+2] = ((value >> 16) & 0xff);
                buffer[Index+3] = ((value >> 24) & 0xff);
            }

        template <class... Rs>
        struct set_memory_helper;

        template <>
        struct set_memory_helper<>
        {
            template <std::size_t, class Buffer, class MemCont>
            static void serialize(Buffer&, MemCont& cont)
            {
            }
        };

        template <class R, class... Rs>
        struct set_memory_helper<R, Rs...>
        {
            template <std::size_t FirstAddress, class Buffer, class MemCont>
            static void serialize(Buffer& buffer, MemCont const & cont)
            {
                typename info_of_t<R>::wrapper const & r_slice = cont;
                using content_type = typename info_of_t<R>::content_type;
                auto value = (r_slice.*info_of_t<R>::mem_ptr);
                constexpr std::size_t index = info_of_t<R>::address - FirstAddress;
                ser_primitive<index, content_type>(buffer, value);
                set_memory_helper<Rs...>::template serialize<FirstAddress>(buffer, cont);
            }
        };
    }

    template <class Buffer, class... Ts>
    void serialize(std::uint8_t id, Buffer& buffer, set_memory_content_request<Ts...> const & request)
    {
        constexpr std::size_t start_address = set_memory_content_request<Ts...>::start_address;
        constexpr std::size_t total_length = set_memory_content_request<Ts...>::total_length;

        std::array<std::uint8_t, total_length> tmp_buffer{};

        details::sort_on_address<details::set_memory_helper, Ts...>::template serialize<start_address>(tmp_buffer, request);

        serialize(id, buffer, write_memory_request< start_address, total_length> { tmp_buffer });
    }

    namespace details
    {
        template <class Buffer, class Request>
        void sync_read_helper(std::uint16_t address, std::uint16_t length, Buffer& buffer, Request const & request)
        {
            if (request.ids.size() == 0)
                throw std::invalid_argument("sync_read's ids should not have empty ids.");

            auto start = buffer.size();

            buffer.push_back(0xff);
            buffer.push_back(0xff);
            buffer.push_back(0xfd);

            buffer.push_back(0x00);

            buffer.push_back(0xfe);

            buffer.push_back({});
            buffer.push_back({});

            buffer.push_back(0x82);

            buffer.push_back(address & 0xff);
            buffer.push_back(address >> 8 & 0xff);
            buffer.push_back(length & 0xff);
            buffer.push_back(length >> 8 & 0xff);

            add_byte_stuffings(request.ids.begin(), request.ids.end(), std::back_inserter(buffer));

            uint16_t no_crc_total_len = buffer.size() - start;
            uint16_t payload_len = no_crc_total_len - 7;
            uint16_t packet_len = payload_len + 2;

            buffer[start + 5] = packet_len & 0xff;
            buffer[start + 6] = (packet_len >> 8) & 0xff;

            auto crc = update_crc(0, &buffer[start], no_crc_total_len);

            buffer.push_back(crc & 0xff);
            buffer.push_back((crc >> 8) & 0xff);
        }

    }

    template <class Buffer>
    void serialize(Buffer& buffer, sync_read<dynamic_read_memory_request> const & request)
    {
        details::sync_read_helper(
                request.request.address, 
                request.request.length, buffer, request);
    }

    template <class Buffer, std::size_t Address, std::size_t Length>
    void serialize(Buffer& buffer, sync_read<read_memory_request<Address, Length>> const & request)
    {
        details::sync_read_helper(
                static_cast<std::uint16_t>(Address), 
                static_cast<std::uint16_t>(Length), buffer, request);
    }

    template <class Buffer, class... Ts>
    void serialize(Buffer& buffer, sync_read<get_memory_content_request<Ts...>> const & request)
    {
        details::sync_read_helper(
                static_cast<std::uint16_t>(get_memory_content_request<Ts...>::start_address), 
                static_cast<std::uint16_t>(get_memory_content_request<Ts...>::total_length), buffer, request);
    }

    template <class Buffer, class... Ts>
    void serialize(Buffer& buffer, sync_read<info_request> const & request)
    {
        using namespace memory_handles;
        using rmr = get_memory_content_request<moving, moving_status, present_pwm, present_current, present_velocity, present_position, velocity_trajectory, 
                                               position_trajectory, present_input_voltage, present_temperature>;

        details::sync_read_helper(
                static_cast<std::uint16_t>(rmr::start_address), 
                static_cast<std::uint16_t>(rmr::total_length), buffer, request);
    }

    namespace details
    {
        template <class Buffer>
        void append_helper(Buffer& buffer, dynamic_write_memory_request const & req)
        {
            std::copy(req.data.begin(), req.data.end(), std::back_inserter(buffer));
        }

        template <class Buffer, std::size_t Address, std::size_t Length>
        void append_helper(Buffer& buffer, write_memory_request<Address, Length> const & req)
        {
            std::copy(req.data.begin(), req.data.end(), std::back_inserter(buffer));
        }

        template <class Buffer, class... Ts>
        void append_helper(Buffer& buffer, set_memory_content_request<Ts...> const & req)
        {
            constexpr std::size_t start_address = set_memory_content_request<Ts...>::start_address;
            constexpr std::size_t total_length = set_memory_content_request<Ts...>::total_length;

            std::array<std::uint8_t, total_length> tmp_buffer{};

            details::sort_on_address<details::set_memory_helper, Ts...>::template serialize<start_address>(tmp_buffer, req);
            std::copy(tmp_buffer.begin(), tmp_buffer.end(), std::back_inserter(buffer));
        }

        template <class Buffer>
        void append_helper(Buffer& buffer, command_request const & req)
        {
            using namespace memory_handles;
            append_helper(buffer, 
                    make_set_memory_content_request(
                            goal_current{ req.current_limit },
                            goal_velocity { 0 },
                            profile_acceleration { 0 },
                            profile_velocity { req.velocity_limit },
                            goal_position { req.position }));
        }
    }

    namespace details
    {
        template <class Buffer, class Request>
        void sync_write_helper(std::uint16_t address, std::uint16_t length, Buffer& buffer, Request const & request)
        {
            auto start = buffer.size(); 

            buffer.push_back(0xff);
            buffer.push_back(0xff);
            buffer.push_back(0xfd);

            buffer.push_back(0x00);

            buffer.push_back(0xfe);

            buffer.push_back(0x00);
            buffer.push_back(0x00);

            buffer.push_back(0x83);

            buffer.push_back(address & 0xff);
            buffer.push_back(address >> 8 & 0xff);
            buffer.push_back(length & 0xff);
            buffer.push_back(length >> 8 & 0xff);

            std::vector<std::uint8_t> tmp_buffer;

            for (auto& p : request.requests)
            {
                tmp_buffer.push_back(p.first);
                append_helper(tmp_buffer, p.second);
            }

            add_byte_stuffings(tmp_buffer.begin(), tmp_buffer.end(), std::back_inserter(buffer));

            uint16_t no_crc_total_len = buffer.size() - start;
            uint16_t payload_len = no_crc_total_len - 7;
            uint16_t packet_len = payload_len + 2;

            buffer[start + 5] = packet_len & 0xff;
            buffer[start + 6] = (packet_len >> 8) & 0xff;

            auto crc = update_crc(0, &buffer[start], no_crc_total_len);

            buffer.push_back(crc & 0xff);
            buffer.push_back((crc >> 8) & 0xff);
        }


    }

    template <class Buffer>
    void serialize(Buffer& buffer, sync_write<dynamic_write_memory_request> const & request)
    {
        if (request.requests.size() == 0)
            return;

        auto& first = request.requests[0];

        if (!std::all_of(request.requests.begin(), request.requests.end(), 
                    [&first](std::pair<std::uint8_t, dynamic_write_memory_request> const & p)
        {
           return p.second.address == first.second.address && p.second.data.size() == first.second.data.size();
        }))
        {
            throw std::runtime_error("All requests in the sync write should specify the same address and length");
        }

        details::sync_write_helper(first.second.address, first.second.data.size(), buffer, request);
    }

    template <class Buffer, std::size_t Address, std::size_t Length>
    void serialize(Buffer& buffer, sync_write<write_memory_request<Address, Length>> const & request)
    {
        details::sync_write_helper(static_cast<std::uint16_t>(Address), static_cast<std::uint16_t>(Length), buffer, request);
    }

    template <class Buffer, class... Ts>
    void serialize(Buffer& buffer, sync_write<set_memory_content_request<Ts...>> const & request)
    {
        details::sync_write_helper(static_cast<std::uint16_t>(set_memory_content_request<Ts...>::start_address), 
                                   static_cast<std::uint16_t>(set_memory_content_request<Ts...>::total_length), buffer, request);

    }

    template <class Buffer>
    void serialize(Buffer& buffer, sync_write<command_request> const & request)
    {
        using namespace memory_handles;
        using smr = set_memory_content_request<goal_current, goal_velocity, profile_acceleration, profile_velocity, goal_position>;

        details::sync_write_helper(static_cast<std::uint16_t>(smr::start_address), 
                                   static_cast<std::uint16_t>(smr::total_length), buffer, request);
    }


    namespace details
    {
        std::size_t address_helper(dynamic_read_memory_request const & req) { return req.address; }
        std::size_t length_helper(dynamic_read_memory_request const & req) { return req.length; }

        template <std::size_t Address, std::size_t Length>
        std::size_t address_helper(read_memory_request<Address, Length> const &) { return Address; }
        template <std::size_t Address, std::size_t Length>
        std::size_t length_helper(read_memory_request<Address, Length> const &) { return Length; }

        template <class... Ts>
        std::size_t address_helper(get_memory_content_request<Ts...> const &) { return get_memory_content_request<Ts...>::start_address; }
        template <class... Ts>
        std::size_t length_helper(get_memory_content_request<Ts...> const &) { return get_memory_content_request<Ts...>::total_length; }

        std::size_t address_helper(info_request const & req) { 
                using namespace memory_handles;
                return get_memory_content_request<moving, moving_status, present_pwm, present_current, present_velocity, present_position, velocity_trajectory, 
                                               position_trajectory, present_input_voltage, present_temperature>::start_address; }
        std::size_t length_helper(info_request const & req) {
                using namespace memory_handles;
                return get_memory_content_request<moving, moving_status, present_pwm, present_current, present_velocity, present_position, velocity_trajectory, 
                                               position_trajectory, present_input_voltage, present_temperature>::total_length; }

        std::size_t address_helper(dynamic_write_memory_request const & req) { return req.address; }
        std::size_t length_helper(dynamic_write_memory_request const & req) { return req.data.size(); }

        std::size_t address_helper(command_request const & req) { 
            using namespace memory_handles;
            return get_memory_content_request<goal_current, goal_velocity, profile_acceleration, profile_velocity, goal_position>::start_address; }
        std::size_t length_helper(command_request const & req) { 
            using namespace memory_handles;
            return get_memory_content_request<goal_current, goal_velocity, profile_acceleration, profile_velocity, goal_position>::total_length; }

        template <std::size_t Address, std::size_t Length>
        std::size_t address_helper(write_memory_request<Address, Length> const &) { return Address; }
        template <std::size_t Address, std::size_t Length>
        std::size_t length_helper(write_memory_request<Address, Length> const &) { return Length; }

        template <class... Ts>
        std::size_t address_helper(set_memory_content_request<Ts...> const &) { return get_memory_content_request<Ts...>::start_address; }
        template <class... Ts>
        std::size_t length_helper(set_memory_content_request<Ts...> const &) { return get_memory_content_request<Ts...>::total_length; }


        template <std::size_t I, class Buffer, class Tuple,
                 typename std::enable_if<I == std::tuple_size<Tuple>::value>::type * = nullptr>
        void bulk_read_helper(Buffer& buffer, Tuple& tuple)
        {
        }
        
        template <std::size_t I = 0, class Buffer, class Tuple,
                 typename std::enable_if<I != std::tuple_size<Tuple>::value>::type * = nullptr>
        void bulk_read_helper(Buffer& buffer, Tuple& tuple)
        {
            buffer.push_back(std::get<I>(tuple).first);
            buffer.push_back(address_helper(std::get<I>(tuple).second) & 0xff);
            buffer.push_back((address_helper(std::get<I>(tuple).second)  >> 8) & 0xff);
            buffer.push_back(length_helper(std::get<I>(tuple).second) & 0xff);
            buffer.push_back((length_helper(std::get<I>(tuple).second) >> 8) & 0xff);

            bulk_read_helper<I+1>(buffer, tuple);

        }
    }

    template <class Buffer, class... Requests>
    void serialize(Buffer& buffer, bulk_read<Requests...> const & request)
    {
        auto start = buffer.size();

        buffer.push_back(0xff);
        buffer.push_back(0xff);
        buffer.push_back(0xfd);

        buffer.push_back(0x00);
        buffer.push_back(0xfe);

        buffer.push_back({});
        buffer.push_back({});

        buffer.push_back(0x92);

        std::vector<std::uint8_t> tmp_buffer;

        details::bulk_read_helper(tmp_buffer, request.requests);
        add_byte_stuffings(tmp_buffer.begin(), tmp_buffer.end(), std::back_inserter(buffer));

        uint16_t no_crc_total_len = buffer.size() - start;
        uint16_t payload_len = no_crc_total_len - 7;
        uint16_t packet_len = payload_len + 2;

        buffer[start + 5] = packet_len & 0xff;
        buffer[start + 6] = (packet_len >> 8) & 0xff;

        auto crc = update_crc(0, &buffer[start], no_crc_total_len);

        buffer.push_back(crc & 0xff);
        buffer.push_back((crc >> 8) & 0xff);
    }

    namespace details
    {
        template <std::size_t I, class Buffer, class Tuple,
                 typename std::enable_if<I == std::tuple_size<Tuple>::value>::type * = nullptr>
        void bulk_write_helper(Buffer& buffer, Tuple& tuple)
        {
        }
        
        template <std::size_t I = 0, class Buffer, class Tuple,
                 typename std::enable_if<I != std::tuple_size<Tuple>::value>::type * = nullptr>
        void bulk_write_helper(Buffer& buffer, Tuple& tuple)
        {
            buffer.push_back(std::get<I>(tuple).first);
            buffer.push_back(address_helper(std::get<I>(tuple).second) & 0xff);
            buffer.push_back((address_helper(std::get<I>(tuple).second)  >> 8) & 0xff);
            buffer.push_back(length_helper(std::get<I>(tuple).second) & 0xff);
            buffer.push_back((length_helper(std::get<I>(tuple).second) >> 8) & 0xff);

            append_helper(buffer, std::get<I>(tuple).second);

            bulk_read_helper<I+1>(buffer, tuple);
        }
    }

    template <class Buffer, class... Requests>
    void serialize(Buffer& buffer, bulk_write<Requests...> const & request)
    {
        auto start = buffer.size();

        buffer.push_back(0xff);
        buffer.push_back(0xff);
        buffer.push_back(0xfd);

        buffer.push_back(0x00);
        buffer.push_back(0xfe);

        buffer.push_back({});
        buffer.push_back({});

        buffer.push_back(0x93);

        std::vector<std::uint8_t> tmp_buffer;

        details::bulk_write_helper(tmp_buffer, request.requests);
        add_byte_stuffings(tmp_buffer.begin(), tmp_buffer.end(), std::back_inserter(buffer));

        uint16_t no_crc_total_len = buffer.size() - start;
        uint16_t payload_len = no_crc_total_len - 7;
        uint16_t packet_len = payload_len + 2;

        buffer[start + 5] = packet_len & 0xff;
        buffer[start + 6] = (packet_len >> 8) & 0xff;

        auto crc = update_crc(0, &buffer[start], no_crc_total_len);

        buffer.push_back(crc & 0xff);
        buffer.push_back((crc >> 8) & 0xff);
    }

    template <class Buffer>
    void serialize(std::uint8_t id, Buffer& buffer, info_request const & request)
    {
        using namespace memory_handles;
        serialize(id, buffer, get_memory_content_request<
                                moving,
                                present_current,
                                present_velocity,
                                present_position,
                                present_temperature>{});
    }

    template <class Buffer>
    void serialize(std::uint8_t id, Buffer& buffer, command_request const & request)
    {
        using namespace memory_handles;
        serialize(id, buffer, make_set_memory_content_request(
                            goal_current{ request.current_limit },
                            goal_velocity { 0 },
                            profile_acceleration { 0 },
                            profile_velocity { request.velocity_limit },
                            goal_position { request.position }));
    }


}
}

#endif

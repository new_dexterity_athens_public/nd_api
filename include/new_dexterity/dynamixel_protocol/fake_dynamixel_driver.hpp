#ifndef ND_DYNAMIXEL_PROTOCOL_FAKE_DYNAMIXEL_DRIVER_HPP
#define ND_DYNAMIXEL_PROTOCOL_FAKE_DYNAMIXEL_DRIVER_HPP

#include <new_dexterity/dynamixel_protocol/requests.hpp>
#include <new_dexterity/dynamixel_protocol/parser.hpp>
#include <new_dexterity/dynamixel_protocol/memory_content.hpp>
#include <new_dexterity/dynamixel_protocol/in_connection.hpp>
#include <new_dexterity/dynamixel_protocol/out_connection.hpp>
#include <boost/optional.hpp>
#include <type_traits>
#include <tuple>
#include <vector>
#include <utility>
#include <algorithm>

namespace new_dexterity
{
namespace dynamixel_protocol
{

/*! 
 * @brief Helper class that is aware of the Dynamixel 2.0 protocol
 * and acts as a dynamixel mocker.
 * Currently is only torque_enable and position aware.
 */
class fake_dynamixel_driver
{
private:
    using message_type = new_dexterity::dynamixel_protocol::message_type;
    using in_connection = new_dexterity::dynamixel_protocol::in_connection;
    using out_connection = new_dexterity::dynamixel_protocol::out_connection;

public:
    fake_dynamixel_driver(in_connection& in, out_connection& out, std::uint8_t id)
        : auto_subscribe_(in,  [this](message_type const & msg){ handle_message(msg); })
        , out_connection_(out)
        , id_(id)
    {
        factory_reset();
    }

    void handle_message(message_type const & message)
    {
        if (message.packet_id == id_ && message.instruction == 0x01)
        {
            handle_ping(message);
        }
        else if (message.packet_id == id_ && message.instruction == 0x02)
        {
            handle_read(message);
        }
        else if (message.packet_id == id_ && message.instruction == 0x03)
        {
            handle_write(message);
        }
        else if (message.packet_id == id_ && message.instruction == 0x04)
        {
            handle_reg_write(message);
        }
        else if (message.packet_id == id_ && message.instruction == 0x05)
        {
            handle_action(message);
        }
        else if (message.packet_id == id_ && message.instruction == 0x06)
        {
            handle_factory_reset(message);
        }
        else if (message.packet_id == id_ && message.instruction == 0x08)
        {
            handle_reboot_request(message);
        }
        else if (message.packet_id == 0xfe && message.instruction == 0x82)
        {
            handle_sync_read_request(message);
        }
        else if (message.packet_id == 0xfe && message.instruction == 0x83)
        {
            handle_sync_write_request(message);
        }
        else if (message.packet_id == 0xfe && message.instruction == 0x92)
        {
            handle_bulk_read_request(message);
        }
        else if (message.packet_id == 0xfe && message.instruction == 0x93)
        {
            handle_bulk_write_request(message);
        }
    }

    bool successfully_handled_ping = false;
    bool successfully_handled_read = false;
    bool successfully_handled_write = false;
    bool successfully_handled_reg_write = false;
    bool successfully_handled_factory_reset = false;
    bool successfully_handled_reboot = false;

private:
    template <class Appender>
    void prepare_and_send(std::uint8_t instruction, std::uint8_t error, Appender appender)
    {
        out_connection_.access([&](std::vector<std::uint8_t>& buffer)
        {
            buffer.push_back(0xff);
            buffer.push_back(0xff);
            buffer.push_back(0xfd);
            buffer.push_back(0x00);

            buffer.push_back(id_);

            buffer.push_back({});
            buffer.push_back({});

            buffer.push_back(instruction);

            buffer.push_back(error);

            std::vector<std::uint8_t> tmp_buffer;
            appender(tmp_buffer);

            add_byte_stuffings(tmp_buffer.begin(), tmp_buffer.end(), std::back_inserter(buffer));

            uint16_t no_crc_total_len = buffer.size();
            uint16_t payload_len = no_crc_total_len - 7;
            uint16_t packet_len = payload_len + 2;

            buffer[5] = packet_len & 0xff;
            buffer[6] = (packet_len >> 8) & 0xff;

            auto crc = update_crc(0, buffer.data(), no_crc_total_len);

            buffer.push_back(crc & 0xff);
            buffer.push_back((crc >> 8) & 0xff);

        });

        out_connection_.send_bytes();
    }

    void handle_ping(message_type const & message)
    {
        if (message.payload.size() != 0)
        {
            prepare_and_send(0x55, 0x01, [](std::vector<std::uint8_t>&){});
            return;
        }

        prepare_and_send(0x55, 0x00, [this](std::vector<std::uint8_t>& params)
        {
            params.push_back(control_table_[0]);
            params.push_back(control_table_[1]);
            params.push_back(control_table_[2]);
        });

        successfully_handled_ping = true;
    }

    void handle_generic_read(std::uint16_t address, std::uint16_t length)
    {
        if (!valid_address_length(address, length))
        {
            prepare_and_send(0x55, 0x07, [](std::vector<std::uint8_t>&){});
            return;
        }

        //torque enable
        if (address >= 0 && address <= 63 && !control_table_[64])
        {
            prepare_and_send(0x55, 0x07, [&](std::vector<std::uint8_t>& params) { });

            return;
        }

        prepare_and_send(0x55, 0x00, [&](std::vector<std::uint8_t>& params)
        {
            std::copy_n(&control_table_[address], length, std::back_inserter(params));
        });

        successfully_handled_read = true;

    }

    void handle_read(message_type const & message)
    {
        if (message.payload.size() != 4)
        {
            prepare_and_send(0x55, 0x01, [](std::vector<std::uint8_t>&){});
            return;
        }

        std::uint16_t address = message.payload[0] | (message.payload[1] << 8);
        std::uint16_t length = message.payload[2] | (message.payload[3] << 8);

        handle_generic_read(address, length);
    }

    template <class Handler>
    void handle_generic_write(std::uint16_t address, std::uint16_t length, Handler handler)
    {
        //invalid range
        if (!valid_address_length(address, length))
        {
            prepare_and_send(0x55, 0x07, [](std::vector<std::uint8_t>&){});
            return;
        }

        //rom when torque enabled
        if (address >= 0 && address <= 63 && control_table_[64])
        {
            prepare_and_send(0x55, 0x07, [&](std::vector<std::uint8_t>& params) { });

            return;
        }

        handler();

        std::copy_n(&control_table_[116], 4, &control_table_[132]);

        prepare_and_send(0x55, 0x00, [](std::vector<std::uint8_t>&){});

        successfully_handled_write = true;
    }

    void handle_write(message_type const & message)
    {
        if (message.payload.size() < 2)
        {
            prepare_and_send(0x55, 0x01, [](std::vector<std::uint8_t>&){});
            return;
        }

        std::uint16_t address = message.payload[0] | (message.payload[1] << 8);
        std::uint16_t length = message.payload.size() - 2;

        handle_generic_write(address, length, [&]{

        std::copy(message.payload.begin() + 2, message.payload.end(), control_table_.begin() + address); 

        });
    }

    void handle_reg_write(message_type const & message)
    {
        if (message.payload.size() < 2)
        {
            prepare_and_send(0x55, 0x01, [](std::vector<std::uint8_t>&){});
            return;
        }

        std::uint16_t address = message.payload[0] | (message.payload[1] << 8);
        std::uint16_t length = message.payload.size() - 2;

        if (!valid_address_length(address, length))
        {
            prepare_and_send(0x55, 0x07, [](std::vector<std::uint8_t>&){});
            return;
        }

        registered_write_ = std::make_pair(address, message.payload);

        prepare_and_send(0x55, 0x00, [](std::vector<std::uint8_t>&){});

        successfully_handled_reg_write = true;
    }

    void handle_action(message_type const & message)
    {
        if (message.payload.size() != 0)
        {
            prepare_and_send(0x55, 0x01, [](std::vector<std::uint8_t>&){});
            return;
        }

        if (!registered_write_)
        {
            prepare_and_send(0x55, 0x02, [](std::vector<std::uint8_t>&){});
            return;
        }

        auto address = registered_write_->first;
        auto length = registered_write_->second.size() - 2;

        handle_generic_write(address, length, [&]{
            std::copy(registered_write_->second.begin() + 2, registered_write_->second.end(), control_table_.begin() + registered_write_->first);
            registered_write_ = boost::none;
        });

    }

    void handle_factory_reset(message_type const & message)
    {
        if (message.payload.size() != 1)
        {
            prepare_and_send(0x55, 0x01, [](std::vector<std::uint8_t>&){});
            return;
        }

        factory_reset();

        prepare_and_send(0x55, 0x00, [](std::vector<std::uint8_t>&){});

        successfully_handled_factory_reset = true;
    }

    void handle_reboot_request(message_type const & message)
    {
        if (message.payload.size() != 0)
        {
            prepare_and_send(0x55, 0x01, [](std::vector<std::uint8_t>&){});
            return;
        }

        reboot();

        prepare_and_send(0x55, 0x00, [](std::vector<std::uint8_t>&){}); 

        successfully_handled_reboot = true;
    }

    void handle_sync_read_request(message_type const & message)
    {
        if (message.payload.size() < 4)
        {
            return;
        }

        std::uint16_t address = message.payload[0] | (message.payload[1] << 8);
        std::uint16_t length = message.payload[2] | (message.payload[3] << 8);

        std::vector<std::uint8_t> ids(message.payload.begin() + 4, message.payload.end());

        if (std::find(ids.begin(), ids.end(), id_) != ids.end())
        {
            handle_generic_read(address, length);
        }

    }

    void handle_bulk_read_request(message_type const & message)
    {
        if (message.payload.size() < 5)
        {
            return;
        }

        std::size_t i = 0;

        while ((i + 4) < message.payload.size())
        {
            if (message.payload[i] == id_)
            {
                std::uint16_t address = message.payload[i+1] | (message.payload[i+2] << 8);
                std::uint16_t length = message.payload[i+3] | (message.payload[i+4] << 8);

                handle_generic_read(address, length);
                return;
            }

            i += 5;
        }
    }

    void handle_sync_write_request(message_type const & message)
    {
        if (message.payload.size() <= 4)
        {
            return;
        }

        std::uint16_t address = message.payload[0] | (message.payload[1] << 8);
        std::uint16_t length = message.payload[2] | (message.payload[3] << 8);

        std::size_t i = 4; 

        while ((i + length) < message.payload.size())
        {
            if (message.payload[i] == id_)
            {
                if (!valid_address_length(address, length))
                    return;

                if (address >= 0 && address <= 63 && control_table_[64])
                    return;

                std::copy_n(&message.payload[i + 1], length, &control_table_[address]);
                std::copy_n(&control_table_[116], 4, &control_table_[132]);

                successfully_handled_write = true;

                return;
            }

            i += (length + 1);
        }

    }

    void handle_bulk_write_request(message_type const & message)
    {
        std::size_t i = 0;

        while ((i + 4) < message.payload.size())
        {
            std::uint8_t id = message.payload[i];
            std::uint16_t address = message.payload[i+1] | (message.payload[i+2] << 8);
            std::uint16_t length = message.payload[i+3] | (message.payload[i+4] << 8);

            if (i + 4 + length >= message.payload.size())
                return;

            if (id == id_)
            {
                if (!valid_address_length(address, length))
                    return;

                if (address >= 0 && address <= 63 && control_table_[64])
                    return;

                std::copy_n(&message.payload[i+5], length, &control_table_[address]);
                std::copy_n(&control_table_[116], 4, &control_table_[132]);

                successfully_handled_write = true;

                return;
            }

            i += (4 + length);
        }
    }

    bool valid_address_length(std::uint16_t address, std::uint16_t length)
    {
        return (address <= 661 && length != 0 && length <= 661 && address + length <= 661);
    }

    void reboot()
    {
        control_table_[64] = 1;
    }

    void factory_reset()
    {
        control_table_.fill(0);
        control_table_[0] = 1040 & 0xff;
        control_table_[1] = (1040 >> 8) & 0xff;
        control_table_[2] = 0;
        control_table_[3] = 0;
        control_table_[4] = 0;
        control_table_[5] = 0;
        control_table_[7] = 1;
        control_table_[8] = 1;
        control_table_[9] = 250;
        control_table_[10] = 0;
        control_table_[11] = 3;
        control_table_[12] = 255;
        control_table_[13] = 2;
        control_table_[24] = 10;
        control_table_[31] = 80;
        control_table_[32] = 300 & 0xff;
        control_table_[33] = (300 >> 8) & 0xff;
        control_table_[34] = 110;
        control_table_[36] = 885 & 0xff;
        control_table_[37] = (885 >> 8) & 0xff;
        control_table_[38] = 689 & 0xff;
        control_table_[39] = (689 >> 8) & 0xff;
        control_table_[44] = 135 & 0xff;
        control_table_[48] = (4095 >> 0) & 0xff;
        control_table_[49] = (4095 >> 8) & 0xff;
        control_table_[50] = (4095 >> 16) & 0xff;
        control_table_[51] = (4095 >> 24) & 0xff;
        control_table_[63] = 52;

        control_table_[68] = 2;
        control_table_[76] = (1920 >> 0) & 0xff;
        control_table_[77] = (1920 >> 8) & 0xff;
        control_table_[78] = 100;
        control_table_[84] = (800 >> 0) & 0xff;
        control_table_[85] = (800 >> 8) & 0xff;

        control_table_[168] = 224;
        control_table_[170] = 225;
        control_table_[172] = 226;
        control_table_[174] = 227;
        control_table_[176] = 228;
        control_table_[178] = 229;
        control_table_[180] = 230;
        control_table_[182] = 231;
        control_table_[184] = 232;
        control_table_[186] = 233;
        control_table_[188] = 234;
        control_table_[190] = 235;
        control_table_[192] = 236;
        control_table_[194] = 237;
        control_table_[196] = 238;
        control_table_[198] = 239;
        control_table_[200] = 240;
        control_table_[202] = 241;
        control_table_[204] = 242;
        control_table_[206] = 243;
        control_table_[208] = 244;
        control_table_[210] = 245;
        control_table_[212] = 246;
        control_table_[214] = 247;
        control_table_[216] = 248;
        control_table_[218] = 249;
        control_table_[220] = 250;
        control_table_[222] = 251;

        control_table_[578] = (634 >> 0) & 0xff;
        control_table_[579] = (634 >> 8) & 0xff;
        control_table_[580] = (635 >> 0) & 0xff;
        control_table_[581] = (635 >> 8) & 0xff;
        control_table_[582] = (636 >> 0) & 0xff;
        control_table_[583] = (636 >> 8) & 0xff;
        control_table_[584] = (637 >> 0) & 0xff;
        control_table_[585] = (637 >> 8) & 0xff;
        control_table_[586] = (638 >> 0) & 0xff;
        control_table_[587] = (638 >> 8) & 0xff;
        control_table_[588] = (639 >> 0) & 0xff;
        control_table_[589] = (639 >> 8) & 0xff;
        control_table_[590] = (640 >> 0) & 0xff;
        control_table_[591] = (640 >> 8) & 0xff;
        control_table_[592] = (641 >> 0) & 0xff;
        control_table_[593] = (641 >> 8) & 0xff;
        control_table_[594] = (642 >> 0) & 0xff;
        control_table_[595] = (642 >> 8) & 0xff;
        control_table_[596] = (643 >> 0) & 0xff;
        control_table_[597] = (643 >> 8) & 0xff;
        control_table_[598] = (644 >> 0) & 0xff;
        control_table_[599] = (644 >> 8) & 0xff;
        control_table_[600] = (645 >> 0) & 0xff;
        control_table_[601] = (645 >> 8) & 0xff;
        control_table_[602] = (646 >> 0) & 0xff;
        control_table_[603] = (646 >> 8) & 0xff;
        control_table_[604] = (647 >> 0) & 0xff;
        control_table_[605] = (647 >> 8) & 0xff;
        control_table_[606] = (648 >> 0) & 0xff;
        control_table_[607] = (648 >> 8) & 0xff;
        control_table_[608] = (649 >> 0) & 0xff;
        control_table_[609] = (649 >> 8) & 0xff;
        control_table_[610] = (650 >> 0) & 0xff;
        control_table_[611] = (650 >> 8) & 0xff;
        control_table_[612] = (651 >> 0) & 0xff;
        control_table_[613] = (651 >> 8) & 0xff;
        control_table_[614] = (652 >> 0) & 0xff;
        control_table_[615] = (652 >> 8) & 0xff;
        control_table_[616] = (653 >> 0) & 0xff;
        control_table_[617] = (653 >> 8) & 0xff;
        control_table_[618] = (654 >> 0) & 0xff;
        control_table_[619] = (654 >> 8) & 0xff;
        control_table_[620] = (655 >> 0) & 0xff;
        control_table_[621] = (655 >> 8) & 0xff;
        control_table_[622] = (656 >> 0) & 0xff;
        control_table_[623] = (656 >> 8) & 0xff;
        control_table_[624] = (657 >> 0) & 0xff;
        control_table_[625] = (657 >> 8) & 0xff;
        control_table_[626] = (658 >> 0) & 0xff;
        control_table_[627] = (658 >> 8) & 0xff;
        control_table_[628] = (659 >> 0) & 0xff;
        control_table_[629] = (659 >> 8) & 0xff;
        control_table_[630] = (660 >> 0) & 0xff;
        control_table_[631] = (660 >> 8) & 0xff;
        control_table_[632] = (661 >> 0) & 0xff;
        control_table_[633] = (661 >> 8) & 0xff;
    }

private:
    new_dexterity::auto_subscribe<in_connection> auto_subscribe_;
    out_connection& out_connection_;

    std::uint8_t id_; 

    std::array<std::uint8_t, 662> control_table_;
    boost::optional<std::pair<std::uint16_t, std::vector<uint8_t>>> registered_write_;
};

}
}

#endif

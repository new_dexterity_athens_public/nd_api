#ifndef ND_DYNAMIXEL_PROTOCOL_COMMUNICATION_INTERFACE_HPP
#define ND_DYNAMIXEL_PROTOCOL_COMMUNICATION_INTERFACE_HPP

#include <new_dexterity/communication_interface.hpp>
#include <new_dexterity/dynamixel_protocol/communication_policy.hpp>

namespace new_dexterity
{
namespace dynamixel_protocol
{
    using communication_interface = new_dexterity::communication_interface< new_dexterity::dynamixel_protocol::communication_policy>;
}
}

#endif

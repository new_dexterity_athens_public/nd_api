#ifndef ND_DYNAMIXEL_PROTOCOL_RESPONSES_HPP
#define ND_DYNAMIXEL_PROTOCOL_RESPONSES_HPP

#include <cstddef>
#include <cstdint>
#include <boost/variant.hpp>
#include <array>
#include <new_dexterity/dynamixel_protocol/memory_content.hpp>
#include <ostream>

namespace new_dexterity
{
namespace dynamixel_protocol
{

/*! 
 * @brief Represents the 6 bits of the dynamixel status' error byte.
 */
enum class instruction_error
{
    result_fail_error,
    instruction_error,
    crc_error,
    data_range_error,
    data_length_error,
    data_limit_error,
    access_error,
    undefined_error
};


std::ostream& operator<<(std::ostream& os, instruction_error const & error)
{
    if (error == instruction_error::result_fail_error)
    {
        os << "result_fail_error";
    }
    else if (error == instruction_error::instruction_error)
    {
        os << "instruction_error";
    }
    else if (error == instruction_error::crc_error)
    {
        os << "crc_error";
    }
    else if (error == instruction_error::data_range_error)
    {
        os << "data_range_error";
    }
    else if (error == instruction_error::data_length_error)
    {
        os << "data_length_error";
    }
    else if (error == instruction_error::data_limit_error)
    {
        os << "data_limit_error";
    }
    else if (error == instruction_error::access_error)
    {
        os << "access_error";
    }
    else if (error == instruction_error::undefined_error)
    {
        os << "undefined_error";
    }

    return os;
}

/*! 
 * @brief Template for responses.
 * @ingroup Responses
 */
template <class Response>
struct response
{
    //! @brief The 7th bit of the dynamixel status' error byte.
    bool hardware_error_alert;
    //! @brief The result of the request. This is either a @ref instruction_error or the provided Request.
    boost::variant<Response, instruction_error> result;
};

/*!
 * @brief Response of compile-time checked read memory request.
 * @ingroup Responses
 */
template <std::size_t Length>
using read_memory_response = response<std::array<std::uint8_t, Length>>;

/*!
 * @brief Response of dynamic read memory request.
 * @ingroup Responses
 */
using dynamic_read_memory_response = response<std::vector<std::uint8_t>>;

struct info
{
    std::int32_t position;
    std::int32_t velocity;
    std::int16_t current;
    std::uint8_t temperature;
    bool moving;
};

/*!
 * @brief Response of info request.
 * @ingroup Responses
 */
using info_response = response<info>;

//! @brief empty struct denoting the acknowledgement of an intruction.
struct instruction_acknowledged {};

/*!
 * @brief Response of write memory requests.
 * @ingroup Responses
 */
using write_memory_response = response<instruction_acknowledged>;

/*!
 * @brief Info retrieved using the ping request.
 */
struct ping_info
{
    //! @brief model number
    std::uint16_t model_number;
    //! @brief firmware_version
    std::uint8_t firmware_version;
};

/*!
 * @brief Response of the ping request.
 * @ingroup Responses
 */
using ping_response = response<ping_info>;

/*!
 * @brief Response of register write requests.
 * @ingroup Responses
 */
using reg_write_response = response<instruction_acknowledged>;

/*!
 * @brief Response of action requests.
 * @ingroup Responses
 */
using action_response = response<instruction_acknowledged>;

/*!
 * @brief Response of factory reset requests.
 * @ingroup Responses
 */
using factory_reset_response = response<instruction_acknowledged>;

/*!
 * @brief Response of reboot requests.
 * @ingroup Responses
 */
using reboot_response = response<instruction_acknowledged>;

/*!
 * @brief Response of @ref get_memory_content_request.
 * 
 * @ingroup Responses
 *
 * This is the bread and butter of this library. If it wasn't for this, @ref get_memory_content_request and @ref set_memory_content_request,
 * this library would not have much purpose. The @ref memory_content provides members with the same names as the @ref memory_handles specified
 * on the @ref get_memory_content_request. These have the appropriate lengths and signess and the library automatically deserializes them providing
 * them as members of the @ref memory_content result.
 */
template <class... Ts>
using get_memory_content_response = response<memory_content<Ts...>>;

}
}

#endif

#ifndef ND_DYNAMIXEL_PROTOCOL_OUT_CONNECTION_HPP
#define ND_DYNAMIXEL_PROTOCOL_OUT_CONNECTION_HPP

#include <new_dexterity/out_connection.hpp>

namespace new_dexterity
{
namespace dynamixel_protocol
{
    using out_connection = new_dexterity::out_connection;
}
}

#endif

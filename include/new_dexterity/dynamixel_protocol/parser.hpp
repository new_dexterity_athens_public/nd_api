#ifndef ND_DYNAMIXEL_PROTOCOL_PARSER_HPP
#define ND_DYNAMIXEL_PROTOCOL_PARSER_HPP

#include <cstdint>
#include <vector>
#include <new_dexterity/utilities.hpp>
#include <boost/variant.hpp>

namespace new_dexterity
{
namespace dynamixel_protocol
{


std::uint16_t update_crc(std::uint16_t crc_accum, std::uint8_t *data_blk_ptr, std::uint16_t data_blk_size)
{
    std::uint16_t i, j;
    std::uint16_t crc_table[256] = {
        0x0000, 0x8005, 0x800F, 0x000A, 0x801B, 0x001E, 0x0014, 0x8011,
        0x8033, 0x0036, 0x003C, 0x8039, 0x0028, 0x802D, 0x8027, 0x0022,
        0x8063, 0x0066, 0x006C, 0x8069, 0x0078, 0x807D, 0x8077, 0x0072,
        0x0050, 0x8055, 0x805F, 0x005A, 0x804B, 0x004E, 0x0044, 0x8041,
        0x80C3, 0x00C6, 0x00CC, 0x80C9, 0x00D8, 0x80DD, 0x80D7, 0x00D2,
        0x00F0, 0x80F5, 0x80FF, 0x00FA, 0x80EB, 0x00EE, 0x00E4, 0x80E1,
        0x00A0, 0x80A5, 0x80AF, 0x00AA, 0x80BB, 0x00BE, 0x00B4, 0x80B1,
        0x8093, 0x0096, 0x009C, 0x8099, 0x0088, 0x808D, 0x8087, 0x0082,
        0x8183, 0x0186, 0x018C, 0x8189, 0x0198, 0x819D, 0x8197, 0x0192,
        0x01B0, 0x81B5, 0x81BF, 0x01BA, 0x81AB, 0x01AE, 0x01A4, 0x81A1,
        0x01E0, 0x81E5, 0x81EF, 0x01EA, 0x81FB, 0x01FE, 0x01F4, 0x81F1,
        0x81D3, 0x01D6, 0x01DC, 0x81D9, 0x01C8, 0x81CD, 0x81C7, 0x01C2,
        0x0140, 0x8145, 0x814F, 0x014A, 0x815B, 0x015E, 0x0154, 0x8151,
        0x8173, 0x0176, 0x017C, 0x8179, 0x0168, 0x816D, 0x8167, 0x0162,
        0x8123, 0x0126, 0x012C, 0x8129, 0x0138, 0x813D, 0x8137, 0x0132,
        0x0110, 0x8115, 0x811F, 0x011A, 0x810B, 0x010E, 0x0104, 0x8101,
        0x8303, 0x0306, 0x030C, 0x8309, 0x0318, 0x831D, 0x8317, 0x0312,
        0x0330, 0x8335, 0x833F, 0x033A, 0x832B, 0x032E, 0x0324, 0x8321,
        0x0360, 0x8365, 0x836F, 0x036A, 0x837B, 0x037E, 0x0374, 0x8371,
        0x8353, 0x0356, 0x035C, 0x8359, 0x0348, 0x834D, 0x8347, 0x0342,
        0x03C0, 0x83C5, 0x83CF, 0x03CA, 0x83DB, 0x03DE, 0x03D4, 0x83D1,
        0x83F3, 0x03F6, 0x03FC, 0x83F9, 0x03E8, 0x83ED, 0x83E7, 0x03E2,
        0x83A3, 0x03A6, 0x03AC, 0x83A9, 0x03B8, 0x83BD, 0x83B7, 0x03B2,
        0x0390, 0x8395, 0x839F, 0x039A, 0x838B, 0x038E, 0x0384, 0x8381,
        0x0280, 0x8285, 0x828F, 0x028A, 0x829B, 0x029E, 0x0294, 0x8291,
        0x82B3, 0x02B6, 0x02BC, 0x82B9, 0x02A8, 0x82AD, 0x82A7, 0x02A2,
        0x82E3, 0x02E6, 0x02EC, 0x82E9, 0x02F8, 0x82FD, 0x82F7, 0x02F2,
        0x02D0, 0x82D5, 0x82DF, 0x02DA, 0x82CB, 0x02CE, 0x02C4, 0x82C1,
        0x8243, 0x0246, 0x024C, 0x8249, 0x0258, 0x825D, 0x8257, 0x0252,
        0x0270, 0x8275, 0x827F, 0x027A, 0x826B, 0x026E, 0x0264, 0x8261,
        0x0220, 0x8225, 0x822F, 0x022A, 0x823B, 0x023E, 0x0234, 0x8231,
        0x8213, 0x0216, 0x021C, 0x8219, 0x0208, 0x820D, 0x8207, 0x0202
    };

    for(j = 0; j < data_blk_size; j++)
    {
        i = ((std::uint16_t)(crc_accum >> 8) ^ data_blk_ptr[j]) & 0xFF;
        crc_accum = (crc_accum << 8) ^ crc_table[i];
    }

    return crc_accum;
}

class parser
{
public:
    struct message_type
    {
        std::uint8_t packet_id;
        std::uint8_t instruction;
        std::vector<std::uint8_t> payload;
    };

private:
    struct state0 {};
    struct state1 { std::uint8_t header1; };
    struct state2 { std::uint8_t header1; std::uint8_t header2;  };
    struct state3 { std::uint8_t header1; std::uint8_t header2; std::uint8_t header3;  };
    struct state4 { std::uint8_t header1; std::uint8_t header2; std::uint8_t header3; std::uint8_t reserved; };
    struct state5 { std::uint8_t header1; std::uint8_t header2; std::uint8_t header3; std::uint8_t reserved; 
                    std::uint8_t packet_id; };
    struct state6 { std::uint8_t header1; std::uint8_t header2; std::uint8_t header3; std::uint8_t reserved; 
                    std::uint8_t packet_id; std::uint8_t length1; };
    struct state7 { std::uint8_t header1; std::uint8_t header2; std::uint8_t header3; std::uint8_t reserved; 
                    std::uint8_t packet_id; std::uint8_t length1; std::uint8_t length2; };
    struct state8 { std::uint8_t header1; std::uint8_t header2; std::uint8_t header3; std::uint8_t reserved; 
                    std::uint8_t packet_id; std::uint8_t length1; std::uint8_t length2; std::uint8_t instruction; };
    struct state9 { std::uint8_t header1; std::uint8_t header2; std::uint8_t header3; std::uint8_t reserved; 
                    std::uint8_t packet_id; std::uint8_t length1; std::uint8_t length2; std::uint8_t instruction; 
                    std::vector<std::uint8_t> payload; };
    struct state10 { std::uint8_t header1; std::uint8_t header2; std::uint8_t header3; std::uint8_t reserved; 
                    std::uint8_t packet_id; std::uint8_t length1; std::uint8_t length2; std::uint8_t instruction; 
                    std::vector<std::uint8_t> payload; std::uint8_t crc1; };

    using state_type = boost::variant<state0, state1, state2, state3, state4, state5, state6, state7, state8, state9, state10>;


    auto next(state0 const & state, std::uint8_t byte) -> boost::variant<state0, state1>
    {
        if (byte == 0xff)
            return state1 { 0xff };
        else
            return state0 {};
    }

    auto next(state1 const & state, std::uint8_t byte) -> boost::variant<state0, state2>
    {
        if (byte == 0xff)
            return state2 { 0xff, 0xff};
        else
            return state0 {};
    }

    auto next(state2 const & state, std::uint8_t byte) -> boost::variant<state0, state2, state3>
    {
        if (byte == 0xfd)
            return state3 { 0xff, 0xff, 0xfd };
        else if (byte == 0xff) 
            return state2 { 0xff, 0xff };
        else 
            return state0 {};
    }

    auto next(state3 const & state, std::uint8_t byte) -> boost::variant<state0, state1, state4>
    {
        if (byte == 0x00)
            return state4 { 0xff, 0xff, 0xfd, 0x00 };
        else if (byte == 0xff)
            return state1 { 0xff };
        else
            return state0 {};
    }

    auto next(state4 const & state, std::uint8_t byte) -> state5
    {
        return state5 { 0xff, 0xff, 0xfd, 0x00, byte };
    }

    auto next(state5 const & state, std::uint8_t byte) -> state6
    {
        return state6 { state.header1, state.header2, state.header3, state.reserved, state.packet_id, byte };
    }

    auto next(state6 const & state, std::uint8_t byte) -> state7
    {
        return state7 { state.header1, state.header2, state.header3, state.reserved, state.packet_id, state.length1, byte };
    }

    auto next(state7 const & state, std::uint8_t byte) -> state8
    {
        return state8 { state.header1, state.header2, state.header3, state.reserved, state.packet_id, state.length1, state.length2, byte };
    }

    auto next(state8 const & state, std::uint8_t byte) -> boost::variant<state0, state9, state10>
    {
        auto length = (state.length2 << 8 | state.length1);

        if (length < 3)
        {
            return state0 {};
        }
        else if (length == 3)
        {
            return state10 { state.header1, state.header2, state.header3, state.reserved, state.packet_id, state.length1, state.length2, state.instruction, {}, byte};
        }
        else
        {
            return state9 { state.header1, state.header2, state.header3, state.reserved, state.packet_id, state.length1, state.length2, state.instruction, {byte} };
        }
    }

    auto next(state9 & state, std::uint8_t byte) -> boost::variant<state9, state10>
    {
        std::size_t length = (state.length2 << 8 | state.length1) - 3;

        if (state.payload.size() < length)
        {
            state.payload.push_back(byte);
            return state;
        }
        else
        {
            return state10 { state.header1, state.header2, state.header3, state.reserved, state.packet_id, state.length1, state.length2, state.instruction, std::move(state.payload), byte};
        }
    }

    template <class Buffer>
    void remove_byte_stuffing(Buffer& bytes)
    {
        enum class state_type { searching_first, searching_second, searching_third, searching_fourth } state;

        std::size_t padding_bytes_found = 0;

        auto it = bytes.begin();

        state = state_type::searching_first;

        while (static_cast<std::size_t>(std::distance(it, bytes.end())) > padding_bytes_found)
        {
            auto byte = *it;

            switch (state)
            {
            case state_type::searching_first:
                if (byte == 0xff)
                    state = state_type::searching_second;
                ++it;
                break;
            case state_type::searching_second:
                if (byte == 0xff)
                    state = state_type::searching_third;
                else
                    state = state_type::searching_first;
                ++it;
                break;
            case state_type::searching_third:
                if (byte == 0xfd)
                    state = state_type::searching_fourth;
                else if (byte == 0xff)
                    state = state_type::searching_third;
                else
                    state = state_type::searching_first;
                ++it;
                break;
            case state_type::searching_fourth:
                if (byte == 0xfd)
                {
                    ++padding_bytes_found;
                    std::rotate(it, std::next(it), bytes.end());
                    state = state_type::searching_first;
                }
                else if (byte == 0xff)
                {
                    state = state_type::searching_second;
                    ++it;
                }
                else
                {
                    state = state_type::searching_first;
                    ++it;
                }
                break;
            }
        }

        bytes.resize(bytes.size() - padding_bytes_found);
    }


    auto next(state10 & state, std::uint8_t byte) -> state0
    {
        auto crc2 = byte;

        auto cr1 = update_crc(0, &state.header1, 1);
        auto cr2 = update_crc(cr1, &state.header2, 1);
        auto cr3 = update_crc(cr2, &state.header3, 1);
        auto cr4 = update_crc(cr3, &state.reserved, 1);
        auto cr5 = update_crc(cr4, &state.packet_id, 1);
        auto cr6 = update_crc(cr5, &state.length1, 1);
        auto cr7 = update_crc(cr6, &state.length2, 1);
        auto cr8 = update_crc(cr7, &state.instruction, 1);
        auto crc = update_crc(cr8, state.payload.data(), state.payload.size());

        if (state.crc1 == (crc & 0xFF) && crc2 == (crc >> 8 & 0xff))
        {
            message_type message;
            message.packet_id = state.packet_id;
            message.instruction = state.instruction;
            message.payload = std::move(state.payload);
            remove_byte_stuffing(message.payload);
            parsed_messages_.emplace_back(std::move(message));
        }

        return state0 {};
    }

    struct update_state
    {
        update_state(std::uint8_t byte, parser& parser)
            : byte_(byte), parser_(parser)
        {
        }

        template <typename State>
        void operator()(State & state)
        {
            auto next_state = parser_.next(state, byte_);
            parser_.state_ = next_state;
        }

        std::uint8_t byte_;
        parser& parser_;
    };

public:
    parser() : state_(state0{}) {}

    template <typename Buffer>
    void parse(Buffer& buffer, std::size_t available)
    {
        for (std::size_t i = 0; i != available; ++i)
        {
            auto byte = buffer[i];

            auto visitor = update_state(byte, *this);
            new_dexterity::match(state_, visitor);
            /* The C++11 requirement is getting on my nerves. Compare the above with:
            new_dexterity::match(state_, [&](auto & state) { state_ = next(state); });
            */
        }

    }

    void clear()
    {
        parsed_messages_.clear();
    }

    std::vector<message_type> const & parsed_messages() const
    {
        return parsed_messages_;
    }


private:

    state_type state_;
    std::vector<message_type> parsed_messages_;

};

} // dynamixel_protocol
} // new_dexterity

#endif

#ifndef ND_DYNAMIXEL_PROTOCOL_IN_CONNECTION_HPP
#define ND_DYNAMIXEL_PROTOCOL_IN_CONNECTION_HPP

#include <new_dexterity/in_connection.hpp>
#include <new_dexterity/dynamixel_protocol/parser.hpp>

namespace new_dexterity
{
namespace dynamixel_protocol
{
    using in_connection = new_dexterity::in_connection<new_dexterity::dynamixel_protocol::parser>;

}
}

#endif

#ifndef ND_DYNAMIXEL_PROTOCOL_DESERIALIZE_HPP
#define ND_DYNAMIXEL_PROTOCOL_DESERIALIZE_HPP

#include <new_dexterity/dynamixel_protocol/requests.hpp>
#include <new_dexterity/dynamixel_protocol/memory_content.hpp>
#include <new_dexterity/dynamixel_protocol/responses.hpp>
#include <new_dexterity/utilities.hpp>
#include <boost/optional.hpp>

namespace new_dexterity
{
namespace dynamixel_protocol
{

    using message_type = new_dexterity::dynamixel_protocol::parser::message_type;

    static inline 
    std::pair<bool, boost::optional<instruction_error>> check_errors(uint8_t id, message_type const & message)
    {
        bool hardware_alert = message.payload[0] & 0x80;

        if (message.payload.size() != 1)
            return {hardware_alert, boost::none};

        if ((message.payload[0] & 0x7f) == 0x01)
            return {hardware_alert, new_dexterity::dynamixel_protocol::instruction_error::result_fail_error};

        if ((message.payload[0] & 0x7f) == 0x02)
            return {hardware_alert, new_dexterity::dynamixel_protocol::instruction_error::instruction_error};

        if ((message.payload[0] & 0x7f) == 0x03)
            return {hardware_alert, new_dexterity::dynamixel_protocol::instruction_error::crc_error};

        if ((message.payload[0] & 0x7f) == 0x04)
            return {hardware_alert, new_dexterity::dynamixel_protocol::instruction_error::data_range_error};

        if ((message.payload[0] & 0x7f) == 0x05)
            return {hardware_alert, new_dexterity::dynamixel_protocol::instruction_error::data_length_error};

        if ((message.payload[0] & 0x7f) == 0x06)
            return {hardware_alert, new_dexterity::dynamixel_protocol::instruction_error::data_limit_error};

        if ((message.payload[0] & 0x7f) == 0x07)
            return {hardware_alert, new_dexterity::dynamixel_protocol::instruction_error::access_error};

        if ((message.payload[0] & 0x7f) != 0)
            return {hardware_alert, new_dexterity::dynamixel_protocol::instruction_error::undefined_error};

        return {hardware_alert, boost::none};
    }


    template <typename Msg>  
    struct deserializer {};

    template <>
    struct deserializer<response<instruction_acknowledged>>
    {
        static
        boost::optional<response<instruction_acknowledged>> deserialize(uint8_t id, message_type const & message)
        {
            if (message.packet_id != id)
                return boost::none;

            if (message.instruction != 0x55)
                return boost::none;

            auto pair = check_errors(id, message);

            response<instruction_acknowledged> response;
            response.hardware_error_alert = pair.first;

            if (pair.second)
            {
                response.result = *pair.second;
                return response;
            }

            if (message.payload.size() != 1)
                return boost::none;

            response.result = instruction_acknowledged{};
            return response;
        }
    };

    template <>
    struct deserializer<ping_response>
    {
        static
        boost::optional<ping_response> deserialize(uint8_t id, message_type const & message)
        {
            if (message.packet_id != id)
                return boost::none;

            if (message.instruction != 0x55)
                return boost::none;

            auto pair = check_errors(id, message);

            ping_response response;
            response.hardware_error_alert = pair.first;

            if (pair.second)
            {
                response.result = *pair.second;
                return response;
            }
            
            if (message.payload.size() != 4)
                return boost::none;

            ping_info info;
            info.model_number = message.payload[1] | (message.payload[2] << 8);
            info.firmware_version = message.payload[3];

            response.result = info;
            return response;
        }
    };


    template <>
    struct deserializer<dynamic_read_memory_response>
    {
        static
        boost::optional<dynamic_read_memory_response> deserialize(uint8_t id, message_type const & message)
        {
            if (message.packet_id != id)
                return boost::none;

            if (message.instruction != 0x55)
                return boost::none;

            dynamic_read_memory_response response;

            auto pair = check_errors(id, message);

            response.hardware_error_alert = pair.first;

            if (pair.second)
            {
                response.result = *pair.second;
                return response;
            }

            std::vector<std::uint8_t> payload;

            std::copy_n(message.payload.begin() + 1, message.payload.size() - 1, std::back_inserter(payload));

            response.result = payload;
            return response;
        }
    };

    template <std::size_t Length>
        struct deserializer<read_memory_response<Length>>
        {
            static
                boost::optional<read_memory_response<Length>> deserialize(uint8_t id, message_type const & message)
                {
                    if (message.packet_id != id)
                        return boost::none;

                    if (message.instruction != 0x55)
                        return boost::none;

                    read_memory_response<Length> response;

                    auto pair = check_errors(id, message);

                    response.hardware_error_alert = pair.first;

                    if (pair.second)
                    {
                        response.result = *pair.second;
                        return response;
                    }

                    if (message.payload.size() != (Length + 1))
                        return boost::none;

                    std::array<std::uint8_t, Length> payload;

                    std::copy_n(message.payload.begin() + 1, payload.size(), payload.begin());

                    response.result = payload;
                    return response;
                }
        };

    namespace details
    {
        template <std::size_t Index, class T, class Buffer, typename std::enable_if<sizeof(T) == 1>::type * = nullptr>
            void des_primitive(Buffer& buffer, T& value)
            {
                auto b1 = buffer[Index+0];
                value = b1;
            }

        template <std::size_t Index, class T, class Buffer, typename std::enable_if<sizeof(T) == 2>::type * = nullptr>
            void des_primitive(Buffer& buffer, T& value)
            {
                auto b2 = buffer[Index+0];
                auto b1 = (buffer[Index+1] << 8);
                value = b1 | b2;
            }

        template <std::size_t Index, class T, class Buffer, typename std::enable_if<sizeof(T) == 4>::type * = nullptr>
            void des_primitive(Buffer& buffer, T& value)
            {
                auto b4 = buffer[Index+0];
                auto b3 = (buffer[Index+1] << 8);
                auto b2 = (buffer[Index+2] << 16);
                auto b1 = (buffer[Index+3] << 24);
                value = b1 | b2 | b3 | b4;
            }

        template <class... Rs>
            struct get_memory_helper;

        template <>
            struct get_memory_helper<>
            {
                template <std::size_t FirstAddress, class Buffer, class MemCont>
                    static void deserialize(Buffer const &, MemCont&) {}
            };

        template <class R, class... Rs>
            struct get_memory_helper<R, Rs...>
            {
                template <std::size_t FirstAddress, class Buffer, class MemCont>
                    static void deserialize(Buffer const & buffer, MemCont& cont)
                    {
                        using content_type = typename info_of_t<R>::content_type;
                        content_type value;
                        constexpr std::size_t index = info_of_t<R>::address - FirstAddress;
                        des_primitive<index, content_type>(buffer, value);
                        typename info_of_t<R>::wrapper & r_slice = cont;
                        (r_slice.*info_of_t<R>::mem_ptr) = value;
                        get_memory_helper<Rs...>::template deserialize<FirstAddress>(buffer, cont);
                    }
            };

    }

    template <class... Ts>
        struct deserializer<get_memory_content_response<Ts...>>
        {
            static
                boost::optional<get_memory_content_response<Ts...>> deserialize(uint8_t id, message_type const & message)
                {
                    static constexpr std::size_t start_address = details::non_asserting_memory_content_range_info<Ts...>::start_address;
                    static constexpr std::size_t total_length = details::non_asserting_memory_content_range_info<Ts...>::total_length;

                    auto mmemory_response = deserializer<read_memory_response<total_length>>::deserialize(id, message);

                    if (!mmemory_response)
                        return boost::none;

                    auto& memory_response = *mmemory_response;

                    get_memory_content_response<Ts...> response;
                    response.hardware_error_alert = memory_response.hardware_error_alert;

                    return new_dexterity::match<get_memory_content_response<Ts...>>(memory_response.result,
                            [&](instruction_error err)
                            {
                                response.result = err;   
                                return response;
                            },
                            [&](std::array<std::uint8_t, total_length>& memory)
                            {
                                new_dexterity::dynamixel_protocol::memory_content<Ts...> content;

                                details::sort_on_address<details::get_memory_helper, Ts...>::template deserialize<start_address>(memory, content);

                                response.result = content;
                                return response;
                            });
                }
        };

    template <>
        struct deserializer<info_response>
        {
            static
                boost::optional<info_response> deserialize(uint8_t id, message_type const & message)
                {
                    using namespace memory_handles;
                    auto mmemory_response = deserializer<get_memory_content_response<
                        moving,
                        present_current,
                        present_velocity,
                        present_position,
                        present_temperature>>::deserialize(id, message);

                    if (!mmemory_response)
                        return boost::none;

                    auto const & memory_response = *mmemory_response;

                    info_response response;
                    response.hardware_error_alert = memory_response.hardware_error_alert;

                    return new_dexterity::match<info_response>(memory_response.result,
                            [&](instruction_error err)
                            {
                                response.result = err;
                                return response;
                            },
                            [&](memory_content<
                                moving,
                                present_current,
                                present_velocity,
                                present_position,
                                present_temperature> const & content)
                            {
                                info info;
                                info.position = content.present_position;
                                info.velocity = content.present_velocity;
                                info.current = content.present_current;
                                info.temperature = content.present_temperature;
                                info.moving = content.moving;
                                response.result = info;
                                return response;
                            });

                }
        };
}
}

#endif

#ifndef ND_METAPROGRAMMING_HPP
#define ND_METAPROGRAMMING_HPP

#include <tuple>
#include <type_traits>

namespace new_dexterity
{
    template <template <typename> class Fun, typename TypeList>
    struct apply;

    template <template <typename> class Fun, template <typename...> class TypeList, typename... Ts>
    struct apply<Fun, TypeList<Ts...>>
    {
        using type = TypeList<typename Fun<Ts>::type...>;
    };

    template <template <typename> class Fun, typename TypeList>
    using apply_t = typename apply<Fun, TypeList>::type;

    template <size_t I = 0, typename Fun, typename Tuple>
    typename std::enable_if<I == std::tuple_size<Tuple>::value>::type for_each(Tuple& tuple, Fun) {}

    template <size_t I = 0, typename Tuple, typename Fun>
    typename std::enable_if<I < std::tuple_size<Tuple>::value>::type for_each(Tuple& tuple, Fun fun)
    {
        fun(std::get<I>(tuple));
        for_each<I+1>(tuple, fun);
    }

    template <typename T, typename TypeList>
    struct cons;

    template <typename T, template <typename...> class TypeList, typename... Ts>
    struct cons<T, TypeList<Ts...>>
    {
        using type = TypeList<T, Ts...>;
    };

    template <typename T, typename TypeList>
    using cons_t = typename cons<T, TypeList>::type;

    template <template <typename> class Predicate, typename TypeList>
    struct filter;

    template <template <typename> class Predicate, template <typename...> class TypeList>
    struct filter<Predicate, TypeList<>>
    {
        using type = TypeList<>;
    };

    template <template <typename> class Predicate, template <typename...> class TypeList, typename Head, typename... Tail>
    struct filter<Predicate, TypeList<Head, Tail...>>
    {
        using rest = typename filter<Predicate, TypeList<Tail...>>::type;

        using type = typename std::conditional<Predicate<Head>::value, 
                                typename cons<Head, rest>::type,
                                rest>::type;
    };

    template <template <typename> class Predicate, typename TypeList>
    using filter_t = typename filter<Predicate, TypeList>::type;

    template <typename T>
    struct type_lit
    {
        using type = T;
    };

    template <typename T>
    struct type_lit_of
    {
        using type = type_lit<T>;
    };

} // new_dexterity

#endif

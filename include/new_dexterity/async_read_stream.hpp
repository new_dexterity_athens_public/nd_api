#ifndef ND_ASYNC_READ_STREAM__HPP
#define ND_ASYNC_READ_STREAM__HPP

//! @file
//! @brief AsyncReadStream type erasure interface

#include <boost/asio.hpp>
#include <functional>

namespace new_dexterity
{
    class async_read_stream
    {
    public:
        virtual ~async_read_stream() {}
        virtual void async_read_some(boost::asio::mutable_buffers_1 const & mb, const std::function<void(const boost::system::error_code&, std::size_t)>&) = 0;
    };

    template <typename AsyncReadStream>
    class async_read_stream_wrapper : public async_read_stream
    {
    public:
        explicit async_read_stream_wrapper(AsyncReadStream& read_stream)
            : read_stream_(read_stream)
        {
        }

        void async_read_some(boost::asio::mutable_buffers_1 const & mb, const std::function<void(const boost::system::error_code&, std::size_t)>& callback) final
        {
            read_stream_.async_read_some(mb, callback);
        }

    private:
        AsyncReadStream& read_stream_;
    };
}

#endif

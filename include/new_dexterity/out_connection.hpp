#ifndef ND_OUT_CONNECTION_HPP
#define ND_OUT_CONNECTION_HPP

//! @file
//! @brief out_connection definition

#include <vector>
#include <mutex>
#include <cstdint>

namespace new_dexterity
{

/*! @brief A class representing the outgoing connection
 * 
 * Multiple objects may want to queue bytes to the internal buffer
 * before sending for performance reasons.
 * This class allows interfaces to act independently and gives synchronized access
 * to the above shared buffer.
 */

class out_connection
{
public:

    /*! @brief Generic constructor suporting multiple Stream types.
     * @tparam SyncWriteStream A type supporting the following expressions:
     * @code{.cpp}
     * //s is SyncWriteStream, v is std::vector<std::uint8_t>
     * boost::asio::write(s, boost::asio::buffer(v));
     * @endcode
     * @param stream The stream to write to.
     */

    template <typename SyncWriteStream>
    out_connection(SyncWriteStream& stream)
        : write_stream_(new new_dexterity::write_stream_wrapper<SyncWriteStream>(stream))
    {
    }

    /*! @brief Uses the functor supplied to atomically access the underlying buffer.
     * @tparam Accessor
     * @code{.cpp}
     * //s is Accessor
     * //v is std::vector<std::uint8_t>
     * s(v)
     * @endcode
     * @param accessor The Accessor to be provided
     */
    template <class Accessor>
    void access(Accessor accessor)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        accessor(buffer_);
    }

    /*! @brief Sends the queued bytes to the stream.
     *
     * 
     */
    void send_bytes()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        write_stream_->write(asio::buffer(buffer_));
        buffer_.clear();
    }

private:
    std::mutex mutex_;
    std::unique_ptr<new_dexterity::write_stream> write_stream_;
    std::vector<std::uint8_t> buffer_;
};

} // new_dexterity

#endif

#ifndef ND_NDX_PROTOCOL_HPP
#define ND_NDX_PROTOCOL_HPP

#include <new_dexterity/ndx_protocol/requests.hpp>
#include <new_dexterity/ndx_protocol/responses.hpp>
#include <new_dexterity/ndx_protocol/requests_io.hpp>
#include <new_dexterity/ndx_protocol/responses_io.hpp>
#include <new_dexterity/ndx_protocol/deserialize.hpp>
#include <new_dexterity/ndx_protocol/serialize.hpp>
#include <new_dexterity/ndx_protocol/response_of.hpp>
#include <new_dexterity/ndx_protocol/message_dispatcher.hpp>
#include <new_dexterity/ndx_protocol/in_connection.hpp>
#include <new_dexterity/ndx_protocol/out_connection.hpp>
#include <new_dexterity/ndx_protocol/communication_interface.hpp>
#include <new_dexterity/ndx_protocol/fake_hand_hardware.hpp>
#include <new_dexterity/ndx_protocol/discoverer.hpp>
#include <new_dexterity/ndx_protocol/protocol.hpp>

#endif

#ifndef ND_COMMUNICATION_INTERFACE_COMMUNICATION_INTERFACE_HPP
#define ND_COMMUNICATION_INTERFACE_COMMUNICATION_INTERFACE_HPP

//! @file
//! @brief Low-level implementation of the Communication Interface

#include <boost/asio.hpp>
#include <boost/variant.hpp>
#include <boost/optional.hpp>
#include <boost/asio/high_resolution_timer.hpp>
#include <new_dexterity/message_dispatcher.hpp>
#include <chrono>
#include <thread>
#include <new_dexterity/utilities.hpp>
#include <new_dexterity/metaprogramming.hpp>
#include <new_dexterity/in_connection.hpp>
#include <new_dexterity/out_connection.hpp>
#include <new_dexterity/communication_interface/message_handler_queue.hpp>
#include <new_dexterity/metaprogramming.hpp>
#include <functional>
#include <queue>
#include <utility>
#include <cstring>
#include <cstddef>
#include <future>

namespace asio = boost::asio;

namespace new_dexterity
{
    namespace impl
    {


        template <typename CommunicationPolicy>
        struct communication_interface
        {

        public:
                using policy = CommunicationPolicy;
                using parser_type = typename policy::parser_type;
                using message_type = typename parser_type::message_type;
                using in_connection_type = new_dexterity::in_connection<parser_type>;
                using implicit_identifier = typename policy::implicit_identifier;

        public:
                communication_interface(in_connection_type& in, new_dexterity::out_connection& out, implicit_identifier id)
                    : identifier_(id)
                    , handler_queue_(identifier_)
                    , in_connection_(in)
                    , out_connection_(out)
                    , auto_subscribe_(in, [this](message_type const & msg){ handle_message(msg); })
                {
                }

                template <typename Request, typename Callback, typename Duration>
                void queue_request(Request const & request, Callback callback, Duration duration)
                {
                    handler_queue_.template queue<Request>(callback, duration);
                    out_connection_.access(
                            [&](std::vector<std::uint8_t>& buffer) 
                            { 
                                policy::serialize(identifier_, buffer, request); 
                            });
                }

                template <typename Request>
                void queue_request(Request const & request)
                {
                    out_connection_.access(
                            [&](std::vector<std::uint8_t>& buffer) 
                            { 
                                policy::serialize(identifier_, buffer, request); 
                            });
                }

                in_connection_type& in_connection()
                {
                    return in_connection_;
                }

                new_dexterity::out_connection& out_connection() 
                {
                    return out_connection_;
                }

                template <typename Request, typename Callback, typename Duration>
                void wait_response_of(Callback callback, Duration duration)
                {
                    handler_queue_.template queue<Request>(callback, duration);
                }

                void send_bytes()
                {
                    out_connection_.send_bytes();
                }

            private:

                void handle_message(message_type const & message)
                {
                    handler_queue_.serve(message);
                }

                implicit_identifier identifier_;
                new_dexterity::message_handler_queue<policy> handler_queue_;
                in_connection_type& in_connection_;
                new_dexterity::out_connection& out_connection_;
                new_dexterity::auto_subscribe<in_connection_type> auto_subscribe_;

        };
    } // impl
} // new_dexterity

#endif

#ifndef ND_COMMUNICATION_INTERFACE_RESPONSE_HANDLER_QUEUE_H
#define ND_COMMUNICATION_INTERFACE_RESPONSE_HANDLER_QUEUE_H

#include <new_dexterity/communication_interface/response_slot.hpp>
#include <boost/asio.hpp>
#include <future>
#include <thread>
#include <mutex>
#include <list>
#include <memory>

namespace new_dexterity
{

template <typename Msg>
class message_handler
{
public:
    virtual bool handle(Msg const & msg) = 0;
};

template <typename Request, typename CommunicationPolicy>
class response_slot_handler : public message_handler<typename CommunicationPolicy::message_type>
{
public:
    using response_type = typename CommunicationPolicy::template response_of<Request>;
    using implicit_identifier = typename CommunicationPolicy::implicit_identifier;
    using message_type = typename CommunicationPolicy::message_type;

    template <typename Consumer, typename Duration>
    response_slot_handler(boost::asio::io_service& io_service, std::mutex& mutex, implicit_identifier& id, Consumer consumer, Duration duration) 
        : slot_(io_service)
        , id_(id)
        , mutex_(mutex)
    {
        slot_.stage_handler(consumer, duration, mutex_);
    }


    bool handle(message_type const & msg) final
    {
        auto result = CommunicationPolicy::template deserialize<response_type>(id_, msg);

        if (result)
        {
            auto response = *result;

            slot_.set_response(response);

            return true;
        }
            
        return false;
    }

private:
    new_dexterity::response_slot<response_type> slot_;
    implicit_identifier& id_;
    std::mutex& mutex_;

};

template <typename CommunicationPolicy>
class message_handler_queue
{
public:

    using implicit_identifier = typename CommunicationPolicy::implicit_identifier;
    using message_type = typename CommunicationPolicy::message_type;

    message_handler_queue(implicit_identifier& identifier)
        : io_service_()
        , identifier_(identifier)
        , runner_()
        , work_(io_service_)
    {
        runner_ = std::async(std::launch::async, [&]{ io_service_.run(); });
    }

    ~message_handler_queue()
    {
        io_service_.stop();
    }

    template <typename Request, typename Callback, typename Duration>
    void queue(Callback callback, Duration duration)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        using response_type = typename CommunicationPolicy::template response_of<Request>;

        handlers_.push_back({});

        auto it = --handlers_.end();

        auto response_handler = [this, it, callback](boost::optional<response_type> const & mresponse)
        {
            if (mresponse)
            {
                callback(*mresponse);
            }
            else
            {
                callback(boost::none);
                handlers_.erase(it);
            }

        };

        std::unique_ptr<message_handler<message_type>> handler = std::unique_ptr<message_handler<message_type>>(
                new response_slot_handler<Request, CommunicationPolicy>(io_service_, mutex_, identifier_, response_handler, duration));

        *it = std::move(handler);
    }

    void serve(message_type const & msg)
    {
        std::lock_guard<std::mutex> lock(mutex_);

        auto it = handlers_.begin();

        auto end = handlers_.end();

        while (it != end)
        {
            if ((*it)->handle(msg) == true)
            {
                handlers_.erase(it);
                break;
            }
            else
            {
                ++it;
            }
        }
    }

private:
    boost::asio::io_service io_service_;
    implicit_identifier identifier_;
    std::future<void> runner_;
    boost::asio::io_service::work work_;
    std::list<std::unique_ptr<message_handler<message_type>>> handlers_;
    std::mutex mutex_;
    
};

}

#endif

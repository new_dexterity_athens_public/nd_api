#ifndef ND_COMMUNICATION_INTERFACE_RESPONSE_SLOT_HPP
#define ND_COMMUNICATION_INTERFACE_RESPONSE_SLOT_HPP

//! @file
//! @brief Implementation of response slots used in the definition of the Communication Interface
#include <boost/optional.hpp>
#include <boost/asio.hpp>
#include <memory>
#include <mutex>

namespace new_dexterity
{

template <typename Response>
class response_slot_worker : public std::enable_shared_from_this<response_slot_worker<Response>>
{
public:
    template <typename... Args>
    static std::shared_ptr<response_slot_worker> create(Args&&... args)
    {
        return std::shared_ptr<response_slot_worker>(new response_slot_worker(std::forward<Args>(args)...));
    }

    void stop()
    {
        timer_.cancel_one();
    }

    template <typename Consumer, typename Duration>
    void stage_handler(Consumer consumer, Duration duration, std::mutex& mutex)
    {
        timer_.cancel_one();

        consumer_ = consumer;

        #ifdef BOOST_DATE_TIME_POSIX_TIME_STD_CONFIG
        timer_.expires_from_now(boost::posix_time::nanoseconds(std::chrono::duration_cast<std::chrono::nanosecods>(duration).count()));
        #else
        timer_.expires_from_now(boost::posix_time::microseconds(std::chrono::duration_cast<std::chrono::microseconds>(duration).count()));
        #endif

        auto self(this->shared_from_this());

        timer_.async_wait([self, &mutex](boost::system::error_code const & ec)
        {
            if (!ec)
            {
                std::lock_guard<std::mutex> lock(mutex);
                if (self->consumer_)
                {
                    self->consumer_(boost::none);
                    self->consumer_ = nullptr;
                }
            }
        });
    }

    void set_response(Response const & response)
    {
        if (consumer_)
        {
            consumer_(response);
            consumer_ = nullptr;
        }
    }

private:
    response_slot_worker(boost::asio::io_service& io_service)
        : timer_(io_service)
    {
    }

    boost::asio::deadline_timer timer_;
    std::function<void(boost::optional<Response> const&)> consumer_;

};

template <typename Response>
class response_slot
{
public:
    template <typename... Args>
    response_slot(Args&&... args)
        : worker_(response_slot_worker<Response>::create(std::forward<Args>(args)...))
    {
    }

    ~response_slot()
    {
        worker_->stop();
    }

    template <typename Consumer, typename Duration>
    void stage_handler(Consumer consumer, Duration duration, std::mutex& mutex)
    {
        worker_->stage_handler(consumer, duration, mutex);
    }

    void set_response(Response const & response)
    {
        worker_->set_response(response);
    }

private:
    std::shared_ptr<response_slot_worker<Response>> worker_;
};

}

#endif

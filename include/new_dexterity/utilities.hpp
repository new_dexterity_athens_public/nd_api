#ifndef ND_UTILITIES_UTILITIES_HPP
#define ND_UTILITIES_UTILITIES_HPP

//! @file
//! @brief Utilities header.

#include <boost/variant.hpp>
#include <boost/variant/static_visitor.hpp>

namespace new_dexterity
{
    struct unit {};

    template<typename ResultType, typename ... Callables>
    struct visitor;

    template <typename ResultType, typename Callable>
    struct visitor<ResultType, Callable> : public Callable, public boost::static_visitor<ResultType>
    {
        explicit visitor(Callable callable) : Callable(callable)
        {
        }

        using Callable::operator();

    }; 

    template<typename ResultType, typename Callable, typename ... Callables>
    struct visitor<ResultType, Callable, Callables...> : public Callable, public visitor<ResultType, Callables...>
    {
        visitor(Callable callable, Callables... callables)
            : Callable(callable), visitor<ResultType, Callables...>(callables...)
        {
        }

        using Callable::operator();
        using visitor<ResultType, Callables...>::operator();

    }; // class visitor


    /*! @ingroup Functions
     *  @brief Pattern matching utility.
     * 
     * `boost::variant` is a proper sum type. Unfortunately there is no C++ support for
     * pattern matching although `boost::variant` allows pattern matching via the construction of visitors.
     * This process is time consuming and requires a lot of boilerplate which we would like to avoid.
     * This helper takes any viable sequence of callables, constructs a viable visitor for the given variant,
     * and runs it all in one step.
     *
     * Example:
     *
     * @code{.cpp}
     * // n is boost::variant<int, double>
     * auto is_larger_than_ten = new_dexterity::match<bool>(n,
        [](int const & n) { return n > 10; },
        [](float const & f) { return f > 10.0; });
     * @endcode
     *
     * @tparam ResultType The result type of the operation. Not required if void.
     * @tparam Variant The given variant's type, can be deduced automatically.
     * @tparam Callables A sequence of functors, each handling one type of the given variant.
     * @param variant The given variant.
     * @param callables The functor sequence.
     */
    template <typename ResultType = void, typename Variant, typename... Callables>
    ResultType match(Variant&& variant, Callables... callables)
    {
        new_dexterity::visitor<ResultType, Callables...> visitor(callables...);

        return boost::apply_visitor(visitor, std::forward<Variant>(variant));
    }


    /*! @brief Helper class for ignoring cases when using new_dexterity::match
     */

    struct ignore
    {
        template <typename T>
        void operator()(const T&) const {}
    };

    template <typename... Args>
    struct type_list{};

}

#endif

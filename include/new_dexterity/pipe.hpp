#ifndef ND_PIPE_HPP
#define ND_PIPE_HPP

//#include <boost/predef.h>
#include <boost/asio.hpp>

#if BOOST_OS_WINDOWS
    #include <Winbase.h>
    #include <string> 
    #include <boost/lexical_cast.hpp>
#endif

#if BOOST_OS_UNIX
    #include <unistd.h>
#endif

namespace asio = boost::asio;

namespace new_dexterity
{

#if BOOST_OS_WINDOWS
    using pipe_endpoint = asio::windows::stream_handle;

    inline void create_pipe(pipe_endpoint& in, pipe_endpoint& out)
    {
        static unsigned int nextid = 0; 
        HANDLE hs[2];
        SECURITY_ATTRIBUTES sa; 
        ZeroMemory(&sa, sizeof(sa)); 
        sa.nLength = sizeof(sa); 
        sa.lpSecurityDescriptor = NULL; 
        sa.bInheritHandle = FALSE; 
        std::string pipe = "\\\\.\\pipe\\boost_process_" + boost::lexical_cast<std::string>(GetCurrentProcessId()) + "_" + boost::lexical_cast<std::string>(nextid++); 
        hs[0] = ::CreateNamedPipeA(pipe.c_str(), PIPE_ACCESS_INBOUND | FILE_FLAG_OVERLAPPED, 0, 1, 8192, 8192, 0, &sa); 
        hs[1] = ::CreateFileA(pipe.c_str(), GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL); 

        OVERLAPPED overlapped; 
        ZeroMemory(&overlapped, sizeof(overlapped)); 
        overlapped.hEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL); 
        BOOL b = ::ConnectNamedPipe(hs[0], &overlapped); 
        if (!b) 
        { 
            if (::GetLastError() == ERROR_IO_PENDING) 
            { 
                if (::WaitForSingleObject(overlapped.hEvent, INFINITE) == WAIT_FAILED) 
                { 
                    ::CloseHandle(overlapped.hEvent); 
                } 
            } 
            else if (::GetLastError() != ERROR_PIPE_CONNECTED) 
            { 
                ::CloseHandle(overlapped.hEvent); 
            } 
        } 
        ::CloseHandle(overlapped.hEvent); 
        in.assign(hs[0]);
        out.assign(hs[1]);
    }
#endif

#if BOOST_OS_UNIX
    using pipe_endpoint = asio::posix::stream_descriptor;

    inline void create_pipe(pipe_endpoint& in, pipe_endpoint& out)
    {
        int pipesfd[2] = {0};
        if (!pipe(pipesfd))
        {
            in.assign(pipesfd[0]);
            out.assign(pipesfd[1]);
        }
    }

#endif

}

#endif

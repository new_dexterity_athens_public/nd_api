/**
 *
 * \mainpage Main page
 *
 * \section Description

 * The NdApi library provides utilities for developing robust applications for the New Dexterity Hand.
 *
 * \section getting_started Getting Started
 *
 * The library attempts to abstract away the details of low-level communication using specific protocols.
 * It uses generic streams to allow communication via serial ports, pipes, sockets and even mockup streams.
 * Moreover it automatically uses registered parsers, serializers and deserializers to map low-level protocol
 * messages into high-level requests and responses and also takes care and notifies of timeouts.
 * It currently supports the deprecated NDX protocol and Robotis' Dynamixel Protocol 2.0 by providing
 * built-in high-level requests and responses.
 * The examples at the [Examples](examples.html) page can be useful for getting up and running with the library.
 */

/*! @defgroup Functions Functions
 *
 * All the freestanding functions provided by the library.
 */

/*! @defgroup Metafunctions Metafunctions
 *
 * All the metafunctions provided by the library.
 */

/*! @defgroup Requests Requests
 *
 * The available requests that can be queued and sent to the NDX hand via the @ref new_dexterity::communication_interface.
 */

/*! @defgroup Responses Responses
 *
 * The available responses that correspond to the @ref Requests.
 */







from conans import ConanFile, CMake

class NdApi(ConanFile):
    name = "NdApi"
    version = "1.0.0"
    settings = "os", "compiler", "build_type", "arch"
    requires = "boost/1.69.0@conan/stable"
    generators = "cmake_paths"
    exports_sources = "include/*", "src/*", "CMakeLists.txt", "cmake/*", "tests/*", "doc/*", "scripts/*", "examples/*"

    def configure(self):
        self.options["Boost"].shared = True

    def build(self):
        self.configure();
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        self.configure();
        cmake = CMake(self)
        cmake.configure()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["nd-api"]



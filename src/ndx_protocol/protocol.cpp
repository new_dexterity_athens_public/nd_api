#include <new_dexterity/ndx_protocol/protocol.hpp>

ACK_msg ack_msg;
DEV_ID_msg devid_msg;
HSTATUS_msg hstatus_msg;
CSTATUS_msg cstatus_msg;

uint16_t max_msgID[4];
uint16_t min_msgID[4];

/**
 * Initialization of the protocol messages
 */
void protocol_init(uint16_t id)
{
    ack_msg.header.header = NDHEAD;
    ack_msg.header.class_id = ACK_CLASS;
    ack_msg.header.payload_len = sizeof(ACK_msg) - sizeof(NDHeader) -2;
    ack_msg.header.id = id;
    calculate_header_checksum(&ack_msg.header);

    devid_msg.header.header = NDHEAD;
    devid_msg.header.class_id = MSG_CLASS;
    devid_msg.header.msg_id = ID_ID;
    devid_msg.header.payload_len =0;
    devid_msg.header.id = id;
    calculate_header_checksum(&(devid_msg.header));

    hstatus_msg.header.header = NDHEAD;
    hstatus_msg.header.class_id = MSG_CLASS;
    hstatus_msg.header.msg_id = STH_ID;
    hstatus_msg.header.payload_len = sizeof(HSTATUS_msg) - sizeof(NDHeader) -2;
    hstatus_msg.header.id = id;
    calculate_header_checksum(&(hstatus_msg.header));
    hstatus_msg.commands_exec=0;
    hstatus_msg.commands_recv=0;
    hstatus_msg.commands_recv_valid=0;
    hstatus_msg.last_error_of_hand=0;
    hstatus_msg.temperature=0;
    hstatus_msg.time = 0;

    cstatus_msg.header.header = NDHEAD;
    cstatus_msg.header.class_id = MSG_CLASS;
    cstatus_msg.header.msg_id = STC_ID;
    cstatus_msg.header.payload_len = sizeof(CSTATUS_msg) - sizeof(NDHeader) -2;
    cstatus_msg.header.id = id;
    calculate_header_checksum(&(cstatus_msg.header));
    cstatus_msg.last_error_of_comm=0;
    cstatus_msg.recv_packets_all=0;
    cstatus_msg.recv_packets_valid=0;
    cstatus_msg.reserved1=0;
    cstatus_msg.time=0;
    cstatus_msg.tran_packets_all=0;
    cstatus_msg.tran_packets_valid=0;

    min_msgID[ACK_CLASS] = 0;
    min_msgID[MSG_CLASS] = 1;
    min_msgID[COM_CLASS] = 1;
    min_msgID[CFG_CLASS] = 1;

    max_msgID[ACK_CLASS] = 1;
    max_msgID[MSG_CLASS] = 7;
    max_msgID[COM_CLASS] = 5;
    max_msgID[CFG_CLASS] = 6;
}

/**
 * Calculate the checksum of the header
 * @param header The message header
 */
void calculate_header_checksum(NDHeader *header)
{
    uint8_t a = 0;
    uint8_t b = 0;
    uint8_t *ptr;

    ptr = (uint8_t *)header;
    uint16_t i = 6;
    while(i--)
    {
        a = a + *ptr++;
        b = b + a;
    }
    header->cs[0] = a;
    header->cs[1] = b;
}

/**
 * Calculate the checksum of a message
 * @param msg The message buffer
 */
void calculate_checksum(uint8_t *msg)
{
    uint8_t a = 0;
    uint8_t b = 0;
    uint8_t *ptr;

    ptr = &((NDHeader *)msg)->id;
    uint16_t i = ((NDHeader *)msg)->payload_len+6;
    while(i--)
    {
        a = a + *ptr++;
        b = b + a;
    }
    msg[((NDHeader *)msg)->payload_len + 8 ] = a;
    msg[((NDHeader *)msg)->payload_len + 9 ] = b;
}

/**
 * Validate the checksum of the header message
 * @param header The message header
 * @return 1 for success, 0 for fail
 */
int validate_header_checksum(NDHeader header)
{
    uint8_t a = 0;
    uint8_t b = 0;
    uint8_t *ptr;

    ptr = (uint8_t *)&header;
    uint16_t i = 6;
    while(i--)
    {
        a = a + *ptr++;
        b = b + a;
    }

    return (int)((header.cs[0]==a) && (header.cs[1]==b));
}

/**
 * Validate the checksum of a message
 * @param msg The message buffer
 * @return 1 for success, 0 for fail
 */
int validate_checksum(const uint8_t *msg)
{
    uint16_t payload = ((NDHeader *)msg)->payload_len;

    uint8_t a = 0;
    uint8_t b = 0;
    uint8_t *ptr;

    ptr = &((NDHeader *)msg)->id;
    uint16_t i = ((NDHeader *)msg)->payload_len+6;
    while(i--)
    {
        a = a + *ptr++;
        b = b + a;
    }

    return (int)((msg[payload+8]==a) && (msg[payload+9]==b));
}

/**
 * Validate the protocol message
 * @param msg The message buffer
 * @return an error based on ::ack_status
 */

ack_status validate_message(const uint8_t *msg, uint16_t len, uint16_t *pack_len)
{
    if (len<sizeof(NDHeader))
    {
        *pack_len = 0;
        return ERROR_LENGTH;
    }

    NDHeader header;

    memcpy((void *)(&header),(void *)msg,sizeof(NDHeader));

    *pack_len = header.payload_len;

    //Check Header ND
    if (header.header!=NDHEAD)
        return UNKNOWN_HEADER;

    //Check Header Checksum
    if (!validate_header_checksum(header))
        return HEADER_CHECKSUM_FAILED;

    //Check Class
    if (header.class_id>3)
        return CLASS_ID_ERROR;

    //Check MSG ID
    if ((header.msg_id<min_msgID[header.class_id]) || (header.msg_id>max_msgID[header.class_id]))
        return MSG_ID_ERROR;

    //Check payload size
    if ((header.payload_len+sizeof(NDHeader))>len)
        return ERROR_LENGTH;

    //Check Payload and Checksum for payload not zero
    if (header.payload_len==0)
        return VALID;
    else
    {
        //Check Checksum if payload
        if (validate_checksum(msg))
            return VALID;
        else
            return CHECKSUM_FAILED;
    }
    return UNRESOLVED_ERROR;
}

/**
 * Create and send ACK message to be send
 * @param class_id, msg_id and status for ack
 *
 */
void send_ack(uint8_t class_id, uint8_t msg_id, uint8_t status)
{
    ack_msg.header.msg_id = NACK_ID;
    ack_msg.ack.err_code = status;
    ack_msg.ack.class_id = class_id;
    ack_msg.ack.msg_id = msg_id;

    if (status==VALID)
        ack_msg.header.msg_id = ACK_ID;

    calculate_header_checksum(&ack_msg.header);
    calculate_checksum((uint8_t*)&ack_msg);
}

